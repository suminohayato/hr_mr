# coding:utf-8
import numpy as np
import os, yaml


class Predictor:

    def __init__(self, path):
        self.compression_no = 2
        self.w_in = np.loadtxt(path + "w_in.csv", delimiter=",")
        self.b_in = np.loadtxt(path + "b_in.csv", delimiter=",").flatten()
        self.w_in2 = []
        self.b_in2 = []
        for i in range(self.compression_no):
            self.w_in2.append(np.loadtxt(path + "w_in" + str(i) + ".csv", delimiter=","))
            self.b_in2.append(np.loadtxt(path + "b_in" + str(i) + ".csv", delimiter=",").flatten())
        self.w = np.loadtxt(path + "w.csv", delimiter=",")
        self.w_out = np.loadtxt(path + "w_out.csv", delimiter=",")
        self.b_out = np.loadtxt(path + "b_out.csv", delimiter=",")

        self.means = np.loadtxt(path + "means.csv", delimiter=",").flatten()
        self.stds = np.loadtxt(path + "stds.csv", delimiter=",").flatten()

        self.features2 = sum([self.w_in2[i].shape[0] for i in range(self.compression_no)])
        self.features1 = self.w_in.shape[0] - sum([self.w_in2[i].shape[1] for i in range(self.compression_no)])
        print self.features1, self.features2

    def predict(self, X):
        """Xはリスト"""
        N = len(X)
        if N > 6:
            print "7以上はダメ"
            exit()

        features = self.features1 + self.features2
        s = np.zeros((X[0].shape[0], self.w.shape[0]))
        for i, x in enumerate(X):
            x_ = (x - self.means[i * features:(i + 1) * features]) / self.stds[i * features:(i + 1) * features]
            x1 = x_[:, :self.features1]
            x2 = []
            start = self.features1
            for j in range(self.compression_no):
                end = start + self.w_in2[j].shape[0]
                x2.append(x_[:, start:end])
                start = end

            pseudo_inputs = []
            for j in range(self.compression_no):
                temp = np.dot(x2[j], self.w_in2[j]) + self.b_in2[j]
                temp = temp * (temp >= 0.0)
                pseudo_inputs.append(temp)

            s = np.dot(np.concatenate([x1] + pseudo_inputs, axis=1), self.w_in) + self.b_in + np.dot(s, self.w)
            s = s * (s >= 0.0)

        return 1.0 / (1.0 + np.exp(-(np.dot(s, self.w_out) + self.b_out)))