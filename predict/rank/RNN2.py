# coding:utf-8
import numpy as np
import os, yaml


class Predictor:

    def __init__(self, path):
        info = yaml.load(open(path + "data.info", "r"))
        self.w_in = np.loadtxt(path + "w_in.csv", delimiter=",")
        self.w_in2 = np.loadtxt(path + "w_in2.csv", delimiter=",")
        self.b_in = np.loadtxt(path + "b_in.csv", delimiter=",").flatten()
        self.b_in2 = np.loadtxt(path + "b_in2.csv", delimiter=",").flatten()
        self.w = np.loadtxt(path + "w.csv", delimiter=",")
        self.w_out = np.loadtxt(path + "w_out.csv", delimiter=",")
        self.b_out = np.loadtxt(path + "b_out.csv", delimiter=",")

        self.means = np.loadtxt(path + "means.csv", delimiter=",").flatten()
        self.stds = np.loadtxt(path + "stds.csv", delimiter=",").flatten()

        self.features2 = self.w_in2.shape[0]
        self.features1 = self.w_in.shape[0] - self.w_in2.shape[1]
        print self.features1, self.features2

    def predict(self, X):
        """Xはリスト"""
        N = len(X)
        if N > 4:
            print "5以上はダメ"
            exit()

        features = self.features1 + self.features2
        s = np.zeros((X[0].shape[0], self.w.shape[0]))
        for i, x in enumerate(X):
            x_ = (x - self.means[i * features:(i + 1) * features]) / self.stds[i * features:(i + 1) * features]
            x1 = x_[:, :self.features1]
            x2 = x_[:, self.features1:]
            temp = np.dot(x2, self.w_in2) + self.b_in2
            temp = temp * (temp >= 0.0)
            s = np.dot(np.concatenate((x1, temp), axis=1), self.w_in) + self.b_in + np.dot(s, self.w)
            s = s * (s >= 0.0)

        return 1.0 / (1.0 + np.exp(-(np.dot(s, self.w_out) + self.b_out)))