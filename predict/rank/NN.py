import numpy as np
import os


class Predictor:

    def __init__(self, path):
        files = os.listdir(path)
        files = filter(lambda s:(s[0] == "W") and (s[-4:] == ".csv") and len(s) == 6, files)
        self.W_no = len(files)
        self.Ws = []
        self.bs = []
        for i in range(self.W_no):
            self.Ws.append(np.loadtxt(path + "W" + str(i + 1) + ".csv", delimiter=","))
            self.bs.append(np.loadtxt(path + "b" + str(i + 1) + ".csv", delimiter=","))
        self.W = np.loadtxt(path + "W.csv", delimiter=",").flatten()
        self.b = np.loadtxt(path + "b.csv", delimiter=",").flatten()
        self.means = np.loadtxt(path + "means.csv", delimiter=",").flatten()
        self.stds = np.loadtxt(path + "stds.csv", delimiter=",").flatten()

    def predict(self, X):
        z = (X - self.means) / self.stds

        for W, b in zip(self.Ws, self.bs):
            z = np.dot(z, W) + b
            z = z * (z >= 0)
        return 1.0 / (1.0 + np.exp(-(np.sum(self.W * z, axis=1) + self.b)))
