# coding:utf-8
import numpy as np
import os


class Predictor:

    def __init__(self, path):
        self.w_in = np.loadtxt(path + "w_in.csv", delimiter=",")
        self.b_in = np.loadtxt(path + "b_in.csv", delimiter=",").flatten()
        self.w = np.loadtxt(path + "w.csv", delimiter=",")
        self.w_out = np.loadtxt(path + "w_out.csv", delimiter=",")
        self.b_out = np.loadtxt(path + "b_out.csv", delimiter=",")

        self.means = np.loadtxt(path + "means.csv", delimiter=",").flatten()
        self.stds = np.loadtxt(path + "stds.csv", delimiter=",").flatten()

    def predict(self, X):
        """Xはリスト"""
        N = len(X)
        if N > 4:
            print "5以上はダメ"
            exit()

        features = self.w_in.shape[0]
        s = np.zeros((X[0].shape[0], self.w.shape[0]))
        for i, x in enumerate(X):
            x_ = (x - self.means[i * features:(i + 1) * features]) / self.stds[i * features:(i + 1) * features]
            s = np.dot(x_, self.w_in) + self.b_in + np.dot(s, self.w)
            s = s * (s >= 0.0)

        return 1.0 / (1.0 + np.exp(-(np.dot(s, self.w_out) + self.b_out)))