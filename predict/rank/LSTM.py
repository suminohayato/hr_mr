# coding:utf-8
import numpy as np
import os, yaml


class Predictor:

    def __init__(self, path):
        self.compression_no = 2
        self.w_in2 = []
        self.b_in2 = []
        for i in range(self.compression_no):
            self.w_in2.append(np.loadtxt(path + "w_in" + str(i) + ".csv", delimiter=","))
            self.b_in2.append(np.loadtxt(path + "b_in" + str(i) + ".csv", delimiter=",").flatten())
        self.i_w = np.loadtxt(path + "i_w.csv", delimiter=",")
        self.i_b = np.loadtxt(path + "i_b.csv", delimiter=",")
        self.j_w = np.loadtxt(path + "j_w.csv", delimiter=",")
        self.j_b = np.loadtxt(path + "j_b.csv", delimiter=",")
        self.f_w = np.loadtxt(path + "f_w.csv", delimiter=",")
        self.f_b = np.loadtxt(path + "f_b.csv", delimiter=",")
        self.o_w = np.loadtxt(path + "o_w.csv", delimiter=",")
        self.o_b = np.loadtxt(path + "o_b.csv", delimiter=",")
        self.w_out = np.loadtxt(path + "w_out.csv", delimiter=",")
        self.b_out = np.loadtxt(path + "b_out.csv", delimiter=",")

        self.means = np.loadtxt(path + "means.csv", delimiter=",").flatten()
        self.stds = np.loadtxt(path + "stds.csv", delimiter=",").flatten()

        self.features2 = sum([self.w_in2[i].shape[0] for i in range(self.compression_no)])
        self.features1 = self.i_w.shape[0] - self.i_w.shape[1] - sum([self.w_in2[i].shape[1] for i in range(self.compression_no)])
        print self.features1, self.features2

    def predict(self, X):
        """Xはリスト"""
        N = len(X)
        if N > 6:
            print "7以上はダメ"
            exit()

        def sigmoid(x):
            return 1.0 / (1.0 + np.exp(-x))

        def tanh(x):
            return (np.exp(2.0 * x) - 1.0) / (np.exp(2.0 * x) + 1.0)

        features = self.features1 + self.features2
        c = np.zeros((X[0].shape[0], self.i_w.shape[1]))
        h = np.zeros((X[0].shape[0], self.i_w.shape[1]))
        for i, x in enumerate(X):
            x_ = (x - self.means[i * features:(i + 1) * features]) / self.stds[i * features:(i + 1) * features]
            x1 = x_[:, :self.features1]
            x2 = []
            start = self.features1
            for j in range(self.compression_no):
                end = start + self.w_in2[j].shape[0]
                x2.append(x_[:, start:end])
                start = end

            pseudo_inputs = []
            for j in range(self.compression_no):
                temp = np.dot(x2[j], self.w_in2[j]) + self.b_in2[j]
                temp = temp * (temp >= 0.0)
                pseudo_inputs.append(temp)

            real_input = np.concatenate([x1] + pseudo_inputs, axis=1)
            pair = np.concatenate([real_input, h], axis=1)
            f = sigmoid(np.dot(pair, self.f_w) + self.f_b + 1.0)
            i = sigmoid(np.dot(pair, self.i_w) + self.i_b)
            c_temp = tanh(np.dot(pair, self.j_w) + self.j_b)
            c = f * c + i * c_temp
            o = sigmoid(np.dot(pair, self.o_w) + self.o_b)
            h = o * tanh(c)

        return 1.0 / (1.0 + np.exp(-(np.dot(h, self.w_out) + self.b_out)))