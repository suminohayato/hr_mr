import numpy as np


class Predictor:

    def __init__(self, path):
        self.W = np.loadtxt(path + "W.csv", delimiter=",").flatten()
        self.b = np.loadtxt(path + "b.csv", delimiter=",").flatten()
        self.means = np.loadtxt(path + "means.csv", delimiter=",").flatten()
        self.stds = np.loadtxt(path + "stds.csv", delimiter=",").flatten()

    def predict(self, X):
        reguralized = (X - self.means) / self.stds
        return np.sum(self.W * reguralized, axis=1) + self.b