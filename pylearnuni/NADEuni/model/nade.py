# coding: utf-8
"""
Neural autoregressive density estimator (NADE) implementation
"""
__authors__ = "Vincent Dumoulin"
__copyright__ = "Copyright 2014, Universite de Montreal"
__credits__ = ["Jorg Bornschein", "Vincent Dumoulin"]
__license__ = "3-clause BSD"
__maintainer__ = "Vincent Dumoulin"


import csv,getpass
import numpy
import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams
from pylearn2.models.model import Model
from pylearn2.utils import sharedX
from pylearn2.space import VectorSpace
from NADEuni.libForNADE.utils.unrolled_scan import unrolled_scan
from NADEuni.libForNADE.models.directed_probabilistic import (
    JointDistribution, ConditionalDistribution
)
import numpy as np
from pylearn2.compat import OrderedDict
from pylearn2.space import CompositeSpace

theano_rng = RandomStreams(seed=2341)


class NADEBase(Model):
    """
    WRITEME
    """
    def __init__(self, dim, dim_hid, indices, types, clamp_sigmoid=False, unroll_scan=1):
        """
        Parameters
        ----------
        dim : int
            Number of observed binary variables
        dim_hid : int
            Number of latent binary variables
        clamp_sigmoid : bool, optional
            WRITEME. Defaults to `False`.
        unroll_scan : int, optional
            WRITEME. Defaults to 1.
        """
        super(NADEBase, self).__init__()

        self.dim = dim
        self.dim_hid = dim_hid
        self.clamp_sigmoid = clamp_sigmoid
        self.unroll_scan = unroll_scan
        self.indices = indices
        self.types = types

        self.input_space = VectorSpace(dim=self.dim,dtype="float32")
        self.output_space = VectorSpace(dim=1,dtype="float32")

        # Visible biases
        b_value = numpy.zeros(self.dim)
        self.b = sharedX(b_value, 'b')
        # Hidden biases
        c_value = numpy.zeros(self.dim_hid)
        self.c = sharedX(c_value, 'c')
        # Encoder weights
        W_value = self._initialize_weights(self.dim, self.dim_hid)
        self.W = sharedX(W_value, 'W')
        # Decoder weights
        V_value = self._initialize_weights(self.dim_hid, self.dim)
        self.V = sharedX(V_value, 'V')

    def _initialize_weights(self, dim_0, dim_1):
        """
        Initialize a (dim_0, dim_1)-shaped weight matrix

        Parameters
        ----------
        dim_0 : int
            First dimension of the weights matrix
        dim_1 : int
            Second dimension of the weights matrix

        Returns
        -------
        rval : `numpy.ndarray`
            A (dim_0, dim_1)-shaped, properly initialized weights matrix
        """
        rval = (2 * numpy.random.normal(size=(dim_0, dim_1)) - 1) / dim_0
        return rval

    def sigmoid(self, x):
        """
        WRITEME

        Parameters
        ----------
        x : WRITEME
        """
        if self.clamp_sigmoid:
            return T.nnet.sigmoid(x)*0.9999 + 0.000005
        else:
            return T.nnet.sigmoid(x)

    def get_params(self):
        """
        Returns
        -------
        params : list of tensor-like
            The model's parameters
        """
        return [self.b, self.c, self.W, self.V]

    def get_weights(self):
        """
        Aliases to `NADE.get_encoder_weights`
        """
        return self.get_encoder_weights()

    def set_weights(self, weights):
        """
        Aliases to `NADE.set_encoder_weights`
        """
        self.set_encoder_weights(weights)

    def get_encoder_weights(self):
        """
        Returns
        -------
        rval : `numpy.ndarray`
            Encoder weights
        """
        return self.W.get_value()

    def set_encoder_weights(self, weights):
        """
        Sets encoder weight values

        Parameters
        ----------
        weights : `numpy.ndarray`
            Encoder weight values to assign to self.W
        """
        self.W.set_value(weights)

    def get_decoder_weights(self):
        """
        Returns
        -------
        rval : `numpy.ndarray`
            Decoder weights
        """
        return self.V.get_value()

    def set_decoder_weights(self, weights):
        """
        Sets decoder weight values

        Parameters
        ----------
        weights : `numpy.ndarray`
            Decoder weight values to assign to self.V
        """
        self.V.set_value(weights)

    def get_visible_biases(self):
        """
        Returns
        -------
        rval : `numpy.ndarray`
            Visible biases
        """
        return self.b.get_value()

    def set_visible_biases(self, biases):
        """
        Sets visible bias values

        Parameters
        ----------
        biases : `numpy.ndarray`
            Visible bias values to assign to self.b
        """
        self.b.set_value(biases)

    def get_hidden_biases(self):
        """
        Returns
        -------
        rval : `numpy.ndarray`
            Hidden biases
        """
        return self.c.get_value()

    def set_hidden_biases(self, biases):
        """
        Sets hidden bias values

        Parameters
        ----------
        biases : `numpy.ndarray`
            Hidden bias values to assign to self.c
        """
        self.c.set_value(biases)

    #calc_combined_costは石見が書いた&杉山が加筆した
    def calc_combined_cost(self,X):
        """
        M=  [[[     0,     0,     0, ... ,                 0,  0,  0],
              [X[0,0],X[0,1],     0, ... ,                 0,  0,  0],
              ...
              [X[0,0],X[0,1],X[0,2], ... ,X[0,start[-1] - 1],  0,  0]],
              ...
             [[       0,       0,       0, ... ,                   0,  0,  0],
              [X[N-1,0],X[N-1,1],       0, ... ,                   0,  0,  0],
              ...
              [X[N-1,0],X[N-1,1],X[N-1,2], ... ,X[N-1,start[-1] - 1],  0,  0]]]
              where N = X.shape[0] (データ数)
        のようなものを作りたい(上のはあくまで例)
        shapeで階段の形を作っておき、それを(X.dimshuffle(0, 'x', 1) * T.ones((len(self.start), X.shape[1]))にかけることで作成
        """

        batch_size = X.shape[0]
        shape = np.array([[float(d < i) for d in range(self.dim)] for i in self.indices])
        M = (X.dimshuffle(0, 'x', 1) * T.ones((len(self.indices), X.shape[1])) * numpy.asarray(shape,dtype=theano.config.floatX))
        # データごと、featureごとにhが作られる
        # shapeは(データ数, feature数, dim_hid)
        h = self.sigmoid(T.dot(M, self.W) + self.c)
        costs = T.zeros((batch_size,))

        starts = self.indices
        ends = self.indices[1:] + [self.dim]

        for i, start, end in zip(range(len(starts)), starts, ends):
            if self.types[i] == "categorical":
                v = T.nnet.softmax(self.b[start:end] + T.dot(h[:,i], self.V[:,start:end]))
                costs += -(X[:, start:end] * T.log(v)).sum(axis=1)
            elif self.types[i] == "continuous":
                v = self.b[start] + T.dot(h[:,i], self.V[:, start])
                costs += (X[:, start] - v) * (X[:, start] - v)
            else:
                v = self.sigmoid(self.b[start] + T.dot(h[:,i], self.V[:, start]))
                costs += -(X[:, start] * T.log(v) + (1 - X[:, start]) * T.log(1 - v))

        return costs


    def get_monitoring_data_specs(self):
        """
        Returns data specs requiring both inputs and targets.
        Returns
        -------
        data_specs: TODO
            The data specifications for both inputs and targets.
        """

        space = CompositeSpace([(self.get_input_space())])
        source = (self.get_input_source(),)
        return space, source

    #_iはi番目の項目までを利用しi番目の項目のみ予測する関数
    #old_XWは前の項目を計算するのに用いたXW、lは前の項目からiまでの長さ
    def predict_i(self, X, i, old_XW, l):

        starts = theano.shared(np.array(self.indices))
        ends = theano.shared(np.array(self.indices[1:] + [self.dim]))
        start = starts[i]
        end = ends[i]

        type_dict = {"categorical":0, "continuous":1, "binary":2}
        Types = theano.shared(np.array(map(lambda s: type_dict[s], self.types)))

        XW = old_XW + T.dot(X[:, start - l:start], self.W[start - l:start])
        h = self.sigmoid(XW + self.c)

        v_prior1 = (self.b[start:end] + T.dot(h, self.V[:,start:end])).astype(theano.config.floatX)
        categorical_v = T.nnet.softmax(v_prior1)
        continuous_v = v_prior1
        binary_v = self.sigmoid(v_prior1)

        return T.switch(T.eq(Types[i], 0), categorical_v, T.switch(T.eq(Types[i], 1), continuous_v, binary_v)), XW

    def predict_i2(self, X, i):

        starts = theano.shared(np.array(self.indices))
        ends = theano.shared(np.array(self.indices[1:] + [self.dim]))
        start = starts[i]
        end = ends[i]

        type_dict = {"categorical":0, "continuous":1, "binary":2}
        Types = theano.shared(np.array(map(lambda s: type_dict[s], self.types)))

        h = self.sigmoid(T.dot(X[:, :start], self.W[:start]) + self.c)

        v_prior1 = (self.b[start:end] + T.dot(h, self.V[:,start:end])).astype(theano.config.floatX)
        categorical_v = T.nnet.softmax(v_prior1)
        continuous_v = v_prior1
        binary_v = self.sigmoid(v_prior1)

        return T.switch(T.eq(Types[i], 0), categorical_v, T.switch(T.eq(Types[i], 1), continuous_v, binary_v))


class NADE(NADEBase, JointDistribution):
    """
    An implementation of Larochelle's and Murray's neural autoregressive
    density estimator (NADE)
    """
    def _log_likelihood(self, X):
        return self._base_log_likelihood(X, self.W, self.V, self.b, self.c)

    def _sample(self, num_samples):
        return self._base_sample(num_samples, self.W, self.V, self.b, self.c)


class CNADE(NADEBase, ConditionalDistribution):
    """
    An implementation of Larochelle's and Murray's neural autoregressive
    density estimator (NADE) conditioned on an external observation
    """
    def __init__(self, dim, dim_hid, dim_cond, clamp_sigmoid=False, unroll_scan=1):
        """
        Parameters
        ----------
        dim : int
            Number of observed binary variables
        dim_hid : int
            Number of latent binary variables
        dim_cond : int
            Number of conditioning variables
        clamp_sigmoid : bool, optional
            WRITEME. Defaults to `False`.
        unroll_scan : int, optional
            WRITEME. Defaults to 1.
        """
        super(CNADE, self).__init__(dim=dim, dim_hid=dim_hid,
                                    clamp_sigmoid=clamp_sigmoid,
                                    unroll_scan=unroll_scan)

        self.dim_cond = dim_cond

        # Conditioning weights matrix for visible biases
        U_b_value = self._initialize_weights(self.dim_cond, self.dim)
        self.U_b = sharedX(U_b_value, 'U_b')
        # Conditioning weights matrix for hidden biases
        U_c_value = self._initialize_weights(self.dim_cond, self.dim_hid)
        self.U_c = sharedX(U_c_value, 'U_c')

    def get_params(self):
        """
        Returns
        -------
        params : list of tensor-like
            The model's parameters
        """
        params = super(CNADE, self).get_params()
        params.extend([self.U_c, self.U_b])
        return params

    def _log_likelihood(self, X, Y):
        # Conditioned visible biases, shape is (batch_size, self.dim_hid)
        b_cond = self.b + T.dot(Y, self.U_b)
        # Conditioned hidden biases, shape is (batch_size, self.dim_hid)
        c_cond = self.c + T.dot(Y, self.U_c)

        return self._base_log_likelihood(X, self.W, self.V, b_cond, c_cond)

    def _sample(self, Y):
        num_samples = Y.shape[0]
        # Conditioned visible biases, shape is (batch_size, self.dim_hid)
        b_cond = self.b + T.dot(Y, self.U_b)
        # Conditioned hidden biases, shape is (batch_size, self.dim_hid)
        c_cond = self.c + T.dot(Y, self.U_c)

        # Here we give b_cond.T as argument because it will be used in a scan
        # loop which systematically loops over the first axis.
        return self._base_sample(num_samples, self.W, self.V, b_cond.T, c_cond)

    def get_visible_conditioning_weights(self):
        """
        Returns
        -------
        rval : `numpy.ndarray`
            Visible conditioning weights
        """
        return self.U_b.get_value()

    def set_visible_conditioning_weights(self, weights):
        """
        Sets visible conditioning weight values

        Parameters
        ----------
        weights : `numpy.ndarray`
            Visible conditioning weight values to assign to self.U_b
        """
        self.U_b.set_value(weights)

    def get_hidden_conditioning_weights(self):
        """
        Returns
        -------
        rval : `numpy.ndarray`
            Hidden conditioning weights
        """
        return self.U_c.get_value()

    def set_hidden_conditioning_weights(self, weights):
        """
        Sets hidden conditioning weight values

        Parameters
        ----------
        weights : `numpy.ndarray`
            Hidden conditioning weight values to assign to self.U_c
        """
        self.U_c.set_value(weights)
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
