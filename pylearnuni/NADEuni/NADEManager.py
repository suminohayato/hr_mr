# coding: utf-8
import sys, os, csv, pickle, math, h5py, yaml
import numpy as np
import numpy.random

class NADEManager:
    """
    pathを設定しそこにdata.info, data.h5, features.yaml(, order.txt)を置く
    """

    # infoファイルの文字列から、そのデータ型が価格予測のNNにおいて何に対応するか調べ、それをNADEの型に対応づける
    # もしNADEの型にないもの（None）がきたら、NADE_special_typesで変換
    feature_types = ("car", "multi_scaled", "model", "unary", "past_years", "categorical", "inspection", "binary", "color", "transmission")
    NADE_types = {"car":None, "multi_scaled":"continuous", "model":None, "unary":"continuous", "past_years":None, "categorical":"categorical", "inspection":None, "binary":"binary", "color":"binary", "transmission":None, "hyokaten":"continuous"}
    NADE_special_types = {u"車検_残月":"continuous", u"車検":"categorical"# 車検は全て0になることもあるから本当はcategoricalではない
                          ,u"経過年":"continuous", u"値付け年":"continuous", u"値付け月":"continuous"
                          ,u"車種":"categorical", u"トランスミッション名":"categorical"
                          ,"SHAKEN_DAY":"categorical"}
    target_columns = [u"最新価格", "RAKU_RYUSATSU_KAKAKU"]

    #dataはndarray
    def arrange(self, data, indices):
        return np.array([[data[j, i] for i in indices] for j in range(data.shape[0])])

    def normalize(self, data, continuous_indices, means, stds):
        res = data.copy()
        for i, index in enumerate(continuous_indices):
            res[:, index] = (data[:, index] - means[i]) / stds[i]
        return res

    def normalize_inv(self, data, continuous_indices, means, stds):
        res = data.copy()
        for i, index in enumerate(continuous_indices):
            res[:, index] = data[:, index] * stds[i] + means[i]
        return res

    def normalize_dataset(self, dataset, continuous_indices, means, stds):
        chunk_no = dataset.shape[0] / self.chunksize
        if chunk_no == 0:
            chunk_no = 1
        for i in range(chunk_no):
            sys.stderr.write('\r\033[K' + str(i + 1) + "/" + str(chunk_no))
            sys.stderr.flush()
            start_id = i * self.chunksize
            end_id = min((i + 1) * self.chunksize, dataset.shape[0])
            for j, index in enumerate(continuous_indices):
                dataset[start_id:end_id, index] = (dataset[start_id:end_id, index] - means[j]) / stds[j]

    def create_order(self):
        info = yaml.load(open(self.path + "data.info"))
        features = yaml.load(open(self.path + "features.yaml"))

        #まず、各項目の型を得る
        column_features = []
        for s in info["column_name"]:
            s = s.split("<")[0]
            if s in self.target_columns:
                continue

            if s in features.keys():
                f = self.NADE_types[features[s]["feature_type"]]
                if f is None and s in self.NADE_special_types.keys():
                    f = self.NADE_special_types[s]
            elif s in self.NADE_special_types.keys():
                f = self.NADE_special_types[s]
            assert(f in ["binary", "continuous", "categorical"])

            column_features.append(f)

        #categorical, continuous, binaryの順で並び替えたものをorder.txtに保存
        column_names = info["column_name"]
        order_f = open(self.path + "order.txt", "w")
        for target_column in self.target_columns:
            if target_column in column_names:
                column_names.remove(target_column)
        column_names = map(lambda s: s.encode("utf-8"), column_names)
        for f_type in ["categorical", "continuous", "binary"]:
            for s, f in zip(column_names, column_features):
                if f == f_type:
                    order_f.writelines([s + os.linesep])
        order_f.close()

    def set_self_indices(self):
        """
        self.indices, self.dim, self.starts, self.typesをset
        """

        info = yaml.load(open(self.path + "data.info"))
        features = yaml.load(open(self.path + "features.yaml"))
        
        self.indices = []
        self.types = []

        #まず、各項目の型を得る(column_featuresに辞書で格納)
        column_features = {}

        for s in info["column_name"]:

            #<~>の部分を取り除く
            s = s.split("<")[0]

            #最新価格は無視
            if s in self.target_columns:
                continue

            #NADEでのデータ型を求める
            if s in features.keys():
                f = self.NADE_types[features[s]["feature_type"]]
                if f is None and s in self.NADE_special_types.keys():
                    f = self.NADE_special_types[s]
            elif s in self.NADE_special_types.keys():
                f = self.NADE_special_types[s]
            assert(f in ["binary", "continuous", "categorical"])

            column_features[s] = f

        column_name = info["column_name"]
        order = map(lambda s: unicode(s.strip(), "utf-8"), [s for s in open(self.path + "order.txt", "r")])
        for target_column in self.target_columns:
            if target_column in column_name:
                column_name.remove(target_column)
            if target_column in order:
                order.remove(target_column)

        scanned = []
        for i, s in enumerate(order):
            self.indices.append(column_name.index(s))
            s = s.split("<")[0]
            if s not in scanned:
                scanned.append(s)
                self.starts.append(i)
                self.types.append(column_features[s])

        self.dim = len(order)

    def __init__(self, path, chunksize=30000, create_arranged_everytime=True):
        self.path = path
        self.chunksize = chunksize
        self.create_arranged_everytime = create_arranged_everytime

        if self.path[-1] != "/":
            self.path += "/"

        self.Var = None
        self.means = None
        self.stds = None
        self.pred_i = None
        
        #indices[i]は変換後のi番目の項目が元々どのindexかを表す
        self.indices = []
        self.types = []
        
        self.dim = None
        self.starts = []

    #datasetにoriginalを並べ替えたものを追加していく
    def arrange_dataset(self, original, dataset):
        chunk_no = original.shape[0] / self.chunksize
        if chunk_no == 0:
            chunk_no = 1
        for i in range(chunk_no):
            start = i * self.chunksize
            end = min((i + 1) * self.chunksize, original.shape[0])

            arr = np.zeros((end - start, original.shape[1]))
            original_chunk = original[start:end, :]
            for time, j in enumerate(self.indices):
                sys.stderr.write('\r\033[K' + str(i + 1) + "/" + str(chunk_no) + "chunks " + str(time + 1) + "/" + str(len(self.indices)) + "columns")
                sys.stderr.flush()
                arr[:, time] = original_chunk[:, j]

            dataset[start:end] = arr
        print ""

    def create_yaml(self):

        if self.dim is None:
            self.set_self_indices()

        nadeyaml = open(os.path.dirname(os.path.abspath(__file__))+"/nade.yaml", "w")
        for line in open(os.path.dirname(os.path.abspath(__file__))+"/base.yaml", "r"):
            s = line.strip().split(" ")[0].strip(":")
            if s == "dataset_path":
                line = line % ("'" + self.path + "arranged.h5'")
            elif s == "dim":
                line = line % str(self.dim)
            elif s == "indices":
                line = line % str(self.starts)
            elif s == "types":
                line = line % str(self.types)
            elif s == "save_path":
                line = line % self.path

            nadeyaml.writelines([line])

        nadeyaml.close()

    #arranged.h5を生成する
    def create_arranged(self):
        print "preprocessing..."
        data = h5py.File(self.path + "data.h5")
        arranged = h5py.File(self.path + "arranged.h5", "w")

        print "datashape:" + str(data["X"].shape)
        chunk_no = (arranged["X"].shape[0] - 1) / self.chunksize + 1

        #目的変数yの方はチャンクごとそのままコピー
        arranged.create_dataset("/y", (data["y"].shape[0], 1), chunks=(min(data["y"].shape[0], self.chunksize), 1), compression="gzip")
        arranged.create_dataset("/test/y", (data["test"]["y"].shape[0], 1), chunks=(min(data["test"]["y"].shape[0], self.chunksize), 1), compression="gzip")
        arranged.create_dataset("/valid/y", (data["valid"]["y"].shape[0], 1), chunks=(min(data["valid"]["y"].shape[0], self.chunksize), 1), compression="gzip")
        for i in range(chunk_no):
            arranged["y"][i * self.chunksize: min((i + 1) * self.chunksize, data["y"].shape[0])] = data["y"][i * self.chunksize: min((i + 1) * self.chunksize, data["y"].shape[0])]
            arranged["test"]["y"][i * self.chunksize: min((i + 1) * self.chunksize, data["test"]["y"].shape[0])] = data["test"]["y"][i * self.chunksize: min((i + 1) * self.chunksize, data["test"]["y"].shape[0])]
            arranged["valid"]["y"][i * self.chunksize: min((i + 1) * self.chunksize, data["valid"]["y"].shape[0])] = data["valid"]["y"][i * self.chunksize: min((i + 1) * self.chunksize, data["valid"]["y"].shape[0])]

        arranged.create_dataset("/X", data["X"].shape, chunks=(min(data["X"].shape[0], self.chunksize), data["X"].shape[1]), compression="gzip")
        arranged.create_dataset("/test/X", data["test"]["X"].shape, chunks=(min(data["test"]["X"].shape[0], self.chunksize), data["test"]["X"].shape[1]), compression="gzip")
        arranged.create_dataset("/valid/X", data["valid"]["X"].shape, chunks=(min(data["valid"]["X"].shape[0], self.chunksize), data["valid"]["X"].shape[1]), compression="gzip")

        print "arranging train data"
        self.arrange_dataset(data["X"], arranged["X"])
        print "arranging test data"
        self.arrange_dataset(data["test"]["X"], arranged["test"]["X"])
        print "arranging valid data"
        self.arrange_dataset(data["valid"]["X"], arranged["valid"]["X"])

        #ここから正規化
        continuous_no = self.types.count("continuous")
        totals = np.zeros((continuous_no))

        continuous_indices = []
        for i, type in zip(self.starts, self.types):
            if type == "continuous":
                continuous_indices.append(i)

        for i in range(chunk_no):
            totals += np.sum(arranged["X"][i * self.chunksize: min((i + 1) * self.chunksize, data["X"].shape[0]), continuous_indices], axis=0)
        means = totals / data["X"].shape[0]

        totals = np.zeros((continuous_no))
        for i in range(chunk_no):
            X = arranged["X"][i * self.chunksize: min((i + 1) * self.chunksize, data["X"].shape[0]), continuous_indices]
            totals += np.sum((X - means) * (X - means), axis=0)
        vars = totals / data["X"].shape[0]
        stds = np.sqrt(vars)
        means_f = open(self.path + "means.txt", "w")
        stds_f = open(self.path + "stds.txt", "w")
        for mean, std in zip(means, stds):
            means_f.writelines([str(mean) + os.linesep])
            stds_f.writelines([str(std) + os.linesep])

        #同じmeans, stdsを使って正規化する
        def normalize(dataset):
            self.normalize_dataset(dataset, continuous_indices, means, stds)

        print "normalizing..."
        normalize(arranged["X"])
        normalize(arranged["test"]["X"])
        normalize(arranged["valid"]["X"])

        arranged.flush()
        data.close()
        arranged.close()
        print "done"

    def train(self):

        if not(os.path.isfile(self.path + "order.txt")):
            print "generating order.txt..."
            self.create_order()
            print "done"

        self.set_self_indices()
        if not(os.path.isfile(self.path + "arranged.h5")) or self.create_arranged_everytime:
            self.create_arranged()
        else:
            print "skip preprocessing"

        print "generating nade.yaml..."
        self.create_yaml()
        print "done"
        print "training..."

        os.system("python "+os.path.dirname(os.path.abspath(__file__))+"/train.py "+os.path.dirname(os.path.abspath(__file__))+"/nade.yaml")

        print "done"
        print "calculating Var..."

        arranged = h5py.File(self.path + "arranged.h5", "r")
        result = pickle.load(open(self.path + "best_nade_param.pkl", "r"))

        #予測関数を準備
        x = T.dmatrix()
        index_T = T.iscalar()
        y = result.predict_i2(x, index_T)
        pred_i = function(inputs=[x, index_T], outputs=y)

        pred_chunksize = 1000
        chunk_no = (arranged["X"].shape[0] - 1) / pred_chunksize + 1
        total = 0
        for i in range(chunk_no):
            sys.stderr.write('\r\033[K' + str(i + 1) + "/" + str(chunk_no))
            sys.stderr.flush()
            start = i * pred_chunksize
            end = min((i + 1) * pred_chunksize, arranged["X"].shape[0])
            data = np.asarray(arranged["X"][start:end], dtype=np.float32)
            for j, type, index in zip(range(len(self.types)), self.types, self.indices):
                if type == "continuous":
                    total += np.sum((pred_i(data, j).reshape(data.shape[0]) - data[:, index]) * (pred_i(data, j).reshape(data.shape[0]) - data[:, index]))

        Var = total / arranged["X"].shape[0] / self.types.count("continuous")

        File = open(self.path + "Var.txt","w")
        File.write(str(Var))
        File.close()

        arranged.close()

        print "done"

    def sample_init(self):
        self.set_self_indices()
        if os.path.isfile(self.path + "best_nade_param.pkl"):
            result = pickle.load(open(self.path + "best_nade_param.pkl", "r"))
        elif os.path.isfile(self.path + "result.pkl"):
            result = pickle.load(open(self.path + "result.pkl", "r"))
        else:
            print "best_nade_param.pkl (or result.pkl) is not found"
            exit()

        #仮定している正規分布の分散
        self.Var = float(open(self.path + "Var.txt", "r").read())

        #学習データの項目ごと平均、分散をロード
        self.means = [float(row) for row in open(self.path + "means.txt", "r")]
        self.stds = [float(row) for row in open(self.path + "stds.txt", "r")]

        #NNの出力を得る関数のコンパイル
        x = T.dmatrix()
        index_T = T.iscalar()
        old_XW = T.dmatrix()
        XW = T.dmatrix()
        l = T.iscalar()
        y, XW = result.predict_i(x, index_T, old_XW, l)

        self.pred_i = function(inputs=[x, index_T, old_XW, l], outputs=[y, XW])

    #入力する項目に最新価格を含まないようにすること（変更しました）
    def sample(self, data, n):
        if os.path.isfile(self.path + "best_nade_param.pkl"):
            result = pickle.load(open(self.path + "best_nade_param.pkl", "r"))
        elif os.path.isfile(self.path + "result.pkl"):
            result = pickle.load(open(self.path + "result.pkl", "r"))
        else:
            print "best_nade_param.pkl (or result.pkl) is not found"
            exit()

        # 順番入れ替え&正規化
        continuous_indices = []
        for i, type in zip(self.starts, self.types):
            if type == "continuous":
                continuous_indices.append(i)

        data = self.normalize(self.arrange(data, self.indices), continuous_indices, self.means, self.stds)

        #shape指定
        data2 = np.zeros([data.shape[0], data.shape[1], n])

        starts = self.starts
        ends = self.starts[1:] + [self.dim]

        print "sampling..."
        for i in range(n):
            sys.stderr.write('\r\033[K' + str(i + 1) + "/" + str(n))
            sys.stderr.flush()

            length = 0
            old_XW = np.zeros((data.shape[0], result.dim_hid))

            #特徴量ごとサンプリング
            for j, index, type, start, end in zip(range(len(self.starts)), self.starts, self.types, starts, ends):

                #NNの出力（categorical,binaryでは確率そのもの）
                p, old_XW = self.pred_i(data2[:, :, i], j, old_XW, length)
                length = end - start

                for k in range(data.shape[0]):
                    if math.isnan(data[k, index]):
                        if type == "categorical":
                            #確率分布の和が1以下でないとnp.random.multinomialが動かないことに対する処方箋
                            for l in range(p.shape[1]):
                                if p[k][l] > 0.0000001:
                                    p[k][l] -= 0.0000001
                            data2[k, start:end, i] = np.random.multinomial(1, p[k])

                        elif type == "continuous":
                            data2[k, start, i] = np.random.normal(p[k], self.Var)

                        elif type == "binary":
                            data2[k, start, i] = (p[k] >= np.random.rand())

                    else:
                        for l in range(start, end):
                            assert(not(math.isnan(data[k, l])))
                        data2[k, start:end, i] = data[k, start:end]
        print ""

        data2 = self.normalize_inv(data2, continuous_indices, self.means, self.stds)

        # 項目の順番を元に戻す
        res = np.zeros([data.shape[0], data.shape[1], n])
        for i in range(len(self.indices)):
            res[:, self.indices[i], :] = data2[:, i, :]

        return res


if __name__ == "__main__":

    if len(sys.argv) <= 1:
        print u"pathを指定してください。"
        quit()

    manager = NADEManager(sys.argv[1])
    manager.train()


