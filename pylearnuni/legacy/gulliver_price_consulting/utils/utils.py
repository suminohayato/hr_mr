import time
import pickle
import os
import numpy
np = numpy


class messages(object):

    def __init__(self, message='Starting...', verbose=True):
        self.verbose = verbose
        self.message = message

    def __enter__(self):
        if self.verbose:
            print self.message

    def __exit__(self, type, value, traceback):
        if self.verbose:
            print 'Done'


class measuring_speed(messages):

    def __init__(self, starting_message='Starting computation...', ending_message='Done in',
                 unit='sec', verbose=True):
        super(measuring_speed, self).__init__(starting_message, verbose)
        self.ending_message = ending_message
        self.obuffer = np.zeros(1)
        self.norm = 1.0
        self.unit = unit
        if unit == 'min':
            self.norm = 60.0
        elif unit == 'hour':
            self.norm = 60.0**2

    def __enter__(self):
        super(measuring_speed, self).__enter__()
        self.timer = time.time()
        return self.obuffer

    def __exit__(self, type, value, traceback):
        self.timer = time.time() - self.timer
        self.obuffer[0] = self.timer
        if self.verbose:
            print self.ending_message + (' %g %ss' % (self.timer / self.norm, self.unit))

    def __repr__(self):
        return 'Duration :%g [%ss]' % (self.timer / self.norm, self.unit)

    def float(self):
        return self.timer


class pushd():

    def __init__(self, location):
        self.original_dir = os.path.realpath(os.curdir)
        self.location = location

    def __enter__(self):
        if not os.path.isdir(self.location):
            os.makedirs(self.location)
        os.chdir(self.location)

    def __exit__(self, type, value, traceback):
        os.chdir(self.original_dir)


def load_if_exists(type='function', default_path=None):
    def outer(generator):
        if type == 'function':
            def inner(path=default_path, overwrite=False, **kwargs):
                assert(path)
                if overwrite or (not os.path.exists(path)):
                    obj = generator(**kwargs)
                    if obj is not None:
                        with open(path, 'w') as fp:
                            pickle.dump(obj, fp)
                else:
                    with open(path) as fp:
                        obj = pickle.load(fp)
                return obj
        elif type == 'method':
            def inner(self, path=default_path, overwrite=False, **kwargs):
                assert(path)
                if overwrite or (not os.path.exists(path)):
                    obj = generator(self, **kwargs)
                    if obj is not None:
                        with open(path, 'w') as fp:
                            pickle.dump(obj, fp)
                else:
                    with open(path) as fp:
                        obj = pickle.load(fp)
                return obj
        return inner
    return outer
