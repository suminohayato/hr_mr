#!/usr/bin/env python
# coding: utf-8

import sys
import csv
import os
import pickle
import pandas
import numpy as np
import theano
import theano.tensor as T
import gulliver_price_consulting.data.process_data2 as process_data
from gulliver_price_consulting.utils import utils
from pylearn2.config import yaml_parse
import datetime

PROJECTROOT = os.getenv('GULLIVER_PROJECT_PATH')
nvis = int(os.getenv("LEGACY_NVIS"))


def mean_squared_error(y, y_pred):
    return np.sum((np.array(y) - np.array(y_pred)) ** 2) / len(y)


def train_step(config_file, params=None, exe=True):
    dirname, filename = os.path.split(os.path.abspath(config_file))
    with utils.pushd(dirname):
        assert(os.path.exists(filename))
        print dirname, filename
        _yaml = open(filename).read() % (params)
        _train = yaml_parse.load(_yaml)
        if exe:
            _train.main_loop()
        return _train, _yaml


def generate_predictor(model):
    _input = T.matrix('input')
    prediction = model.fprop(_input)
    predict = theano.function([_input], prediction)
    return predict


def as_price(x):
    return np.asarray(np.round(np.exp(x.flat)), dtype='int32')


def regression(dataset, model, batch_size=100, verbose=True):
    predict = generate_predictor(model)
    y_actual, y_predicted = [], []
    nbatches = len(dataset.X) / batch_size
    assert(len(dataset.X) % batch_size == 0)
    for idx in xrange(nbatches):
        X_ = dataset.X[idx * batch_size:(idx + 1) * batch_size]
        y_ = dataset.y[idx * batch_size:(idx + 1) * batch_size]
        act_ = np.asarray(np.exp(y_), dtype='int32')
        pre_ = np.asarray(np.round(np.exp(predict(np.asarray(X_, dtype='float32')).flat)), dtype='int32')
        y_actual.append(act_)
        y_predicted.append(pre_)
        if verbose:
            for p, a in zip(pre_.flat, act_.flat):
                print p, ' -> ', a
    y_actual = np.asarray(y_actual, dtype='float').flatten()
    y_predicted = np.asarray(y_predicted, dtype='float').flatten()
    if verbose:
        print '#cases: %g' % len(y_actual)
        print 'RMSE: %g' % np.sqrt(((y_actual - y_predicted)**2).mean())
    return y_actual, y_predicted


def save_with_full_dataslots(mlp, datasets, savepath='./results.csv', with_counts=True):
    #datasets = _train.algorithm.monitoring_dataset
    test_data = datasets['test']
    train_data = datasets['train']
    y_actual, y_predicted = regression(test_data, mlp)

    orgdf = process_data.as_type_of(data_object=test_data, return_type='original_data_frame')
    orgdf['予測結果'] = y_predicted

    columns = list(orgdf.keys())
    columns.remove('予測結果')
    columns.remove('最新価格')
    columns = ['予測結果', '最新価格'] + columns
    orgdf = orgdf.reindex(columns=columns)

    fs = process_data.feature_names()
    keys = orgdf.keys()
    used_in_training = pandas.DataFrame([k in fs for k in keys], index=keys, dtype=int).T
    orgdf = pandas.concat((used_in_training, orgdf))

    if with_counts:
        train_orgdf = process_data.as_type_of(data_object=test_data, return_type='original_data_frame')
        orgdf["メーカーコードカウント"] = [(train_orgdf["メーカーコード"] == i).sum() for i in orgdf["メーカーコード"]]
        orgdf["メーカー/車種"] = [(train_orgdf[['メーカーコード', '車種コード']].as_matrix() == tmp).all(axis=1).sum() for tmp in orgdf[['メーカーコード', '車種コード']].as_matrix()]
        orgdf["メーカー/車種/グレード"] = [(train_orgdf[['メーカーコード', '車種コード', 'グレードコード']].as_matrix() == tmp).all(axis=1).sum() for tmp in orgdf[['メーカーコード', '車種コード', 'グレードコード']].as_matrix()]
    print orgdf.keys()
    orgdf.to_csv(savepath)


def save_results(y_actual, y_predicted, filename='prediction.csv'):
    x = np.asarray([y_predicted, y_actual])
    with open(filename, 'w') as f:
        f.write('predictions, actual values\n')
        for r in x.T:
            f.write('%d,%d\n' % tuple(r))
    #np.savetxt(filename, x, delimiter=',', header='predictions, actual values')


def save_logs():
    with open(os.path.join(PROJECTROOT, 'log.csv'), 'a') as f:
        d = datetime.datetime.today()
        os0 = os.path.abspath("mlp.yaml")
        r = s = t = i = 0
        while i < len(os0) - 1:
            i += 1
            if os0[i] == '/':
                r = t
                t = s
                s = i
        trim0 = os0[t + 1:s]
        trim1 = os0[r + 1:t]

        row = d.strftime("%Y/%m/%d %H:%M") + "," + trim1 + "," + trim0 + "\n"
        f.write(row)


def train_main(nvis_, trainsize_, testsize_):
    yaml_path = 'mlp.yaml'
    nvis = nvis_
    trainsize = trainsize_
    testsize = testsize_
    params = {'data_path': '/home/public/CS150930_prepro', 'save_path': './model', 'nvis': nvis, 'trainsize': trainsize, 'testsize': testsize}
    best_mlp_path = '%(save_path)s/best_mlp.pkl' % (params)
    _train, _yaml = train_step(yaml_path, params)
    mlp = pickle.load(open(best_mlp_path))

    return mlp


def main(output_path="./output", yaml_path="mlp.yaml"):
    params = {'data_path': output_path, 'save_path': output_path, 'nvis': nvis}
    best_mlp_path = '%(save_path)s/best_mlp.pkl' % (params)
    if os.path.exists(best_mlp_path):
        mlp = pickle.load(open(best_mlp_path))
        _train, _yaml = train_step(yaml_path, params, False)
    else:
        _train, _yaml = train_step(yaml_path, params)
        mlp = pickle.load(open(best_mlp_path))

    datasets = _train.algorithm.monitoring_dataset
    test_data = datasets['test']

    y_actual, y_predicted = regression(test_data, mlp)
    print '#cases: %g' % len(y_actual)
    print 'RMSE: %g' % np.sqrt(((y_actual - y_predicted)**2).mean())
    #save_results(y_actual, y_predicted)
    save_logs()

    datasets = _train.algorithm.monitoring_dataset
    save_with_full_dataslots(mlp, datasets)

    #print("TRAIN_RMSE:%g, TEST_RMSE:%g"%(rmse))
