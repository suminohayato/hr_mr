#!/usr/binenv python
# coding: utf-8

'''
reimplementation of process_data.py where data specification is suppulied as yaml
instead of being hard-coded in the program
'''

import pandas
import re
import os
import yaml
import pickle
import numpy as np
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrixPyTables
from collections import defaultdict
import getpass
pd = pandas
from guppy import hpy
heap = hpy()

DATAROOT = os.getenv('GULLIVER_DATA_PATH')
assert(DATAROOT)
INPUT_CSV_PATH = os.getenv("LEGACY_INPUT_CSV_PATH")
FEATURES_SPEC = os.getenv("LEGACY_FEATURES_YAML_PATH")
OUTPUT_CSV_PATH = os.path.join(DATAROOT, "data.csv")
OUTPUT_BIN_PATH = os.path.join(DATAROOT, "data.bin")
OUTPUT_INFO_PATH = os.path.join(DATAROOT, "data.info")
OUTPUT_IDX_PATH = os.path.join(DATAROOT, "data.idx")

info_keys = ('DATAROOT', 'INPUT_CSV_PATH', 'VERSION', 'FEATURES_SPEC',
             'OUTPUT_CSV_PATH', 'OUTPUT_BIN_PATH', 'OUTPUT_INFO_PATH', 'OUTPUT_IDX_PATH')

TARGET_LABEL = '最新価格'
TARGET_LABEL2 = '初期登録価格'
SEED = 1234

missing_values = {"メーカーコード": 'ZZ',
                  "車種コード": 'S999',
                  "グレードコード": 'G999',
                  "ドア数": 0, "乗車定員": 0, "シート列数": 0,
                  '新車時車両本体価格(税抜)': 0.0,
                  "排気量": 0.0,
                  #"福祉車両フラグ":0,
                  "装備フラグ予備１": 0,
                  "装備フラグ予備２": 0,
                  "装備フラグ予備１６": 0,
                  "装備フラグ予備１４": 'Z',
                  "装備フラグ予備１３": 'Z'}


def showheap():
    print heap.heap()


def time(func):
    """
    デバッグ用デコレータ
    実効時間を表示するデコレータ
    """
    import functools
    import datetime

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start = datetime.datetime.today()
        result = func(*args, **kwargs)
        end = datetime.datetime.today()
        print '-------------------------------------------------------------'
        print func
        print 'runningtime:', end - start
        print '-------------------------------------------------------------'
        return result
    return wrapper


def ftype(feature_name):
    """
input:特徴名(str)
output:その特徴の種類(str)
example:
    ftype("ドア数")            ->  "unary"
    ftype("メーカーコード")    ->  "categorical"
    ftype("メーカーコード_HO") -> エラー　これは展開後に現れる特徴のため
    """
    flags = [k for k, v in feature_types().items() if feature_name in v]
    if len(flags) != 1:
        print "flags:"
        print flags
        print "len(flags),", len(flags)
    assert(len(flags) == 1)
    return flags[0]

discriminate_feature_type = ftype

feature_types_gl = None


def feature_types():
    """
output:特徴種類名→特徴名リストの辞書(dict)
example:
    feature_types()['binary']  ->  ["サンルーフフラグ","4WDフラグ",...,]
    ※feature_types()['binary']の中には"メーカーコード_HO"等は含まれない、なぜなら展開後に現れる特徴のため
    """
    global feature_types_gl
    if feature_types_gl is None:
        print "calculating feature_types"
        with open(FEATURES_SPEC) as f:
            yaml_ = yaml.load(f)
        feature_types_ = yaml_['feature_types']
        tmp = defaultdict(list)
        for fname, v in feature_types_.items():
            tmp[v['ftype']].append(fname.encode('utf8'))
        feature_types_gl = tmp
    return feature_types_gl

feature_names_gl = None


def feature_names():
    """
output:特徴
    """
    global feature_names_gl
    if feature_names_gl is None:
        print "calculating feature_names"
        with open(FEATURES_SPEC) as f:
            yaml_ = yaml.load(f)
        feature_names_gl = [f.encode('utf8') for f in yaml_['feature_list']]
    return feature_names_gl

data_info_gl = None


def data_info():
    global data_info_gl
    if data_info_gl is None:
        with open(OUTPUT_INFO_PATH, 'r') as f:
            info = yaml.load(f.read())
            data_info_gl = info
    return data_info_gl

# def fname_mapping():
#    mappingdata=data_info()["feature_name_mappings"]
#    return mappingdata


def extended_feature_names():
    return data_info()["description"]

load_pkl_gl = {}


def load_pkl(pklname):
    if pklname not in load_pkl_gl:
        print "loading ", pklname,
        with open(pklname, 'r') as f:
            load_pkl_gl[pklname] = pickle.load(f)
    return load_pkl_gl[pklname]

read_csv_raw_gl = {}


def read_csv_raw(csvfile=INPUT_CSV_PATH):
    if csvfile in read_csv_raw_gl:
        print "rawcsv ", csvfile, " is already loaded."
    else:
        read_csv_raw_gl[csvfile] = pandas.read_csv(csvfile)

    return read_csv_raw_gl[csvfile]

read_csv_gl = {}


def read_csv(debug=False, do_fillna=True, csvfile=INPUT_CSV_PATH, shuffle=True, size=500000):
    print "read_csv", locals()
    if debug == False and csvfile in read_csv_gl:
        print "already loaded."
        dataset = read_csv_gl[csvfile]
    else:
        print "calculating for the first time."
        dataset_org = read_csv_raw(csvfile)

        np.random.seed(SEED)
        dataset_org = dataset_org.reindex(np.random.permutation(dataset_org.index))
        dataset_org = dataset_org[0:size]

        if debug:
            return dataset_org

        print "CSV1 shape:", dataset_org.shape
        print("done\nPre-processing...")
        print "dataset_org.shape before drop:", dataset_org.shape
        dataset_org = drop_illegal_rows(dataset_org)
        dataset = pandas.DataFrame()
        name_mappings = {}
        print "dataset_org.shape after drop: ", dataset_org.shape
        for name in feature_names():
            if ftype(name) == "past_years":
                nil, mappings_ = convert_past_years(dataset_org.ix[:, [name, "最終掲載日時"]], dataset)
            else:
                nil, mappings_ = __feature_converters[ftype(name)](dataset_org[name], dataset)
            print name
            print "dataset shape:", dataset.shape
            name_mappings.update(mappings_)
        #dataset.__unipro_feature_name_mappings = name_mappings
        read_csv_gl[csvfile] = dataset

    print "dataset shape before postproc", dataset.shape
    if do_fillna:
        dataset = dataset.fillna(0)  # .set_index(target_name)
    for feature in dataset.keys():
        assert(dataset[feature].dtype != 'O')
    if shuffle:
        np.random.seed(SEED)
        dataset = dataset.reindex(np.random.permutation(dataset.index))
    print "dataset shape after postproc", dataset.shape
    return dataset


def expand_features(df, do_fillna=True):
    dataset_org = df
    dataset = pandas.DataFrame()
    name_mappings = {}
    for name in feature_names():
        # print name
        if ftype(name) == "past_years":
            nil, mappings_ = convert_past_years(dataset_org.ix[:, [name, "最終掲載日時"]], dataset)
        else:
            nil, mappings_ = __feature_converters[ftype(name)](dataset_org[name], dataset)
        name_mappings.update(mappings_)
    if do_fillna:
        dataset = dataset.fillna(0)
    for feature in dataset.keys():
        assert(dataset[feature].dtype != 'O')
    # dataset.__unipro_feature_name_mappings = name_mappings #iranai
    dataset2 = pd.DataFrame()
    for f in extended_feature_names():
        if f in dataset:
            dataset2[f] = dataset[f]
        else:
            dataset2[f] = np.zeros(dataset.shape[0])
    return dataset2


def __expand_as_one_hot(data_column, values):
    assert(len(data_column.shape) == 1)
    #name = data_column.keys()[0]
    name = data_column.name
    keys = ['%(name)s_%(val)s' % locals() for val in values]
    return {key: (data_column.astype(np.string_) == val).astype(int) for key, val in zip(keys, values)}


def one_to_many_converter(fun):
    '''converter functions that maps a variable to
    a collection of variables (e.g. one-hot conversion)'''
    def inner(data_column, df=None):
        assert(len(data_column.shape) == 1)
        if df is None:
            df = pandas.DataFrame()
        dict_ = fun(data_column)
        for k, v in dict_.items():
            df[k] = v
        name = data_column.name
        name_mappings = {k: name for k in dict_.keys()}
        name_mappings.update({name: dict_.keys()})
        return df, name_mappings
    return inner


def one_to_one_converter(fun):
    'converter functions that maps a variable to another'
    def inner(data_column, df=None):
        assert(len(data_column.shape) == 1)
        name = data_column.name
        if df is None:
            df = pandas.DataFrame()
        df[name] = fun(data_column)
        name_mappings = {name: name}
        return df, name_mappings
    return inner

# todo hardcording


def many_to_many_converter(fun):
    '''converter functions that maps a variable to
    a collection of variables (e.g. one-hot conversion)'''
    def inner(data_column, df=None):
        if df is None:
            df = pandas.DataFrame()
        dict_ = fun(data_column)
        for k, v in dict_.items():
            df[k] = v
        name = "年式"
        name_mappings = {k: name for k in dict_.keys()}
        name_mappings.update({name: dict_.keys()})
        return df, name_mappings
    return inner


@one_to_one_converter
def convert_multi_scaled(data_column):
    '''
    convert a varible that takes on values from
    multiple scales (e.g. [10^0, 10^4]) with logarithm
    '''
    df = np.log(data_column)

    df.loc[data_column == 0] = 0
    return df


@one_to_one_converter
def convert_model(data_column):
    '''convert the year of the release of a car into the age of the car'''
    return 2015 - data_column


@one_to_one_converter
def convert_unary(data_column):
    '''convert an unary (e.g., binary or trinary) variables'''
    return data_column


@many_to_many_converter
def convert_past_years(data_column):
    '''convert an unary (e.g., binary or trinary) variables'''
    pricedyear = []
    pricedmonth = []
    for i in data_column.index:
        date = data_column['最終掲載日時'][i]
        if isinstance(date, np.float):  # 欠損値はnp.nan
            assert(np.isnan(date))
            year = np.nan
            month = np.nan
        else:
            year = int(date.split('/')[0])
            month = int(date.split('/')[1])
        pricedyear.append(year)
        pricedmonth.append(month)

    return {'経過年': pricedyear - data_column['年式'], '値付け年': pricedyear, '値付け月': pricedmonth}

convert_binary = convert_unary
convert_color = convert_unary


@one_to_many_converter
def convert_categorical(data_column):
    '''convert an unary categorical variable with one_hot encoding'''
    values = data_column.dropna().drop_duplicates()
    print "len(values) in convert categorical", len(values)
    return __expand_as_one_hot(data_column, values)


@one_to_many_converter
def convert_car(data_column):
    dataset_org = read_csv_raw(INPUT_CSV_PATH)
    dataset_org = drop_illegal_rows(dataset_org)
    '''convert an unary categorical variable with one_hot encoding'''
    values = data_column.dropna().drop_duplicates()
    values_pre = dataset_org.ix[:, ["メーカーコード", "車種コード"]].dropna().drop_duplicates()
    values = values_pre["メーカーコード"] + "_" + values_pre["車種コード"]
    values.reset_index(drop=True)
    print "len(values) in convert car", len(values)
    return __expand_as_one_hot(data_column, values)


@one_to_many_converter
def convert_inspection(data_column):
    '''convert the inspection variable'''
    inspection_statuses = ('新車未登録', '国内未登録', '車検整備別', '車検整備無', '車検整備付')

    def isnan(x):
        if isinstance(x, str) or isinstance(x, unicode):
            return x.strip() == "" or x.strip().lower() == "nan"
        else:
            return np.isnan(x)
    dict_ = __expand_as_one_hot(data_column, inspection_statuses)
    patterns = data_column.dropna().drop_duplicates()
    d = {}
    for p in patterns:
        if isnan(p):
            continue
        m = re.search('\AH*([^/.]*)[/.](.*)\Z', p)
        if m:
            year, month = m.groups()
            d[p] = int(year) * 12 + int(month)
    if len(d) != 0:
        dmin = min(d.values())
    for p in patterns:
        if p in d:
            d[p] = d[p] - dmin
    for p in inspection_statuses:
        d[p] = -10
    name_ = '車検_残月'
    dict_[name_] = pandas.Series(np.zeros(data_column.shape[0], dtype='int64'), index=data_column.index)
    for p in patterns:
        if isnan(p):
            for v in dict_.values():
                v.loc[data_column == p] = np.float("nan")
        else:
            dict_[name_].loc[data_column == p] = d[p]
    nanindex = data_column != data_column  # because nan!=nan
    for v in dict_.values():
        v.loc[nanindex] = np.float("nan")
    return dict_

__feature_converters = {k: eval('convert_' + k) for k in feature_types().keys()}


def drop_illegal_rows(dataset):
    dataset = dataset.drop(dataset.index[(dataset['走行'] > 9e5)])
    dataset = dataset.drop(dataset.index[(dataset['最新価格'] > 1e8) & ~np.isnan(dataset["最新価格"])])
    dataset = dataset.drop(dataset.index[(dataset['排気量'] > 1e4)])
    dataset = dataset.drop(dataset.index[(dataset['年式'] < 1900)])
    return dataset


@time
def generate_CSV2(dataset=None):
    'load CSV1, clean the data, and save as CSV2'
    if not os.path.exists(OUTPUT_CSV_PATH):
        if dataset is None:
            dataset = read_csv()
        print("Generating CSV2... shape:{}".format(dataset.shape))
        dataset.to_csv(OUTPUT_CSV_PATH)
    else:
        print 'CSV2 has been found. \nAborting.'
    return dataset

generate_BIN_gl = None


@time
def generate_BIN(dataset=None):
    'load CSV1, clean the data, and save as a bininary file'
    if not (os.path.exists(OUTPUT_BIN_PATH) and os.path.exists(OUTPUT_INFO_PATH)):
        if dataset is None:
            dataset = read_csv()
        print("Generating a binary file...")
        info = {}
        datamat = dataset.as_matrix()
        info['shape'] = datamat.shape
        info['description'] = list(dataset.keys())
        info['CSV1'] = os.path.abspath(INPUT_CSV_PATH)
        info['CSV2'] = os.path.abspath(OUTPUT_CSV_PATH)
        # info['feature_name_mappings'] = dataset.__unipro_feature_name_mappings
        info['SEED'] = SEED
        with open(OUTPUT_INFO_PATH, 'w') as f:
            f.write(yaml.dump(info))
        datamat.tofile(OUTPUT_BIN_PATH)
        index = np.asarray(dataset.index, dtype='int32')
        index.tofile(OUTPUT_IDX_PATH)
    else:
        print 'The binary file has been found!\nloading...'
        with open(OUTPUT_INFO_PATH, 'r') as f:
            info = yaml.load(f.read())

        global generate_BIN_gl
        if generate_BIN_gl is None:
            print "Loading ", OUTPUT_IDX_PATH
            index = np.fromfile(OUTPUT_IDX_PATH, dtype='int32')
            print "Loading ", OUTPUT_BIN_PATH
            datamat = np.fromfile(OUTPUT_BIN_PATH).reshape(info['shape'])
            generate_BIN_gl = [index, datamat]
        else:
            print "The file was already loaded."
            index, datamat = generate_BIN_gl

        print 'done'
    return dataset, datamat, info, index


def as_type_of(return_type, data_object):
    kwargs = {k: v for k, v in data_object.__unipro_info.items() if k in ('dataset_name', 'test_size', 'train_size', 'target_value_threshold', 'return_type', 'data_object')}
    kwargs['return_type'] = return_type
    return load_datamat(**kwargs)


load_datamat_datamat_gl = {}


def load_datamat(dataset_name='train', test_size=5000, train_size=400000,
                 target_value_threshold=None, return_type='DenseDesignMatrixPyTables'):
    if not os.getenv("LEGACY_TRAIN_SIZE") is None:
        train_size = int(os.getenv("LEGACY_TRAIN_SIZE"))

    ns = locals().copy()
    ns.update(globals())
    unipro_info = {k: v for k, v in ns.items() if k is not 'return_type'}
    'interface for pylearn2'
    dataset, datamat, info, index = generate_BIN()
    shape = info['shape']
    description = info['description']
    #feature_name_mappings = info['feature_name_mappings']
    target_mask = np.asarray([item == TARGET_LABEL for item in description])
    description.remove(TARGET_LABEL)

    if target_value_threshold in load_datamat_datamat_gl:
        print "target value threshold", target_value_threshold, "is already calculated."
        drop_mask, datamat, index = load_datamat_datamat_gl[target_value_threshold]
    else:
        if target_value_threshold:
            print "dropping target value threshold ", target_value_threshold
            drop_mask = datamat[:, target_mask].flat > np.log(target_value_threshold)
            datamat = datamat[~drop_mask, :]
            index = index[~drop_mask]
            load_datamat_datamat_gl[target_value_threshold] = (drop_mask, datamat, index)

    if return_type == 'original_data_frame':
        orgdf = read_csv(True)
        if dataset_name == 'test':
            data = orgdf.iloc[list(index[:test_size])]
        elif dataset_name == 'train':
            data = orgdf.iloc[list(index[test_size:(test_size + train_size)])]
        elif dataset_name == 'valid':
            data = orgdf.iloc[list(index[(test_size + train_size):(2 * test_size + train_size)])]
    elif return_type == 'DataFrame':
        orgdf = pandas.DataFrame(data=datamat, columns=extended_feature_names())
        if dataset_name == 'test':
            data = orgdf.iloc[:test_size]
        elif dataset_name == 'train':
            data = orgdf.iloc[test_size:(test_size + train_size)]
        elif dataset_name == 'valid':
            data = orgdf.iloc[(test_size + train_size):(2 * test_size + train_size)]
    elif return_type == 'DenseDesignMatrixPyTables':
        X, y = datamat[:, ~target_mask], datamat[:, target_mask]
        if dataset_name == 'test':

            data = DenseDesignMatrixPyTables(X=X[:test_size, :], y=y[:test_size, :])
        elif dataset_name == 'train':
            data = DenseDesignMatrixPyTables(X=X[test_size:(test_size + train_size), :], y=y[test_size:(test_size + train_size), :])
        elif dataset_name == 'valid':
            data = DenseDesignMatrixPyTables(X=X[(test_size + train_size):(2 * test_size + train_size), :], y=y[(test_size + train_size):(2 * test_size + train_size), :])
        data.__unipro_description = description
        #data.__unipro_feature_name_mappings = feature_name_mappings
    else:
        raise ValueError
    data.__unipro_info = unipro_info
    return data


def test_index():
    orgcsv = read_csv(True)
    index = load_datamat('test', return_type='index')
    test = load_datamat('test')
    assert((np.abs(np.log(orgcsv.iloc[list(index[:100])]['最新価格']).as_matrix() - test.y[:100].flat) < 0.001).all())


@time
def find_examples(known_values, df):
    example_index = True
    for k, v in known_values.items():
        if k in df:
            example_index = example_index and df[k] == v
    return df[example_index]

if __name__ == "__main__":
    dataset, datamat, info, index = generate_BIN(generate_CSV2())
