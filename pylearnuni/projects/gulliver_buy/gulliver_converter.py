#!/usr/binenv python
# -*- coding: utf-8 -*-

import pandas
import numpy
import re

from pylearnuni.preprocess.converter import Converter
from pylearnuni.utils import column_name_generater as Name


class GulliverConverter(Converter):
    feature_types = ("car", "multi_scaled", "model", "unary", "past_years", "categorical", "inspection", "binary", "color", "transmission")
    log_multi_scaled = True

    def check_consistency(func):
        def inner(cls, column_name, df):
            len_ = len(df)
            dict_ = func(cls, column_name, df)  # 1
            for val in dict_.values():
                assert len_ == len(val), func.__name__
            return dict_
        return inner

    @classmethod
    @check_consistency
    def convert_multi_scaled(cls, column_name, df):
        '''
        convert a varible that takes on values from
        multiple scales (e.g. [10^0, 10^4]) with logarithm
        '''
        if cls.log_multi_scaled:
            column_ = numpy.log(df[column_name])
            column_.loc[df[column_name] == 0] = 0

        return {column_name: (column_).tolist()}

    @classmethod
    @check_consistency
    def convert_model(cls, column_name, df):
        '''convert the year of the release of a car into the age of the car'''
        return {column_name: (2015 - df[column_name]).tolist()}

    @classmethod
    @check_consistency
    def convert_car(cls, column_name, df):
        '''convert an unary categorical variable with one_hot encoding'''
        df[column_name] = df[column_name].astype("category")

        def convert_name(x1, x2):
            return Name.generate_merged_name(x1, x2)
        column_ = pandas.Series(numpy.vectorize(convert_name)(df['メーカーコード'], df['車種コード']))
        column_.name = "車種"
        values = column_.dropna().drop_duplicates()
        return cls.convert_into_one_hot(column_, values)

    @classmethod
    def convert_transmission(cls, column_name, df):
        transmission_code = df[column_name]

        def reduce_code(code):
            if "MT" in code:
                return "MT"
            elif ("AT" in code) or ("CVT" in code):
                return "AT"
            else:
                raise Excepion("transmission code is not specified in the data")

        df[column_name] = pandas.Series(numpy.vectorize(reduce_code)(df[column_name]))

        return cls.convert_categorical(column_name, df)

    @classmethod
    @check_consistency
    def convert_past_years(cls, column_name, df):
        '''convert an unary (e.g., binary or trinary) variables'''
        nenshiki = []
        for i in df.index:
            year = df.ix[i, column_name]
            if year[:1] == "H":
                nenshiki.append(int(year[1:3]) + 1988)
            elif year[:1] == "S":
                nenshiki.append(int(year[1:3]) + 1925)

        priced_year = []
        priced_month = []
        for index, val in df["取得年月日"].iteritems():
            if isinstance(val, numpy.float):  # 欠損値はnp.nan
                assert(numpy.isnan(val))
                year = numpy.nan
                month = numpy.nan
            else:
                year = int(val.split('/')[0])
                month = int(val.split('/')[1])
            priced_year.append(year)
            priced_month.append(month)

        return {'経過年': (numpy.array(priced_year) - numpy.array(nenshiki)).tolist(), '値付け年': priced_year, '値付け月': priced_month}

    @classmethod
    def convert_binary(cls, column_name, df):
        return cls.convert_unary(column_name, df)

    @classmethod
    def convert_color(cls, column_name, df):
        return cls.convert_unary(column_name, df)

    @classmethod
    @check_consistency
    def convert_inspection(cls, column_name, df):
        '''convert the inspection variable'''
        inspection_statuses = ('新車未登録', '国内未登録', '車検整備別', '車検整備無', '車検整備付')
        dict_ = cls.convert_into_one_hot(df[column_name], inspection_statuses)
        patterns = df[column_name].dropna().drop_duplicates()

        d = {}
        for p in patterns:
            if isinstance(p, numpy.float64):
                continue
            m = re.search('\AH*([^/.]*)[/.](.*)\Z', p)
            if m:
                year, month = m.groups()
                d[p] = int(year) * 12 + int(month)
        for p in patterns:
            if isinstance(p, numpy.float64):
                continue
            if p in d:
                d[p] = d[p] - min(d.values())
        for p in inspection_statuses:
            if isinstance(p, numpy.float64):
                continue
            d[p] = -10

        name_ = '車検_残月'
        series_ = pandas.Series(numpy.zeros(df[column_name].shape[0], dtype='int64'), index=df[column_name].index)

        for p in patterns:
            if isinstance(p, numpy.float64):
                continue
            series_.loc[df[column_name] == p] = d[p]
        dict_[name_] = series_.tolist()

        return dict_

    @classmethod
    def generate_car_column_name(cls, column_name, df):
        def convert_name(x1, x2):
            return Name.generate_merged_name(x1, x2)
        column_ = pandas.Series(numpy.vectorize(convert_name)(df['メーカーコード'], df['車種コード']))
        values = column_.dropna().drop_duplicates()
        flag_names = [Name.generate_one_hot_name("車種", val) for val in values]
        return flag_names

    @classmethod
    def generate_multi_scaled_column_name(cls, column_name, df):
        return cls.generate_unary_column_name(column_name, df)

    @classmethod
    def generate_model_column_name(cls, column_name, df):
        return cls.generate_unary_column_name(column_name, df)

    @classmethod
    def generate_past_years_column_name(cls, column_name, df):
        return ["経過年", "値付け年", "値付け月"]

    @classmethod
    def generate_binary_column_name(cls, column_name, df):
        return cls.generate_unary_column_name(column_name, df)

    @classmethod
    def generate_color_column_name(cls, column_name, df):
        return cls.generate_unary_column_name(column_name, df)

    @classmethod
    def generate_inspection_column_name(cls, column_name, df):
        values = ['新車未登録', '国内未登録', '車検整備別', '車検整備無', '車検整備付']
        flag_names = ['車検_残月']

        flag_names.extend([Name.generate_one_hot_name(column_name, val) for val in values])
        return flag_names

    @classmethod
    def generate_transmission_column_name(cls, column_name, df):
        return [Name.generate_one_hot_name(column_name, val) for val in ['AT', 'MT']]
