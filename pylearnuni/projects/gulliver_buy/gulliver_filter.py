#!/usr/binenv python
# -*- coding: utf-8 -*-

import pandas
import numpy
from pylearnuni.preprocess.filter import Filter


class GulliverFilter(Filter):


    @classmethod
    def filter(cls, df):
        #cls.drop(df, ( (df["最新価格"] > 1e8) & (~numpy.isnan(df["最新価格"]) ) ) )
        df["本体価格"] = pandas.to_numeric(df["本体価格"], errors='coerce')
        df.dropna(subset=["本体価格"], inplace=True)
        cls.drop(df, df["本体価格"] > 1e8)
        cls.drop(df, df["本体価格"] < 1e4)
        cls.drop(df, df["走行距離"] > 9e5)
        cls.drop(df, df["排気量"] > 1e4)
        cls.drop(df, df["年式"] < 1900)
        return df
