#!/usr/binenv python
# -*- coding: utf-8 -*-

import pandas
import numpy
import re

from pylearnuni.preprocess.converter import Converter
from pylearnuni.utils import column_name_generater as Name
import calendar


class Baseball_fielding_Converter(Converter):
    feature_types = ("unary", "categorical", "multi_scaled")
    log_multi_scaled = True

    def check_consistency(func):
        def inner(cls, column_name, df):
            len_ = len(df)
            dict_ = func(cls, column_name, df)  # 1
            for val in dict_.values():
                assert len_ == len(val), func.__name__
            return dict_
        return inner

    @classmethod
    @check_consistency
    def convert_multi_scaled(cls, column_name, df):
        '''
        convert a varible that takes on values from
        multiple scales (e.g. [10^0, 10^4]) with logarithm
        '''
        if cls.log_multi_scaled:
            column_ = numpy.log(df[column_name])
            column_.loc[df[column_name] == 0] = 0

        return {column_name: (column_).tolist()}

    @classmethod
    def generate_multi_scaled_column_name(cls, column_name, df):
        return cls.generate_unary_column_name(column_name, df)
