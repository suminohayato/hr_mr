#!/usr/binenv python
# -*- coding: utf-8 -*-

import pandas
import numpy
from pylearnuni.preprocess.filter import Filter


class Gulliver_AA_jp_v1_Filter(Filter):

    @classmethod
    def filter(cls, df, period_from = '1012/12/29 13:49:37', period_to = '3012/12/29 13:49:37'):
        #cls.drop(df, ( (df["最新価格"] > 1e8) & (~numpy.isnan(df["最新価格"]) ) ) )
        #df[""] = pandas.to_numeric(df["RAKU_RYUSATSU_KAKAKU"], errors='coerce')
        df.dropna(subset=["落札流札価格"], inplace=True)
        cls.drop(df, df["開催日"] < period_from)
        cls.drop(df, df["開催日"] > period_to)
        cls.drop(df, df["落札流札価格"] > 1e8)
        cls.drop(df, df["落札流札価格"] < 1e4)
        cls.drop(df, df["走行キロ"] > 9e5)
        cls.drop(df, df["排気量"] > 1e4)
        cls.drop(df, df["年式"] < 1900)
        return df
