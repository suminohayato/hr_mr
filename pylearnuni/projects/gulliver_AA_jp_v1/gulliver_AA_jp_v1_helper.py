#!/usr/binenv python
# -*- coding: utf-8 -*-

import os
import pandas
import numpy
import yaml
import theano
import theano.tensor as T
import h5py

from pylearnuni.helper.helper import Helper as Helper
from pylearnuni.preprocess.preprocess import Preprocess as Preprocess
from gulliver_AA_jp_v1_filter import Gulliver_AA_jp_v1_Filter as Filter
from gulliver_AA_jp_v1_converter import Gulliver_AA_jp_v1_Converter as Converter
from pylearnuni.utils import column_name_generater as Name
from pylearnuni.utils import io_handler as IO
from pylearnuni.postprocess.add_stat_data import Add_stat_data


class Gulliver_AA_jp_v1_Helper(Helper):
    """ Machine Learning Helper class """

    def __init__(self):
        Helper.__init__(self)
        self.target_feature = "落札流札価格"
        self.threshold = 3e6

        self.version = "G1.0"
        self._period_from = '1012/12/29 13:49:37'
        self._period_to = '3012/12/29 13:49:37'

    def get_column_name(self):
        return self._get_column_name(Converter)

    def process_data(self, period_from = '1012/12/29 13:49:37', period_to = '3012/12/29 13:49:37'):
        self._period_from = period_from
        self._period_to = period_to
        if not os.path.isfile(os.path.join(self.output_path, "data.info")):
            self._process_data(self.convert_dataframe)

    def convert_dataframe(self, df, convert=True):
        # 範囲外の値を除外
        df = Filter.filter(df, self._period_from, self._period_to)
        # 行をシャッフル
        df = Preprocess.shuffle_index(df, self.SEED)
        # target_featreが閾値以下の物を除外
        df = Filter.target_value_upperboud(df, self.target_feature, self.threshold)
        # Nanを0で埋める
        df = Preprocess.fill_nan_with_0(df)
        if convert:
            # 機械学習に読める形に変換
            df = Preprocess.convert_dataframe(df, self.features, Converter)
            # Nanを0で埋める
            df = Preprocess.fill_nan_with_0(df)
            # 何をしている？
            Preprocess.validate_dataframe(df)

        return df

    def set_columns(self, yaml_path=None):
        if yaml_path is None:
            yaml_path = os.path.join(self.output_path, "data.info")
        unicodename = yaml.load(open(yaml_path))["column_name"]
        self.column_name = [i.encode("utf-8") for i in unicodename]
        self.column_name_without_target = self.column_name[:]
        self.column_name_without_target.remove(self.target_feature)

    def set_predictor(self, model_path=None):
        if model_path is None:
            model_path = os.path.join(self.output_path, "best_param.pkl")
        model = IO.read_pickle(model_path)
        _input = T.matrix('input')
        prediction = model.fprop(_input)
        predict = theano.function([_input], prediction)
        self.predict = predict

    def dataframe2array(self, df):
        columns_ = list(self.column_name)
        target_index = columns_.index(self.target_feature)
        N_columns = len(columns_)
        target_mask = numpy.asarray([False] * N_columns)
        target_mask[target_index] = True
        df_ = pandas.DataFrame(columns=self.column_name)
        df2 = self.convert_dataframe(df)
        df_ = pandas.concat([df_, df2], copy=False)
        df_ = Preprocess.fill_nan_with_0(df_)
        df_.columns = range(N_columns)
        y_ = df_.iloc[:, target_mask].as_matrix().astype(numpy.float32)
        X_ = df_.iloc[:, ~target_mask].as_matrix().astype(numpy.float32)
        return X_, y_

    def get_h5py(self):
        file = h5py.File(os.path.join(self.output_path, 'data.h5'))
        return file

    def array2dataframe(self, X, y):
        original_column_name = self.column_name

        columns = original_column_name[:]
        columns.remove(self.target_feature)
        df = pandas.DataFrame(X, columns=columns)
        df[self.target_feature] = y
        df = df[original_column_name]
        return df

    def example(self, dict, X=None, y=None):
        if X is None:
            h5 = self.get_h5py()
            X = h5["X"]
            y = h5["y"]
        column_name_X = self.column_name[:]
        column_name_X.remove(self.target_feature)

        dict2 = {}
        for k, v in dict.items():
            if self.features[k]["feature_type"] == 'categorical':
                k = Name.generate_one_hot_name(k, v)
                v = 1
            dict2.update({k: v})
        dict2 = {column_name_X.index(k): v for k, v in dict2.items()}

        batch_size = 10000
        datalen = X.shape[0]
        examples = []
        for b in range(0, datalen, batch_size)[:-1]:
            X_batch = X[b:b + batch_size]
            y_batch = y[b:b + batch_size].flatten()
            index = True
            for k, v in dict2.items():
                index = index & (X_batch[k] == v)
            examples.append(y_batch[index])
        return numpy.exp(numpy.concatenate(examples))

    def old_missing(self, dict, X=None):
        if X is None:
            h5 = self.get_h5py()
            X = h5["X"]
            y = h5["y"]
        example_num = 500
        X = X[0:example_num]
        y = numpy.zeros(example_num)
        df = self.array2dataframe(X, y)
        for k, v in dict.items():
            df[k] = v
        X = df[self.column_name_without_target]
        pred = self.predict(X.astype(numpy.float32)).flatten()
        return numpy.exp(pred)

    def _filter_data(self, df):
        return df
    def add_stat_data(self,csv_path,dataset_csv_path,output_csv_path = None):
        if not output_csv_path:
            output_csv_path = os.path.join(self.output_path,"result_with_stat.csv")

        result_df = pandas.read_csv(csv_path)
        dataset = Filter.filter(pandas.read_csv(dataset_csv_path))
        ist = Add_stat_data(result_df,dataset,target_feature = "落札流札価格",maker = "メーカーコード",car = "車名コード", year = "年式", distance = "走行キロ")
        ist.main().to_csv(output_csv_path,index=False)
