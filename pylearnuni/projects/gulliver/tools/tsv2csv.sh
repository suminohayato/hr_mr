#!/bin/sh

for dir in `find . ! -path . -type d`; do
  echo "processing $dir"
  python tsv2csv.py $dir/$dir.tsv >> history_all.csv
done
