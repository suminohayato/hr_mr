#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import numpy as np

INPUT_CSV_PATH = "/Users/HayatoSumino/Desktop/for_rsync/data/latest_data.csv"
OUTPUT_CSV_PATH = "/Users/HayatoSumino/Desktop/for_rsync/data/latest_data_CS3.csv"

def form(dataset):
    dataset2 = pd.DataFrame()

    link_key = pd.read_csv("s3link.csv")
    for i in link_key.index:
        if type(link_key.ix[i,"s3"]) == str and type(link_key.ix[i,"carsensor_use"]) == str:
            print type(link_key.ix[i,"s3"]),type(link_key.ix[i,"carsensor_use"])
            print link_key.ix[i,"s3"],"->",link_key.ix[i,"carsensor_use"]
            dataset2[link_key.ix[i,"carsensor_use"]] = dataset[link_key.ix[i,"s3"]]
        else:
            dataset2[link_key.ix[i,"carcensor"]] = ""

    dataset2["最終掲載日時"] = [add_slash(dataset2.ix[i,"最終掲載日時"]) for i in dataset2.index]
    dataset2["型式"] = [rm_hf(dataset2.ix[i,"型式"]) for i in dataset2.index]
    dataset2["車検"] = [shaken(dataset2.ix[i,"車検"]) for i in dataset2.index]
    dataset2.to_csv(OUTPUT_CSV_PATH,index = False)


#20160401 -> 2016/04/01
def add_slash(datetime):
    datetime = str(datetime)
    name = "{0}/{1}/{2}".format(datetime[0:4],datetime[4:6],datetime[6:8])
    return name


def shaken(datetime):
    if "." in datetime:
        name = "H{0}/{1}".format(datetime.split(".")[0],datetime.split(".")[1])
        return name
    else:
        return datetime


def rm_hf(katashiki):
    str1 = str(katashiki)
    if "-" in str1:
        return str1.split("-")[1]
    else:
        return str1


if __name__ == "__main__":
    dataset = pd.read_csv(INPUT_CSV_PATH)
    form(dataset)
