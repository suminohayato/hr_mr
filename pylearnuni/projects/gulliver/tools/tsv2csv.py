import pandas
import sys
import csv

reader  = pandas.read_csv(sys.argv[1], header=None, delimiter='\t', dtype=str, chunksize=10000)
for data in reader:
    print data.to_csv(quoting=csv.QUOTE_ALL, header=None, index=None)
