# -*- coding: utf-8 -*-

#how to use
#"sh get_bukken.sh data.csv"
# to get bukken data as file "data.csv"

echo "*downloading..."
aws s3 cp s3://kmj-prd-datafederation/CS_DATA/bukken_gulliver.tar.gz . --profile gulliver
echo "*unzipping..."
tar -zxvf bukken_gulliver.tar.gz
echo "*converting shift-jis to utf-8..."
iconv bukken_gulliver.txt -f SHIFT_JISX0213 -t UTF8 > bukken_utf.csv
echo "*adding column names..."
cat column_names.csv bukken_utf.csv > bukken0.csv
echo "*replacing commas with Zenkaku Commas..."
sed -e "s/,/、/g" bukken0.csv > bukken1.csv
echo "*replacing tabs with commas..."
sed -e "s/\t/,/g" bukken1.csv > $1
echo "*removing tempfiles..."

rm bukken_gulliver.tar.gz
rm bukken_gulliver.txt
rm bukken_utf.csv
rm bukken0.csv
rm bukken1.csv

echo "*saved bukken data to '${1}'"
echo "*end."
