#usage
#python csvdivider.py if output_name N
from random import Random
import argparse
parser = argparse.ArgumentParser(description='Diivide csv file to N files')
parser.add_argument("input_file")
parser.add_argument("output_name")
parser.add_argument("divisor",type=int)
args = parser.parse_args()
r = Random()
with open(args.input_file,'r') as f :
    g = []
    for num in range(args.divisor):
        g.append(open(args.output_name + str(num) + ".csv",'w'))
    for line in f:
        r.choice(g).write(line)
    for h in g:
        h.close()
