#!/bin/sh
#fixed : hogehoge -> ./hogehoge (for not to change path/):14 April 2016
#fixed : combine > shuffle -> combine > divide 10 > shuffle > combine( cannnot shuffle very big file) 14 April 2016
#fetch history from aws

mkdir ./temp
cd ./temp
aws s3 cp s3://kmj-prd-datafederation/CS_DATA/history ./ --profile gulliver --recursive

#inflate zip files
unzip "./*.zip"

#move *.tsv to temp/download
mkdir ./download
mv ./*/*.tsv ./download

# remove zips
rm ./*.zip

# combine tsv files
for file in ./download/*.tsv
do
    echo $file
    python ../csvcombiner.py $file ./combined.tsv
done

#remove downloaded tsv files
rm -rf ./download

#divide tsv files
python csvdivider.py ./combined.tsv ./part 10

rm ./combined.tsv

#shuffle divided file and gzip the output
for num in {0..9}
do
    python ../csvshuffler.py ./part${num}.csv ./shuffled${num}.tsv
    rm ./part${num}.csv
done

#combine shuffled files
for num in {0..9}
do
    python ../csvcombiner.py ./shuffled${num}.tsv ./history_shuffled.tsv
    rm ./shuffled${num}.tsv
done

gzip ./history_shuffled.tsv
mv ./history_shuffled.tsv.gz ../
cd ../
rm -r ./temp
