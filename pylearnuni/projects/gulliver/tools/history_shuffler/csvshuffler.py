import argparse
from random import Random
import linecache
def linecount(f):
    return sum(1 for line in f)

r = Random()
parser = argparse.ArgumentParser(description='Combine csv files to one file')
parser.add_argument('inputfile',metavar='if')
parser.add_argument('outputfile',metavar='of')
args = parser.parse_args()

with open(args.inputfile,'r') as f :
    items = range(1,1+linecount(f))

r.shuffle(items)
with open(args.outputfile,'w') as g :
    for num in items:
        g.write(linecache.getline(args.inputfile,num))
linecache.clearcache()
