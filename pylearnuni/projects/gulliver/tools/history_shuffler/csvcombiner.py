import argparse

parser = argparse.ArgumentParser(description='Combine csv files to one file')
parser.add_argument('inputfile',metavar='if')
parser.add_argument('outputfile',metavar='of')
args = parser.parse_args()

with open(args.outputfile,"a") as f ,open(args.inputfile,"r") as g:
    for line in g:
        f.write(line)
    #check the last line of inputfile reterned?
    g.seek(-1,2)
    if g.read(1) != '\n':
        f.write('\n')

    
