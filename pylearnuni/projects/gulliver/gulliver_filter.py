#!/usr/binenv python
# -*- coding: utf-8 -*-

import pandas
import numpy
from pylearnuni.preprocess.filter import Filter
import datetime as dt

class GulliverFilter(Filter):
    exclusion_codes = []
    inclusion_codes = []

    @classmethod
    def filter(cls, df, period_from = '1012/12/29 13:49:37', period_to = '3012/12/29 13:49:37'):
        #cls.drop(df, ( (df["最新価格"] > 1e8) & (~numpy.isnan(df["最新価格"]) ) ) )
        df["最新価格"] = pandas.to_numeric(df["最新価格"], errors='coerce')
        df.dropna(subset=["最新価格"], inplace=True)
        cls.drop(df, df["最新価格"] > 1e8)
        cls.drop(df, df["最新価格"] < 1e4)
        cls.drop(df, df["走行"] > 9e5)
        cls.drop(df, df["排気量"] > 1e4)
        cls.drop(df, df["年式"] < 1900)
        cls.drop(df, df["福祉車両フラグ"] == 1)
        cls.drop(df, df["車検"] != df["車検"])
        cls.drop(df, df["最終掲載日時"] < period_from)
        cls.drop(df, df["最終掲載日時"] > period_to)
        return df

    @classmethod
    def set_filter_code(cls, company_code):
        # 除外する法人コードを追加する
        cls.exclusion_codes.append(numpy.int64(company_code))

    @classmethod
    def set_filter_codelist(cls, filterpath):
        # 抽出する法人コードのリストをCSVから読み込む
        # フォーマットは/home/public/data/pylearnuni/CSStoreMaster.csvを参照
        assert (os.path.isfile(filterpath))
        label = "掲載先顧客法人C"
        df = IO.read_csv(filterpath)[label]
        cls.inclusion_codes.extend(df.to_dict().values())

    @classmethod
    def _filter_data(cls, df):
        label = "法人コード"

        # self.inclusion_codesが空でなければ、これに含まれていない法人コードは除去する
        if cls.inclusion_codes != []:
            df = df[df[label].isin(cls.inclusion_codes)]

        # self.exclusion_codesに含まれている法人コードは除去する
        return df[-df[label].isin(cls.exclusion_codes)]
