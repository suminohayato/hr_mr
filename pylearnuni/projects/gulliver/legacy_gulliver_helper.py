#!/usr/binenv python
# -*- coding: utf-8 -*-

import os
import sys


class LegacyHelper:
    """ Machine Learning Helper class """

    def __init__(self):

        self.input_csv_path = "./data.csv"
        self.features_yaml_path = "./features.yaml"
        self.train_config_yaml_path = "./train_config.yaml"
        self.output_path = "./output/"
        self.legacy_code_path = "../legacy/"
        self.nvis = 2718
        self.train_size = 200000

    def set_abspath(func):
        def wrapper(self, filename):
            filename = os.path.abspath(os.path.expanduser(filename))
            return func(self, filename)
        return wrapper

    def check_output_path(self):
        if not os.path.isdir(self.output_path):
            os.makedirs(self.output_path)

    @set_abspath
    def set_input_csv_path(self, filename):
        assert(os.path.isfile(filename))
        self.input_csv_path = filename

    @set_abspath
    def set_features_yaml_path(self, filename):
        assert(os.path.isfile(filename))
        self.features_yaml_path = filename

    @set_abspath
    def set_train_config_yaml_path(self, filename):
        assert(os.path.isfile(filename))
        self.train_config_yaml_path = filename

    @set_abspath
    def set_output_path(self, output_path):
        self.output_path = output_path
        self.check_output_path()

    @set_abspath
    def set_legacy_code_path(self, legacy_code_path):
        self.legacy_code_path = legacy_code_path

    def set_nvis(self, nvis):
        assert(nvis)
        self.nvis = nvis

    def set_train_size(self, train_size):
        assert(train_size)
        self.train_size = train_size

    def set_env(self):
        sys.path.append(os.path.abspath(self.legacy_code_path))
        os.environ["GULLIVER_DATA_PATH"] = os.path.abspath(self.output_path)
        os.environ["GULLIVER_PROJECT_PATH"] = os.path.abspath(self.output_path)
        os.environ["LEGACY_INPUT_CSV_PATH"] = os.path.abspath(self.input_csv_path)
        os.environ["LEGACY_FEATURES_YAML_PATH"] = os.path.abspath(self.features_yaml_path)
        os.environ["LEGACY_NVIS"] = str(self.nvis)
        os.environ["LEGACY_TRAIN_SIZE"] = str(self.train_size)

    def process_data(self):
        self.check_output_path()
        self.set_env()
        import gulliver_price_consulting.data.process_data2 as process_data
        dataset, datamat, info, index = process_data.generate_BIN(process_data.generate_CSV2())

    def train(self):
        self.check_output_path()
        self.set_env()
        import gulliver_price_consulting.train.train as train
        train.main(self.output_path, self.train_config_yaml_path)
