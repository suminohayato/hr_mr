#!/usr/binenv python
# -*- coding: utf-8 -*-

import pandas
import numpy
from pylearnuni.preprocess.filter import Filter


class Gulliver_AA_Filter(Filter):

    @classmethod
    def filter(cls, df, period_from = '1012/12/29 13:49:37', period_to = '3012/12/29 13:49:37'):
        #cls.drop(df, ( (df["最新価格"] > 1e8) & (~numpy.isnan(df["最新価格"]) ) ) )
        df["RAKU_RYUSATSU_KAKAKU"] = pandas.to_numeric(df["RAKU_RYUSATSU_KAKAKU"], errors='coerce')
        df["HYOKATEN"] = pandas.to_numeric(df["HYOKATEN"], errors='coerce')
        df.dropna(subset=["RAKU_RYUSATSU_KAKAKU"], inplace=True)
        df["RAKU_RYUSATSU_KBN"].loc[df["RAKU_RYUSATSU_KBN"].astype(str) == "2"] = 0
        df["IMPORT_FLG"].loc[df["IMPORT_FLG"].astype(str) == "8"] = 0
        cls.drop(df, df["AA_KAISAI_DAY"] < period_from)
        cls.drop(df, df["AA_KAISAI_DAY"] > period_to)
        cls.drop(df, df["RAKU_RYUSATSU_KAKAKU"] > 1e8)
        cls.drop(df, df["RAKU_RYUSATSU_KAKAKU"] < 1e4)
        cls.drop(df, df["SOKO_KYORI"] > 9e5)
        cls.drop(df, df["HAIKIRYO"] > 1e4)
        cls.drop(df, df["SEIREKI_NEN"] < 1900)
        return df
