#!/usr/bin/env python
# coding: utf-8


import pandas
import numpy
import os
import sys


def compare(katashiki_file, comp_file, output_dir="./"):
    katashiki_df = pandas.read_csv(katashiki_file,dtype=str)
    comp_df = pandas.read_csv(comp_file,dtype=str)
    katashiki_df.rename(columns={'RMSE':'priceless_RMSE',"台数":"priceless台数"}, inplace=True)
    comp_df.rename(columns={"車種名":"車種",'台数': 'AIPE台数'}, inplace=True)
    _katashiki_df = katashiki_df.ix[:,["型式","車種","メーカー","priceless_RMSE","priceless台数"]]
    _comp_df = comp_df.ix[:,["型式","車種","メーカー","AIPE_RMSE","AIPE台数","査定士_RMSE"]]
    _comp_df["査定士_RMSE"]= _comp_df["査定士_RMSE"].str.replace(",","")
    _comp_df["AIPE_RMSE"]= _comp_df["AIPE_RMSE"].str.replace(",","")
    df = pandas.merge(_katashiki_df,_comp_df,on=['型式',"車種","メーカー"], how='outer')
    df.fillna("-",inplace=True)

    def win_lose(x1, x2):
        if x1 == "-" or x2 == "-":
            return "-"
        else:
            x1 = int(x1)
            x2 = int(x2)
            if x1 > x2:
                return "負"
            elif x1 < x2:
                return "勝"
            elif x1 == x2:
                return "分"
    df["勝敗"] = pandas.Series(numpy.vectorize(win_lose)(df['priceless_RMSE'], df["AIPE_RMSE"]))

    def win_lose10(x0,x1, x2):
        if x1 == "-" or x2 == "-":
            return "-"
        else:
            x1 = int(x1)
            x2 = int(x2)
            if x1 > 10 and x2 > 10:
                return x0
            else:
                return "-"
    df["お互い10台以上"] = pandas.Series(numpy.vectorize(win_lose10)(df["勝敗"],df['priceless台数'], df["AIPE台数"]))

    def rmse_diff(x1, x2):
        if x1 == "-" or x2 == "-":
            return "-"
        else:
            x1 = int(x1)
            x2 = int(x2)
            return str(x1 - x2)
    df["差"] = pandas.Series(numpy.vectorize(rmse_diff)(df['priceless_RMSE'], df["AIPE_RMSE"]))
    df["査定士のと差"] = pandas.Series(numpy.vectorize(rmse_diff)(df['priceless_RMSE'], df["査定士_RMSE"]))

    columns=["メーカー","車種","型式","priceless_RMSE","priceless台数","AIPE_RMSE","AIPE台数","勝敗","お互い10台以上","差","査定士_RMSE","査定士との差"]
    df = df.reindex(columns=columns)
    df.to_csv(os.path.join(output_dir,"compare.csv"),index=False)

    max = 0
    for val in df["priceless台数"]:
        if val == "-":
            continue
        else:
            v = int(val)
            if v > max:
                max = v
    assert(max > 0)
    df2 = pandas.DataFrame({"台数":range(1,max+1)})

    def count(x0):
        return len(_df[ (_df["priceless台数"] > x0) & (_df["AIPE台数"] > x0) ])
    _df = df.ix[df["勝敗"]=="勝",["priceless台数","AIPE台数"]].astype(int)
    df2["勝"] = pandas.Series(numpy.vectorize(count)(df2["台数"]))
    _df = df.ix[df["勝敗"]=="負",["priceless台数","AIPE台数"]].astype(int)
    df2["負"] = pandas.Series(numpy.vectorize(count)(df2["台数"]))

    def win_rate(x1,x2):
        if x1 + x2 == 0:
            return 0.0
        else:
            return 1.0 * x1 / (x1 + x2)
    df2["勝率"] = pandas.Series(numpy.vectorize(win_rate)(df2["勝"],df2["負"]))
    df2.to_csv(os.path.join(output_dir,"compare(win_rate).csv"),index=False)


    df3 = pandas.DataFrame({"結果":["-","勝","分","負"]})
    def count2(x0,column_name):
        return len(df[ df[column_name] == x0 ])
    df3["全型式(pricelessに含まれるもの)"]= pandas.Series(numpy.vectorize(count2)(df3["結果"],"勝敗"))
    df3["お互い10台以上"]= pandas.Series(numpy.vectorize(count2)(df3["結果"],"お互い10台以上"))
    df2.to_csv(os.path.join(output_dir,"compare(summary).csv"),index=False)

if __name__ == "__main__":
    param = sys.argv
    compare(param[1], param[2])