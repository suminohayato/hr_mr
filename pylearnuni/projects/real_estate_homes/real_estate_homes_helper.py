#!/usr/binenv python
# -*- coding: utf-8 -*-

import os
import pandas
import numpy
import re
import sys

from pylearnuni.helper.helper import Helper as Helper
from pylearnuni.preprocess.preprocess import Preprocess as Preprocess
from real_estate_homes_filter import RealestateFilter as Filter
from real_estate_homes_converter import RealestateConverter as Converter


class RealestateHelper(Helper):
    """ Machine Learning Helper class """

    def __init__(self):
        Helper.__init__(self)
        self.target_feature = "価格"
        self.threshold = 3e6

        self.version = "R1.0"

    def get_column_name(self):
        return self._get_column_name(Converter)

    def clean_csv(self, filepath=None):
        if not os.path.isfile(os.path.join(self.output_path, "data.info")):
            self._clean_csv(filepath)

    def generate_yaml(self, new_yaml_path, org_yaml_path, csv_path):
        self._generate_yaml(new_yaml_path, org_yaml_path, csv_path)

    def process_data(self):
        if not os.path.isfile(os.path.join(self.output_path, "data.info")):
            self._process_data(self.convert_dataframe)

    def clean_dataframe(self, df):
        df = df.drop_duplicates(["緯度","経度","価格","築年月","所在階","向き","間取り","登録日","有効期限"])
        ##self.clean_int_columns(df)
        ##self.clean_float_columns(df)
        df = df.reset_index(drop=True)
        self.get_new_column(df,"設備・サービス")
        self.get_new_column(df,"セキュリティ")
        self.get_new_column(df,"その他")
        self.convert_management(df)
        return df

    def _filter_data(self, df):
        return df

    ##intのカラムにint以外があれば除去
    def clean_int_columns(self,df):
        columns=["価格","管理費等","修繕積立金","交通1徒歩","交通2徒歩","交通3徒歩","所在階","階建","総戸数"]
        for i in columns:
            if df[i].dtype != int:
                df[i] = df[i].fillna(0)
                for j in df.index:
                    try:
                        df.ix[j,i] = int(df.ix[j,i])
                    except:
                        df.ix[j,i] = int(0)
            df[i] = df[i].astype(int)

    def clean_float_columns(self,df):
        columns=["専有面積","バルコニー面積","敷地面積"]
        for i in columns:
            if df[i].dtype != float:
                df[i] = df[i].fillna(0)
                for j in df.index:
                    try:
                        df.ix[j,i] = float(df.ix[j,i])
                    except:
                        df.ix[j,i] = int(0)
            df[i] = df[i].astype(float)

    #カラムを増やす
    def get_new_column(self,df,column_name):
        global len
        row_count = len(df.index)
        dataset_option = df[column_name]
        option_lists = []
        for i in range(row_count):
            option = dataset_option.iloc[i]
            if type(option) == str:
                option_list = dataset_option.iloc[i].split("、")
                option_list_len = len(option_list)
                for j in range(option_list_len):
                    if len(option_list[j].decode(sys.stdin.encoding)) > 14:
                        del option_list[j]
                    else:
                        if option_list[j] not in option_lists:
                            option_lists.append("added_"+option_list[j])
        len_lists = len(option_lists)
        #make_feature_types(option_lists)
        for k in range(len_lists):
            df[option_lists[k]] = dataset_option.str.contains(option_lists[k]).fillna(0).astype(int)

    #管理カラムを管理、管理形態、管理組合と管理会社に分割,features.yamlに入れる場合はカラム名に"added_"をつける
    def convert_management(self,df):
        for i in df.index:
            management = df.ix[i,"管理"]
            if type(management) == str:
                a_list = management.split("、")
                for i in range(len(a_list)):
                    b_list=a_list[i].split("：")
                    if "管理員" in b_list:
                        df.ix[i,"管理"] = b_list[1]
                    elif "管理形態" in b_list:
                        df.ix[i,"管理形態"] = b_list[1]
                    elif "管理組合" in b_list:
                        if b_list[1] == "有":
                            df.ix[i,"管理組合"] = 1
                        elif b_list[1] == "無":
                            df.ix[i,"管理組合"] = 0
                    elif "管理会社" in b_list:
                        df.ix[i,"管理会社"] = b_list[1]


    #駐車場カラムを駐車場と駐車場値段に分割
    def convert_parking_lot(self,df):
        for i in df.index:
            parking = df.ix[i,"駐車場"]
            if type(parking) == str and parking.count(" "):
                tmp_list = parking.split(" ")
                df.ix[i,"駐車場"] = tmp_list[0]
                df.ix[i,"駐車場値段"] = tmp_list[1]
                parking_p = tmp_list[1]
                if type(parking_p) == str and parking_p.count("円"):
                    parking_p = parking_p.replace("円","")
                    if parking_p.count("万"):
                        tmp2_list = parking_p.split("万")
                        try:
                            df.ix[i,"駐車場値段"] = int(tmp2_list[0])*10000 + int(tmp2_list[1])
                        except:
                            df.ix[i,"駐車場値段"] = int(tmp2_list[0])*10000
        df["駐車場値段"].replace("駐車場無料","0")


    def generate_csv(self):
        self._generate_csv(self.convert_dataframe)

    def _filter_data(self, df):
        return df

    def convert_dataframe(self, df):
        # 範囲外の値を除外
        df = Filter.filter(df)
        # 行をシャッフル
        df = Preprocess.shuffle_index(df, self.SEED)
        # target_featreが閾値以下の物を除外
        df = Filter.target_value_upperboud(df, self.target_feature, self.threshold)
        # 機械学習に読める形に変換
        df = Preprocess.convert_dataframe(df, self.features, Converter)
        # Nanを0で埋める
        df = Preprocess.fill_nan_with_0(df)
        # 何をしている？
        Preprocess.validate_dataframe(df)

        return df
