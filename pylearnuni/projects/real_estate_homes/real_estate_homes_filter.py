#!/usr/binenv python
# -*- coding: utf-8 -*-

import pandas
import numpy
from pylearnuni.preprocess.filter import Filter


class RealestateFilter(Filter):

    @classmethod
    def filter(cls, df):
        #cls.drop(df, ( (df["最新価格"] > 1e8) & (~numpy.isnan(df["最新価格"]) ) ) )
        df["価格"] = df["価格"].convert_objects(convert_numeric=True, copy=False)
        df.dropna(subset=["価格"], inplace=True)
        cls.drop(df, df["価格"] > 1e8)
        cls.drop(df, df["価格"] < 10)
        return df
