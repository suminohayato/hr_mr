#!/usr/binenv python
# -*- coding: utf-8 -*-

import pandas
import numpy
import re

from pylearnuni.preprocess.converter import Converter
from pylearnuni.utils import column_name_generater as Name


class RealestateConverter(Converter):
    feature_types = ("multi_scaled", "unary", "past_years", "categorical", "binary")

    def check_consistency(func):
        def inner(cls, column_name, df):
            len_ = len(df)
            dict_ = func(cls, column_name, df)  # 1
            for val in dict_.values():
                assert len_ == len(val), func.__name__
            return dict_
        return inner

    @classmethod
    @check_consistency
    def convert_multi_scaled(cls, column_name, df):
        '''
        convert a varible that takes on values from
        multiple scales (e.g. [10^0, 10^4]) with logarithm
        '''
        # debug
        df[column_name] = df[column_name].convert_objects(convert_numeric=True, copy=False)

        column_ = numpy.log(df[column_name])

        column_.loc[df[column_name] == 0] = 0

        return {column_name: (column_).tolist()}

    @classmethod
    @check_consistency
    def convert_past_years(cls, column_name, df):
        '''convert an unary (e.g., binary or trinary) variables'''
        priced_year = [2016 for i in df.index]
        priced_month = [7 for i in df.index]
        built_year=[]
        built_month=[]
        for index, val in df[column_name].iteritems():
            if isinstance(val, numpy.float):  # 欠損値はnp.nan
                assert(numpy.isnan(val))
                year = numpy.nan
                month = numpy.nan
            else:
                year = int(val.split('-')[0])
                month = int(val.split('-')[1])
            built_year.append(year)
            built_month.append(month)

        return {'経過年': (numpy.array(priced_year) - (numpy.array(built_year))).tolist(), '値付け年': priced_year, '値付け月': priced_month}

    @classmethod
    def convert_binary(cls, column_name, df):
        return cls.convert_unary(column_name, df)


    @classmethod
    def generate_multi_scaled_column_name(cls, column_name, df):
        return cls.generate_unary_column_name(column_name, df)


    @classmethod
    def generate_past_years_column_name(cls, column_name, df):
        return ["経過年", "値付け年", "値付け月"]

    @classmethod
    def generate_binary_column_name(cls, column_name, df):
        return cls.generate_unary_column_name(column_name, df)

