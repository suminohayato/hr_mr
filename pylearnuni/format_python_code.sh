#!/bin/bash

autopep8 --in-place -r -a -a --max-line-length 1000 --exclude pylearn2 .
