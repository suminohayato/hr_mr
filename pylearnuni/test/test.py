#!/usr/binenv python
# -*- coding: utf-8 -*-

# [result]
# RMSE: 324608

from projects.gulliver.gulliver_helper import GulliverHelper as Helper

if __name__ == "__main__":

    helper = Helper()

    # 読み込むデータのパスを設定
    helper.load_data("/home/public/data/pylearnuni/test.csv", chunk=True)
    # helper.input_csv_pathに入力データのファイルパスを格納
    # helper.dfにcsvファイルのデータが格納される
    # データに対応するfeatures.yamlのパスを設定
    helper.load_features("../projects/gulliver/features/gulliver_features.yaml")
    # helper.featuresにfeatures.yamlの情報が格納される
    # 出力データをまとめて保存するフォルダのパスを設定
    helper.set_output_path("/home/public/data/pylearnuni/test")
    # helper.output_pathに、出力先のフォルダのパスが格納される
    # helper.datainfo_pathに、data.infoのパスが格納される(helper.output_path配下のdata.info)
    # データをpylearn2が読み込める形に変形
    helper.process_data()
    # data.infoがなければ前処理を行う。data.infoがあれば、前処理が終わっているものとみなし、変換を行わない。

    # 変形したデータをデータセットに分けて保存
    helper.generate_dataset()

    helper.generate_csv()

    # 読み込む学習モデルのyamlのパスを設定
    helper.set_train_config_path("../train_config/test.yaml")

    # 学習開始
    helper.train()
