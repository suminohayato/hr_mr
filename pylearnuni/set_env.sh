#!/bin/bash

# スクリプトがあるディレクトリの絶対パスを取得
DIRNAME=`dirname ${BASH_SOURCE[0]}`
CURRENT_DIR=$( cd ${DIRNAME} && pwd )

# 環境変数PYTHONPATHにpylearnuniとpylearn2のパスを追加
export PYTHONPATH=${CURRENT_DIR}:${CURRENT_DIR}/pylearn2:${PYTHONPATH}

# GPUを使う為の設定
export THEANO_FLAGS="mode=FAST_RUN,device=gpu,floatX=float32,allow_gc=False"
export CUDA_HOME=/usr/local/cuda-6.5
export LD_LIBRARY_PATH=${CUDA_HOME}/lib64
export PATH=${CUDA_HOME}/bin:${PATH}
