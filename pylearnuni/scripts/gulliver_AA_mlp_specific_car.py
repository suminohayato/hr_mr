#!/usr/binenv python
# -*- coding: utf-8 -*-

import sys
#sys.path.append("./project/gulliver_syuppin")
from projects.gulliver_AA.gulliver_AA_helper import Gulliver_AA_Helper as Helper
#from projects.gulliver.gulliver_helper import GulliverHelper as Helper
import pandas as pd

if __name__ == "__main__":
    # 車種指定csvのあるディレクトリ
    dir_path = "/media/data1/pylearnuni_dataset/gulliver_price_consulting/AAD/encoded/shashu/"
    # features.yamlは手動で必要ないカラムを削除した
    features_path = "./gulliver_AA_features_specific_car.yaml"
    # 指定した車種のみ
    car_type = "プリウス"

    # 余計なcolumnを削除
    df = pd.read_csv(dir_path + car_type + ".csv")
    df = df.drop("MAKER_NAME", axis = 1)
    df = df.drop("SHASHU_NAME", axis = 1)
    df = df.drop("MAKER_CD", axis = 1)
    df = df.drop("MAKER_CD2", axis = 1)
    df = df.drop("車種コード1", axis = 1)
    df = df.drop("車種コード2", axis = 1)
    df = df.drop("SYASHU_CD", axis = 1)
    df.to_csv(dir_path + car_type + "_arranged.csv")


    # 学習フェーズ
    helper = Helper()

    # 読み込むCSVデータのパスを設定
    helper.load_data(dir_path + car_type + "_arranged.csv", chunk_size=100000, chunk=True)
    # データに対応するfeatures.yamlのパスを設定
    helper.load_features(features_path)
    # 出力データを保存するフォルダへのパス（ここにすべて出力される）
    # TODO: 車種をアルファベットで表現してcar_type = "prius"とできるようにする
    helper.set_output_path("/media/data1/code/sakaguchi/specific_car_result/" + "prius")

    helper.generate_csv()
    # データをpylearn2が読み込める形に変形
    helper.process_data()

    helper.generate_dataset()

    # 読み込むmlp.yamlのパス
    helper.set_train_config_path("../train_config/multilayer_perceptron.yaml")

    # 学習開始
    helper.train()
