#!/usr/bin/env python
#  -*- coding: utf-8 -*-

from pylearnuni.train import kerastrain
import h5py
import numpy
from projects.gulliver_AA.gulliver_AA_helper import Gulliver_AA_Helper as Helper
import os
import sys

import keras
from keras.models import Sequential
from keras.layers.core import Dense, Activation
from keras.layers import Dropout
from keras.layers.normalization import BatchNormalization
import itertools
from pylearnuni.preprocess.process_csv import shuffle_csv
import pylearnuni.train.train_handler as train_handler

def main():
    input_csv_path="/media/data1/pylearnuni_dataset/gulliver_price_consulting/AAD/encoded/Q_AAD情報_utf8_20160416_20160714_withcd.csv"
    features_yaml_path="../../../projects/gulliver_AA/gulliver_AA_features_with_katashiki.yaml"
    output_path="./AA_dropout_filtered_katashiki_search"

    if not os.path.exists(os.path.join(output_path,"data.info")):
        shuffle_csv(input_csv_path)
        input_csv_path="./shuffled_data.csv"
        helper = Helper()
        helper.load_data(input_csv_path, chunk_size=10000, chunk=True)
        helper.load_features(features_yaml_path)
        helper.set_output_path(output_path)
        helper.generate_csv()
        helper.process_data()
        helper.generate_dataset(test_size=50000,valid_size=50000)

    for (dropoutrate,layernum,layersize) in itertools.product([0.1,0.2],[2,3,4],[300,600,900,1200]):
        memo="""
        /media/data1/pylearnuni_dataset/gulliver_price_consulting/AAD/encoded/Q_AAD情報_utf8_20160416_20160714_withcd.csv
        300万フィルタ
        型式有り
        dropout in the first layer
        layernum:{}
        layersize:{}
        dropoutrate:{}
        """.format(layernum,layersize,dropoutrate)
        kerasoutputpath=os.path.join(output_path,"hlayernum{}_size{}_dropoutrate{}".format(layernum,layersize,dropoutrate))
        model=learn(hdf5_path=os.path.join(output_path, "data.h5"), keras_output_path=kerasoutputpath, modelmaker=lambda inputdim:makemodel(inputdim=inputdim, hiddenlayernum=layernum, layersize=layersize,dropoutrate=dropoutrate), memo=memo)
        f=h5py.File(os.path.join(output_path,"data.h5"))
        predict=model.predict(f["test"]["X"])

        train_handler.save_result_csv(predict=predict,csv_path=output_path,target_feature="RAKU_RYUSATSU_KAKAKU",dataset_path=os.path.join(output_path,"./data.h5"),output_path=kerasoutputpath)
        train_handler.print_result(actual=numpy.exp(f["test"]["y"].value.flatten()),predict=predict.flatten())
        train_handler.calc_each_katashiki_rmse(kerasoutputpath)


def makemodel(inputdim,hiddenlayernum,layersize,dropoutrate):
    model = Sequential()
    firstlayerflag=True

    for _ in range(hiddenlayernum):
        if firstlayerflag:
            model.add(Dropout(dropoutrate,input_shape=(inputdim,)))
            model.add(Dense(output_dim=layersize))
            firstlayerflag=False
        else:
            model.add(Dense(output_dim=layersize))
        model.add(Activation('relu'))

    if firstlayerflag:
        model.add(Dropout(dropoutrate,input_shape=(inputdim,)))
        model.add(Dense(output_dim=1))
        firstlayerflag=False
    else:
        model.add(Dense(output_dim=1))
    model.add(Activation('linear'))
    model.compile(loss='mse', optimizer='rmsprop')
    return model


def learn(hdf5_path, keras_output_path, modelmaker, memo):
    #***hdf5が準備できたらkerasで学習***
    #データ
    f=h5py.File(hdf5_path)
    train_set=(f["X"],numpy.exp(f["y"].value.flatten()))
    valid_set=(f["valid"]["X"].value,numpy.exp(f["valid"]["y"].value.flatten()))
    test_set=(f["test"]["X"].value,numpy.exp(f["test"]["y"].value.flatten()))

    model=modelmaker(train_set[0].shape[1])

    #学習オプションのリスト設定
    callbacks=[]
    #earlystopping:patience回改善されなかったらやめるオプション
    earlystopping=keras.callbacks.EarlyStopping(monitor='val_loss', patience=20, verbose=0, mode='auto')
    callbacks.append(earlystopping)
    checkpoint=keras.callbacks.ModelCheckpoint(os.path.join(keras_output_path,"best_param.hdf5"), monitor='val_loss', verbose=1, save_best_only=True, mode='auto')
    callbacks.append(checkpoint)

    #学習して、一連のファイルをフォルダに保存
    kerastrain.keras_train2(
        output_path=keras_output_path,
        train_set=train_set,valid_set=valid_set,model=model,callbacks=callbacks,
        nb_epoch=300,
        batch_size=100,
        load_chunk_size=100000,#⑤一度にhdf5からメモリに読み込む大きさ（batch_sizeの倍数であること！）
        save_name="last_param.hdf5",
        memo=memo
    )
    #⑥RMSEを求めて、出力フォルダに出力する関数を「kerastrain.evaluate」に分離しました。
    kerastrain.evaluate(keras_output_path,test_set)
    return model

if __name__=="__main__":
    main()
