#!/usr/bin/env python
#  -*- coding: utf-8 -*-

from pylearnuni.train import kerastrain
import h5py
import numpy
from projects.gulliver_AA.gulliver_AA_helper import Gulliver_AA_Helper as Helper
import os
import sys

import keras
from keras.models import Sequential
from keras.layers.core import Dense, Activation
from keras.layers.normalization import BatchNormalization

from pylearnuni.preprocess.process_csv import shuffle_csv
import pylearnuni.train.train_handler as train_handler


#大きなデータの学習に対応するために、kerasの使い方に変更を加えました。
#①〜⑦の変更点に目を通してください。
def main():
    input_csv_path="/media/data1/pylearnuni_dataset/gulliver_price_consulting/AAD/encoded/Q_AAD情報_utf8_20160416_20160714_withcd.csv"
    features_yaml_path="../../../projects/gulliver_AA/gulliver_AA_features_with_katashiki.yaml"
    output_path="./with_katashiki"

    memo="""
    /media/data1/pylearnuni_dataset/gulliver_price_consulting/AAD/encoded/Q_AAD情報_utf8_20160416_20160714_withcd.csv
    ３００万フィルタ
    型式無し
    tesuto
    """

    ###hdf5出力まで
    if not os.path.exists(os.path.join(output_path,"data.info")):
        shuffle_csv(input_csv_path)
        input_csv_path="./shuffled_data.csv"
        helper = Helper()
        helper.load_data(input_csv_path, chunk_size=10000, chunk=True)
        helper.load_features(features_yaml_path)
        helper.set_output_path(output_path)
        #①結果のcsvを出力するための準備として、helper.generate_csvを呼ぶ
        helper.generate_csv()
        helper.process_data()
        #②ここで、testとvalidの大きさを決めるように変更
        helper.generate_dataset(test_size=50000,valid_size=50000)


    ###hdf5が準備できたらkerasで学習
    #データ
    f=h5py.File(os.path.join(output_path,"data.h5"))
    #③データの渡しかたについて
    #trainのXは、一気に読み込むとメモリに載り切らないので、f["X"]のまま渡すこと。
    #test,validは、一度で読み込んでしまったほうが速いので、f["test"]["X"].valueでnumpy arrayの形で渡す。
    train_set=(f["X"],numpy.exp(f["y"].value.flatten()))
    valid_set=(f["valid"]["X"].value,numpy.exp(f["valid"]["y"].value.flatten()))
    test_set=(f["test"]["X"].value,numpy.exp(f["test"]["y"].value.flatten()))

    #モデル作成
    model = Sequential()
    model.add(Dense(input_dim=train_set[0].shape[1],output_dim=384))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=384))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=384))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=384))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=1))
    model.add(Activation('linear'))
    model.compile(loss='mse', optimizer='rmsprop')

    #学習オプションのリスト設定
    callbacks=[]
    #earlystopping:patience回改善されなかったらやめるオプション
    earlystopping=keras.callbacks.EarlyStopping(monitor='val_loss', patience=20, verbose=0, mode='auto')
    callbacks.append(earlystopping)

    #学習して、一連のファイルをフォルダに保存
    kerasoutputpath=os.path.join(output_path,"./keras")
    #④大きいデータ対応版の名前は「keras_train2」にしました。
    #引数の変化
    #削除：validation_split,test_set
    #追加：valid_set（validationに使うデータ（X,y）のtuple）
    #追加：load_chunk_size
    kerastrain.keras_train2(
        output_path=kerasoutputpath,
        train_set=train_set,valid_set=valid_set,model=model,callbacks=callbacks,
        nb_epoch=300,
        batch_size=100,
        load_chunk_size=100000,#⑤一度にhdf5からメモリに読み込む大きさ（batch_sizeの倍数であること！）
        memo=memo
    )
    #⑥RMSEを求めて、出力フォルダに出力する関数を「kerastrain.evaluate」に分離しました。
    kerastrain.evaluate(kerasoutputpath,test_set)

    #result_csv排出
    #⑦結果のcsvを吐き出す部分の関数名等が変わりました。
    f=h5py.File(os.path.join(output_path,"data.h5"))
    predict=model.predict(f["test"]["X"])
    train_handler.save_result_csv(predict=predict,csv_path=output_path,target_feature="RAKU_RYUSATSU_KAKAKU",dataset_path=os.path.join(output_path,"./data.h5"),output_path=kerasoutputpath)
    train_handler.print_result(actual=numpy.exp(f["test"]["y"].value.flatten()),predict=predict.flatten())
    train_handler.calc_each_katashiki_rmse(kerasoutputpath)


if __name__=="__main__":
    main()
