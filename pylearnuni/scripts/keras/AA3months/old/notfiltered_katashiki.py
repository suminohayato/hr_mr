#!/usr/bin/env python
#  -*- coding: utf-8 -*-

from pylearnuni.train import kerastrain
import h5py
import numpy
from projects.gulliver_AA.gulliver_AA_helper import Gulliver_AA_Helper as Helper
import os
import sys

import keras
from keras.models import Sequential
from keras.layers.core import Dense, Activation
from keras.layers.normalization import BatchNormalization

from pylearnuni.preprocess.process_csv import shuffle_csv

def main():
    input_csv_path="/media/data1/pylearnuni_dataset/gulliver_price_consulting/AAD/encoded/Q_AAD情報_utf8_20160416_20160714_withcd.csv"
    features_yaml_path="../../../projects/gulliver_AA/gulliver_AA_features_with_katashiki.yaml"
    output_path="./AA_notfiltered_katashiki"

    memo="""
    /media/data1/pylearnuni_dataset/gulliver_price_consulting/AAD/encoded/Q_AAD情報_utf8_20160416_20160714_withcd.csv
    一億フィルタ
    型式有り
    """

    if not os.path.exists(os.path.join(output_path,"data.info")):
        shuffle_csv(input_csv_path)
        input_csv_path="./shuffled_data.csv"
        helper = Helper()
        helper.threshold=1e8
        helper.load_data(input_csv_path, chunk_size=10000, chunk=True)
        helper.load_features(features_yaml_path)
        helper.set_output_path(output_path)
        helper.process_data()
        helper.generate_dataset(test_size=50000,valid_size=1000)

    #***hdf5が準備できたらkerasで学習***
    #データ
    f=h5py.File(os.path.join(output_path,"data.h5"))
    train_set=(f["X"],numpy.exp(f["y"].value.flatten()))
    test_set=(f["test"]["X"],numpy.exp(f["test"]["y"].value.flatten()))

    #モデル作成
    model = Sequential()
    model.add(Dense(input_dim=train_set[0].shape[1],output_dim=384))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=384))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=384))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=384))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=1))
    model.add(Activation('linear'))
    model.compile(loss='mse', optimizer='rmsprop')

    #学習オプションのリスト設定
    callbacks=[]
    #earlystopping:patience回改善されなかったらやめるオプション
    earlystopping=keras.callbacks.EarlyStopping(monitor='val_loss', patience=20, verbose=0, mode='auto')
    callbacks.append(earlystopping)

    #学習して、一連のファイルをフォルダに保存
    kerastrain.keras_train(
        output_path=os.path.join(output_path,"./keras"),
        train_set=train_set,test_set=test_set,model=model,callbacks=callbacks,
        validation_split=0.1, #validation setの割合
        nb_epoch=500,         #epoch数
        batch_size=100,
        memo=memo
    )

if __name__=="__main__":
    main()
