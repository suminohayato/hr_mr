#!/usr/bin/env python
#  -*- coding: utf-8 -*-

from pylearnuni.train import kerastrain
import h5py
import numpy
from projects.gulliver_AA.gulliver_AA_helper import Gulliver_AA_Helper as Helper
import os
import sys

import keras
from keras.models import Sequential
from keras.layers.core import Dense, Activation
from keras.layers.normalization import BatchNormalization
import itertools

from pylearnuni.preprocess.process_csv import shuffle_csv
import pandas as pd


def main():
        # 車種指定csvのあるディレクトリ
    dir_path = "/media/data1/pylearnuni_dataset/gulliver_price_consulting/AAD/encoded/shashu/"
    # features.yamlは手動で必要ないカラムを削除した
    features_path = "../../gulliver_AA_features_specific_car.yaml"
    # 指定した車種のみ
    car_type = "プリウス"

    # 余計なcolumnを削除
    df = pd.read_csv(dir_path + car_type + ".csv")
    df = df.drop("MAKER_NAME", axis = 1)
    df = df.drop("SHASHU_NAME", axis = 1)
    df = df.drop("MAKER_CD", axis = 1)
    df = df.drop("MAKER_CD2", axis = 1)
    df = df.drop("車種コード1", axis = 1)
    df = df.drop("車種コード2", axis = 1)
    df = df.drop("SYASHU_CD", axis = 1)
    df.to_csv(dir_path + car_type + "_arranged.csv")


    # 学習フェーズ
    helper = Helper()

    # 読み込むCSVデータのパスを設定
    helper.threshold=1e8
    shuffle_csv(dir_path + car_type + "_arranged.csv")
    helper.load_data("./shuffled_data.csv", chunk_size=100000, chunk=True)
    # データに対応するfeatures.yamlのパスを設定
    helper.load_features(features_path)
    # 出力データを保存するフォルダへのパス（ここにすべて出力される）
    # TODO: 車種をアルファベットで表現してcar_type = "prius"とできるようにする

    output_path="./AA_priussearch"

    helper.set_output_path(output_path)

    #helper.generate_csv()
    # データをpylearn2が読み込める形に変形
    helper.process_data()
    helper.generate_dataset(test_size=5000,valid_size=1000)


    for (layernum,layersize) in itertools.product([2,3,4],[300,600,900,1200]):
        memo="""
        /media/data1/pylearnuni_dataset/gulliver_price_consulting/AAD/encoded/Q_AAD情報_utf8_20160416_20160714_withcd.csv
        プリウス
        型式無し
        layernum:{}
        layersize:{}
        """.format(layernum,layersize)
        kerasoutputpath=os.path.join(output_path,"hlayernum{}_size{}".format(layernum,layersize))
        learn(hdf5_path=os.path.join(output_path, "data.h5"), keras_output_path=kerasoutputpath,output_path=output_path, modelmaker=lambda inputdim:makemodel(inputdim=inputdim, hiddenlayernum=layernum, layersize=layersize), memo=memo)

def makemodel(inputdim,hiddenlayernum,layersize):
    model = Sequential()
    firstlayerflag=True

    for _ in range(hiddenlayernum):
        if firstlayerflag:
            model.add(Dense(input_dim=inputdim,output_dim=layersize))
            firstlayerflag=False
        else:
            model.add(Dense(output_dim=layersize))
        model.add(Activation('relu'))

    if firstlayerflag:
        model.add(Dense(input_dim=inputdim,output_dim=1))
        firstlayerflag=False
    else:
        model.add(Dense(output_dim=1))
    model.add(Activation('linear'))
    model.compile(loss='mse', optimizer='rmsprop')
    return model


def learn(hdf5_path, keras_output_path,output_path, modelmaker, memo):
    #***hdf5が準備できたらkerasで学習***
    #データ
    f=h5py.File(hdf5_path)
    train_set=(f["X"],numpy.exp(f["y"].value.flatten()))
    valid_set=(f["valid"]["X"].value,numpy.exp(f["valid"]["y"].value.flatten()))
    test_set=(f["test"]["X"].value,numpy.exp(f["test"]["y"].value.flatten()))

    model=modelmaker(train_set[0].shape[1])

    #学習オプションのリスト設定
    callbacks=[]
    #earlystopping:patience回改善されなかったらやめるオプション
    earlystopping=keras.callbacks.EarlyStopping(monitor='val_loss', patience=20, verbose=0, mode='auto')
    callbacks.append(earlystopping)
    checkpoint=keras.callbacks.ModelCheckpoint(os.path.join(keras_output_path,"best_param.hdf5"), monitor='val_loss', verbose=1, save_best_only=True, mode='auto')
    callbacks.append(checkpoint)

    #学習して、一連のファイルをフォルダに保存
    kerastrain.keras_train2(
        output_path=keras_output_path,
        train_set=train_set,valid_set=valid_set,model=model,callbacks=callbacks,
        nb_epoch=700,
        batch_size=100,
        load_chunk_size=100000,#⑤一度にhdf5からメモリに読み込む大きさ（batch_sizeの倍数であること！）
        save_name="last_param.hdf5",
        memo=memo
    )
    #⑥RMSEを求めて、出力フォルダに出力する関数を「kerastrain.evaluate」に分離しました。
    kerastrain.evaluate2(keras_output_path=keras_output_path,test_set=test_set,parent_output_path=output_path,target_feature="RAKU_RYUSATSU_KAKAKU", weight_file_name="best_param.hdf5")

if __name__=="__main__":
    main()
