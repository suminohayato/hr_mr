#!/usr/bin/env python
#  -*- coding: utf-8 -*-

from pylearnuni.train import kerastrain
import h5py
import numpy
from projects.real_estate_homes.real_estate_homes_helper import RealestateHelper as Helper
import os
import sys

import keras
from keras.models import Sequential
from keras.layers.core import Dense, Activation
from keras.layers.normalization import BatchNormalization


def main():
    input_csv_path="/media/data1/real_estate/homes/detailHOMESkaitori.csv"
    output_path="/media/data1/code/sakaguchi/output/real_estate_homes"
    features_org_yaml_path = "/media/data1/code/sakaguchi/pylearnuni/projects/real_estate_homes/real_estate_homes_features.yaml"
    features_after_yaml_path= os.path.join(output_path,"real_estate_homes_features_after.yaml")

    print "preprocessing..."
    if not os.path.exists(os.path.join(output_path,"data.info")):
        helper = Helper()
        helper.set_output_path(output_path)
        # cleaned.csvがあったらclean_csvをしない
        if not os.path.exists(os.path.join(output_path, "cleaned.csv")):
            print "cleaning..."
            helper.clean_csv(input_csv_path)
        print "generating yaml..."
        helper.generate_yaml(features_after_yaml_path, features_org_yaml_path, os.path.join(output_path, "cleaned.csv"))
        helper.load_features(features_after_yaml_path)
        helper.load_data(filepath = os.path.join(output_path,"cleaned.csv"), chunk_size=10000, chunk=True)
        print "processing..."
        helper.process_data()
        helper.generate_dataset(test_size=3000,valid_size=1000)
    print "end"
    #***hdf5が準備できたらkerasで学習***
    #データ
    f=h5py.File(os.path.join(output_path,"data.h5"))
    train_set=(f["X"],numpy.exp(f["y"].value.flatten()))
    test_set=(f["test"]["X"],numpy.exp(f["test"]["y"].value.flatten()))

    #モデル作成
    model = Sequential()
    model.add(Dense(input_dim=train_set[0].shape[1],output_dim=384))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=384))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=384))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=384))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=1))
    model.add(Activation('linear'))
    model.compile(loss='mse', optimizer='rmsprop')

    #学習オプションのリスト設定
    callbacks=[]
    #earlystopping:patience回改善されなかったらやめるオプション
    earlystopping=keras.callbacks.EarlyStopping(monitor='val_loss', patience=20, verbose=0, mode='auto')
    callbacks.append(earlystopping)

    #学習して、一連のファイルをフォルダに保存
    kerastrain.keras_train2(
        output_path=os.path.join(output_path,"./keras"),
        train_set=train_set,test_set=test_set,model=model,callbacks=callbacks,
        validation_split=0.1, #validation setの割合
        nb_epoch=500,         #epoch数
        batch_size=100,
    )

if __name__=="__main__":
    main()

