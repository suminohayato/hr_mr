#!/usr/bin/env python
#  -*- coding: utf-8 -*-

from pylearnuni.train import kerastrain
import h5py
import numpy
from projects.real_estate_fudosanjapan.real_estate_fudosanjapan_helper import RealestateHelper as Helper
import os
import sys

import keras
from keras.models import Sequential
from keras.layers.core import Dense, Activation
from keras.layers.normalization import BatchNormalization

from pylearnuni.preprocess.process_csv import shuffle_csv
import pylearnuni.train.train_handler as train_handler


def main():
    input_csv_path="/media/data1/real_estate/fudosan_japan/fudosan_japan_160723_new.csv"
    output_path="/media/data1/code/maemura/output/real_estate/real_estate_fudosanjapan"
    features_org_yaml_path = "/media/workspace/code/maemura/pylearnuni/projects/real_estate_fudosanjapan/real_estate_fudosanjapan_features.yaml"
    features_after_yaml_path=output_path + "/real_estate_fudosanjapan_features_after.yaml"

    if not os.path.exists(os.path.join(output_path,"data.info")):
        shuffle_csv(input_csv_path)
        input_csv_path="./shuffled_data.csv"
        helper = Helper()
        helper.set_output_path(output_path)
        helper.clean_csv(input_csv_path)
        helper.generate_yaml(features_after_yaml_path, features_org_yaml_path, os.path.join(output_path, "cleaned.csv"))
        helper.load_features(features_after_yaml_path)
        helper.load_data(filepath=None, chunk_size=100000, chunk=True)
        helper.generate_csv()
        helper.process_data()
        helper.generate_dataset(test_size=3000,valid_size=1000)
    print "end"
    #***hdf5が準備できたらkerasで学習***
    #データ
    f=h5py.File(os.path.join(output_path,"data.h5"))
    train_set=(f["X"],numpy.exp(f["y"].value.flatten()))#読み込ませすぎないように、Xはhdf5形式にする
    valid_set=(f["test"]["X"].value,numpy.exp(f["test"]["y"].value.flatten()))#これらは、.valueをとって、numpy arrayにしてしまうこと。
    test_set=(f["test"]["X"].value,numpy.exp(f["test"]["y"].value.flatten())) #（そのほうが速い）

    #モデル作成
    model = Sequential()
    model.add(Dense(input_dim=train_set[0].shape[1],output_dim=384))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=384))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=384))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=384))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=1))
    model.add(Activation('linear'))
    model.compile(loss='mse', optimizer='rmsprop')

    #学習オプションのリスト設定
    callbacks=[]
    #earlystopping:patience回改善されなかったらやめるオプション
    earlystopping=keras.callbacks.EarlyStopping(monitor='val_loss', patience=20, verbose=0, mode='auto')
    callbacks.append(earlystopping)

    #学習して、一連のファイルをフォルダに保存
    kerasoutputpath=os.path.join(output_path,"./keras")
    kerastrain.keras_train2(
        output_path=kerasoutputpath,
        train_set=train_set,valid_set=valid_set,model=model,callbacks=callbacks,
        nb_epoch=300,         #epoch数
        batch_size=100,
        load_chunk_size=1000,
    )
    kerastrain.evaluate(kerasoutputpath,test_set)

    #result_csv排出
    f=h5py.File(os.path.join(output_path,"data.h5"))
    predict=model.predict(f["test"]["X"])
    train_handler.save_result_csv(predict=predict,csv_path=output_path,target_feature="価格",dataset_path=os.path.join(output_path,"./data.h5"),output_path=kerasoutputpath)
    train_handler.print_result(actual=numpy.exp(f["test"]["y"].value.flatten()),predict=predict.flatten())

if __name__=="__main__":
    main()

