#!/usr/bin/env python
#  -*- coding: utf-8 -*-

from pylearnuni.train import kerastrain
import h5py
import numpy
from projects.gulliver_AA.gulliver_AA_helper import Gulliver_AA_Helper as Helper
import os
import sys

import keras
from keras.models import Sequential
from keras.layers.core import Dense, Activation
from keras.layers.normalization import BatchNormalization

from pylearnuni.preprocess.process_csv import shuffle_csv
import pylearnuni.train.train_handler as train_handler


#大きなデータの学習に対応するために、kerasの使い方に変更を加えました。
#①〜⑧のポイントをチェックしてください。

#8月6日アップデート分は追記分は●マークをつけました。　●④、●⑦、●⑧を読んで下さい。
#この●④、●⑦、●⑧が、先日chatworkで送った変更の解説の①、②、③にあたります。（わかりにくくてすいません）

def main():
    input_csv_path="/media/data1/pylearnuni_dataset/gulliver_price_consulting/AAD/encoded/Q_AAD情報_utf8_20160416_20160714_withcd.csv"
    features_yaml_path="../../../projects/gulliver_AA/gulliver_AA_features.yaml"
    output_path="./bigdatatest"

    memo="""
    /media/data1/pylearnuni_dataset/gulliver_price_consulting/AAD/encoded/Q_AAD情報_utf8_20160416_20160714_withcd.csv
    ３００万フィルタ
    型式無し
    """

    ###hdf5出力まで
    if not os.path.exists(os.path.join(output_path,"data.info")):
        shuffle_csv(input_csv_path)
        input_csv_path="./shuffled_data.csv"
        helper = Helper()
        helper.load_data(input_csv_path, chunk_size=100000, chunk=True)
        helper.load_features(features_yaml_path)
        helper.set_output_path(output_path)
        #①結果のcsvを出力するための準備として、helper.generate_csvを呼ぶ
        helper.generate_csv()
        helper.process_data()
        #②ここで、testとvalidの大きさを決める
        helper.generate_dataset(test_size=50000,valid_size=50000)


    ###hdf5が準備できたらkerasで学習
    #データ
    f=h5py.File(os.path.join(output_path,"data.h5"))
    #③データの渡しかたについて
    #trainのXは、一気に読み込むとメモリに載り切らないので、f["X"]のまま渡すこと。
    #test,validは、一度で読み込んでしまったほうが速いので、f["test"]["X"].valueでnumpy arrayの形で渡す。
    train_set=(f["X"],numpy.exp(f["y"].value.flatten()))
    valid_set=(f["valid"]["X"].value,numpy.exp(f["valid"]["y"].value.flatten()))
    test_set=(f["test"]["X"].value,numpy.exp(f["test"]["y"].value.flatten()))

    #モデル作成
    model = Sequential()
    model.add(Dense(input_dim=train_set[0].shape[1],output_dim=384))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=384))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=384))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=384))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=1))
    model.add(Activation('linear'))
    model.compile(loss='mse', optimizer='rmsprop')


    keras_output_path=os.path.join(output_path,"./keras")
    #学習オプションのリスト設定
    callbacks=[]
    #earlystopping:patience回改善されなかったらやめるオプション
    earlystopping=keras.callbacks.EarlyStopping(monitor='val_loss', patience=20, verbose=0, mode='auto')
    callbacks.append(earlystopping)
    #modelcheckpoint:●④最も良い途中経過を"best_param.hdf5"という名前で保存するオプション
    checkpoint=keras.callbacks.ModelCheckpoint(os.path.join(keras_output_path,"best_param.hdf5"), monitor='val_loss', verbose=1, save_best_only=True, mode='auto')
    callbacks.append(checkpoint)

    #学習して、一連のファイルをフォルダに保存
    #⑤大きいデータ対応版の名前は「keras_train2」にしました。
    kerastrain.keras_train2(
        output_path=keras_output_path,
        train_set=train_set,valid_set=valid_set,model=model,callbacks=callbacks,
        nb_epoch=3,
        batch_size=100,
        load_chunk_size=100000,#⑥一度にhdf5からメモリに読み込む大きさ（batch_sizeの倍数であること！）
        save_name="last_param.hdf5",#●⑦最終結果は"last_param.hdf5"の名前で保存する
        memo=memo,
    )
    #●⑧RMSEを求めて、result.csv等を出力する関数をkerastrain.evaluate"2"にした。
    #save_result_csvとprint_resultをこの中に組み込んだ。
    #calc_each_katashikiは、gulliverAA専用なので、外に出したまま。
    kerastrain.evaluate2(keras_output_path=keras_output_path,test_set=test_set,parent_output_path=output_path,target_feature="RAKU_RYUSATSU_KAKAKU", weight_file_name="best_param.hdf5")

    train_handler.calc_each_katashiki_rmse(keras_output_path)


if __name__=="__main__":
    main()
