#!/usr/bin/env python
#  -*- coding: utf-8 -*-

from pylearnuni.train import kerastrain
import h5py
import numpy
from projects.real_estate_fudosanjapan.real_estate_fudosanjapan_helper import RealestateHelper as Helper
import os
import sys

import keras
from keras.models import Sequential
from keras.layers.core import Dense, Activation
from keras.layers.normalization import BatchNormalization
import itertools
from pylearnuni.preprocess.process_csv import shuffle_csv
import pylearnuni.train.train_handler as train_handler

def main():
    input_csv_path="/media/data1/real_estate/fudosan_japan/fudosan_japan_160723_new.csv"
    output_path="/media/data1/code/maemura/output/real_estate/real_estate_fudosanjapan/filteredsearch/"
    features_org_yaml_path = "/media/workspace/code/maemura/pylearnuni/projects/real_estate_fudosanjapan/real_estate_fudosanjapan_features.yaml"
    features_after_yaml_path = output_path + "/real_estate_fudosanjapan_features_after.yaml"


    if not os.path.exists(os.path.join(output_path,"data.info")):
        shuffle_csv(input_csv_path)
        input_csv_path="./shuffled_data.csv"
        helper = Helper()
        helper.set_output_path(output_path)
        helper.clean_csv(input_csv_path)
        helper.generate_yaml(features_after_yaml_path, features_org_yaml_path, os.path.join(output_path, "cleaned.csv"))
        helper.load_features(features_after_yaml_path)
        helper.load_data(filepath=None, chunk_size=10000, chunk=True)
        helper.generate_csv()
        helper.process_data()
        helper.generate_dataset(test_size=3000,valid_size=1000)

    for (layernum,layersize) in itertools.product([2,3,4],[300,600,900,1200]):
        memo="""
        /media/data1/real_estate/fudosanjapan_160723_new.csv
        layernum:{}
        layersize:{}
        """.format(layernum,layersize)
        kerasoutputpath=os.path.join(output_path,"hlayernum{}_size{}".format(layernum,layersize))
        model=learn(hdf5_path=os.path.join(output_path, "data.h5"), output_path=kerasoutputpath, modelmaker=lambda inputdim:makemodel(inputdim=inputdim, hiddenlayernum=layernum, layersize=layersize), memo=memo)
        f=h5py.File(os.path.join(output_path,"data.h5"))
        predict=model.predict(f["test"]["X"])


def makemodel(inputdim,hiddenlayernum,layersize):
    model = Sequential()
    firstlayerflag=True

    for _ in range(hiddenlayernum):
        if firstlayerflag:
            model.add(Dense(input_dim=inputdim,output_dim=layersize))
            firstlayerflag=False
        else:
            model.add(Dense(output_dim=layersize))
        model.add(Activation('relu'))

    if firstlayerflag:
        model.add(Dense(input_dim=inputdim,output_dim=1))
        firstlayerflag=False
    else:
        model.add(Dense(output_dim=1))
    model.add(Activation('linear'))
    model.compile(loss='mse', optimizer='rmsprop')
    return model


def learn(hdf5_path, output_path, modelmaker, memo):
    #***hdf5が準備できたらkerasで学習***
    #データ
    f=h5py.File(hdf5_path)
    train_set=(f["X"],numpy.exp(f["y"].value.flatten()))
    test_set=(f["test"]["X"],numpy.exp(f["test"]["y"].value.flatten()))

    model=modelmaker(train_set[0].shape[1])

    #学習オプションのリスト設定
    callbacks=[]
    #earlystopping:patience回改善されなかったらやめるオプション
    earlystopping=keras.callbacks.EarlyStopping(monitor='val_loss', patience=20, verbose=0, mode='auto')
    callbacks.append(earlystopping)

    #学習して、一連のファイルをフォルダに保存
    kerastrain.keras_train2(
        output_path=output_path,
        train_set=train_set,test_set=test_set,model=model,callbacks=callbacks,
        validation_split=0.1, #validation setの割合
        nb_epoch=300,         #epoch数
        batch_size=100,
        memo=memo
    )
    return model

if __name__=="__main__":
    main()
