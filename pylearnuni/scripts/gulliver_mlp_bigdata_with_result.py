#!/usr/binenv python
# -*- coding: utf-8 -*-

from projects.gulliver.gulliver_helper import GulliverHelper as Helper
from pylearnuni.preprocess.preprocess import Preprocess as Preprocess

if __name__ == "__main__":
    Preprocess.shuffle = False

    helper = Helper()

    # 読み込むCSVデータのパスを設定
    #helper.load_data("/home/public/data/whole_period.csv", chunk=True)
    helper.load_data("/home/unipro/sunaga/shuffled_data.csv", chunk_size=100000, chunk=True)
    # データに対応するfeatures.yamlのパスを設定
    helper.load_features("/home/public/data/pylearnuni/features.yaml")
    # 出力データを保存するフォルダへのパス（ここにすべて出力される）
    helper.set_output_path("/home/public/data/pylearnuni/sunaga")

    helper.generate_csv()
    # データをpylearn2が読み込める形に変形
    helper.process_data()
    helper.generate_dataset()

    # 読み込むmlp.yamlのパス
    helper.set_train_config_path("../train_config/multilayer_perceptron.yaml")

    # 学習開始
    helper.train()
