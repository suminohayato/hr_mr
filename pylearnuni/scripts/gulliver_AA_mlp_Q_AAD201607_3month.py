#!/usr/binenv python
# -*- coding: utf-8 -*-

import sys
#sys.path.append("./project/gulliver_syuppin")
from projects.gulliver_AA.gulliver_AA_helper import Gulliver_AA_Helper as Helper

if __name__ == "__main__":

    helper = Helper()

    # 読み込むCSVデータのパスを設定
    helper.load_data("/media/data1/pylearnuni_dataset/gulliver_price_consulting/AAD/encoded/Q_AAD情報_utf8_20160416_20160714_withcd.csv", chunk_size=100000, chunk=True)
    # データに対応するfeatures.yamlのパスを設定
    helper.load_features("../projects/gulliver_AA/gulliver_AA_features.yaml")
    # 出力データを保存するフォルダへのパス（ここにすべて出力される）
    helper.set_output_path("/media/data1/code/h_sumino/output/aad_3e6_allcolumn_3month_priced_week/")
    #pylearnUNI/projects/gulliver_AA/gulliver_AA_helper.pyのself.threshold = 3e6を1e8に変更した場合は下記
    # helper.set_output_path("/media/data1/code/h_sumino/output/aad_1e8_allcolumn/")

    helper.generate_csv()
    # データをpylearn2が読み込める形に変形
    helper.process_data()

    helper.generate_dataset()

    # 読み込むmlp.yamlのパス
    # helper.set_train_config_path("../train_config/test.yaml")
    helper.set_train_config_path("../train_config/multilayer_perceptron_20160716best.yaml")

    # 学習開始
    helper.train()
