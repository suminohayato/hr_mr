#!/usr/binenv python
# -*- coding: utf-8 -*-

import sys
#sys.path.append("./project/gulliver_syuppin")
from projects.gulliver_AA.gulliver_AA_helper import Gulliver_AA_Helper as Helper

if __name__ == "__main__":

    helper = Helper()

    # 読み込むCSVデータのパスを設定
    helper.load_data("/media/data1/code/h_sumino/dbo_TT_IF_AAD_souba_info_DATUM2.csv", chunk_size=100000, chunk=True)
    # データに対応するfeatures.yamlのパスを設定
    helper.load_features("../project/gulliver_AA/gulliver_AA_features.yaml")
    # 出力データを保存するフォルダへのパス（ここにすべて出力される）
    helper.set_output_path("/media/data1/code/h_sumino/output/syuppin/")

    # データをpylearn2が読み込める形に変形
    helper.process_data()

    helper.generate_dataset()

    # 読み込むmlp.yamlのパス
    helper.set_train_config_path("../train_config/multilayer_perceptron.yaml")

    # 学習開始
    helper.train()
