#!/usr/binenv python
# -*- coding: utf-8 -*-

#条件項目の直積ごとの、価格の不偏分散を計算するスクリプト
#condition_column_namesに対し、左から重要な順にカラム名を記入
#python analize.pyで実行
#by iwami

import pandas as pd
import numpy as np
from projects.gulliver.gulliver_filter import GulliverFilter as Filter
from pylearnuni.preprocess.preprocess import Preprocess as Preprocess
import copy

def func(df,index,names):

    condition_column_name = condition_column_names[index]
    #重複なしの中身の値
    condition_column_values = list(set(df.ix[:, condition_column_name].values.flatten()))
    for condition_column_value in condition_column_values:
        #e.g. ある車種コードの最新価格のndarray
        sub_df = df[df[condition_column_name] == condition_column_value]
        sub_names = copy.copy(names)
        sub_names.append(str(condition_column_value))

        if index+1 >= len(condition_column_names):
            #出力用リストにappend
            if len(sub_df.ix[:, target_column_name]) > 1:
                var = np.var(sub_df.ix[:, target_column_name],ddof=1)
                output_names.append(sub_names)
                output_vars.append(var)
                output_nums.append(len(sub_df.ix[:, target_column_name]))
        else:
            func(sub_df,index+1,sub_names)


if __name__ == "__main__":

    target_column_name = "最新価格"
    condition_column_names = ("車種コード","グレードコード","禁煙車フラグ")
    output_names = []
    output_vars = []
    output_nums = []

    print "loading csv..."
    df = pd.read_csv("/home/public/data/gulliver_price_data/whole_period_20151201-20160202.csv")

    print "filtering csv..."

    # 範囲外の値を除外
    df = Filter.filter(df)
    # target_featreが閾値以下の物を除外
    df = Filter.target_value_upperboud(df, target_column_name, 1e8)
    # Nanを0で埋める
    df = Preprocess.fill_nan_with_0(df)

    print "analizing...."

    func(df,0,[])

    print "exporting..."

    name_df = pd.DataFrame()
    for i,output_name in enumerate(output_names):
        name_df = name_df.append(pd.DataFrame(output_name).T, ignore_index=True)
    var_df = pd.DataFrame(output_vars)
    num_df = pd.DataFrame(output_nums)
    output_df = pd.concat([name_df, var_df,num_df], axis=1)
    print output_df
    output_df.to_csv("analize.csv")

    print "finished!"





