#!/usr/binenv python
# -*- coding: utf-8 -*-

from projects.gulliver.gulliver_helper import GulliverHelper as Helper
import sys
import os

if __name__ == "__main__":

    # arg1:data  chunksize  sys.argv  outputpath

    helper = Helper()

    helper.load_features("/home/unipro/iso/pylearnuni/projects/gulliver/gulliver_features_nodp.yaml")
    helper.set_output_path("/home/unipro/iso/output/whole_drop_nodp")

    helper.set_columns()
    helper.set_predictor()

    dict = {"車種コード": "S180"}

    ex = helper.example(dict)
    mi = helper.old_missing(dict)

    print ex
    print mi
