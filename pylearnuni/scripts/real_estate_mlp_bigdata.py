#!/usr/binenv python
# -*- coding: utf-8 -*-
import sys
sys.path.append('/media/workspace/code/maemura/pylearnuni/projects/real_estate_fudosanjapan')
from projects.real_estate_fudosanjapan.real_estate_fudosanjapan_helper import RealestateHelper as Helper

if __name__ == "__main__":

    helper = Helper()

    # 出力データをまとめて保存するフォルダのパスを設定
    helper.set_output_path("/media/data1/code/maemura/output/fudosanjapan")
    # helper.output_pathに、出力先のフォルダのパスが格納される
    # helper.datainfo_pathに、data.infoのパスが格納される(helper.output_path配下のdata.info)
    # helper.input_pathに、cleaned.csvのパスが格納される(helper.output_path配下のpreprocessed.csv)

    # 生のデータのパスを設定
    helper.clean_csv("/media/workspace/code/maemura/detail201607232015.csv")
    # helper.raw_csv_pathに生データのファイルパスを格納
    # helper.input_csv_pathに入力データのファイルパスを格納
    # helper.input_csv_pathにclean後のcsvが出力される

    # データに対応するfeatures.yamlのパスを設定
    helper.load_features("/media/data1/code/maemura/output/real_estate/real_estate_fudosanjapan/real_estate_fudosanjapan_features_after.yaml")
    # helper.featuresにfeatures.yamlの情報が格納される

    helper.load_data(chunk=True)
    # helper.dfにcsvファイルのデータが格納される

    # データをpylearn2が読み込める形に変形
    helper.process_data()
# data.infoがなければ前処理を行う。data.infoがあれば、前処理が終わっているものとみなし、変換を行わない。

# 変形したデータをデータセットに分けて保存
#    helper.generate_dataset()

#    helper.generate_csv()

# 読み込む学習モデルのyamlのパスを設定
#    helper.set_train_config_path(train_model_yaml)

# 学習開始
#    helper.train()
