#!/usr/binenv python
# -*- coding: utf-8 -*-

from projects.gulliver.gulliver_helper import GulliverHelper as Helper

if __name__ == "__main__":

    helper = Helper()

    # 読み込むCSVデータのパスを設定
    helper.load_data("/home/public/data/pylearnuni/test.csv")
    # データに対応するfeatures.yamlのパスを設定
    helper.load_features("../projects/gulliver/gulliver_features.yaml")
    # 出力データを保存するフォルダへのパス（ここにすべて出力される）
    helper.set_output_path("/home/public/data/pylearnuni/output/")

    # データをpylearn2が読み込める形に変形
    helper.process_data()

    helper.generate_dataset()

    # 読み込むmlp.yamlのパス
    helper.set_train_config_path("../train_config/multilayer_perceptron.yaml")

    # 学習開始
    helper.train()
