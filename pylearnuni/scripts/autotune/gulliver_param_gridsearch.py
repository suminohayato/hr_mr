#!/usr/binenv python
# -*- coding: utf-8 -*-

from projects.gulliver.gulliver_helper import GulliverHelper as Helper
from pylearnuni.preprocess.preprocess import Preprocess as Preprocess
import sys
import os
import datetime
import math
import pandas
import yaml
import argparse
from pylearn2.config import yaml_parse
import numpy
import scipy.stats
import theano
import abc
import pickle
import theano.tensor as T
import numpy as np
import time
import pandas as pd
from pylearnuni.train import dataset_handler as DatasetHandler
from pylearnuni.train import train_handler as train_handler
from pylearnuni.utils import io_handler as IO
import shutil
import plot_mse


class Space:
    """
    abstruct class for random sampling space classes
    """
    __meta_class__ = abc.ABCMeta

    @abc.abstractmethod
    def sampling(self):
        pass


class LinSpace(Space):
    """
    A class representing for random sampling space delimited evenly on linear scale.

    Parameters
    ----------
    start: float
        start point of sampling space

    stop: float
        stop point of sampling space

    discr: boolean, optional
        sampling with discrete (integer) value
    """

    def __init__(self, start, stop, discr=False):
        self.start = min(start, stop)
        self.stop = max(start, stop)
        self.discr = discr

    def sampling(self):
        if not hasattr(self, "discr") or self.discr == False:
            return numpy.random.uniform(self.start, self.stop)
        else:
            return int(numpy.random.randint(self.start, self.stop + 1))


class FixSpace(Space):
    """
    A class representing for fixed space.

    Parameters
    ----------
    value: fixed value

    """

    def __init__(self, value):
        self.value = value

    def sampling(self):
        return self.value


class LogSpace(Space):
    """
    A class representing for random sampling space delimited evenly on log scale.

    Parameters
    ----------
    start: float
        start point of sampling space

    stop: float
        stop point of sampling space

    discr: boolean, optional
        sampling with discrete (integer) value
    """

    def __init__(self, start, stop, discr=False):
        self.start = min(start, stop)
        self.stop = max(start, stop)
        self.discr = discr

    def sampling(self):
        if not hasattr(self, "discr") or self.discr == False:
            return scipy.stats.reciprocal.rvs(10 ** self.start, 10 ** self.stop)
        else:
            raise NotImplementedError("Logspace have not responded yet to discrete values")


class SetSpace(Space):
    """
    A class representing for random sampling space given by set

    Parameters
    ----------
    sample: list
        set of sampling values
    """

    def __init__(self, sample):
        self.sample = sample

    def sampling(self):
        return numpy.random.choice(self.sample)

    def get_set(self):
        return self.sample


class CostSpace(Space):
    """
    A class representing for random sampling space of cost functions

    Parameters
    ----------
    dropout: tuning.Space, optional
        searching space for dropout

    l1: tuning.Space, optional
        searching space for L1 regularization

    l2: tuning.Space, optional
        searching space for L2 regularization
    """

    def __init__(self, dropout=None, l1=None, l2=None):
        self.dropout = dropout
        self.l1 = l1
        self.l2 = l2

    def sampling(self):
        funcs = []
        if hasattr(self, "dropout") and self.dropout is not None:
            funcs.append(self.dropout)
        if hasattr(self, "l1") and self.l1 is not None:
            funcs.append(self.l1)
        if hasattr(self, "l2") and self.l2 is not None:
            funcs.append(self.l2)
        if len(funcs) == 0:
            raise ValueError("parameters for some of cost functions must be given")
        f = numpy.random.choice(funcs)

        if f is self.dropout:
            name = "dropout"
        elif f is self.l1:
            name = "l1"
        elif f is self.l2:
            name = "l2"

        return name, f.sampling()


def tuning(config="params.yaml", template="template.yaml", save_path="./configs", folder_name=""):
    """
    search optimal parameters for model randomly.

    Parameters
    ----------
    config: str
        file path for the YAML train_config of random search

    template: str
        file path for the template YAML file of Pylearn2 batch file

    save_path: str
        path for saving learning result

    folder_name: str
        name of folder

    """
    fix_names = ["cost", "beta_lr_scale", "sparse_init"]
    set_names = ["batch_size", "h_layer_dim", "layer_size"]
    param_names = fix_names + set_names

    def check_param(conf):
        for name in param_names:
            if name not in conf:
                raise ValueError("YAML train_config file need {} parameter".format(name))

    # def build_cost_param(p):
    #     hlayers = ["h0", "h1", "h2", "h3", "h4", "h5", "h6"]
    #     return yaml.dump({"coeffs": {l:p for l in hlayers}})

    def generate_params(conf, folder_name):
        params_ = []
        param_index = 0
        p_common = {"save_path": save_path, "folder_name": folder_name}
        for name in fix_names:
            p_common[name] = conf[name].sampling()

        batch_size_set = conf["batch_size"].get_set()
        h_layer_dim_set = conf["h_layer_dim"].get_set()
        layer_size_set = conf["layer_size"].get_set()
        nparams = len(batch_size_set) * len(h_layer_dim_set) * len(layer_size_set)
        for batch_size in batch_size_set:
            for h_layer_dim in h_layer_dim_set:
                for layer_size in layer_size_set:
                    tag = "{:0{}}".format(
                        param_index + 1, int(math.log10(nparams) + 1))  # イテレーションの回数

                    p = p_common.copy()
                    p["batch_size"] = batch_size
                    p["h_layer_dim"] = h_layer_dim
                    p["layer_size"] = layer_size
                    p["tag"] = tag

                    # RectifiedLinearの層を複数差し込む
                    layers_string = ""

                    for i in range(layer_size):
                        layers_string += "            !obj:pylearn2.models.mlp.RectifiedLinear {\n" \
                                         "               layer_name: 'h" + str(i) + "',\n" \
                            "               dim: " + str(p["h_layer_dim"]) + ",\n" \
                            "               sparse_init: " + str(p["sparse_init"]) + ",\n" \
                            "               init_bias: 0,\n" \
                            "            },\n"

                    p["rectified_linears"] = layers_string
                    p["dataset_path_holder"] = "%(dataset_path)s"
                    p["target_feature_holder"] = "%(target_feature)s"
                    p["nvis_holder"] = "%(nvis)s"
                    p["output_path_holder"] = "%(output_path)s"

                    # if p["cost"][0] == "dropout":
                    #     p["cost_func"] = "dropout.Dropout"
                    #     p["cost_param"] = yaml.dump({"default_input_include_prob": p["cost"][1]})
                    # elif p["cost"][0] == "l1":
                    #     p["cost_func"] = "L1WeightDecay"
                    #     p["cost_param"] = build_cost_param(p["cost"][1])
                    # elif p["cost"][0] == "l2":
                    #     p["cost_func"] = "WeightDecay"
                    #     p["cost_param"] = build_cost_param(p["cost"][1])
                    # else:
                    #     raise ValueError("cost parameter should be any of dropout, l1, l2")

                    p["hdim"] = p["h_layer_dim"]
                    params_.append(p)
                    param_index = param_index + 1

        return params_

    # load train_config YAML
    conf = None
    with open(config) as config_f:
        conf = yaml.load(config_f)
        check_param(conf)

    tmpl = None
    with open(template) as tmpl_f:
        tmpl = tmpl_f.read()

    # 結果集計用
    batch_size_list = []
    beta_lr_scale_list = []
    sparse_init_list = []
    h_layer_dim_list = []
    layer_size_list = []
    rmse_list = []
    mape_list = []
    err_ave_list = []
    time_list = []

    params = generate_params(conf, folder_name)
    print params

    for i, param in enumerate(params):
        print "\nチューニング試行回数: {}回目/{}回".format(i + 1, len(params))

        write_tmp_result("チューニング試行回数: {}回目/{}回\n".format(i + 1, len(params)))

        # best_param.pklを削除して再度学習を回す
        if os.path.isfile(os.path.join(helper.output_path, "best_param.pkl")):
            os.remove(os.path.join(helper.output_path, "best_param.pkl"))

        start_time = time.clock()

        tag = param["tag"]

        yaml_batch = tmpl % param
        with open("{}/{}/train_config{}.yaml".format(save_path, folder_name, tag), "w") as cfg_yaml:
            cfg_yaml.write(yaml_batch)
        print yaml_batch

        batch_size_list.append(param['batch_size'])
        beta_lr_scale_list.append(param['beta_lr_scale'])
        sparse_init_list.append(param['sparse_init'])
        h_layer_dim_list.append(param['h_layer_dim'])
        layer_size_list.append(param['layer_size'])

        write_tmp_result("batch_size : {}\n".format(param['batch_size']))
        write_tmp_result("h_layer_dim : {}\n".format(param['h_layer_dim']))
        write_tmp_result("layer_size : {}\n".format(param['layer_size']))

        # 学習
        # 読み込むmlp.yamlのパス
        print "{}/{}/train_config{}.yaml".format(save_path, folder_name, tag)
        helper.set_train_config_path("{}/{}/train_config{}.yaml".format(save_path, folder_name, tag))
        # 学習開始
        helper.train()

        end_time = time.clock()
        # 実時間でなくCPU時間
        time_list.append(end_time - start_time)

        # 予測
        # テストデータ読み込み
        model = IO.read_pickle(os.path.join(helper.output_path, "best_param.pkl"))
        dataset_path = os.path.join(helper.output_path, "data.h5")
        dataset = DatasetHandler.fetch_dataset(dataset_name="test", dataset_path=dataset_path, target_feature="最新価格")
        y_actual, y_predicted = train_handler.cal_regression(dataset, model, verbose=False)
        # 結果出力

        print "#cases: %g" % len(y_actual)

        rmse = np.sqrt(((y_actual - y_predicted)**2).mean())
        print "RMSE: %g" % rmse
        rmse_list.append(rmse)
        write_tmp_result("RMSE: %g\n" % rmse)

        mape = ((np.abs((y_actual - y_predicted).astype(np.float64) / y_actual)).mean() * 100)
        print "MAPE: %g %%" % mape
        mape_list.append(mape)

        err_ave = (np.abs(y_actual - y_predicted)).mean()
        print "誤差の絶対値の平均: %g" % err_ave
        err_ave_list.append(err_ave)

        # train及びvalidのmseを可視化
        plot_mse.plot("{}/{}/mlp{}.pkl".format(save_path, folder_name, tag), "0.0:1.0")

        if not os.path.isfile(os.path.join(helper.output_path, "result.csv")):
            print "cannot find file at ".format(os.path.join(helper.output_path, "result.csv"))
        shutil.copyfile(os.path.join(helper.output_path, "result.csv"), "{}/{}/result{}.csv".format(save_path, folder_name, tag))

    # 全体の結果をまとめてcsvに書き出す
    # pythonのList→PandasのSeries→PandasのDataFrameの順で変換

    # learning_rate: SGDの学習率
    # batch_size: SGDのバッチサイズ
    # beta_lr_rate: 出力層の正規分布の精度の学習率係数
    # sparse_init: 各層のノードのうち何個を0初期化するか
    # h_layer_dim: 隠れ層(ReLU)のノード数
    all_df = pd.DataFrame({
        'batch_size': batch_size_list,
        'beta_lr_scale': beta_lr_scale_list,
        'sparse_init': sparse_init_list,
        'h_layer_dim': h_layer_dim_list,
        'layer_size': layer_size_list,
        'RMSE': pd.Series(rmse_list, dtype='float32'),
        'MAPE': pd.Series(mape_list, dtype='float32'),
        '誤差の絶対値の平均': pd.Series(err_ave_list, dtype='float32'),
        '経過CPU時間': pd.Series(time_list, dtype='float32'), },
        columns=['batch_size',
                 'beta_lr_scale', 'sparse_init', 'h_layer_dim', 'layer_size', 'RMSE',
                 'MAPE', '誤差の絶対値の平均', '経過CPU時間'])

    all_df.to_csv("{}/{}/all_result.csv".format(save_path, folder_name))


def write_tmp_result(str):
    with open("{}/{}/tmp_result.txt".format(save_path, folder_name), 'a') as tmp_f:
        tmp_f.write(str)


def predict(dataset, model, batch_size=1, verbose=True):
    X = T.matrix("input")
    y = model.fprop(X)
    f = theano.function([X], y)
    y_actual, y_predicted = [], []
    print "length of dataset.X:{}".format(len(dataset.X))
    nbatches = len(dataset.X) / batch_size
    assert(len(dataset.X) % batch_size == 0)
    for idx in xrange(nbatches):
        X_ = dataset.X[idx * batch_size:(idx + 1) * batch_size]
        y_ = dataset.y[idx * batch_size:(idx + 1) * batch_size]
        y_actual_ = np.asarray(y_, dtype="float32")
        y_predicted_ = np.asarray(f(np.asarray(X_, dtype="float32")).flat, dtype="float32")
        y_actual.append(y_actual_)
        y_predicted.append(y_predicted_)
        if verbose:
            for x, y in zip(X_.flat, y_.flat):
                print "x:{}  y:{}".format(x, y)
            for p, a in zip(y_predicted_.flat, y_actual_.flat):
                print p, " -> ", a
    y_actual = np.asarray(y_actual, dtype="float").flatten()
    y_predicted = np.asarray(y_predicted, dtype="float").flatten()
    if verbose:
        print "#cases: %g" % len(y_actual)
        print "RMSE: %g" % np.sqrt(((y_actual - y_predicted)**2).mean())
    return y_actual, y_predicted

if __name__ == "__main__":

    if (len(sys.argv) != 2):   # 引数が足りない場合は、その旨を表示
        print 'Usage: # python %s param_file' % sys.argv[0]
        quit()

    config = sys.argv[1]
    helper = Helper()

    # 読み込むCSVデータのパスを設定
    # chankを指定するとhdf5で出力 しないとbin
    helper.load_data("/home/public/data/gulliver_price_data/whole_period_20151201-20160202.csv", chunk_size=100000, chunk=True)
    # データに対応するfeatures.yamlのパスを設定
    helper.load_features("/home/public/data/pylearnuni/features.yaml")

    folder_name = datetime.datetime.now().strftime("%y%m%d%H%M")
    save_path = "./configs"
    os.mkdir("{}/{}".format(save_path, folder_name))
    shutil.copy("result/data.h5", "{}/{}/data.h5".format(save_path, folder_name))
    shutil.copy("result/data.info", "{}/{}/data.info".format(save_path, folder_name))

    # 出力データを保存するフォルダへのパス（ここにすべて出力される
    # 並列化のため
    helper.set_output_path("{}/{}".format(save_path, folder_name))

    helper.target_feature = "最新価格"
    # データをpylearn2が読み込める形に変形
    helper.process_data()
    helper.generate_dataset()

    tuning(config=config, save_path=save_path, folder_name=folder_name)
