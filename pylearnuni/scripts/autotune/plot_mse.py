#!/usr/binenv python
# -*- coding: utf-8 -*-

# モデルを入力すると、train及びvalidデータのmseの遷移を可視化し出力する

import gc
import numpy as np
import sys

from theano.compat.six.moves import input, xrange
from pylearn2.utils import serial
from theano.printing import _TagGenerator
from pylearn2.utils.string_utils import number_aware_alphabetical_key
from pylearn2.utils import contains_nan, contains_inf
import argparse

channels = {}

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


def plot(model_path, yrange):

    model_name = model_path.replace('.pkl', '')
    print "loaded " + model_path

    try:
        model = serial.load(model_path)
    except Exception:
        if model_path.endswith('.yaml'):
            print("error")
            quit(-1)
        raise
    this_model_channels = model.monitor.channels

    postfix = ":" + model_name

    for channel in this_model_channels:
        channels[channel + postfix] = this_model_channels[channel]
    del model
    gc.collect()

    # Make a list of short codes for each channel so user can specify them
    # easily
    tag_generator = _TagGenerator()
    codebook = {}
    sorted_codes = []
    for channel_name in sorted(channels,
                               key=number_aware_alphabetical_key):
        code = tag_generator.get_tag()
        codebook[code] = channel_name
        codebook['<' + channel_name + '>'] = channel_name
        sorted_codes.append(code)
    if len(channels.values()) == 0:
        print("there are no channels to plot")
        quit()
    # If there is more than one channel in the monitor ask which ones to
    # plot
    prompt = len(channels.values()) > 1
    if prompt:
        # Display the codebook
        final_names = set(["valid_y_mse" + postfix, "train_y_mse" + postfix])
        x_axis = 'epoch'
    else:
        print "There is no channel"
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
    styles = list(colors)
    styles += [color + '--' for color in colors]
    styles += [color + ':' for color in colors]
    fig = plt.figure()
    ax = plt.subplot(1, 1, 1)
    # plot the requested channels
    for idx, channel_name in enumerate(sorted(final_names)):
        channel = channels[channel_name]
        y = np.asarray(channel.val_record)
        if contains_nan(y):
            print(channel_name + ' contains NaNs')
        if contains_inf(y):
            print(channel_name + 'contains infinite values')
        if x_axis == 'example':
            x = np.asarray(channel.example_record)
        elif x_axis == 'batche':
            x = np.asarray(channel.batch_record)
        elif x_axis == 'epoch':
            try:
                x = np.asarray(channel.epoch_record)
            except AttributeError:
                # older saved monitors won't have epoch_record
                x = np.arange(len(channel.batch_record))
        elif x_axis == 'second':
            x = np.asarray(channel.time_record)
        elif x_axis == 'hour':
            x = np.asarray(channel.time_record) / 3600.
        else:
            assert False

        ax.plot(x,
                y,
                styles[idx % len(styles)],
                marker='.',  # add point margers to lines
                label=channel_name)
    plt.xlabel('# ' + x_axis + 's')
    ax.ticklabel_format(scilimits=(-3, 3), axis='both')
    handles, labels = ax.get_legend_handles_labels()
    lgd = ax.legend(handles, labels, loc='upper left',
                    bbox_to_anchor=(1.05, 1.02))
    # Get the axis positions and the height and width of the legend
    plt.draw()
    ax_pos = ax.get_position()
    pad_width = ax_pos.x0 * fig.get_size_inches()[0]
    pad_height = ax_pos.y0 * fig.get_size_inches()[1]
    dpi = fig.get_dpi()
    lgd_width = ax.get_legend().get_frame().get_width() / dpi
    lgd_height = ax.get_legend().get_frame().get_height() / dpi
    # Adjust the bounding box to encompass both legend and axis.  Axis should be 3x3 inches.
    # I had trouble getting everything to align vertically.
    ax_width = 3
    ax_height = 3
    total_width = 2 * pad_width + ax_width + lgd_width
    total_height = 2 * pad_height + np.maximum(ax_height, lgd_height)
    fig.set_size_inches(total_width, total_height)
    ax.set_position([pad_width / total_width, 1 - 6 * pad_height / total_height, ax_width / total_width, ax_height / total_height])
    if(yrange is not None):
        ymin, ymax = map(float, yrange.split(':'))
        plt.ylim(ymin, ymax)
    plt.savefig(model_name)


if __name__ == "__main__":

    param = sys.argv
    if len(param) < 2:
        print "python plot_mse model_name (yrange=None e.g. 0.0:1.0)"
    elif len(param) < 3:
        plot(param[1], None)
    elif len(param) == 3:
        plot(param[1], param[2])
