#!/usr/binenv python
# coding: utf-8

import pandas
import os
import pickle
import getpass
import sys
import pandas as pd
import datetime

DATAROOT = os.getenv('GULLIVER_DATA_PATH')
assert(DATAROOT)

today = datetime.datetime.today()

INPUT_CSV_PATH = os.path.join(DATAROOT, "whole_period.csv")
OUTPUT_CSV_PATH = os.path.join(DATAROOT, "whole_period_2months5.csv")


def extract_latest():
    print "extracting latest data..."
    reader = pd.read_csv(INPUT_CSV_PATH, chunksize=100000)
    two_month = today - datetime.timedelta(days=600)
    df_maeged = pd.DataFrame()
    debug_proceeded_counter = 0
    debug_ectracted_counter = 0
    for df in reader:

        debug_proceeded_counter = debug_proceeded_counter + 1
        if debug_proceeded_counter <= 41:
            continue

        df['latest_flag'] = 0

        for i in xrange(df.shape[0]):
            d = datetime.datetime.strptime(df.ix[i, "最終掲載日時"], '%Y/%m/%d %H:%M:%S')
            if(two_month < d):
                df.ix[i, 'latest_flag'] = 1
                debug_ectracted_counter = debug_ectracted_counter + 1

        df_extracted = df.loc[df['latest_flag'] == 1]
        del df_extracted['latest_flag']
        df_maeged = pd.concat([df_maeged, df_extracted], ignore_index=True)
        print "{0}0million".format(debug_proceeded_counter)
        if debug_proceeded_counter > 50:
            break

    print "{0}rows are extracted!".format(debug_ectracted_counter)
    df_maeged.to_csv(OUTPUT_CSV_PATH)


if __name__ == "__main__":
    extract_latest()
