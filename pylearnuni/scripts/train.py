#!/usr/binenv python
# -*- coding: utf-8 -*-

import sys

from projects.gulliver.gulliver_helper import GulliverHelper as Helper

if __name__ == "__main__":
    if len(sys.argv) <= 4:
        print '''Pylearnuni usage:
train.py $input_data_csv $feature_yaml $output_path $train_model_yaml
'''
        sys.exit(1)

    input_data_csv = sys.argv[1]
    feature_yaml = sys.argv[2]
    output_path = sys.argv[3]
    train_model_yaml = sys.argv[4]

    helper = Helper()

    # 読み込むデータのパスを設定
    helper.load_data(input_data_csv, chunk=True)
    # helper.input_csv_pathに入力データのファイルパスを格納
    # helper.dfにcsvファイルのデータが格納される
    # データに対応するfeatures.yamlのパスを設定
    helper.load_features(feature_yaml)
    # helper.featuresにfeatures.yamlの情報が格納される
    # 出力データをまとめて保存するフォルダのパスを設定
    helper.set_output_path(output_path)
    # helper.output_pathに、出力先のフォルダのパスが格納される
    # helper.datainfo_pathに、data.infoのパスが格納される(helper.output_path配下のdata.info)
    # データをpylearn2が読み込める形に変形
    helper.process_data()
    # data.infoがなければ前処理を行う。data.infoがあれば、前処理が終わっているものとみなし、変換を行わない。

    # 変形したデータをデータセットに分けて保存
    helper.generate_dataset()

    helper.generate_csv()

    # 読み込む学習モデルのyamlのパスを設定
    helper.set_train_config_path(train_model_yaml)

    # 学習開始
    helper.train()
