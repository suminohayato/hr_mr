#!/usr/binenv python
# -*- coding: utf-8 -*-

import pandas
import sys
import numpy

###単語の有りなしフラグをつける
###単語学習用のfeatures.yamlを生成する

#言語学習用　詳しくはwordlearn_prepare.pyを見てください

def flagwords(df,words):
    df2=df[["掲載コメント","メインキャプション","最新価格","予測価格","予測誤差"]]
    for word in words:
        print word
        df2.loc[:,("wordflag:"+word)]= (df['掲載コメント'].str.contains(word) | df['メインキャプション'].str.contains(word)).astype(numpy.int8)
    return df2

def makefeaturesyaml(words):
    f=open("features.yaml","w")
    f.write("予測誤差:\n")
    f.write("  feature_type: unary\n")
    f.write("  dtype: int32\n")
    for word in words:
        f.write("wordflag:"+word+":\n")
        f.write("  "+"feature_type: binary\n")
        f.write("  "+"dtype: int8\n")

def main():
    datapath=sys.argv[1]
    wordspath=sys.argv[2]

    words=[w.split()[0] for w in open(wordspath).read().split("\n")[:100] if len(w)!=0]
    reader=pandas.read_csv(datapath,chunksize=100000,sep="\t" )
    firstchunk=True
    for df in reader:
        print df.shape
        df2=flagwords(df,words)
        if firstchunk:
            mode="w"
            header=True
            firstchunk=False
        else:
            mode="a"
            header=False
        df2.to_csv("flagged.tsv",sep="\t",mode=mode,header=header)
    makefeaturesyaml(words)

main()