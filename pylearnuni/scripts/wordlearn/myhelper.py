#!/usr/binenv python
# -*- coding: utf-8 -*-

import os
import numpy
import pandas

from pylearnuni.helper.helper import Helper as Helper
from pylearnuni.preprocess.preprocess import Preprocess as Preprocess
from projects.gulliver.gulliver_filter import GulliverFilter as Filter
from projects.gulliver.gulliver_converter import GulliverConverter as Converter
from pylearnuni.utils import io_handler as IO
from pylearnuni.train.train_handler import generate_predictor
from pylearnuni.utils import io_handler
import yaml

class MyGulliverHelper(Helper):

#### Main Functions ####
    def __init__(self,target_feature):
        Helper.__init__(self)
        self.target_feature =target_feature
        self.version = "G1.0"
        self.before_convert=lambda df:df
        self.after_convert=lambda df:df

    def load_data(self, filepath, chunk, chunk_size,sep):
        if not filepath is None:
            self.input_csv_path = filepath
        assert(os.path.isfile(self.input_csv_path))

        self.sep=sep

        if chunk:
            self.chunk = True
            self.reader = IO.read_csv(self.input_csv_path, chunksize=chunk_size,sep=sep)
            self.df = None
        else:
            self.reader = None
            self.df = IO.read_csv(self.input_csv_path,sep=sep)

    def load_info(self,infopath):
        self.info_columnname=io_handler.read_yaml(infopath)["column_name"]

    def process_data(self):
        if not os.path.isfile(os.path.join(self.output_path, "data.info")):
            self._process_data(self.convert_dataframe)

    def generate_csv(self):
        self._generate_csv(self.convert_dataframe)

    def set_filters(self,before,after):
        if before:
            self.before_convert=before
        if after:
            self.after_convert=after

    def set_predictor(self,parampath=None):
        if parampath==None:
            parampath=os.path.join(self.output_path,"best_param.pkl")
        self.predictor=generate_predictor(parampath)

    def predict_df2(self,df):
        print "predicting:",df.shape
        df_ = pandas.DataFrame(columns=self.info_columnname)
        df = pandas.concat([df_, df], copy=False)
        df = Preprocess.fill_nan_with_0(df)
        df = df.reindex(columns=self.info_columnname, copy=False)
        print "filled:",df.shape

        keys=list(df.keys())
        keys.remove(self.target_feature)
        X=df[keys].as_matrix().astype(numpy.float32)
        print "Xshape:",X.shape
        yhat=self.predictor(X).flatten()
        y=df[self.target_feature]
        return yhat,y

#### Hidden Functions ####
    def get_column_name(self):
        return self._get_column_name(Converter)

    def _get_column_name(self, converter):
        reader_ = IO.read_csv(self.input_csv_path, chunksize=100000,sep=self.sep)#sep!
        set_ = set()
        for df in reader_:
            for x in Preprocess.get_column_name(df, self.features, converter):
                set_.add(x)
        return sorted(list(set_))

    def _filter_data(self,df):
        ### called from _process_data for each chunk. ###
        df=self.before_convert(df)
        return df

    def convert_dataframe(self, df, convert=True):
        ### sent to self._process_data as  argument:"dataframe_converter" ###
        ### called from _process_data for each chunk.
        ### used after _filter_data

        df = Preprocess.fill_nan_with_0(df)
        if convert:
            df = Preprocess.convert_dataframe(df, self.features, Converter)
            df = Preprocess.fill_nan_with_0(df)
            Preprocess.validate_dataframe(df)
        df=self.after_convert(df)
        return df

###################################### Stuff I dont know about #####################################

    def set_filter_code(self, company_code):
        # 除外する法人コードを追加する
        self.exclusion_codes.append(numpy.int64(company_code))

    def set_filter_codelist(self, filterpath):
        # 抽出する法人コードのリストをCSVから読み込む
        # フォーマットは/home/public/data/pylearnuni/CSStoreMaster.csvを参照
        assert(os.path.isfile(filterpath))
        label = "掲載先顧客法人C"
        df = IO.read_csv(filterpath)[label]
        self.inclusion_codes.extend(df.to_dict().values())

    def filter_houjin(self, df):
        label = "法人コード"

        # self.inclusion_codesが空でなければ、これに含まれていない法人コードは除去する
        if self.inclusion_codes != []:
            df = df[df[label].isin(self.inclusion_codes)]

        # self.exclusion_codesに含まれている法人コードは除去する
        return df[-df[label].isin(self.exclusion_codes)]
###################################################################################################
