#!/usr/binenv python
# -*- coding: utf-8 -*-

import pandas

class DfSaver():
    def __init__(self,path,sep):
        self.path=path
        self.sep=sep
        self.isfirstchunkflag=True

    def write(self,df):
        if self.isfirstchunkflag:
            mode="w"
            header=True
            self.isfirstchunkflag=False
        else:
            mode="a"
            header=False
        df.to_csv(self.path,sep=self.sep,mode=mode,header=header)
