# coding: utf-8
import MeCab
import pandas
import sys
import numpy
from collections import Counter

#言語学習用　詳しくはwordlearn_prepare.pyを見てください

mt = MeCab.Tagger("mecabrc")

def printlist(ll):
    for l in ll:
        for i in l:
            print i,
        print

def sentencetowords(s):
    if s!=s: #nan
        return None
    res = mt.parseToNode(s)
    ret=[]
    while res:
        worddata=res.feature.split(",")
        worddata.insert(0,res.surface)

        if not worddata[1] in ["動詞","名詞","形容詞","形容動詞"]:
            pass
        elif worddata[2] in ["数"]:
            pass
        elif worddata[-3] in [
            "♪","ー","-","－","*",
            "する","ある","いる","おる","なる","せる","れる",
            "車",
            "くださる","下さる",
            "頂く","頂ける","いただく","いただける"
        ]:
            pass
        else:
            ret.append(worddata)

        res = res.next
    return ret

def datatowords(df):
    ret=[]
    for r in zip(df['掲載コメント'], df['メインキャプション']):
        a=sentencetowords(r[0])
        if a:
            ret.extend(a)
        b=sentencetowords(r[0])
        if b:
            ret.extend(b)
    return ret

def datatocnt(df):
    cnt=Counter()
    for r in zip(df['掲載コメント'], df['メインキャプション']):
        a=sentencetowords(r[0])
        if a:
            for i in a:
                cnt[i[-3]]+=1
        b=sentencetowords(r[1])
        if b:
            for i in b:
                cnt[i[-3]]+=1
    return cnt

def test():
    reader=pandas.read_csv(sys.argv[1],sep="\t",chunksize=100000)
    cnt=Counter()
    for df in reader:
        sys.stderr.write('counting words... most common so far: {}\n'.format(cnt.most_common(1)))
        cnt+=datatocnt(df)
    printlist(cnt.most_common(10000) )

test()