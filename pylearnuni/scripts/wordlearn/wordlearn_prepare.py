# coding: utf-8
import pandas
import sys
import numpy
from projects.gulliver.gulliver_helper import GulliverHelper
from projects.gulliver.gulliver_filter import GulliverFilter as Filter
from pylearnuni.preprocess.preprocess import Preprocess as Preprocess
from pylearnuni.train.train_handler import generate_predictor
from projects.gulliver.gulliver_converter import GulliverConverter as Converter
from myhelper import MyGulliverHelper
from myio import DfSaver
import os

#言語学習の方法
#①予測器を作る
#②extract_words.pyで重要そうな単語をリストアップする
#③このファイルで、1.予測器の出力と単語の有るなしフラグをもったtsv 2.単語学習用のfeatures.yaml　を出力する
#④learn_words.pyで元の予測器の結果＆単語の有るなしフラグ→真の価格 の　重回帰を行う
#⑤出てきたpklファイルを検証して、各単語の影響を出力する（スクリプト化してない）

#学習済みの予測器を使用して、そこから、
#予測器での予測価格＆単語データ　＝＞　真の価格　という学習のための準備をするスクリプト。
#
#このファイルの入力：
# ①学習済みの予測器関連のファイル(pklファイル、infoファイル、yamlファイル)
#　②入力データ
#　③単語リスト（extract_words.pyによって作る）
#このファイルの出力：
#  ①with_prediction.tsv
#    "掲載コメント","メインキャプション","最新価格,"予測価格","予測誤差",書く単語のあるなしのフラグたくさん("wordflag:○○"という名前)　のcolumnを持ったtsv
#  ②wordfeatures.yaml
#    次の学習のためのfeature.yaml
#
#依存するファイル：
#myhelper.py
#myio.py
#が隣にあればいい（依存ファイルができてしまってすいません）
#
#
#このファイルを配布することを予定していなかったのでわかりにくい感じになってしまってすいません



def flagwords(df,words):
    for word in words:
        print word
        df.loc[:,("wordflag:"+word)]= (df['掲載コメント'].str.contains(word) | df['メインキャプション'].str.contains(word)).astype(numpy.int8)
    return df

def makefeaturesyaml(words,outputpath):
    f=open(outputpath,"w")
    f.write("最新価格:\n")
    f.write("  feature_type: multi_scaled\n")
    f.write("  dtype: int32\n")
    f.write("予測価格:\n")
    f.write("  feature_type: multi_scaled\n")
    f.write("  dtype: int32\n")
    for word in words:
        f.write("wordflag:"+word+":\n")
        f.write("  "+"feature_type: binary\n")
        f.write("  "+"dtype: int8\n")

if __name__=="__main__":
    datapath=sys.argv[1]#データのパス
    if datapath.endswith("tsv"):
        sep="\t"
    else:
        sep=","
    yamlpath=sys.argv[2]#元の予測器のyamlのパス
    pklpath=sys.argv[3]#元の予測器のbest_param.pklファイルのパス
    infopath=sys.argv[4]#元の予測器のdata.infoのパス
    wordspath=sys.argv[5]#単語リストのパス

    outputpath=sys.argv[6]#新しいoutputpath

    helper=MyGulliverHelper("最新価格")
    helper.set_output_path(outputpath)
    helper.load_data(datapath,chunk=True,chunk_size=10000,sep=sep)
    helper.load_features(yamlpath)
    helper.set_predictor(pklpath)
    helper.load_info(infopath)

    #warning!!! this part is written in a really weird way!!================================

    filteredraw=None  #this should stay global!
    dfsaver=DfSaver(os.path.join(helper.output_path,"with_prediction.tsv"),sep="\t")# this should stay global!
    words=[w.split()[0] for w in open(wordspath).read().split("\n")[:100] if len(w)!=0]#the first 100
    makefeaturesyaml(words,os.path.join(helper.output_path,"wordfeatures.yaml"))

    def before_filter(df):#called for each chunk *right BEFORE* being converted
        print "before filter",df.shape
        df = Filter.filter(df)
        df = Filter.target_value_upperboud(df,"最新価格",3000000)#300 man filter
        print "filtered",df.shape
        global filteredraw #save part of the chunk to a global register
        filteredraw=df[["掲載コメント","メインキャプション","最新価格"]].copy()
        return df

    def after_filter(df2):#called for each chunk *right AFTER* being converted
        print "converted",df2.shape
        yhat,y=helper.predict_df2(df2)
        global filteredraw #uses the saved chunk in the global register
        filteredraw.loc[:,"予測価格"]=numpy.exp(yhat)
        filteredraw.loc[:,"予測誤差"]=numpy.exp(y)-numpy.exp(yhat)
        filteredraw=flagwords(filteredraw,words)
        print "saving",filteredraw.shape
        dfsaver.write(filteredraw) #append to a csv
        return df2
    #weird part ends.============================================================================

    helper.set_filters(before_filter,after_filter)
    helper.process_data()

