#!/usr/binenv python
# -*- coding: utf-8 -*-

###単語のリストを作る

#言語学習用　詳しくはwordlearn_prepare.pyを見てください

from myhelper import MyGulliverHelper
import sys

if __name__ == "__main__":
    datapath=sys.argv[1]
    featurespath=sys.argv[2]
    trainyamlpath=sys.argv[3]
    outputpath=sys.argv[4]

    helper = MyGulliverHelper("予測誤差")
    helper.load_data(datapath,chunk=True,chunk_size=100000,sep="\t")
    helper.load_features(featurespath)
    helper.set_output_path(outputpath)

    helper.process_data()
    helper.generate_dataset()
    helper.set_train_config_path(trainyamlpath)

    helper.train()
