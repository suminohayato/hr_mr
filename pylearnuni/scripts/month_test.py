#!/usr/binenv python
# -*- coding: utf-8 -*-


from projects.gulliver.gulliver_helper import GulliverHelper as Helper
import sys

if __name__ == "__main__":

    helper = Helper()

    # 読み込むデータのパスを設定
    helper.load_data(sys.argv[1], chunk=True)
    # helper.input_csv_pathに入力データのファイルパスを格納
    # helper.dfにcsvファイルのデータが格納される
    # データに対応するfeatures.yamlのパスを設定
    helper.load_features("../projects/gulliver/gulliver_features_with_stock_price.yaml")
    # helper.featuresにfeatures.yamlの情報が格納される
    # 出力データをまとめて保存するフォルダのパスを設定
    helper.set_output_path(sys.argv[2])
    # helper.output_pathに、出力先のフォルダのパスが格納される
    # helper.datainfo_pathに、data.infoのパスが格納される(helper.output_path配下のdata.info)
    # データをpylearn2が読み込める形に変形
    helper.process_data()
    # data.infoがなければ前処理を行う。data.infoがあれば、前処理が終わっているものとみなし、変換を行わない。

    # 変形したデータをデータセットに分けて保存
    helper.generate_dataset()

    # 読み込む学習モデルのyamlのパスを設定
    helper.set_train_config_path("../train_config/multiple_regression_analysis.yaml")

    # 学習開始
    helper.train()
