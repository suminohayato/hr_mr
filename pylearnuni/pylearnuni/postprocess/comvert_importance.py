#/usr/binenv python
# coding: utf-8

import os
import sys
reload(sys)
sys.setdefaultencoding("utf-8")
import pandas
import numpy
import re

# 表示するDataFrameの行と列の数と折り返しの行数の指定
pandas.set_option('display.max_rows', 10)
pandas.set_option('display.max_columns', 7)
pandas.set_option('line_width', 200)

PRODUCT_FEATURES = ("メーカーコード", "車種コード")
# 作成する交差項の組み合わせ
TARGET_FEATURE = "メーカーコード_X_車種コード"
# 車種名以外で動作するか分からない
TARGET_FEATURES_FOR_CROSS_TERM = ("CDナビフラグ", "DVDナビフラグ", "HDDナビフラグ")
# ftypeが"binary"であることが必要

cross_term_delimiter = re.compile('_x_')
product_delimiter = re.compile('_X_')

# CSVファイルの読み込み
# 絶対パスで指定推奨


def read_csv(csv_file):
    print "Read CSV File : ", csv_file
    df = pandas.read_csv(csv_file, names=["cross_term", "value"])
    return df


def write_csv(csv_file, df):
    print "Write CSV File : ", csv_file
    df.to_csv(csv_file)
    return


def delete_illegal_row(df):
    # Nanのある行を消去
    df = df.dropna()
    return df


def convert_importance(df_):
    df = pandas.DataFrame(columns=[TARGET_FEATURE, PRODUCT_FEATURES[0], PRODUCT_FEATURES[1]])
    car_names = []

    for index, row in df_.iterrows():
        s = cross_term_delimiter.split(row["cross_term"])
        if len(s) == 2:
            car = s[0]
            feature = s[1]
            value = row["value"]
            if car in car_names:
                df.loc[df[TARGET_FEATURE] == car, feature] = value
            else:
                ss = product_delimiter.split(car)
                assert(len(ss) == 2)
                row = pandas.DataFrame([[car, ss[0], ss[1], value]])
                row.columns = [TARGET_FEATURE, PRODUCT_FEATURES[0], PRODUCT_FEATURES[1], feature]
                row.index = [len(df) + 1]
                df = pandas.concat([df, row])
                car_names.append(car)

    return df

####################
#       main       #
####################
if __name__ == "__main__":
    argv = sys.argv
    assert(len(argv) == 3)
    IMPORTANCE_CSV = argv[1]
    OUTPUT_CSV = argv[2]

    # csvファイルの読み込み
    df = read_csv(IMPORTANCE_CSV)
    df = delete_illegal_row(df)
    print "########## Original Data ##########"
    print df
    print "########## ########## ########## ##########"

    df_converted = convert_importance(df)
    print "########## Extended Data ##########"
    print df_converted
    print "########## ########## ########## ##########"

    # データの保存
    write_csv(OUTPUT_CSV, df_converted)
