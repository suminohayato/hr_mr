#!/usr/binenv python
# -*- coding: utf-8 -*-

class Add_stat_data(object):
    def __init__(self,df,dataset,target_feature,**kwargs):
        '''
        Examples
        Add_stat_data("result.csv","latest_data_CS3.csv",target_feature = "最新価格",maker = "メーカー名",car = "車種名", year = "年式", distance = "走行")
        '''
        self.target_feature = target_feature
        self.column_name = kwargs
        self.df = df
        self.dataset_org = dataset[[target_feature] + kwargs.values()]
        self.pbar = ProgressBar(maxval = self.df.shape[0])
        self.i = 0

    #datasetから特定のメーカー車種年式走行距離のものの価格分布を返す
    def get_filt_data(self,maker,car,year,distance):
        dataset_ = self.dataset_org
        for key in self.column_name.keys():
            if key == "distance":
                dataset_ = dataset_[dataset_[self.column_name[key]] <= eval(key) + 10000]
                dataset_ = dataset_[dataset_[self.column_name[key]] >= eval(key) - 10000]
            else:
                dataset_ = dataset_[dataset_[self.column_name[key]] == eval(key)]

        return dataset_[self.target_feature]

    #フィルタリングしたデータの相場価格
    def get_stat_data(self,maker,car,year,distance):
        self.i += 1
        self.pbar.update(self.i)

        price_distrib = self.get_filt_data(maker,car,year,distance)

        def max_likelihood(value):
            histgram = [value[value >= i*1e5][value < (i+1)*1e5].shape[0] for i in range(100)]
            str1 = ""
            for j in range(len(histgram)):
                if histgram[j] == max(histgram):
                    str1 = str(j*10)+"万円〜"+str((j+1)*10)+"万円"
            return str1

        if price_distrib.shape[0]:
            stat = price_distrib.describe()
            #max_likelihood_range = max_likelihood(price_distrib)

            #print stat["count"],int(stat["mean"]),int(stat["25%"]),int(stat["50%"]),int(stat["75%"]),max_likelihood_range
            return [stat["count"],int(stat["mean"]),int(stat["25%"]),int(stat["50%"]),int(stat["75%"])]#,max_likelihood_range]
        else:
            return [0 for i in range(5)]

    def main(self):
        if "Unnamed: 0" in self.df.columns:
            del self.df["Unnamed: 0"]
        if "Unnamed: 0.1" in self.df.columns:
            del self.df["Unnamed: 0.1"]
        print "adding count/mean/first quartile/median/third quartile..."

        stat_data_list = ["条件合致データ数","平均値","第一四分位数","中央値","第三四分位数"]#,"最頻価格帯"]

        for key in stat_data_list:
            self.df[key] = ""

        input_data = self.df[[self.column_name["maker"],self.column_name["car"],self.column_name["year"],self.column_name["distance"]]].as_matrix()

        self.df[stat_data_list] = [self.get_stat_data(*input_data[i]) for i in self.df.index]
        columns = list(self.df.keys())
        columns = columns[:2] + columns[-5:] + columns[2:-5]
        self.df = self.df.reindex(columns = columns)

        return self.df
