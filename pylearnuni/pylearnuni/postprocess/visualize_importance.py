import pickle
import pandas
import numpy

import data.process_data2 as process_data

np = numpy

if __name__ == '__main__':
    with open('model/best_mlp.pkl') as f:
        model = pickle.load(f)
    w = model.layers[0].get_weights().flatten()
    order = w.argsort()
    labels = process_data.load_datamat('test').__unipro_description
    labels = [labels[i] for i in order]
    df = pandas.DataFrame(data=w[order][np.newaxis, :], columns=labels)
    df = df.T
    df.to_csv('importance.csv')
