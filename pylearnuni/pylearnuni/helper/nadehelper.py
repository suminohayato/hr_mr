#!/usr/binenv python
# -*- coding: utf-8 -*-
import sys
import os
from NADEuni import NADEManager
import pandas
import copy
import yaml
import pylearnuni.utils.io_handler as io
def nadefiller(nadepath,target_feature):
    manager = NADEManager.NADEManager(nadepath)
    manager.sample_init()
    column_name_dict = io.read_yaml(nadepath + "/column_name_dict.yaml")
    datainfo = io.read_yaml(nadepath + "/data.info")
    column_name=datainfo["column_name"]
    def _nadefiller(df):
        #nadeが要求している型にあわせる
        df = df.reindex(columns = column_name)
        for val in column_name_dict.values():
            df[val] = df[val].dropna(how="all").fillna(value=0)
        buf = copy.deepcopy(df[target_feature])
        df_ = df
        df_.drop(target_feature, axis=1,inplace=True)
        #nadeに補完してもらう
        arr = manager.sample(df_.values,1)
        column_name_ = copy.deepcopy(column_name)
        column_name_.remove(target_feature)
        df1 = pandas.DataFrame(data = arr[:,:,0],index = df_.index, columns = column_name_)
        df1[target_feature] = buf
        df1.reindex(columns = column_name)
        return df1

    def inner(df):
        if df.shape[0] < 25001:
            return _nadefiller(df)
        else :
            return inner(df[:-25000]).append(_nadefiller(df[-25000:]))

    return inner

def nadetrain(helper):
    output = copy.deepcopy(helper.output_path)
    helper.set_output_path(helper.nade_path)
    validate_bin = helper.validate_bin
    helper.validate_bin = True

    helper.process_data()
    column_name_dict = helper.get_column_name_dict()
    helper.generate_dataset()

    os.system("cp "+ helper.features_yaml_path + " " + helper.nade_path + "/features.yaml")
    io.write_yaml(column_name_dict,helper.nade_path + "/column_name_dict.yaml")
    #helperの状態をもとにもどしておく
    helper.load_data()
    helper.set_output_path(output)
    helper.validate_bin = validate_bin


    os.system("python "+os.path.dirname(os.path.abspath(__file__))+"/../../NADEuni/NADEManager.py "+helper.nade_path)

