#!/usr/binenv python
# -*- coding: utf-8 -*-

import os

from hdf5helper import HDF5Helper as HDF5Helper
from pylearnuni.utils import io_handler as IO
from pylearnuni.preprocess.filter import Filter as Filter
from pylearnuni.preprocess.converter import Converter as Converter
from pylearnuni.preprocess.preprocess import Preprocess as Preprocess
from pylearnuni.train import dataset_handler as DatasetHandler
from pylearnuni.train import train_handler as TrainHandler
import nadehelper

class Helper(object):
    """ Machine Learning Helper class """

    SEED = 1234

    def __init__(self):
        self.chunk = False
        self.df = None
        self.reader = None
        self.store = None
        self.features = {}
        self.info = {}

        self.features_yaml_path = "./features.yaml"
        self.train_config_path = "./train_config.yaml"
        self.output_path = "./output/"
        self.raw_csv_path = "./data.csv"
        self.input_csv_path = "./data.csv"
        self.datainfo_path = os.path.join(self.output_path, "data.info")

        self.target_features = []
        #バージョンはgitのコミット番号をいれるようにしたい
        self.version = "1.0"

        self.train_size = None
        self.test_size = 5000
        self.valid_size = 5000
        self.batch_size = 1000
        self.chunk_size = 100000

        self.fillna = True
        self.nade = False
        self.nade_path = None
        self.validate_bin = False
    def check_output_path(self):
        IO.check_dirpath(self.output_path)

    def set_train_config_path(self, filepath):
        assert(os.path.isfile(filepath))
        self.train_config_path = filepath

    def set_output_path(self, output_path):
        self.output_path = output_path
        self.check_output_path()
        self.datainfo_path = os.path.join(self.output_path, "data.info")

    def set_nade_path(self, nade_path):
        print "消した"
        pass

    def set_test_size(self, test_size):
        self.test_size = test_size

    def set_valid_size(self, valid_size):
        self.valid_size = valid_size

    def set_chunk_size(self, chunk_size):
        self.chunk_size = chunk_size

    def load_data(self, filepath=None, chunk=None, chunk_size=None):
        if not filepath is None:
            self.input_csv_path = filepath
        assert(os.path.isfile(self.input_csv_path))

        if not chunk_size is None:
            self.chunk_size = chunk_size

        if not chunk is None:
            self.chunk = chunk

        if self.chunk:
            self.reader = IO.read_csv(self.input_csv_path, chunksize=self.chunk_size)
            self.df = None
        else:
            self.reader = None
            self.df = IO.read_csv(self.input_csv_path)

    def load_features(self, filepath=None):
        if not filepath is None:
            self.features_yaml_path = filepath
        self.features = IO.read_yaml(self.features_yaml_path)

    def get_column_name(self):
        raise NotImplementedError

    def _get_column_name(self, converter):
        set_names = [["騎手", "前回騎手", "前々回騎手", "前３回騎手"], ["人気", "前回人気", "前々回人気", "前３回人気"], ["馬番", "前回ｹﾞｰﾄ", "前々回ｹﾞｰﾄ", "前３回ｹﾞｰﾄ"]]
        int_names = ["人気", "前回人気", "前々回人気", "前３回人気", "馬番", "前回ｹﾞｰﾄ", "前々回ｹﾞｰﾄ", "前３回ｹﾞｰﾄ"]
        reader_ = IO.read_csv(self.input_csv_path, chunksize=self.chunk_size)
        list_ = list()
        for df in reader_:
            for x in Preprocess.get_column_name(df, self.features, converter):
                if "<" in x:
                    s, name = x[:-1].split("<")
                    if s in int_names and name.replace(".","").isdigit():
                        if "." in name:
                            name = name.split(".")[0]
                        x = s + "<" + name + ">"
                list_.append(x)

        for int_name in int_names:
            list_.append(int_name + "<0>")

        res = []
        for x in list_:
            if x not in res:
                res.append(x)

        # one hot化の時、時間（前回、前々回、前３回）で数が変わらないようにする
        for set_name in set_names:
            categorical_names = set([])
            for x in res:
                if "<" not in x:
                    continue
                s, name = x[:-1].split("<")
                if s in set_name:
                    categorical_names.add(name)
            for name in categorical_names:
                for s in set_name:
                    onehot_name = s + "<" + name + ">"
                    if onehot_name not in res:
                        res.append(onehot_name)

        res2 = []
        for key in self.features.keys():
            temp = []
            for x in res:
                s = x.split("<")[0]
                if s == key:
                    temp.append(x)
            res2.extend(sorted(temp))
        return res2

    def get_column_name_dict(self):
        raise NotImplementedError
        
    def _get_column_name_dict(self, converter):
        reader_ = IO.read_csv(self.input_csv_path, chunksize=100000)
        dict_ = {feature_name:set() for feature_name in self.features.keys()}
        for df in reader_:
            for feature_name, values in Preprocess.get_column_name_dict(df, self.features, converter).items():
                dict_[feature_name]= dict_[feature_name].union(values)
        return {feature_name : sorted(list(dict_[feature_name])) for feature_name in self.features.keys()}

    def clean_csv(self):
        raise NotImplementedError

    def _clean_csv(self, filepath=None):
        if not filepath is None:
            self.raw_csv_path = filepath
        assert(os.path.isfile(self.raw_csv_path))
        self.input_csv_path = os.path.join(self.output_path, "cleaned.csv")

        reader = IO.read_csv(self.raw_csv_path, chunksize=self.chunk_size)

        first_chunk = True
        for df in reader:
            df_ = self.clean_dataframe(df)
            if first_chunk:
                df_.to_csv(self.input_csv_path)
                first_chunk = False
            else:
                df_.to_csv(os.path.join(self.input_csv_path), mode="a", header=None)

    def generate_yaml(self):
        raise NotImplementedError

    def _generate_yaml(self, new_yaml_path, org_yaml_path, csv_path):
        df = IO.read_csv(csv_path)
        columns = df.columns
        yaml_ = IO.read_yaml(org_yaml_path)
        keys = yaml_.keys()
        #for key, value in yaml_:
        #    keys.append(key)
        for column in columns:
            if column not in keys and column.find('added')==0:
                yaml_[column] = {'feature_type': 'binary', 'dtype': 'int8'}
        IO.write_yaml(yaml_, new_yaml_path)

    def process_data(self):
        raise NotImplementedError

    def _process_data(self, dataframe_converter):
        if os.path.isfile(self.datainfo_path):
            return
        else:
            self.info["version"] = self.version
            self.info["input_csv_path"] = self.input_csv_path
            self.info["features_yaml_path"] = self.features_yaml_path

        if len(self.features) == 0:
            self.load_features(self.features_yaml_path)
        self.check_output_path()

        column_name = self.get_column_name()

        if self.chunk:
            self.info["data_type"] = "hdf5"
            # チャンクのデータを扱うとき
            filepath = os.path.join(self.output_path, "data.h5")
            self.store = HDF5Helper(filepath, column_name, self.target_features)
            index_ = []

            for df in self.reader:
                df_ = dataframe_converter(df)
                index_.extend(list(df_.index))
                self.store.append(df_)
            self.info["nvis"] = len(column_name) - len(self.target_features)
            self.info["column_name"] = column_name
            self.info["chunk_size"] = self.reader.chunksize
        else:
            self.info["data_type"] = "bin"
            if self.df is None:
                self.load_data()
            self.df = dataframe_converter(self.df)
            index_ = list(self.df.index)
            self.info["nvis"] = self.df.shape[1] - len(self.target_features)
            self.info["column_name"] = list(self.df.columns.values)
        IO.write_yaml(self.info, self.datainfo_path)

    def generate_csv(self):
        raise NotImplementedError


    def _generate_csv(self, dataframe_converter):
        self.check_output_path()

        reader = IO.read_csv(self.input_csv_path, chunksize=self.chunk_size)
        # チャンクのデータを扱うとき
        flag = 0
        for df in reader:
            df_ = dataframe_converter(df, False)
            if flag == 0:
                df_.to_csv(os.path.join(self.output_path, "test_dataset.csv.gz"),index=False,compression="gzip")
                flag = 1
            else:
                df_.to_csv(os.path.join(self.output_path, "test_dataset.csv.gz"), mode="a",index=False ,header=None,compression="gzip")

    def generate_dataset(self, test_size=None, valid_size=None, train_size=None, batch_size=None):
        if not test_size is None:
            self.test_size = test_size
        if not valid_size is None:
            self.valid_size = valid_size
        if not train_size is None:
            self.train_size = train_size
        if not batch_size is None:
            self.batch_size = batch_size
        if len(self.info) == 0:
            return
        if self.chunk:
            self.store.generate_dataset(self.output_path, test_size=self.test_size, valid_size=self.valid_size, train_size=self.test_size, batch_size=self.batch_size)
        else:
            DatasetHandler.generate_dataset(self.df, self.output_path,test_size=self.test_size, valid_size=self.valid_size, train_size=self.train_size, batch_size=self.batch_size)

    def normalize(self):
        print "消した"

    def clean_dataframe(self, df):
        raise NotImplementedError

    def convert_dataframe(self, df,convert=True):
        raise NotImplementedError

    def train(self, best_param_path=None, **kwargs):
        self._tprain(best_param_path, **kwargs)

    def _train(self, best_param_path=None, **kwargs):
        print "消した"
        pass
