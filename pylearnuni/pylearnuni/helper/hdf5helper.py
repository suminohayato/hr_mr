#!/usr/binenv python
# -*- coding: utf-8 -*-

import os
import h5py
import pandas
import numpy

from pylearnuni.preprocess.preprocess import Preprocess as Preprocess
from normalize_helper import NormalizeHelper


class HDF5Helper(object):

    def __init__(self, filepath, columns, target_names):
        assert(not os.path.exists(filepath))
        self.filepath = filepath
        self.hdf5 = h5py.File(filepath)

        self.columns = columns
        self.N_columns = len(columns)
        self.N_targets = len(target_names)

        self.target_index = [columns.index(name) for name in target_names]
        self.target_mask = numpy.asarray([False] * self.N_columns)
        for index in self.target_index:
            self.target_mask[index] = True

        self.dataset_X = self.hdf5.create_dataset('X', (0, self.N_columns - self.N_targets), maxshape=(None, self.N_columns - self.N_targets), chunks=(1000, self.N_columns - self.N_targets), dtype='float32', compression="gzip", compression_opts=1)
        self.dataset_y = self.hdf5.create_dataset('y', (0, self.N_targets), maxshape=(None, self.N_targets), chunks=(1000, self.N_targets), dtype='float32', compression="gzip", compression_opts=1)

    def __del__(self):
        try:
            self.hdf5.close()
        except:
            pass

    def append(self, df):
        for s in (set(df.keys()) - set(self.columns)):
            print s.decode("utf-8")
        assert(set(df.keys()).issubset(set(self.columns)))

        df_ = pandas.DataFrame(columns=self.columns)
        df_ = pandas.concat([df_, df], copy=False)
        df_ = Preprocess.fill_nan_with_0(df_)
        df_ = df_.reindex(columns=self.columns, copy=False)

        y_ = df_.iloc[:, self.target_mask].as_matrix()
        X_ = df_.iloc[:, ~self.target_mask].as_matrix()

        pos = len(self.dataset_y)
        assert(pos == self.dataset_X.shape[0])

        self.dataset_X.resize(pos + len(X_), axis=0)
        self.dataset_y.resize(pos + len(y_), axis=0)
        self.dataset_X[pos:, :] = X_[:, :]
        self.dataset_y[pos:, :] = y_[:, :]

        self.hdf5.flush()

    def generate_dataset(self, dirpath=".", test_size=5000, valid_size=5000, train_size=None, batch_size=1000):

        data_size = len(self.dataset_y)
        if train_size:
            assert(data_size > test_size + valid_size + train_size)
            assert(train_size % batch_size == 0)
            assert(test_size % batch_size == 0)
            assert(valid_size % batch_size == 0)
        else:
            assert(data_size > test_size + valid_size + batch_size)
            assert(test_size % batch_size == 0)
            assert(valid_size % batch_size == 0)

        nrow = batch_size * (len(self.dataset_y) / batch_size)
        self.dataset_X.resize(nrow, axis=0)
        self.dataset_y.resize(nrow, axis=0)

        test_X = self.hdf5.create_dataset('test/X', (test_size, self.N_columns - self.N_targets), maxshape=(test_size, self.N_columns - self.N_targets), chunks=(batch_size, self.N_columns - self.N_targets), dtype='float32', compression="gzip", compression_opts=1)
        test_y = self.hdf5.create_dataset('test/y', (test_size, self.N_targets), maxshape=(test_size, self.N_targets), chunks=(batch_size, self.N_targets), dtype='float32', compression="gzip", compression_opts=1)
        valid_X = self.hdf5.create_dataset('valid/X', (valid_size, self.N_columns - self.N_targets), maxshape=(valid_size, self.N_columns - self.N_targets), chunks=(batch_size, self.N_columns - self.N_targets), dtype='float32', compression="gzip", compression_opts=1)
        valid_y = self.hdf5.create_dataset('valid/y', (valid_size, self.N_targets), maxshape=(valid_size, self.N_targets), chunks=(batch_size, self.N_targets), dtype='float32', compression="gzip", compression_opts=1)

        pos = len(self.dataset_y)
        test_X[:, :] = self.dataset_X[pos - test_size:, :]
        test_y[:, :] = self.dataset_y[pos - test_size:, :]
        self.dataset_X.resize(pos - test_size, axis=0)
        self.dataset_y.resize(pos - test_size, axis=0)
        pos = len(self.dataset_y)
        valid_X[:, :] = self.dataset_X[pos - valid_size:, :]
        valid_y[:, :] = self.dataset_y[pos - valid_size:, :]
        self.dataset_X.resize(pos - valid_size, axis=0)
        self.dataset_y.resize(pos - valid_size, axis=0)
        self.hdf5.flush()
        self.hdf5.close()

    def normalize(self):
        y = NormalizeHelper(self.filepath, "y")
        y_mean, y_var = y.normalize()
        X = NormalizeHelper(self.filepath, "X")
        X_mean, X_var = X.normalize()

        valid_y = NormalizeHelper(self.filepath, "valid/y")
        valid_y.mean = y_mean
        valid_y.var = y_var
        valid_y.normalize()
        valid_X = NormalizeHelper(self.filepath, "valid/X")
        valid_X.mean = X_mean
        valid_X.var = X_var
        valid_X.normalize()

        test_y = NormalizeHelper(self.filepath, "test/y")
        test_y.mean = y_mean
        test_y.var = y_var
        test_y.normalize()
        test_X = NormalizeHelper(self.filepath, "test/X")
        test_X.mean = X_mean
        test_X.var = X_var
        test_X.normalize()

        return {"y_mean": y_mean.tolist(), "y_var": y_var.tolist(), "X_mean": X_mean.tolist(), "X_var": X_var.tolist()}
