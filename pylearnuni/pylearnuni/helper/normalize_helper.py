#!/usr/binenv python
# -*- coding: utf-8 -*-

import os
import h5py
import pandas
import numpy


class NormalizeHelper(object):
    min_var = 0.000001

    def __init__(self, filepath, label):
        assert(os.path.exists(filepath))
        self.filepath = filepath
        self.hdf5 = h5py.File(filepath, 'r+')
        self.label = label
        self.dataset = self.hdf5[self.label]
        self.nrow = self.dataset.shape[0]
        self.ncolumn = self.dataset.shape[1]
        self.summation = numpy.zeros(self.ncolumn)
        self.mean = numpy.zeros(self.ncolumn)
        self.var = numpy.zeros(self.ncolumn)
        self.chunk_size = 1000

    def __del__(self):
        try:
            self.hdf5.close()
        except:
            pass

    def get_mean(self):
        if not self.mean.any():
            self._get_mean()
        return self.mean

    def _get_mean(self):
        count = 0
        while self.chunk_size * count < self.nrow:
            self.summation += numpy.sum(self.dataset[(self.chunk_size * count): (self.chunk_size * (count + 1)), :], axis=0)
            count += 1
        else:
            self.mean = self.summation / float(self.nrow)

        return self.mean

    def get_var(self):
        if not self.var.any():
            self._get_var()
        return self.var

    def _get_var(self):
        if not self.mean.any():
            self._get_mean()
        tmp = numpy.zeros(self.ncolumn)
        count = 0
        while self.chunk_size * count < self.nrow:
            var_ = self.dataset[self.chunk_size * count: self.chunk_size * (count + 1), :] - self.mean
            var_ = numpy.square(var_)
            tmp += numpy.sum(var_, axis=0)
            count += 1
        else:
            self.var = tmp / float(self.nrow)

        return self.var

    def normalize(self):
        if not self.var.any():
            self._get_var()
        count = 0
        while self.chunk_size * count < self.nrow:
            tmp_ = (self.dataset[self.chunk_size * count: self.chunk_size * (count + 1), :] - self.mean) / (numpy.sqrt(self.var) + self.min_var)
            self.dataset[self.chunk_size * count: self.chunk_size * (count + 1), :] = tmp_

            self.hdf5.flush()
            count += 1
        else:
            self.summation = numpy.zeros(self.ncolumn)
            mean = self.mean
            self.mean = numpy.zeros(self.ncolumn)
            var = self.var
            self.var = numpy.ones(self.ncolumn)
        self.hdf5.close()

        return mean, var
