#!/usr/binenv python
# -*- coding: utf-8 -*-

import os
import sys
import h5py
import pandas
import numpy

from hdf5helper import HDF5Helper as HDF5Helper
from pylearnuni.utils import io_handler as IO
from pylearnuni.preprocess.filter import Filter as Filter
from pylearnuni.preprocess.converter import Converter as Converter
from pylearnuni.preprocess.preprocess import Preprocess as Preprocess
from pylearnuni.train import dataset_handler as DatasetHandler
from pylearnuni.train import train_handler as TrainHandler


class DistHelper(object):

    def __init__(self):
        self.ipaddrs = []
        self.queue = []

    def add_server(self, ipaddr):
        self.ipaddrs.append(ipaddr)

    def add_servers(self, ipaddrs):
        self.ipaddrs.extend(ipaddrs)

    def add_task(self, task):
        self.queue.append(task)

    def add_tasks(self, tasks):
        self.queue.extend(tasks)

    def send_data(self, data

    def run(self):


class Helper(object):
    """ Machine Learning Helper class """

    SEED = 1234

    def __init__(self):
        self.chunk = False
        self.df = None
        self.reader = None
        self.store = None
        self.features = {}
        self.info = {}

        self.input_csv_path = "./data.csv"
        self.features_yaml_path = "./features.yaml"
        self.train_config_path = "./train_config.yaml"
        self.output_path = "./output/"
        self.datainfo_path = os.path.join(self.output_path, "data.info")

        self.target_feature = ""
        self.version = "1.0"

    def check_output_path(self):
        if not os.path.isdir(self.output_path):
            os.makedirs(self.output_path)

    def set_train_config_path(self, filepath):
        assert(os.path.isfile(filepath))
        self.train_config_path = filepath

    def set_output_path(self, output_path):
        self.output_path = output_path
        self.check_output_path()
        self.datainfo_path = os.path.join(self.output_path, "data.info")

    def load_data(self, filepath=None, chunk=False, chunk_size=100000):
        if not filepath is None:
            self.input_csv_path = filepath
        assert(os.path.isfile(self.input_csv_path))

        if chunk:
            self.chunk = True
            self.reader = IO.read_csv(self.input_csv_path, chunksize=chunk_size)
            self.df = None
        else:
            self.reader = None
            self.df = IO.read_csv(self.input_csv_path)

    def load_features(self, filepath=None):
        if not filepath is None:
            self.features_yaml_path = filepath
        self.features = IO.read_yaml(self.features_yaml_path)

    def get_column_name(self):
        return _get_column_name(Converter)

    def _get_column_name(self, converter):
        reader_ = IO.read_csv(self.input_csv_path, chunksize=100000)
        set_ = set()
        for df in reader_:
            for x in Preprocess.get_column_name(df, self.features, converter):
                set_.add(x)
        return sorted(list(set_))

    def process_data(self):
        self._process_data(self.convert_dataframe)

    def _process_data(self, dataframe_converter):
        if os.path.isfile(self.datainfo_path):
            return
        else:
            self.info["version"] = self.version
            self.info["input_csv_path"] = self.input_csv_path
            self.info["features_yaml_path"] = self.features_yaml_path

        if len(self.features) == 0:
            self.load_features(self.features_yaml_path)
        self.check_output_path()

        column_name = self.get_column_name()

        if self.chunk:
            self.info["data_type"] = "hdf5"
            # チャンクのデータを扱うとき
            filepath = os.path.join(self.output_path, "data.h5")
            self.store = HDF5Helper(filepath, column_name, self.target_feature)
            index_ = []

            for df in self.reader:
                df_ = dataframe_converter(df)
                index_.extend(list(df_.index))
                self.store.append(df_)
            self.info["nvis"] = len(column_name) - 1
            self.info["column_name"] = column_name
            self.info["chunk_size"] = self.reader.chunksize
        else:
            self.info["data_type"] = "bin"
            if self.df is None:
                self.load_data()
            self.df = dataframe_converter(self.df)
            index_ = list(self.df.index)
            self.info["nvis"] = self.df.shape[1] - 1
            self.info["column_name"] = list(self.df.columns.values)
        IO.write_yaml(self.info, self.datainfo_path)

    def generate_dataset(self):
        if len(self.info) == 0:
            return
        if self.chunk:
            self.store.generate_dataset(self.output_path)
        else:
            DatasetHandler.generate_dataset(self.df, self.output_path)

    def convert_dataframe(self):
        # 範囲外の値を除外
        df = Filter.filter(df)
        # 行をシャッフル
        df = Preprocess.shuffle_index(df, self.SEED)
        # 機械学習に読める形に変換
        df = Preprocess.convert_dataframe(df, self.features, Converter)
        # Nanを0で埋める
        df = Preprocess.fill_nan_with_0(df)
        # 何をしている？
        Preprocess.validate_data(df)
        return df

    def train(self, best_param_path=None, **kwargs):
        self.check_output_path()
        if len(self.info) == 0:
            self.info = IO.read_yaml(self.datainfo_path)
        params={}
        params["nvis"] = self.info["nvis"]
        params["column_name"] = self.info["column_name"]

        if best_param_path is None:
            params["output_path"] = self.output_path
        else:
            params["output_path"] = best_param_path

        params["target_feature"] = self.target_feature

        if "batch_size" in kwargs.keys():
            params["batch_size"] = kwargs["batch_size"]
        else:
            params["batch_size"] = 100

        if self.info["data_type"] in 'hdf5':
            params["dataset_path"] = os.path.join(self.output_path, "data.h5")
        elif self.info["data_type"] in 'bin':
            params["dataset_path"] = self.output_path
        else:
            raise ValueError

        TrainHandler.main(self.train_config_path, **params)
