#!/usr/bin/env python
# coding: utf-8

import os
import pandas
import numpy
import logging


from pylearnuni.utils import io_handler as IO
from pylearnuni.utils import encoding_converter as enc


def fetch_dataset(dataset_name, dataset_path, **kwargs):
    dataset_name = dataset_name.encode('utf-8')
    dataset_path = dataset_path.encode('utf-8')
    kwargs = enc.encode_container(kwargs, 'utf-8')

    root, ext = os.path.splitext(dataset_path)
    if ext in ['.bin']:
        print "please wait until implement"
        dataset = fetch_bin(dataset_name, dataset_path, **kwargs)
    elif ext in ['.hdf5', '.h5']:
        dataset = fetch_hdf5(dataset_name, dataset_path, **kwargs)
    elif not ext:
        print ext
        print root
        dataset = fetch_bin(dataset_name, dataset_path, **kwargs)
    else:
        raise ValueError

    return dataset


def fetch_hdf5(dataset_name, dataset_path, **kwargs):
    assert(os.path.isfile(dataset_path))
    if dataset_name == "train":
        return HDF5Dataset(filename=dataset_path, X="X", y="y")
    else:
        return HDF5Dataset(filename=dataset_path, X=dataset_name + "/X", y=dataset_name + "/y")


def fetch_bin(dataset_name, dataset_path, target_feature, memmap=False, **kwargs):
    assert(os.path.isdir(dataset_path))
    if memmap:
        read_bin_func = IO.read_memmap
    else:
        read_bin_func = IO.read_bin
    datamat = read_bin_func(os.path.join(dataset_path, dataset_name + ".bin"))
    info = IO.read_yaml(os.path.join(dataset_path, dataset_name + ".info"))
    datamat = datamat.reshape(info["shape"])
    column_index = info["column_index"]

    target_mask = numpy.asarray([column_name == target_feature for column_name in column_index])

    X = datamat[:, ~target_mask]
    y = datamat[:, target_mask]

    return DenseDesignMatrix(X=X[:, :], y=y[:, :])


def generate_dataset(df, dirpath=".", test_size=50000, valid_size=5000, train_size=None, batch_size=1000, memmap=False):
    data_size = len(df)
    if train_size:
        assert(data_size > test_size + valid_size + train_size)
        assert(train_size % batch_size == 0)
        assert(test_size % batch_size == 0)
        assert(valid_size % batch_size == 0)
    else:
        assert(data_size > test_size + valid_size + batch_size)
        assert(test_size % batch_size == 0)
        assert(valid_size % batch_size == 0)

    q = int((data_size - test_size - valid_size) / batch_size)
    train_size = q * batch_size

    test_df = df[: test_size]
    valid_df = df[test_size: test_size + valid_size]
    train_df = df[test_size + valid_size: test_size + valid_size + train_size]

    generate_bin(test_df, "test", dirpath, memmap)
    generate_bin(valid_df, "valid", dirpath, memmap)
    generate_bin(train_df, "train", dirpath, memmap)


def generate_bin(df, dataset_name, dirpath, memmap=False):
    """
pandasデータフレームの情報を保存する関数。
データ部分は「〜.bin」,　大きさ・列名・行名の情報は.「〜.info」に保存される。
「〜.bin」はnumpyのtofile関数で作られるフォーマットで保存される。
「〜.info」はyamlファイルとして、保存される。中身は、[shape,column_index,row_index]のキーを持ったdict。
inputs:
  df :           pandas.DataFrame 保存したいデータフレーム
  dataset_name : str              "test"とすると、"test.bin"等の名前で保存される。
 dirpath:   str              出力の保存先フォルダ
output:
  無し
    """
    logging.info("generate bin file name:{0} shape:{1}".format(dataset_name, df.shape))
    datamat = df.as_matrix()

    info = {}
    info["shape"] = datamat.shape
    info["column_index"] = list(df.keys())

    if memmap:
        write_bin_func = IO.write_memmap
    else:
        write_bin_func = IO.write_bin

    bin_file = os.path.join(dirpath, dataset_name + ".bin")
    write_bin_func(datamat, bin_file)

    yaml_file = os.path.join(dirpath, dataset_name + ".info")
    IO.write_yaml(info, yaml_file)
    logging.info("generate bin file finished")
