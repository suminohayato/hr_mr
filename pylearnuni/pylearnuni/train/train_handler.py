#!/usr/bin/env python
# coding: utf-8

import os
import numpy

from pylearnuni.train import dataset_handler as DatasetHandler
from pylearnuni.utils import io_handler as IO


def main(train_config_path, output_path, **params):
    params["output_path"] = output_path
    best_param_path = IO.check_dirpath(os.path.join(output_path, "best_param.pkl"))

    if not os.path.isfile(best_param_path):
        train(train_config_path, **params)

    dataset = DatasetHandler.fetch_dataset(dataset_name="test", **params)
    actual,predict =  get_actual_and_predict(dataset, best_param_path)

    lines = ["##### Test Dataset"]
    lines.extend( print_result(actual,predict) )

    save_result_csv(predict,**params)
    calc_each_katashiki_rmse(params["output_path"])

    lines.append("##### Train Dataset")
    dataset = DatasetHandler.fetch_dataset(dataset_name="train", **params)
    actual,predict =  get_actual_and_predict(dataset, best_param_path,verbose=False)
    lines.extend(print_result(actual,predict))
    with open(os.path.join(output_path,"result.csv"), "w") as file:
        for i in lines:
            file.write(i)


def train(train_config_path, **params):
    yaml_ = open(train_config_path, 'r').read() % (params)
    train_ = yaml_parse.load(yaml_)
    train_.main_loop()


def generate_predictor(best_param_file):
    model = IO.read_pickle(best_param_file)
    input_ = theano.tensor.matrix('input')
    prediction = model.fprop(input_)
    predictor = theano.function([input_], prediction)
    return predictor


# yの値の求め方がガリバーのプロジェクトに依存しているため，改善の余地あり
def get_actual_and_predict(dataset, best_param_path, batch_size=100,verbose = True):
    predictor = generate_predictor(best_param_path)

    y_actual, y_predict = [], []
    nbatches = numpy.round(len(dataset.X) / batch_size)

    assert(len(dataset.X) % batch_size == 0)

    for idx in xrange(nbatches):
        X_ = dataset.X[idx * batch_size: (idx + 1) * batch_size]
        y_ = dataset.y[idx * batch_size: (idx + 1) * batch_size]

        y_actual_ = numpy.asarray(numpy.exp(y_), dtype='int32')

        predict_ = predictor(numpy.asarray(X_, dtype='float32')).flat
        y_predict_ = numpy.asarray(numpy.round(numpy.exp(predict_)), dtype='int32')

        y_actual.append(y_actual_)
        y_predict.append(y_predict_)
        if verbose:
            for p, a in zip(y_predict_.flat, y_actual_.flat):
                print p, ' -> ', a

    y_actual = numpy.asarray(y_actual, dtype='float').flatten()
    y_predict = numpy.asarray(y_predict, dtype='float').flatten()

    return y_actual, y_predict

def cal_regression(dataset, best_param_path, batch_size=100,verbose = True):
    actual,predict =  get_actual_and_predict(dataset,best_param_path,batch_size,verbose)
    print_result(actual,predict)
    return actual,predict


def print_result(actual, predict):
    _actual = actual.astype(numpy.float64)
    _predict = predict.astype(numpy.float64)
    results = []
    results.append('#cases: %g' % len(_actual) )
    rmse = numpy.sqrt( ( (_actual - _predict) ** 2).mean() )
    results.append('RMSE: %g' % rmse)
    mape = numpy.abs( (_actual - _predict) / _actual ).mean() * 100
    results.append( "MAPE: %g %%" % mape)
    err_ave = numpy.abs(_actual - _predict).mean()
    results.append( "誤差の絶対値の平均: %g" % err_ave)
    error_rates = numpy.abs(_actual - _predict) / _actual
    mer = numpy.median(error_rates)
    results.append( "MER %g %%" % mer)
    rate5 = 1.0 * numpy.count_nonzero(error_rates <= 0.05) / len(error_rates)
    results.append( "誤差5%%以内の割合 %g %%" % ( rate5 * 100 ))
    rate10 = 1.0 * numpy.count_nonzero(error_rates <= 0.1) / len(error_rates)
    results.append("誤差10%%以内の割合 %g %%" % ( rate10 * 100 ))
    rate20 = 1.0 * numpy.count_nonzero(error_rates <= 0.2) / len(error_rates)
    results.append("誤差20%%以内の割合 %g %%" % ( rate20 * 100 ))
    for i in results:
        print i
    return i



# 出力するtest_dataset.csvが固定
# もっといい方法があるはず
def save_result(best_param_path, **params):
    dataset = DatasetHandler.fetch_dataset(dataset_name="test", **params)
    actual,predict =  get_actual_and_predict(dataset, best_param_path)
    cal_regression(actual,predict)


def save_result_csv(predict,csv_path=None,**params):
    target_feature = params["target_feature"]
    dataset_path = params["dataset_path"]
    output_path = params["output_path"]
    if csv_path == None:
        csv_path=output_path

    dataset = DatasetHandler.fetch_dataset(dataset_name="test", dataset_path=dataset_path, target_feature=target_feature)
    train_size = DatasetHandler.fetch_dataset(dataset_name="train", dataset_path=dataset_path, target_feature=target_feature).y.shape[0]
    valid_size = DatasetHandler.fetch_dataset(dataset_name="valid", dataset_path=dataset_path, target_feature=target_feature).y.shape[0]
    test_size = dataset.y.shape[0]

    if not os.path.exists(os.path.join(csv_path, "test_dataset.csv.gz")):
        print "test_dataset.csv is not found"
        return
    df = IO.read_csv_part(os.path.join(csv_path, "test_dataset.csv.gz"), train_size + valid_size + 1, train_size + valid_size + test_size)
    columns = df.columns.values.tolist()
    columns.remove(target_feature)
    predict_name = target_feature + "(predict)"
    df[predict_name] = predict

    columns = [predict_name, target_feature] + columns
    df = df.reindex(columns=columns)

    df.to_csv(os.path.join(output_path, "result.csv"),index = False)


def calc_each_katashiki_rmse(csv_dir="./"):
    import pandas
    def RMSE(df):
        a = (df["RAKU_RYUSATSU_KAKAKU(predict)"]-df["RAKU_RYUSATSU_KAKAKU"])**2
        return int(numpy.round(numpy.sqrt(a.mean())))
    data = pandas.read_csv(os.path.join(csv_dir,"result.csv"))
    f = open(os.path.join(csv_dir,"RMSE_KATASHIKI.csv"),"w")
    f.write("メーカー,車種,型式,RMSE,台数\r\n")
    maker = sorted(data["MAKER_NAME"].drop_duplicates().tolist())
    for mk in maker:
        mkdata = data[data["MAKER_NAME"]==mk]
        shashu = sorted(mkdata["SHASHU_NAME"].drop_duplicates().tolist())
        for ss in shashu:
            ssdata = mkdata[mkdata["SHASHU_NAME"]==ss]
            katashiki = sorted(ssdata["KATASHIKI"].drop_duplicates().tolist())
            for ks in katashiki:
                ksdata = ssdata[ssdata["KATASHIKI"]==ks]
                f.write(mk+","+ss+","+ks+","+str(RMSE(ksdata))+","+str(ksdata.shape[0])+"\r\n")
    f.close()
