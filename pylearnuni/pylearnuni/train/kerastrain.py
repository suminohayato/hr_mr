#!/usr/bin/env python
#  -*- coding: utf-8 -*-

from __future__ import division
import numpy
import matplotlib.pyplot as plt
import numpy
import os
import keras
import keras.models
from keras.models import Sequential
from keras.layers.core import Dense, Activation
from keras.layers.normalization import BatchNormalization
import h5py
import sys
import yaml
import pylearnuni.train.train_handler as train_handler

#TODO: big data (train_on_batch or fit_generator or fit)
#TODO: evaluateの部分を分ける


def chunk_generator(train_set,chunk_size,allow_leftover):
    X=train_set[0]
    y=train_set[1]
    whole_size=X.shape[0]
    for starting_point in range(0,whole_size,chunk_size):
        ending_point=min(starting_point+chunk_size,whole_size)
        if (allow_leftover==False) & (ending_point-starting_point!=chunk_size):
            break
        yield (X[starting_point:ending_point],y[starting_point:ending_point])

def chunk_generator2(train_set,big_chunk_size,chunk_size):
    while True:
        for bigchunk in chunk_generator(train_set,big_chunk_size,True):
            for chunk in chunk_generator(bigchunk,chunk_size,False):
                yield chunk

def get_chunk_num(train_set,chunk_size,allow_leftover):
    whole_size=train_set[0].shape[0]
    if allow_leftover:
        return whole_size
    else:
        return (whole_size//chunk_size)*(chunk_size)

def keras_train(output_path,train_set,test_set,model,callbacks,validation_split,nb_epoch,batch_size,memo=None):
    print "これは古いバージョンの関数です。一応しばらく残しておきます。keras_train2を使用してください！"
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    with open(os.path.join(output_path,"model.yaml"),"w") as o:
        o.write(model.to_yaml())

    print "learning..."
    history=model.fit(train_set[0],train_set[1],verbose=1,shuffle='batch',callbacks=callbacks,validation_split=validation_split,nb_epoch=nb_epoch,batch_size=batch_size)

    print "evaluating..."
    testloss=numpy.sqrt(model.evaluate(test_set[0],test_set[1]))
    with open(os.path.join(output_path,"RMSE:{}".format(testloss)),"w") as o:
        pass

    with open(os.path.join(output_path,"history"),"w") as o:
        o.write( str( numpy.vstack((history.history["loss"],history.history["val_loss"])).T ) )
        o.write("\n")
        o.write("final train RMSE:{}".format( numpy.sqrt(history.history["loss"][-1]) ))
        o.write("\n")
        o.write("final valid RMSE:{}".format( numpy.sqrt(history.history["val_loss"][-1]) ))
        o.write("\n")
        o.write("final test RMSE:{}".format(testloss))

    savename='best_param.hdf5'
    model.save_weights(os.path.join(output_path,savename))

    if memo:
        with open(os.path.join(output_path,"memo"),"w") as o:
            o.write(memo)

def keras_train2(output_path,train_set,valid_set,model,callbacks,nb_epoch,batch_size,load_chunk_size,save_name="best_param.hdf5",memo=None):
    #trainはhdf5,valid_setはnumpy array型！ assert する！ yがflatten状態であることもassert
    assert(load_chunk_size%batch_size==0)#倍数にしてね
    assert(not os.path.exists(output_path))#keras_output_pathが存在します。まず消してください

    os.makedirs(output_path)

    with open(os.path.join(output_path,"model.yaml"),"w") as o:
        o.write(model.to_yaml())

    print "learning..."

    generator=chunk_generator2(train_set,big_chunk_size=load_chunk_size,chunk_size=batch_size)
    chunk_size=get_chunk_num(train_set,chunk_size=batch_size,allow_leftover=False)
    history=model.fit_generator(generator=generator,samples_per_epoch=chunk_size,verbose=1,callbacks=callbacks,validation_data=valid_set,nb_epoch=nb_epoch)

    model.save_weights(os.path.join(output_path,save_name))

    history_path=os.path.join(output_path,"history")
    os.makedirs(history_path)

    numpy.array(history.history["loss"]).tofile(os.path.join(history_path,"train_loss"))
    numpy.array(history.history["val_loss"]).tofile(os.path.join(history_path,"val_loss"))

    if memo:
        with open(os.path.join(output_path,"memo"),"w") as o:
            o.write(memo)


def evaluate(output_path,test_set,weight_file_name="best_param.hdf5"):
    model=load_model(output_path=output_path,weight_file_name=weight_file_name)

    print "evaluating..."
    testloss=numpy.sqrt(model.evaluate(test_set[0],test_set[1]))
    with open(os.path.join(output_path,"RMSE:{}".format(testloss)),"w") as o:
        pass

def evaluate2(keras_output_path, test_set, parent_output_path, target_feature, weight_file_name="best_param.hdf5"):
    ###test_setに入れるデータは絶対にdata.h5の["test"]のものをつかわないといけない！
    model=load_model(output_path=keras_output_path, weight_file_name=weight_file_name)
    print "evaluating..."
    X=test_set[0]
    y=test_set[1].flatten()
    predict=model.predict(test_set[0]).flatten()
    testloss=numpy.sqrt(((predict-y)**2).mean())
    with open(os.path.join(keras_output_path, "RMSE:{}".format(testloss)), "w") as o:
        pass
    train_handler.save_result_csv(predict=predict, csv_path=parent_output_path, target_feature=target_feature, dataset_path=os.path.join(parent_output_path,"./data.h5"), output_path=keras_output_path)
    train_handler.print_result(actual=y,predict=predict)


def load_model(output_path,weight_file_name="best_param.hdf5"):
    """
    outputpath:kerasのoutput_path （model.yamlとかbest_param.hdf5とか入ってるディレクトリパス）
    modelを学習後の状態にして返す。
    返ってきたmodelでmodel.predictすれば予測ができる。
    """
    model=keras.models.model_from_yaml(open( os.path.join(output_path,"model.yaml") ))
    model.compile(loss='mse', optimizer='rmsprop')
    model.load_weights(os.path.join(output_path,weight_file_name))
    return model