#!/usr/bin/env python
# coding: utf-8


import os
import gzip
import boto3
import logging
import tempfile
from copy import deepcopy
from datetime import datetime

log = logging.getLogger(__name__)


class MonitorBasedSaveBestToS3(MonitorBasedSaveBest):

    def __init__(self, s3_path=None, **kwargs):
        super(MonitorBasedSaveBestToS3, self).__init__(**kwargs)
        if s3_path is None:
            if os.path.isabs(self.save_path):
                drive, tail = os.path.split(os.path.dirname(self.save_path))
                self.s3_path = os.path.join("train", tail)
            else:
                self.s3_path = os.path.join("train", os.path.dirname(self.save_path))
        else:
            self.s3_path = s3_path

    def on_monitor(self, model, dataset, algorithm):
        monitor = model.monitor
        channels = monitor.channels
        channel = channels[self.channel_name]
        val_record = channel.val_record
        new_cost = val_record[-1]

        temp_dir = tempfile.mkdtemp()
        temp_file = os.path.join(temp_dir, "tmp.pkl")
        serial.save(temp_file, model, on_overwrite='backup')

        f_in = open(temp_file, 'rb')
        f_out = gzip.open(temp_file + ".gz", 'wb')
        f_out.writelines(f_in)
        f_out.close()
        f_in.close()
        os.remove(temp_file)
        temp_file = temp_file + ".gz"

        s3_client = boto3.client('s3')
        # Upload the file to S3
        s3_client.upload_file(temp_file, 'pylearnuni', os.path.join(self.s3_path, "params.pkl"))

        if self.coeff * new_cost < self.coeff * self.best_cost and monitor._epochs_seen >= self.start_epoch:
            self.best_cost = new_cost
            # Update the tag of the model object before saving it.
            self._update_tag(model)
            if self.store_best_model:
                self.best_model = deepcopy(model)
            if self.save_path is not None:
                with log_timing(log, 'Saving to ' + self.save_path):
                    serial.save(self.save_path, model, on_overwrite='backup')

                    filename = datetime.now().strftime("%Y%m%d-%H%M%S") + ".pkl.gz"
                    s3_client.upload_file(temp_file, 'pylearnuni', os.path.join(self.s3_path, "best_params", filename))
                    s3_client.upload_file(temp_file, 'pylearnuni', os.path.join(self.s3_path, "best_params.pkl.gz"))

        os.remove(temp_file)
        os.rmdir(temp_dir)
