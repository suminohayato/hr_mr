#!/usr/binenv python
# -*- coding: utf-8 -*-

import pandas

# 全角半角スペースをアンダーバーに変換


def erase_space(string):
    return string.replace(" ", "_").replace("　", "_")


def add_product_column(df, column_name_A, column_name_B, column_name_AXB):
    df[column_name_AXB] = df[column_name_A] + "_X_" + df[column_name_B]
    return df

# カラムから値の種類を抽出


def get_values_from_column(date_column):
    return date_column.dropna().drop_duplicates()

# 交差項の追加


def make_cross_term(df, target_feature, target_features_for_cross_term):
    cross_term_features = []
    # ”target_feature”の値の種類を取得
    target_feature_values = get_values_from_column(df[target_feature])
    N_feature = len(target_feature_values)

    # 交差項を追加
    for index, val in enumerate(target_feature_values):
        print str(index) + " / " + str(N_feature), "\r",  # デバッグ用
        for feature in target_features_for_cross_term:
            if 1 in df.loc[df[target_feature] == val, feature].values:
                # 新しく追加する交差項の行の名前
                cross_term_name = erase_space(val + "_x_" + feature)
                cross_term_features.append(cross_term_name)
                # 新しい列を0で初期化
                df[cross_term_name] = 0

    # 交差項の条件を満たすものにフラグを立てる
    print "df.iteraterows() start"  # デバッグ用
    N_row = len(df)
    for index, row in df.iterrows():
        print str(index) + " / " + str(N_row), "\r",  # デバッグ用
        for feature in target_features_for_cross_term:
            if row[feature] == 1:
                df.loc[index, erase_space(row[target_feature] + "_x_" + feature)] = 1
