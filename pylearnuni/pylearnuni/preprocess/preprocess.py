#/usr/binenv python
# -*- coding: utf-8 -*-

import sys
import pandas
import numpy
from collections import OrderedDict

class Preprocess(object):
    shuffle = True
    convert = True

    @classmethod
    def fill_nan_with_0(cls, df):
        df.fillna(0, inplace=True)
        return df

    @classmethod
    def validate_dataframe(cls, df):
        for feature in df.keys():
            if df[feature].dtype == 'O' :
                print feature
            assert(df[feature].dtype != 'O')

    @classmethod
    def shuffle_index(cls, df, seed):
        if cls.shuffle:
            return cls._shuffle_index(df, seed)
        else:
            return df

    @classmethod
    def _shuffle_index(cls, df, seed):
        numpy.random.seed(seed)
        df_ = df.reindex(numpy.random.permutation(df.index), copy=False)
        df_.reset_index(drop=True, inplace=True)
        return df_

    @classmethod
    def convert_dataframe(cls, df, features, converter):
        if cls.convert:
            return cls._convert_dataframe(df, features, converter)
        else:
            return df

    @classmethod
    def _convert_dataframe(cls, df, features, converter):
        len_ = len(df)
        column_converter = converter.get_column_converter()
        dict_ = OrderedDict()
        N_feature = len(features)
        for index, (feature_name, values) in enumerate(features.items()):
            sys.stderr.write("\r\033[K {0}/{1} {2}".format(index, N_feature, feature_name))
            sys.stderr.flush()
            dict_.update(column_converter[values['feature_type']](feature_name, df))

        df_ = pandas.DataFrame(dict_)
        assert(len_ == len(df_))
        return df_

    @classmethod
    def get_column_name(cls, df, features, converter):
        column_name_generator = converter.get_column_name_generator()
        list_ = []
        for feature_name, values in features.items():
            list_.extend(column_name_generator[values['feature_type']](feature_name, df))

        return list_
    @classmethod
    def get_column_name_dict(cls, df, features, converter):
        column_name_generator = converter.get_column_name_generator()
        return {feature_name:column_name_generator[values["feature_type"]](feature_name, df) for feature_name, values in features.items()}
