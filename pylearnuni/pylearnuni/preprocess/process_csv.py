#!/usr/binenv python
# -*- coding: utf-8 -*-

import os
import sys
import pandas
import numpy
import linecache
import multiprocessing
import StringIO
import datetime
from dateutil.relativedelta import relativedelta
from csv import reader

from pylearnuni.utils import io_handler as IO

SEED = 1234
PROC = 4


def get_nrow(filepath):
    num_lines = sum(1 for line in open(filepath))
    print "number of lines : ", num_lines - 1
    return num_lines - 1


def get_shuffled_index(nrow, seed):
    numpy.random.seed(seed)
    return numpy.random.permutation(range(nrow))


def shuffle_csv(input_csv_path, output_path="."):
    if os.path.isdir(output_path):
        output_path = os.path.join(output_path, "shuffled_data.csv")
    else:
        pass
    assert(not os.path.isdir(output_path))

    nrow = get_nrow(input_csv_path)
    shuffled_index = get_shuffled_index(nrow, SEED)
    header = linecache.getline(input_csv_path, 1)
    with open(output_path, 'w') as file:
        file.write(header)
        for index in shuffled_index:
            target_line = linecache.getline(input_csv_path, index + 1 + 1)
            file.write(target_line)
    linecache.clearcache()


def check_line(line, dtypes, header, index, check_nan=False):
    try:
        df = pandas.read_csv(StringIO.StringIO(line), dtype=dtypes, names=header, header=None)
    except:
        df = pandas.read_csv(StringIO.StringIO(line), names=header, header=None)
        for k, v in dtypes.items():
            if pandas.isnull(df[k][0]):
                if check_nan:
                    print "line: {}, column name: {} is Nan".format(index + 1, k)
                pass
            elif df[k].dtype == v:
                pass
            elif (df[k].dtype in ["object", "int64", "float64"]) & (v == "string"):
                pass
            elif (df[k].dtype in ["float64"]) & (v in ["int8", "int16", "int32", "int64"]):
                pass
            else:
                print "line: {}, column name: {}, value: {}, type: {}, expected type: {}".format(index + 1, k, df[k].values, df[k].dtype, v)
                return False

    return True


def check_csv(input_csv_path, features_yaml):
    f = open(input_csv_path, 'r')
    features = IO.read_yaml(features_yaml)

    dtypes = {k: val["dtype"] for k, val in features.items()}

    pool = multiprocessing.Pool(PROC)  # use all available cores, otherwise specify the number you want as an argument
    header = []
    nrow = get_nrow(input_csv_path)
    for index, line in enumerate(f):
        sys.stderr.write("\r\033[K {0}/{1}".format(index, nrow))
        sys.stderr.flush()
        if index == 0:
            header = line.rstrip('\n').split(',')
        else:
            pool.apply_async(check_line, args=(line, dtypes, header, index))

    f.close()
    pool.close()
    pool.join()


def check_csv_period(label, input_csv_path):
    f = open(input_csv_path, 'r')

    label_index = None
    latest = None
    oldest = None
    for index, line in enumerate(f):
        if index == 0:
            header = line.rstrip('\n').split(',')
            label_index = header.index(label)
        elif ',' in line:
            s = reader(StringIO.StringIO(line.rstrip('\n'))).next()
            date_str = s[label_index]
            print date_str
            date = datetime.datetime.strptime(date_str, "%Y/%m/%d %H:%M:%S").date()
            if latest is None:
                latest = date
                oldest = date
            elif date > latest:
                latest = date
            elif date < oldest:
                oldest = date
    f.close()
    return latest, oldest


def split_period_csv(label, latest, oldest, time_window=1, time_stride=1, input_csv_path="data.csv", output_path="."):

    itr = latest + relativedelta(months=+1, day=1)

    pool = multiprocessing.Pool(PROC)  # use all available cores, otherwise specify the number you want as an argument
    while itr > oldest:
        end_time = itr
        begin_time = end_time + relativedelta(months=-time_window, day=1)

        period = begin_time.strftime("%Y%m") + "-" + end_time.strftime("%Y%m")
        output_dir = os.path.join(output_path, period)

        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)

        output_file = os.path.join(output_dir, period + ".csv")
        pool.apply_async(extract_period_csv, args=(label, begin_time, end_time, input_csv_path, output_file))
        itr = itr + relativedelta(months=-time_stride, day=1)
    pool.close()
    pool.join()


def extract_period_csv(label, begin_time, end_time, input_csv_path, output_path):
    f = open(input_csv_path, 'r')
    file_ = open(output_path, 'w')

    label_index = None
    header = []
    nrow = get_nrow(input_csv_path)
    for index, line in enumerate(f):
        sys.stderr.write("\r\033[K {0}/{1}".format(index, nrow))
        sys.stderr.flush()
        if index == 0:
            file_.write(line)
            header = line.rstrip('\n').split(',')
            label_index = header.index(label)
        elif ',' in line:
            s = reader(StringIO.StringIO(line.rstrip('\n'))).next()
            date_str = s[label_index]
            date = datetime.datetime.strptime(date_str, "%Y/%m/%d %H:%M:%S").date()
            if begin_time <= date < end_time:
                file_.write(line)
            else:
                pass

    file_.close()
    f.close()


def add_period_dependent_features(input_filepath, output_filepath, datadir, chunksize=100000):
    reader = IO.read_csv(input_filepath, chunksize=chunksize)
    firstchunk = True
    for df in reader:
        print "converting {} datas at a time...".format(chunksize)
        df2 = _add_period_dependent_features(df, datadir)
        print "writing {} datas...".format(chunksize)
        if firstchunk:
            df2.to_csv(output_filepath, index=False, header=True)  # first chunk-> make new file and write header
        else:
            df2.to_csv(output_filepath, index=False, header=False, mode='a')  # other chunks-> add to the file and dont write header
        firstchunk = False


def _add_period_dependent_features(df, datadir):

    stockdata = pandas.read_csv(os.path.join(datadir, "stock_average.csv"))
    pricedata = pandas.read_csv(os.path.join(datadir, "price_average.csv"))

    stockaverage = stockdata["stock"].mean()
    priceaverage = pricedata["price"].mean()

    year = df["最終掲載日時"].str.extract("\A(\d+)").astype(numpy.int)
    month = df["最終掲載日時"].str.extract("/(\d+)").astype(numpy.int)

    stocklist = numpy.zeros(df.shape[0])
    pricelist = numpy.zeros(df.shape[0])

    for y in range(1950, 2020):
        for m in range(1, 13):
            dfindex = numpy.where((year == y) & (month == m))[0]

            stockindex = numpy.where((stockdata["year"] == y) & (stockdata["month"] == m))[0]
            assert(stockindex.shape[0] <= 1)
            if stockindex.shape[0] == 0:
                if dfindex.shape[0] != 0:
                    print "no stock data found for", y, m
                    print "will use average of stock data:", stockaverage
                stock = stockaverage
            else:
                stock = stockdata["stock"][stockindex[0]]
            stocklist[dfindex] = stock

            priceindex = numpy.where((pricedata["year"] == y) & (pricedata["month"] == m))[0]
            assert(priceindex.shape[0] <= 1)
            if priceindex.shape[0] == 0:
                if dfindex.shape[0] != 0:
                    print "no price data found for", y, m
                    print "will use average of price data:", priceaverage
                price = priceaverage
            else:
                price = pricedata["price"][priceindex[0]]
            pricelist[dfindex] = price

    df["平均株価"] = stocklist
    df["平均出品価格"] = pricelist
    df["年"] = year
    df["月"] = month
    df["月番号"] = (year - 2010) * 12 + month

    return df
