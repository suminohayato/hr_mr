#!/usr/binenv python
# -*- coding: utf-8 -*-

import pandas
import numpy

from pylearnuni.utils import column_name_generater as Name

# クラスメソッドとしてではなくて，普通にConverterのインスタンスを作成するようにしたほうがよい
# インスタンスを突っ込むほうが分かりやすい
class Converter(object):
    feature_types = ("unary", "categorical")

    def check_consistency(func):
        def inner(cls, column_name, df):
            len_ = len(df)
            dict_ = func(cls, column_name, df)  # 1
            for val in dict_.values():
                assert len_ == len(val), func.__name__
            return dict_
        return inner

    @classmethod
    def get_column_converter(cls):
        locals_ = {"cls": cls}
        column_converter = {k: eval('cls.convert_' + k, locals_) for k in cls.feature_types}
        return column_converter

    @classmethod
    def get_column_name_generator(cls):
        locals_ = {"cls": cls}
        column_name_generator = {k: eval('cls.generate_' + k + '_column_name', locals_) for k in cls.feature_types}
        return column_name_generator

    @classmethod
    def convert_into_one_hot(cls, data_column, values):
        assert(len(data_column.shape) == 1)
        column_name = data_column.name

        # 新しいカラムの名前を生成
        flag_names = [Name.generate_one_hot_name(column_name, val) for val in values]
        # Columnを格納したディクショナリの生成
        column_ = data_column.astype(str)
        return {flag: (column_ == str(val)).astype(int).tolist() for flag, val in zip(flag_names, values)}

    @classmethod
    @check_consistency
    def convert_unary(cls, column_name, df):
        '''convert an unary (e.g., binary or trinary) variables'''
        return {column_name: df[column_name].tolist()}

    @classmethod
    @check_consistency
    def convert_categorical(cls, column_name, df):
        '''convert an unary categorical variable with one_hot encoding'''
        values = df[column_name].dropna().drop_duplicates()
        return cls.convert_into_one_hot(df[column_name], values)

    @classmethod
    def generate_unary_column_name(cls, column_name, df):
        return [column_name]

    @classmethod
    def generate_categorical_column_name(cls, column_name, df):
        #values = df[column_name].fillna(value=0).drop_duplicates()
        values = df[column_name].dropna().drop_duplicates().sort_values()
        #print values

        flag_names = [Name.generate_one_hot_name(column_name, val) for val in values]
        return flag_names
