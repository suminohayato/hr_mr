#!/usr/binenv python
# -*- coding: utf-8 -*-

class Filter(object):
    DROP_FLAG = True

    @classmethod
    def filter(cls, df):
        raise NotImplementedError

    @classmethod
    def drop(cls, df, mask, *args, **kwargs):
        if cls.DROP_FLAG:
            kwargs["inplace"] = True
            df.drop(df.index[mask], *args, **kwargs)
            assert(len(df) > 0)
        df.reset_index(drop=True, inplace=True)
        return df

    @classmethod
    def target_value_upperboud(cls, df, target_name, threshold):
        #cls.drop(df, df[target_name] > threshold)
        assert(len(df) > 0)
        return df

    @classmethod
    def target_value_lowerboud(cls, df, target_name, threshold):
        cls.drop(df, df[target_name] < threshold)
        assert(len(df) > 0)
        return df
