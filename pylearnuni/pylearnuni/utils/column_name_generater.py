#!/usr/binenv python
# -*- coding: utf-8 -*-

import math


def generate_one_hot_name(column_name, value):
    if isinstance(value, float):
        value = convert_float(value)
    return "{0}<{1}>".format(column_name, value)


def generate_merged_name(column_name1, column_name2):
    return "{0}_x_{1}".format(column_name1, column_name2)


def cross_term_name(column_name1, column_name2):
    return "{0}_X_{1}".format(column_name1, column_name2)


def convert_float(value):
    if value == 0:
        return int(0)
    elif math.fabs(value) >= 1.0:
        return int(value)
    else:
        return value