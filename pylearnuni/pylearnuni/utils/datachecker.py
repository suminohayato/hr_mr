#!/usr/binenv python
# -*- coding: utf-8 -*-

import pandas
import sys

INPUT_CSV_PATH = "/Users/nagatamotoki/bitbucketrepos/ガリバー価格コンサルデータ/Q_AAD情報/Q_AAD情報.txt"
OUTPUT_CSV_PATH = "/Users/nagatamotoki/bitbucketrepos/ガリバー価格コンサルデータ/Q_AAD情報/Q_AAD情報_info.csv"

#1行目: カラム名
#2行目: 型
#3行目: 欠損数
#4行目: データ種類数
#それ以降: drop_duplicates()されたデータの中身

def datachecker(input_csv_path = INPUT_CSV_PATH, output_csv_path = OUTPUT_CSV_PATH):
    input_data = pandas.read_csv(input_csv_path)
    output_file = open(output_csv_path,"wa")

    output_file.write(','.join(input_data.columns))
    output_file.write('\n')
    column_len = len(input_data.columns)
    for j in range(column_len):
        output_file.write(str(input_data.iloc[:,j].dtype))
        output_file.write(',')
    output_file.write('\n')
    for j in range(column_len):
        output_file.write(str(input_data.iloc[:,j].isnull().sum()))
        output_file.write(',')
    output_file.write('\n')

    unique_data_list = []
    unique_data_count = []
    for j in range(column_len):
        unique_data = input_data.iloc[:,j].dropna().drop_duplicates()
        unique_data.sort()
        unique_data_list.append(unique_data)

        unique_data_count.append(unique_data.count())
        output_file.write(str(unique_data.count()))
        output_file.write(',')
    output_file.write('\n\n')

    for i in range(max(unique_data_count)):
        for j in range(column_len):
            if i < unique_data_count[j]:
                output_file.write(str(unique_data_list[j].iloc[i]))
            output_file.write(',')
        output_file.write('\n')
    output_file.close()

if __name__ == "__main__":
    if len(sys.argv) == 3:
        input_data_csv = sys.argv[1]
        output_csv_path = sys.argv[2]
        datachecker(input_csv_path = input_data_csv, output_csv_path = output_csv_path)
    else:
        datachecker()