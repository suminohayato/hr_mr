#!/usr/binenv python
# -*- coding: utf-8 -*-

import codecs
import types


def encode_container(container, encoding='utf-8'):
    if isinstance(container, dict):
        dict_ = {}
        for key, val in container.items():
            dict_[key.encode(encoding)] = encode_container(val, encoding)
        else:
            return dict_
    elif isinstance(container, list):
        list_ = []
        for val in container:
            list_.append(encode_container(val, encoding))
        else:
            return list_
    elif isinstance(container, tuple):
        list_ = []
        for val in container:
            list_.append(encode_container(val, encoding))
        else:
            return list_
    else:
        return encode_variable(container, encoding)


def encode_variable(var, encoding='utf-8'):
    if isinstance(var, types.NoneType):
        return ""
    elif isinstance(var, types.UnicodeType):
        return var.encode(encoding)
    else:
        return var


def decode_container(container, encoding='utf-8'):
    if isinstance(container, dict):
        dict_ = {}
        for key, val in container.items():
            dict_[key.decode(encoding)] = decode_container(val, encoding)
        else:
            return dict_
    elif isinstance(container, list):
        list_ = []
        for val in container:
            list_.append(decode_container(val, encoding))
        else:
            return list_
    elif isinstance(container, tuple):
        list_ = []
        for val in container:
            list_.append(encode_container(val, encoding))
        else:
            return list_
    else:
        return decode_variable(container, encoding)


def decode_variable(var, encoding='utf-8'):
    if isinstance(var, types.NoneType):
        return ""
    elif isinstance(var, str):
        return var.decode(encoding)
    else:
        return str(var).decode(encoding)
