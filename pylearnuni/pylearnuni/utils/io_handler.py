#!/usr/binenv python
# -*- coding: utf-8 -*-

import os
import logging
import pandas
import numpy
import yaml
import pickle
import codecs
from collections import OrderedDict

import encoding_converter as enc


def check_filepath(filepath):
    filepath = os.path.expanduser(filepath)
    filepath = os.path.abspath(filepath)
    assert(os.path.isfile(filepath))
    return filepath


def check_dirpath(filepath):
    filepath = os.path.expanduser(filepath)
    filepath = os.path.abspath(filepath)
    body, ext = os.path.splitext(filepath)
    if ext:
        dirpath = os.path.dirname(filepath)
    else:
        dirpath = filepath
    if not os.path.isdir(dirpath):
        os.makedirs(dirpath)
    return filepath


def read_csv(filepath, **kwargs):
    logging.info("read csv file : " + filepath)
    filepath = check_filepath(filepath)
    df = pandas.read_csv(filepath, **kwargs)
    return df


def read_csv_part(filepath, begin_row, end_row):
    df_ = read_csv(filepath, skiprows=range(1, begin_row), nrows=end_row - begin_row + 1)
    return df_


def write_csv(df, filepath, encoding='utf-8', **kwargs):
    logging.info("write csv file : " + filepath)
    filepath = check_dirpath(filepath)
    df.to_csv(filepath, encoding=encoding, **kwargs)


def read_yaml(filepath, encoding='utf-8'):
    logging.info("read yaml file : " + filepath)
    filepath = check_filepath(filepath)
    # 辞書を順番通りに読み込むおまじない
    yaml.add_constructor(yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG, lambda loader, node: OrderedDict(loader.construct_pairs(node)))
    with codecs.open(filepath, 'r', encoding) as f:
        yaml_ = yaml.load(f)
    #print type(yaml_)
    #print enc.encode_container(yaml_)
    #return enc.encode_container(yaml_)
    res = OrderedDict()
    for key, value in yaml_.items():
        res[key.encode("utf-8")] = value
    return res


def write_yaml(stream, filepath, access_mode='w', encoding='utf-8', **kwargs):
    logging.info("write yaml file : " + filepath)
    filepath = check_dirpath(filepath)

    kwargs_ = kwargs
    kwargs['encoding'] = encoding
    kwargs['allow_unicode'] = True
    kwargs['default_flow_style'] = False
    kwargs['width'] = 1000
    kwargs['indent'] = 4
    kwargs.update(kwargs_)

    if access_mode in 'a':
        filepath = check_filepath(filepath)
        if os.path.isfile(filepath):
            yaml_ = read_yaml(filepath, encoding)
            if isinstance(yaml_, dict):
                yaml_.update(stream)
            elif isinstance(yaml_, list):
                yaml_.extend(stream)
            else:
                raise NotImplementedError
            stream = yaml_
    elif access_mode in 'w':
        pass
    else:
        raise ValueError

    yaml_ = enc.decode_container(stream)
    # 辞書を順番通りに書き込むおまじない
    yaml.add_constructor(yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG, lambda loader, node: OrderedDict(loader.construct_pairs(node)))
    with codecs.open(filepath, 'w', encoding) as f:
        yaml.safe_dump(yaml_, f, **kwargs)


def read_pickle(filepath, **kwargs):
    logging.info("read pkl file : " + filepath)
    filepath = check_dirpath(filepath)
    with open(filepath, 'rb') as f:
        pickle_ = pickle.load(f, **kwargs)
    return pickle_


def write_pickle(stream, filepath, **kwargs):
    logging.info("write pkl file : " + filepath)
    filepath = check_dirpath(filepath)
    with open(filepath, 'wb') as f:
        pickle_ = pickle.dump(stream, f, **kwargs)


def read_bin(filepath, **kwargs):
    logging.info("read bin file : " + filepath)
    filepath = check_filepath(filepath)
    datamat = numpy.fromfile(filepath, **kwargs)
    return datamat


def write_bin(datamat, filepath, **kwargs):
    logging.info("write bin file : " + filepath)
    filepath = check_dirpath(filepath)
    datamat.tofile(filepath, **kwargs)


def read_memmap(filepath, dtype='float64', mode='r+', **kwargs):
    logging.info("read bin file (memmap) : " + filepath)
    filepath = check_dirpath(filepath)
    datamat = numpy.memmap(filepath, dtype=dtype, mode=mode, **kwargs)
    return datamat


def write_memmap(datamat, filepath, dtype='float64', mode='w+', **kwargs):
    logging.info("write bin file (memmap) : " + filepath)
    filepath = check_dirpath(filepath)
    f = numpy.memmap(filepath, dtype=dtype, mode=mode, shape=datamat.shape, **kwargs)
    f[:] = datamat[:]
    del f
