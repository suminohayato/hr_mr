#!/usr/binenv python
# -*- coding: utf-8 -*-

import pandas
import numpy
from pylearnuni.preprocess.filter import Filter
import datetime as dt

class hrFilter(Filter):
    exclusion_codes = []
    inclusion_codes = []

    @classmethod
    def filter(cls, df):
        return df
