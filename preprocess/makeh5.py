#!/usr/binenv python
# -*- coding: utf-8 -*-

from hr_helper import hrHelper as Helper
from pylearnuni.preprocess.preprocess import Preprocess as Preprocess

if __name__ == "__main__":
    Preprocess.shuffle = True

    helper = Helper()

    # 読み込むCSVデータのパスを設定
    helper.load_data("/Users/sugiyamakanta/hr_mr_data/data1800.csv", chunk_size=100000, chunk=True)
    # データに対応するfeatures.yamlのパスを設定
    helper.load_features("/Users/sugiyamakanta/hr_mr/workspace/sugiyama/preprocess/features.yaml")
    # 出力データを保存するフォルダへのパス（ここにすべて出力される）
    helper.set_output_path("/Users/sugiyamakanta/hr_mr_data/")

    # データをpylearn2が読み込める形に変形
    helper.process_data()
    helper.generate_dataset()