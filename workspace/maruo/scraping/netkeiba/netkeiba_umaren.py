# -*- coding: utf-8 -*-

from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd
import random
from time import sleep
import itertools
import os,sys,yaml
import scraping_lib as lib

def main(year, place, day, race, replace_flag=True, result_flag=True, kanji_flag=False):
    places_dict = lib.place_dict_load()
    waku_dict = lib.place_dict_load()

    output_path = 'odds_output/'
    output_file = os.path.join(output_path, 'odds_' + year + places_dict[place].encode('utf-8') + day + '_r' + race + '馬連' + ".csv")

    if os.path.exists(output_file):
        exit()

url = 'http://race.sp.netkeiba.com/?pid=odds_view&type=b4&race_id=' + year + place + day + race +'&housiki=c0'
browser = webdriver.PhantomJS()
#request
print "request: " + url
browser.get(url)

tables = browser.find_elements_by_class_name('RaceOdds_HorseList_Table')

keys = ['馬番1', '馬番2', '馬連']
df = pd.DataFrame([] , columns = keys)

for table in tables:
    trs = table.find_elements_by_tag_name('tr')
    uma1 = int(trs[0].text)
    trs.pop(0)
    for tr in trs:
        uma2 = int(tr.text.split(' ')[0])
        odds = float(tr.text.split(' ')[1])
        _df = pd.DataFrame([[uma1, uma2, odds]], columns = keys)
        df = df.append(_df)


df.to_csv(output_file)

#sleep
# sleep(random.uniform(2,4))

browser.close()
browser.quit()

if __name__ == '__main__':
    year = sys.argv[1]
    place = sys.argv[2]
    day = sys.argv[3]
    race = sys.argv[4]

    replace_flag = True

    main(year, place, day, race, replace_flag=True, result_flag=True, kanji_flag=False)
