#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import random
from time import sleep

import itertools
import os
import sys
import yaml

HR_PATH = os.getenv("HR_PATH")
yaml_path = os.path.join(HR_PATH,"workspace/maruo/scraping/netkeiba/")

f = open(os.path.join(yaml_path,'run/date_dict.yaml'), 'r')
_date_dict = yaml.load(f)
f.close()

odds_type_dict = {
    'tanfuku': '単複',
    'umaren':  '馬連',
    'umatan':  '馬単',
    'wakuren': '枠連',
    'wide':    'ワイド'
}

def places_dict_load():
    f = open(os.path.join(yaml_path,'run/places_dict.yaml'), 'r')
    places_dict = yaml.load(f)
    f.close()
    return places_dict

def waku_dict_load():
    f = open(os.path.join(yaml_path,'run/waku_dict.yaml'), 'r')
    waku_dict = yaml.load(f)
    f.close()
    return waku_dict

def get_date(race_dict):
    return _date_dict[race_dict['year']][race_dict['kanji_place'].decode('utf-8')][race_dict['day']]

def odds_output(race_dict, odds_type, places_dict):
    output_path = os.path.join(yaml_path,
                               'run/output/',
                               race_dict['year']
                               + places_dict[race_dict['place']].encode('utf-8')
                               + race_dict['day'])

    if not os.path.exists(output_path):
        os.mkdir(output_path)

    output_file =  os.path.join(output_path,
                            'odds_'
                            + race_dict['year']
                            + places_dict[race_dict['place']].encode('utf-8')
                            + race_dict['day']
                            + '_r' + race_dict['race']
                            + odds_type_dict[odds_type] + ".csv")

    #複勝専用
    result_file = os.path.join(output_path,
                               'odds_'
                               + race_dict['year']
                               + places_dict[race_dict['place']].encode('utf-8')
                               + race_dict['day']
                               + '_r' + race_dict['race']
                               + '複勝結果' + ".csv")
    return output_file, result_file

def shutuba_output(race_dict, result_flag):
    output_path = os.path.join(yaml_path,
                               'run/output/',
                               race_dict['year']
                               + race_dict['kanji_place']
                               + race_dict['day'])
    if not os.path.exists(output_path):
        os.mkdir(output_path)

    basic_name = os.path.join(output_path,
                              'shutuba_'
                                  + race_dict['year']
                                  + race_dict['kanji_place']
                                  + race_dict['day']
                                  + '_r' + race_dict['race'])
    if result_flag:
        return basic_name + "_result.csv"
    else:
        return basic_name + ".csv"
