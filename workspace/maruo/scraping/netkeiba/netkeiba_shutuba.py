# -*- coding: utf-8 -*-

from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd
import random
from time import sleep
import itertools
import os,sys,yaml
import re

from copy import copy
from PastRace import PastRace
from BasicRaceStat import BasicRaceStat
import scraping_lib as lib

HR_PATH = os.getenv("HR_PATH")

def main(year, place, day, race, replace_flag=True, result_flag=True, kanji_flag=False):

    scraping_shutuba = {'flag': False, 'message': ''}

    places_dict = lib.places_dict_load()
    waku_dict = lib.waku_dict_load()

    if kanji_flag:
        places_dict_inv = {v:k for k, v in places_dict.items()}
        place = places_dict_inv[place]

    now_race_dict = {
        'year': year,
        'place': place,
        'day': day,
        'race': race,
        'kanji_place': places_dict[place].encode('utf-8')}

    sex_lists = [u'牡', u'牝', u'セ']

    PastRace.set_keys([])
    # PastRace.set_keys(['前走'])
    # PastRace.set_keys(['前走', '前々走', '前３走'])

    output_file = lib.shutuba_output(now_race_dict, result_flag)

    if not (replace_flag) and os.path.exists(output_file):
        exit()

    browser = webdriver.PhantomJS()
    keys = ['枠番', '馬番', '馬名', '性別', '齢', '騎手名', '斤量', '体重', '名称', '条件', '賞金']
    # keys = ['競技場', '時刻', 'レース情報', 'レース周り', '天気', '馬場状態', '枠番', '馬番', '馬名', '性別', '齢', '騎手名', '斤量', '体重', '名称', '条件', '賞金']

    if result_flag:
        keys.extend(['順位', 'タイム'])

    df = pd.DataFrame([])

    # 結果を加える
    if result_flag:
        url = 'http://race.sp.netkeiba.com/?pid=race_result&race_id=' + year + place + day + race
        print "request: " + url
        browser.get(url)

        browser.find_element_by_class_name("Button_01").click()
        table = browser.find_element_by_class_name('Table_Show_All')
        trs = table.find_elements_by_tag_name('tr')
        trs.pop(0)

        result_hash = {}

        for i, tr in enumerate(trs):
            tds = tr.find_elements_by_tag_name('td')
            uma_ban = int(tds[2].text)
            race_time = tds[4].text.split("\n")[0]
            result_hash[uma_ban] = {"順位": i+1, "タイム": race_time}

    names_table = []
    jockey_links = []
    trainer_links = []
    if True:
        url = 'http://race.netkeiba.com/?pid=race_old&id=c' + now_race_dict['year'] + now_race_dict['place'] + now_race_dict['day'] + now_race_dict['race']
        print "request: " + url
        browser.get(url)

        table = browser.find_element_by_class_name('race_table_old')
        trs = table.find_elements_by_tag_name('tr')
        trs.pop(0)
        trs.pop(0)
        trs.pop(0)
        for tr in trs:
            tds = tr.find_elements_by_tag_name('td')
            jockey_links.append(tds[6].find_element_by_tag_name('a').get_attribute('href'))
            trainer_links.append(tds[7].find_element_by_tag_name('a').get_attribute('href'))

    for jockey_link, trainer_link in zip(jockey_links, trainer_links):
        browser.get(jockey_link)
        jockey_name = re.sub(r'\s', '', browser.find_element_by_tag_name('h1').text[:4])
        browser.get(trainer_link)
        trainer_name = re.sub(r'\s', '', browser.find_element_by_tag_name('h1').text[:4])
#        names_table.append({'騎手名': jockey_name, '調教師名': trainer_name, '日付':})
        names_table.append({'騎手名': jockey_name.encode('utf-8'),
                            '調教師名': trainer_name.encode('utf-8'),
                            '日付':  lib.get_date(now_race_dict).encode('utf-8')})


    # 出馬表生成
    brs = BasicRaceStat(now_race_dict)
    base_row = brs.row

    if not '障' in base_row['レース情報']:
        url = 'http://race.sp.netkeiba.com/?pid=shutuba&race_id=' + now_race_dict['year'] + now_race_dict['place'] + now_race_dict['day'] + now_race_dict['race']
        print "request: " + url
        browser.get(url)

        table = browser.find_element_by_class_name('Shutuba_Table')
        trs = table.find_elements_by_tag_name('tr')
        trs.pop(0)
        for idx,tr in enumerate(trs):
            row = copy(base_row)
            tds = tr.find_elements_by_tag_name('td')
            if tds[1].text == u'除外' or tds[1].text == u'取消':
                continue
            row['枠番'] = waku_dict[tds[0].value_of_css_property('background-color').encode('utf-8')]
            row['馬番'] = int(tds[0].text)
            splits = tds[2].text.split("\n")
            row['馬名'] = splits[0].replace(' ', '').encode('utf-8')
            for i in sex_lists:
                if i in splits[1]:
                    row['性別'] = i.encode('utf-8')
                    row['齢'] = int(splits[1].replace(i, ''))
                    break
            else:
                row['齢'] = 0
                row['性別'] = ''
            # row['騎手名'] = splits[2].split(' ')[0].encode('utf-8')
            row['斤量'] = float(splits[2].split(' ')[1])
            row['体重'] = float(tds[4].text.split('\n')[0])

            if result_flag:
                row['順位'] = result_hash[row['馬番']]['順位']
                row['タイム'] = result_hash[row['馬番']]['タイム']

#            pr = PastRace(tds[2].find_element_by_tag_name('a').get_attribute('href').split('&id=')[1], now_race_dict)
#            row.update(pr.row)
            row.update(names_table[idx])
            _df = pd.DataFrame([row])
            # uma_ids.append(tr.find_element_by_tag_name('a').get_attribute('href').encode('utf-8').split('=')[2])
            df = df.append(_df)

        df.to_csv(output_file)
        scraping_shutuba['flag'] = True
        scraping_shutuba['message'] = 'Success'
    else:
        scraping_shutuba['flag'] = False
        scraping_shutuba['message'] = '障害レース'

    browser.close()
    browser.quit()

    print 'shutuba scraping: ' + scraping_shutuba['message']
    return scraping_shutuba

if __name__ == '__main__':
    year = sys.argv[1]
    place = sys.argv[2].decode('utf-8')
    day = sys.argv[3]
    race = sys.argv[4]

    replace_flag = True

    main(year, place, day, race, replace_flag=True, result_flag=False, kanji_flag=True)
