# -*- coding: utf-8 -*-

from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd
import random
from time import sleep
import itertools
import os,sys,yaml

from odds.tanfuku import tanfuku
from odds.umaren import umaren
from odds.umatan import umatan
from odds.wide import wide
from odds.wakuren import wakuren
import scraping_lib as lib

def main(year, place, day, race, replace_flag=True, result_flag=True, kanji_flag=False, odds_type=None):

    if not lib.odds_type_dict.has_key(odds_type):
        print 'error: not found odds_type'
        exit()

    places_dict = lib.places_dict_load()
    waku_dict = lib.waku_dict_load()

    if kanji_flag:
        places_dict_inv = {v:k for k, v in places_dict.items()}
        place = places_dict_inv[place]

    race_dict = {
        'year': year,
        'place': place,
        'day': day,
        'race': race }

    output_file, result_file = lib.odds_output(race_dict, odds_type, places_dict)

    if not (replace_flag) and os.path.exists(output_file) and os.path.exists(result_file):
        exit()
    print odds_type

    print "Scraipng: {}".format(odds_type)
    eval(odds_type)(race_dict, output_file, waku_dict, result_flag=result_flag, result_file=result_file)

if __name__ == '__main__':
    year = sys.argv[1]
    place = sys.argv[2].decode('utf-8')
    day = sys.argv[3]
    race = sys.argv[4]
    odds_type = sys.argv[5].encode('utf-8')
#    kanji_flag = bool(sys.argv[6])

    replace_flag = True

    main(year, place, day, race, replace_flag=False, result_flag=True, kanji_flag=True, odds_type=odds_type)
