# -*- coding: utf-8 -*-

from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd
import random
from time import sleep
import itertools
import os,sys,yaml

from odds.tanfuku import tanfuku

HR_PATH = os.getenv("HR_PATH")

def main(year, place, day, race, replace_flag=True, result_flag=True, kanji_flag=False):
    yaml_path=os.path.join(HR_PATH,"workspace/maruo/scraping/netkeiba/")
    f = open(os.path.join(yaml_path,'run/places_dict.yaml'), 'r')
    places_dict = yaml.load(f)
    f.close()

    yaml_path=os.path.join(HR_PATH,"workspace/maruo/scraping/netkeiba/")
    f = open(os.path.join(yaml_path,'run/waku_dict.yaml'), 'r')
    waku_dict = yaml.load(f)
    f.close()

    if kanji_flag:
        places_dict_inv = {v:k for k, v in places_dict.items()}
        place = places_dict_inv[place]
    
    output_path = os.path.join(yaml_path, 'run/output/', year + places_dict[place].encode('utf-8') + day)
    
    if not os.path.exists(output_path):
        os.mkdir(output_path)

    output_file = os.path.join(output_path, 'odds_' + year + places_dict[place].encode('utf-8') + day + '_r' + race + '単複' + ".csv")
    result_file = os.path.join(output_path, 'odds_' + year + places_dict[place].encode('utf-8') + day + '_r' + race + '複勝結果' + ".csv")

    if not (replace_flag) and os.path.exists(output_file) and os.path.exists(result_file):
        exit()

    race_dict = {}
    race_dict['year'] = year
    race_dict['place'] = place
    race_dict['day'] = day
    race_dict['race'] = race

    print "scraipng tanfuku"
    tanfuku(race_dict, output_file, waku_dict, result_flag=result_flag, result_file=result_file)

if __name__ == '__main__':
    year = sys.argv[1]
    place = sys.argv[2]
    day = sys.argv[3]
    race = sys.argv[4]

    replace_flag = True

    main(year, place, day, race, replace_flag=True, result_flag=True, kanji_flag=False)
