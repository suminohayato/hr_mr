# -*- coding: utf-8 -*-

from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd
import random
from time import sleep
import itertools
import os,sys,yaml

def umatan(race_dict, output_file, waku_dict, result_flag=True, result_file=''):

    url = ('http://race.sp.netkeiba.com/?pid=odds_view&type=b6&race_id='
        + race_dict['year']
        + race_dict['place']
        + race_dict['day']
        + race_dict['race']
        + '&housiki=c0')

    browser = webdriver.PhantomJS()
    #request
    print "request: " + url
    browser.get(url)

    tables = browser.find_elements_by_class_name('RaceOdds_HorseList_Table')

    keys = ['馬番1', '馬番2', '馬単']
    df = pd.DataFrame([] , columns = keys)

    for table in tables:
        trs = table.find_elements_by_tag_name('tr')
        uma1 = int(trs[0].text)
        trs.pop(0)
        for tr in trs:
            uma2 = int(tr.text.split(' ')[0])
            odds = tr.text.split(' ')[1]
            if not odds.isdigit():
                continue
            odds = float(odds)
            _df = pd.DataFrame([[uma1, uma2, odds]], columns = keys)
            df = df.append(_df)


    df.to_csv(output_file)

    #sleep
    # sleep(random.uniform(2,4))

    browser.close()
    browser.quit()
