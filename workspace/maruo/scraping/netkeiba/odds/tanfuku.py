#!/usr/bin/env python
# -*- coding: utf-8 -*-

from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd
import random
from time import sleep
import itertools
import os,sys,yaml

def tanfuku(race_dict, output_file, waku_dict, result_flag=True, result_file=''):

    url = 'http://race.sp.netkeiba.com/?pid=odds_view&type=b1&race_id=' + race_dict['year'] + race_dict['place'] + race_dict['day'] + race_dict['race']

    browser = webdriver.PhantomJS()
    #request
    print "request: " + url
    browser.get(url)

    browser.find_element_by_id('btn_odds_summary').click()

    table = browser.find_element_by_id('odds_summary')
    trs = table.find_elements_by_tag_name('tr')
    trs.pop(0)

    keys = ['枠番', '馬番', '馬名', '単勝', '複勝最小', '複勝最大']
    df = pd.DataFrame([] , columns = keys)

    for tr in trs:
        tds = tr.find_elements_by_tag_name('td')
        if tds[3].text == u'除外':
            continue
        if tds[3].text == u'取消':
            continue
        dic = {}
        dic['枠番'] = waku_dict[tds[0].value_of_css_property('background-color').encode('utf-8')]
        dic['馬番'] = int(tds[0].text)
        dic['馬名'] = tds[2].text.encode('utf-8')
        dic['単勝'] = float(tds[3].text)
        dic['複勝最小'] = float(tds[4].text.split('-')[0])
        dic['複勝最大'] = float(tds[4].text.split('-')[1])
        df = df.append(pd.DataFrame([dic]))

    df.to_csv(output_file)

    if result_flag:
        url = 'http://race.sp.netkeiba.com/?pid=race_result&race_id=' + race_dict['year'] + race_dict['place'] + race_dict['day'] + race_dict['race']
        print "request: " + url
        browser.get(url)

        table = browser.find_element_by_class_name('Payout_Detail_Table')
        fukusho = table.find_element_by_class_name('Fukusho')
        ninki = fukusho.find_element_by_class_name('Ninki')
        fukusho_len = len(ninki.find_elements_by_tag_name('span'))
        splits = fukusho.text.split('\n')

        if fukusho_len == 2:
            dics = [{}, {}]
            dics[0]['馬番'] = int(splits[0].split(' ')[1])
            dics[1]['馬番'] = int(splits[1])
            dics[0]['複勝結果'] = int(splits[2].replace(u',','').replace(u'円',''))
            dics[1]['複勝結果'] = int(splits[3].replace(u',','').replace(u'円',''))
        elif fukusho_len == 3:
            dics = [{}, {}, {}]
            dics[0]['馬番'] = int(splits[0].split(' ')[1])
            dics[1]['馬番'] = int(splits[1])
            dics[2]['馬番'] = int(splits[2])
            dics[0]['複勝結果'] = int(splits[3].replace(u',','').replace(u'円',''))
            dics[1]['複勝結果'] = int(splits[4].replace(u',','').replace(u'円',''))
            dics[2]['複勝結果'] = int(splits[5].replace(u',','').replace(u'円',''))
        elif fukusho_len == 4:
            dics = [{}, {}, {}, {}]
            dics[0]['馬番'] = int(splits[0].split(' ')[1])
            dics[1]['馬番'] = int(splits[1])
            dics[2]['馬番'] = int(splits[2])
            dics[3]['馬番'] = int(splits[3])
            dics[0]['複勝結果'] = int(splits[4].replace(u',','').replace(u'円',''))
            dics[1]['複勝結果'] = int(splits[5].replace(u',','').replace(u'円',''))
            dics[2]['複勝結果'] = int(splits[6].replace(u',','').replace(u'円',''))
            dics[3]['複勝結果'] = int(splits[7].replace(u',','').replace(u'円',''))
        else:
            print "error! fukusho_len is {}".format(fukusho_len)
            dics = [{}]

        pd.DataFrame(dics).to_csv(result_file)
    #sleep
    # sleep(random.uniform(2,4))

    browser.close()
    browser.quit()

