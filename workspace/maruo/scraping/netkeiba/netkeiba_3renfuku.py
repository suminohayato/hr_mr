# -*- coding: utf-8 -*-

from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd
import random
from time import sleep
import itertools
import os,sys,yaml

f = open('places_dict.yaml', 'r')
places_dict = yaml.load(f)
f.close()

f = open('waku_dict.yaml', 'r')
waku_dict = yaml.load(f)
f.close()


year = sys.argv[1]
place = sys.argv[2]
day = sys.argv[3]
race = sys.argv[4]

output_path = 'odds_output/'
output_file = os.path.join(output_path, 'odds_' + year + places_dict[place].encode('utf-8') + day + '_r' + race + '3連複' + ".csv")

if os.path.exists(output_file):
    exit()

browser = webdriver.PhantomJS()
keys = ['馬番1', '馬番2', '馬番3', '3連複']
df = pd.DataFrame([] , columns = keys)


url = 'http://race.sp.netkeiba.com/?pid=odds_view&type=b7&race_id=' + year + place + day + race +'&jiku=1'

#request
print "request: " + url
browser.get(url)
selector = browser.find_element_by_id('list_select_horse')
uma_num = len(selector.find_elements_by_tag_name('option')) - 1

for i in range(uma_num):
    uma1 = i+1
    if not i == 0:
        jiku = str(i+1)
        url = 'http://race.sp.netkeiba.com/?pid=odds_view&type=b7&race_id=' + year + place + day + race +'&jiku=' + jiku

        #request
        print "request: " + url
        browser.get(url)

    tables = browser.find_elements_by_class_name('RaceOdds_HorseList_Table')

    for table in tables:
        trs = table.find_elements_by_tag_name('tr')
        uma2 = int(trs[0].text)
        trs.pop(0)
        for tr in trs:
            uma3 = int(tr.text.split(' ')[0])
            odds = float(tr.text.split(' ')[1])
            _df = pd.DataFrame([[uma1, uma2, uma3, odds]], columns = keys)
            df = df.append(_df)

df.to_csv(output_file)

#sleep
# sleep(random.uniform(2,4))

browser.close()
browser.quit()
