# -*- coding: utf-8 -*-

from selenium import webdriver
import pandas as pd
import re

class BasicRaceStat:
    keys = ['競技場', '時刻', 'レース情報', 'レース周り','外回りフラグ', '天気', '馬場状態', '条件', '賞金' ]

    def scraping(self, race_dict):
        browser = webdriver.PhantomJS()
        url = 'http://race.sp.netkeiba.com/?pid=shutuba&race_id=' + race_dict['year'] + race_dict['place'] + race_dict['day'] + race_dict['race']
        print "request: " + url
        browser.get(url)

        self.row = {}
        # parse
        self.row[self.key_name+'競技場'] = re.sub(r'[0-9]', '', race_dict['kanji_place'])
        race_data =  browser.find_element_by_class_name('Race_Data').text.split(' ')
        self.row[self.key_name+'時刻'] = race_data[0].encode('utf-8')
        self.row[self.key_name+'レース情報'] = race_data[1].encode('utf-8')
        self.row[self.key_name+'レース周り'] = race_data[2].replace('(', '').replace(')', '').encode('utf-8')
        if u'外' in race_data[3]:
            self.row[self.key_name+'外回りフラグ'] = '外'
        else:
            self.row[self.key_name+'外回りフラグ'] = ''
        # 技術的負債ゾーン
#        self.row[self.key_name+'天気'] = race_data[-1].split('\n')[0].encode('utf-8')
#        self.row[self.key_name+'馬場状態'] = race_data[-1].split('\n')[1].encode('utf-8')
        self.row[self.key_name+'馬場状態'] = '良'
        browser.find_element_by_class_name('Icon_ArrowB').click()
        RaceDetail = browser.find_element_by_class_name('RaceDetail')
        self.row[self.key_name+'名称'] = browser.find_element_by_class_name('Race_Name').text.encode('utf-8')
        self.row[self.key_name+'条件'] = RaceDetail.find_element_by_tag_name('p').text.split('\n')[0].encode('utf-8')
        self.row[self.key_name+'賞金'] = RaceDetail.find_element_by_tag_name('p').text.split('\n')[1].split(u'：')[-1].encode('utf-8')
        browser.close()
        browser.quit()

    def __init__(self, race_dict, key_name = ''):
        self.key_name = key_name
        self.keys = []
        for key in BasicRaceStat.keys:
          self.keys.append(key_name + key)
        self.row = {}
        self.scraping(race_dict)
