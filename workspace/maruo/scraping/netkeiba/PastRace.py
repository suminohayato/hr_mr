# -*- coding: utf-8 -*-

from selenium import webdriver
import pandas as pd
from BasicRaceStat import BasicRaceStat

num_dict = {
    u'1': u'01',
    u'2': u'02',
    u'3': u'03',
    u'4': u'04',
    u'5': u'05',
    u'6': u'06',
    u'7': u'07',
    u'8': u'08',
    u'9': u'09',
    u'10': u'10',
    u'11': u'11',
    u'12': u'12',
}


class PastRace:
    keys = ["騎手名", "調教師名", "日付"]
    row_keys = ["騎手名", "日付", "タイム", "着順", "タイム差", "斤量",  "馬体重", "通過", "ペース", "上り"]
    zenso_list = []

    @classmethod
    def set_keys(cls, zenso_list):
        cls.zenso_list = zenso_list
        for zenso in zenso_list:
            for key in cls.row_keys:
                cls.keys.append(zenso + "_" + key)

    def not_found_row(self, zenso):
        for key in PastRace.row_keys:
            self.row[zenso + "_" + key] = ''
        for key in BasicRaceStat.keys:
            self.row[zenso + "_" + key] = ''

    def get_row(self, tds, zenso):
        self.row[zenso + "_" + "日付"] = tds[0].text.encode('utf-8')
        self.row[zenso + "_" + "騎手名"] = tds[12].text.encode('utf-8')
        self.row[zenso + "_" + "タイム"] = tds[17].text.encode('utf-8')
        self.row[zenso + "_" + "着順"] = tds[11].text.encode('utf-8')
        self.row[zenso + "_" + "タイム差"] = tds[18].text.encode('utf-8')
        self.row[zenso + "_" + "斤量"] = tds[13].text.encode('utf-8')
        self.row[zenso + "_" + "馬体重"] = tds[23].text.split('(')[0].encode('utf-8')
        self.row[zenso + "_" + "通過"] = tds[20].text.encode('utf-8')
        self.row[zenso + "_" + "ペース"] = tds[21].text.encode('utf-8')
        self.row[zenso + "_" + "上り"] = tds[22].text.encode('utf-8')

        num = tds[4].find_element_by_tag_name('a').get_attribute('href').split('/')[-2]
        year = num[0:4]
        place = num[4:8]
        day = num[8:10]
        race = num[10:12]
        kanji_place = tds[1].text[1:3]

        race_dict = {'year': year.encode('utf-8'), 'place': place.encode('utf-8'), 'day': day.encode('utf-8'), 'race': race.encode('utf-8'), 'kanji_place': kanji_place.encode('utf-8')}

        brs = BasicRaceStat(race_dict, key_name = zenso + "_")
        self.row.update(brs.row)


    def scraping(self, uma_id, now_race_dict):
        browser = webdriver.PhantomJS()
        url = 'http://db.netkeiba.com/horse/' + uma_id
        print "request: " + url
        browser.get(url)

        table = browser.find_element_by_class_name('db_h_race_results')
        trs = table.find_elements_by_tag_name('tr')
        trs.pop(0)

        now_flag = False
        zenso_index = 0

        for tr in trs:
            tds = tr.find_elements_by_tag_name('td')
            if now_flag:
                if zenso_index < len(PastRace.zenso_list):
                    self.get_row(tds, PastRace.zenso_list[zenso_index])
                    zenso_index += 1
                    if zenso_index >= len(PastRace.zenso_list):
                        break
                    continue
            if now_race_dict['year'] == tds[0].text.split('/')[0] and now_race_dict['kanji_place'] in tds[1].text.encode('utf-8'):
                if now_race_dict['day'] == num_dict[tds[1].text.encode('utf-8').split(now_race_dict['kanji_place'])[1]].encode('utf-8') and now_race_dict['race'] == num_dict[tds[3].text].encode('utf-8'):
                    now_flag = True

                    prof_table = browser.find_element_by_class_name('db_prof_table')
                    trs = prof_table.find_elements_by_tag_name('tr')
                    self.row['調教師名'] = trs[1].text.replace(u'調教師 ', '').encode('utf-8')
                    self.row['騎手名'] = tds[12].text.encode('utf-8')
                    self.row['日付'] = tds[0].text.encode('utf-8')
            while zenso_index < len(PastRace.zenso_list):
                self.not_found_row(PastRace.zenso_list[zenso_index])
                zenso_index += 1

        browser.close()
        browser.quit()

    def __init__(self, uma_id, now_race_dict):
        self.row = {}
        self.scraping(uma_id, now_race_dict)
