year=2016
place=小倉
#day=01
# race=01

# {'1002': '小倉',
#   '0402': '新潟',
#   '0101': '札幌'}

races=("01" "02" "03" "04" "05" "06" "07" "08" "09" "10" "11" "12")
days=("01" "02" "03" "04" "05" "06")
#days=("09" "10")
# races=("01" "02")

i=0
for day in ${days[@]}; do
  j=0
  for race in ${races[@]}; do
      python ../netkeiba_odds.py ${year} ${place} ${day} ${race} tanfuku
      python ../netkeiba_odds.py ${year} ${place} ${day} ${race} umaren
      python ../netkeiba_odds.py ${year} ${place} ${day} ${race} umatan
      python ../netkeiba_odds.py ${year} ${place} ${day} ${race} wide
      python ../netkeiba_odds.py ${year} ${place} ${day} ${race} wakuren
      let j++
  done
  let i++
done
