year=2016
place=0402
day=01
# race=01

# {'1002': '小倉',
#   '0402': '新潟',
#   '0101': '札幌'}

# races=("06" "07" "08")
races=("01" "02" "03" "04" "05" "06" "07" "08" "09" "10" "11" "12")
# races=("01" "02" "03")
# days=("04")
days=("01" "02" "03" "04")
# races=("05")
# races=("08")

i=0
for day in ${days[@]}; do
  j=0
  for race in ${races[@]}; do
    python ../netkeiba_shutuba.py ${year} ${place} ${day} ${race}
    let j++
  done
  let i++
done

