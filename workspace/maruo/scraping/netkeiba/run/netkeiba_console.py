# -*- coding: utf-8 -*-

from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd
import random
from time import sleep
import itertools
import os,sys,yaml

f = open('places_dict.yaml', 'r')
places_dict = yaml.load(f)
f.close()

f = open('waku_dict.yaml', 'r')
waku_dict = yaml.load(f)
f.close()

url = sys.argv[1]
browser = webdriver.PhantomJS()
print "request: " + url
browser.get(url)
