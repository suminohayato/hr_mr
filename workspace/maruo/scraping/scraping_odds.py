# -*- coding: utf-8 -*-

from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd
import random
from time import sleep
import itertools
import os,sys

places_dict = {'1002': '新潟',
        '0402': '小倉',
        '0101': '札幌'}
days = ['01', '02', '03', '04']
races = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
output_path = 'output/'

place = str(sys.argv[1])
day = str(sys.argv[2])
race = str(sys.argv[3])

output_file = os.path.join(output_path, 'odds_' + places_dict[place] + day + '_r' + race + ".csv")

if os.path.exists(output_file):
    exit()

url = 'http://race.netkeiba.com/?pid=odds&id=p2016' + place + day + race + '&mode=top'
browser = webdriver.PhantomJS()
#request
print "request: " + url
browser.get(url)

menu_lists = browser.find_element_by_class_name('sub_menu').find_elements_by_tag_name('li')
menu_lists[1].click()

table = browser.find_element_by_id('tblHorseList')
trs = table.find_elements_by_tag_name('tr')
trs.pop(0)

keys = ['枠番', '馬番', '馬名', '単勝オッズ', '複勝オッズ最小', '複勝オッズ最大']

df = pd.DataFrame([] , columns = keys)

for tr in trs:
    tds = tr.find_elements_by_tag_name('td')
    waku_ban = int(tds[0].text)
    uma_ban = int(tds[1].text)
    name = tds[3].text.encode('utf-8')
    tan = float(tds[4].text)
    fuku = tds[5].text
    fuku_min = float(fuku.split('-')[0])
    fuku_max = float(fuku.split('-')[1])
    _df = pd.DataFrame([[waku_ban, uma_ban, name, tan, fuku_min, fuku_max]], columns = keys)
    df = df.append(_df)

df.to_csv(output_file)

#sleep
sleep(random.uniform(2,4))

browser.close()
browser.quit()

# html_source = browser.page_source
# bs_obj = BeautifulSoup(html_source)
