cat "places.csv" | while read place
do
  cat "days.csv" | while read day
  do
    cat "races.csv" | while read race
    do
      python scraping_odds.py ${place} ${day} ${race}
    done
  done
done
