# -*- coding: utf-8 -*-
import smtplib
import os,sys
import commands
import datetime,time
import pandas as pd
from email.Header import Header
from email.Utils import formatdate
from email.MIMEText import MIMEText
import workspace.maruo.scraping.netkeiba.netkeiba_shutuba as netkeiba_shutuba
import workspace.maruo.scraping.netkeiba.netkeiba_odds as netkeiba_odds

#gmailのアドレスとパスワード
from_addr = os.getenv("from_addr")
password = os.getenv("password")
#送り先(のリスト)
to_addrs = ['romio_cinderella@yahoo.co.jp','kantasugiyama96@gmail.com','hs4s@i.softbank.jp']


#何分前にオッズスクレイピングするか
minutes = 15

num_dict = {1:"01",2:"02",3:"03",4:"04",5:"05",6:"06",7:"07",8:"08",9:"09",10:"10",11:"11",12:"12"}

HR_PATH = os.getenv("HR_PATH")
input_data_path  = os.path.join(HR_PATH,"workspace/maruo/scraping/netkeiba/run/output/")

def create_message(from_addr, to_addr, subject, body):
    msg = MIMEText(body)
    msg['Subject'] = subject
    msg['From'] = from_addr
    msg['To'] = to_addr
    msg['Date'] = formatdate()
    return msg

def create_message2(from_addr, to_addr, subject, body, encoding):
    # 'text/plain; charset="encoding"'というMIME文書を作ります
    msg = MIMEText(body, 'plain', encoding)
    msg['Subject'] = Header(subject, encoding)
    msg['From'] = from_addr
    msg['To'] = to_addr
    msg['Date'] = formatdate()
    return msg

def send_via_gmail(from_addr, to_addr, password, msg):
    s = smtplib.SMTP('smtp.gmail.com', 587)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login(from_addr, password)
    s.sendmail(from_addr, [to_addr], msg.as_string())
    s.close()

def send_predict_result(place_name,race_no):
    title = "%s_%s_result" % (place_name,race_no)
    commands.getoutput("source ~/.bash_profile")
    body = commands.getoutput("python calc_return.py bet_min_risk WIN %s %s" % (place_name,race_no))
    body2 = commands.getoutput("python calc_return.py bet_min_risk ALL %s %s" % (place_name,race_no))
    body += body2
    print body
    for to_addr in to_addrs:
        msg = create_message2(from_addr, to_addr, unicode(title), unicode(body), 'ISO-2022-JP')
        send_via_gmail(from_addr, to_addr, password, msg)

def shutuba_scraping(place_name,race):
    #スクレイピングをする
    year,place,day = place_name[:4],place_name[4:-2],place_name[-2:]
    race = num_dict[race]
    place = place.decode('utf-8')
    return netkeiba_shutuba.main(year, place, day, race,
                                     replace_flag=True,
                                     result_flag=False,
                                     kanji_flag=True)

def odds_scraping(place_name,race):
    #スクレイピングをする
    year,place,day = place_name[:4],place_name[4:-2],place_name[-2:]
    race = num_dict[race]
    place = place.decode('utf-8')
    for odds_type in ['tanfuku', 'umatan', 'umaren', 'wakuren', 'wide']:
        netkeiba_odds.main(year, place, day, race,
                               replace_flag=True,
                               result_flag=False,
                               kanji_flag=True,
                               odds_type=odds_type)

#発走時間
def get_start_time(place_name,race_no):
    csv_name_ = "shutuba_%s_r%s.csv" % (place_name,num_dict[race_no])
    if not os.path.exists(os.path.join(input_data_path,place_name,csv_name_)):
        csv_name_ = "shutuba_%s_r%s_result.csv" % (place_name,num_dict[race_no])

    df = pd.read_csv(os.path.join(input_data_path,place_name,csv_name_))
    return pd.to_datetime(df["日付"].iloc[0] + " " + df["時刻"].iloc[0])

def test():
    #smtplib.SMTPAuthenticationErrorが出る場合はgmailで安全性の低いアプリのアクセスを許可するかなんかする
    print "send_email test"
    for to_addr in to_addrs:
        msg = create_message2(from_addr, to_addr, u"タイトル", u"本文", 'ISO-2022-JP')
        send_via_gmail(from_addr, to_addr, password, msg)


#その日に各競馬場に対してスクリプトを走らせる感じ(中山,阪神だったら2つ走らせる）
#例：$python send_email.py 2016小倉01
if __name__ == '__main__':
    if len(sys.argv) == 1:
        exit(test())

    place_name = sys.argv[1]

    r_start = 1
    if len(sys.argv) >= 3:
        r_start = int(sys.argv[2])

    for i in range(r_start,13):
        shutuba_message = shutuba_scraping(place_name,i)
        while True:
            if shutuba_message['flag']:
                start_time = get_start_time(place_name,i)
                print "next race is {0}_r{1} at {2}".format(place_name,num_dict[i],start_time)
                if start_time - datetime.timedelta(minutes=minutes) < datetime.datetime.now():
                    print "working.."
                    odds_scraping(place_name,i)
                    print "sending.."
                    send_predict_result(place_name,i)
                    break
                else:
                    time.sleep(5)
            else:
                print shutuba_message['message']
                break
