#!/usr/bin/env python
#  -*- coding: utf-8 -*-

import numpy as np
from numpy.random import *


def abs_error(a,b):
    return np.abs(a-b).mean()

def ms_error(a,b):
    return ((a-b)**2).mean()

def rms_error(a,b):
    return np.sqrt(((a-b)**2).mean())

def error_expected_val(n,func):
    a = np.array([i for i in range(n)])
    diff = np.zeros(10000)
    for i in range(10000):
        b = choice(a,n,replace=False)
        diff[i] = func(a,b)
    return diff.mean()

if __name__ == "__main__":
    for i in range(18):
        print error_expected_val(i+1,rms_error) / (i+1)



'''
ランダムならabs_error/n の期待値はn->infで1/3に収束
0.0
0.24935
0.295866666667
0.3126875
0.320104
0.324633333333
0.325759183673
0.32840625
0.328239506173
0.330136
0.330109090909
0.331627777778
0.331694674556
0.331647959184
0.331838222222
0.331259375
0.33101384083
0.332241358025

rms_error/n の期待値はn->infで0.408??に収束
0.0
0.24775
0.340319464874
0.371563166924
0.384820882905
0.391749987587
0.394141008555
0.394452656716
0.396038275052
0.400720625803
0.400702523661
0.402228090485
0.402717314204
0.403980364011
0.403243982125
0.404541935052
0.404446599983
0.403840114227
'''