#!/usr/binenv python
# -*- coding: utf-8 -*-

import pandas as pd
import sys,os
import numpy as np
import add_previous_race_data as add_pre
from calc_winning_percentage import *

HR_PATH = os.getenv("HR_PATH")
odds_data_path = os.path.join(HR_PATH,"workspace/maruo/scraping/netkeiba/run/output/")


num_dict = {1:"01",2:"02",3:"03",4:"04",5:"05",6:"06",7:"07",8:"08",9:"09",10:"10",11:"11",12:"12"}
place_names = ["2016小倉"+num_dict[i+1] for i in range(0)] + \
              ["2016新潟"+num_dict[i+1] for i in range(0)] + \
              ["2016札幌"+num_dict[i+1] for i in range(0)] +\
              ["20162札幌"+num_dict[i+1] for i in range(6)]

input_data_path  = os.path.join(HR_PATH,"workspace/maruo/scraping/netkeiba/run/output/")
output_data_path = os.path.join(HR_PATH,"workspace/maruo/scraping/netkeiba/run/output_cleaned/")

add_prev = True
if add_prev:
    print "loading..."
    HR_DATA_PATH = os.getenv("HR_DATA_PATH")
    #HR_DATA_PATH = '/Users/kyoh/Downloads/' #あとで消すbymaru
    df_org = pd.read_csv(os.path.join(HR_DATA_PATH,"HR_data_2014_20160904_cleaned.csv"))

kishu_name_dict = {k:v for v,k in pd.read_csv("data/kishu_name.csv").as_matrix()}
tyokyoshi_name_dict = {k:v for v,k in pd.read_csv("data/tyokyoshi_name.csv").as_matrix()}

def input_dataset_reform(df,add_prev = add_prev,ninki = None):
    df = df.rename(columns = {"時刻":"発走時刻","斤量":"重量","騎手名":"騎手","競技場":"競馬場","順位":"着順","調教師名":"調教師"})
    df["競馬場"] = df["競馬場"].str.replace("2札幌","札幌")
    df["日付"] = df["日付"].str.replace("/","-")
    df["グレード"] = convert_requirement(df)
    df["騎手"] = convert_kishu_name(df["騎手"])
    df["馬場状態"] = df["馬場状態"].str.replace("稍","稍重").str.replace("不","不良")
    df["地面"] = df["レース情報"].str.contains("芝") * 1 + df["レース情報"].str.contains("ダ") * 2
    df["外回りフラグ"] = df["レース情報"].str.contains("外") * 1
    df["コース回り"] = df["レース周り"].str.contains("右") * 1 + df["レース周り"].str.contains("直") * 2 + df["レース周り"].str.contains("左") * 3
    df["距離"] = df["レース情報"].str.extract('([0-9]+)').astype(float).dropna(0)
    df["性別"] = df["性別"].str.replace("セ","せん")
    df["馬数"] = df.shape[0]
    df["調教師"] = convert_tyokyoshi(df["調教師"])
    df["人気"] = ninki
    df = df.reindex(columns = pd.read_csv("data/column.csv")["column"])
    df["今回ｸﾗｽ"] = add_konkai_class(df)

    santi_list = {"(混)":"混合","国際":"国際","父":"父","市":"市","九州産馬":"九州産馬"}
    for name in santi_list:
        df["条件_産地"] = df["条件_産地"].mask(df["条件"].str.contains(name),santi_list[name])

    df["条件_指定"] = pd.DataFrame(np.zeros(df.shape[0]))
    sitei_list = {"[指]":"指定","(指)":"（指定）","(特指)":"（特指）"}
    for name in sitei_list:
        df["条件_指定"] = df["条件_指定"].mask(df["条件"].str.contains(name),sitei_list[name])

    if add_prev:
        df = df.reindex(columns = pd.read_csv("data/column.csv")["column"])
        df = add_pre.add_previous_race_data_to_csv(df,df_org)
        df = add_pre.add_past_all_result(df,df_org)
    return df

def convert_requirement(df):
    columns = {}
    grade_list = {"新馬":"新馬","未勝利":"未勝利","５００万下":"500万円以下","１０００万下":"1000万円以下","１６００万下":"1600万円以下","オープン":"オープン"}
    grade_list_big = ["ＧＩ","ＧＩＩ","ＧＩＩＩ"]

    columns["グレード"] = pd.DataFrame(np.zeros(df.shape[0]))
    for name in grade_list:
        columns["グレード"] = columns["グレード"].mask(df["条件"].str.contains(name),grade_list[name])
    for name in grade_list_big:
        columns["グレード"] = columns["グレード"].mask(df["名称"].fillna("0").str.contains(name),name)

    return columns["グレード"]

def convert_kishu_name(column):
    column = column.tolist()
    def c(i):
        if i in kishu_name_dict.keys():
            return kishu_name_dict[i]
        else:
            print "Error not found %s in 騎手名" % i
            return None

    return [c(i) for i in column]

def convert_tyokyoshi(column):
    column = column.apply(lambda x: x.split(" (")[0]).tolist()
    def c(i):
        if i in tyokyoshi_name_dict.keys():
            return tyokyoshi_name_dict[i]
        else:
            print "Error not found %s in 調教師名" % i
            return None

    return [c(i) for i in column]

def add_konkai_class(df):
    grade_list = ["未勝利","新馬","500万円以下","1000万円以下","1600万円以下","オープン"]
    grade_list_big = ["ＧＩ","ＧＩＩ","ＧＩＩＩ"]
    cd = [5,10,20,30,40,60]
    cd_big = [30,-10,-10]

    column_ = df["今回ｸﾗｽ"]
    column_ = 0
    for i in range(len(grade_list)):
        column_ += df["グレード"].fillna("0").str.contains(grade_list[i]) * cd[i]

    for i in range(len(grade_list_big)):
        column_ += df["名称"].fillna("0").str.contains(grade_list_big[i]) * cd_big[i]

    if column_.sum() == 0:
        print df["条件"]
        exit()
    return column_

def odds_to_ninki(odds):
    ninki_order = np.array(odds).argsort()
    ninki = np.zeros(len(odds))
    for i in range(len(odds)):
        ninki[ninki_order[i]] = i+1
    return ninki

def main(place_names,num_dict = num_dict):
    for place_name in place_names:
        for i in num_dict.keys():
            csv_name_ = "shutuba_%s_r%s_result.csv" % (place_name,num_dict[i])
            if not os.path.exists(os.path.join(input_data_path,place_name,csv_name_)):
                csv_name_ = "shutuba_%s_r%s.csv" % (place_name,num_dict[i]) #競技結果なければこっちを選択

            odds_csv_name = "odds_%s_r%s単複.csv" % (place_name,num_dict[i])
            try:
                df = pd.read_csv(os.path.join(input_data_path,place_name,csv_name_))
            except:
                print "didn't exist ",csv_name_
                continue

            #人気をスクレイピングしていないのでオッズから情報を得る
            ninki = odds_to_ninki(load_odds(odds_data_path, place_name, num_dict[i],ticket = "WIN"))

            if not os.path.exists(os.path.join(output_data_path,place_name)):
                print "mkdir",os.path.join(output_data_path,place_name)
                os.mkdir(os.path.join(output_data_path,place_name))

            df = input_dataset_reform(df,ninki = ninki)
            df.to_csv(os.path.join(output_data_path,place_name,csv_name_),index = False)

def main_single_race(place_name,race_no):
    main([place_name],{race_no:num_dict[race_no]})

if __name__ == "__main__":
    #input_dataset_reform(pd.read_csv(sys.argv[1])).to_csv(sys.argv[1][:-4]+"_cleaned.csv")
    if len(sys.argv) > 2:
        place_name = sys.argv[1]
        race_no = int(sys.argv[2])
        main_single_race(place_name,race_no)
    else:
        main(place_names)
