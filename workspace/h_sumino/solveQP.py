# coding:utf-8

import pandas as pd
import numpy as np
from cvxopt import matrix, solvers
import math

def portfolio(odds,p,graph = False,acceptable_risk = 5,split = 10,p_matrix = None):
    '''
    :param odds: 各馬のオッズ
    :param p: 各馬券の予想勝率
    :param graph: グラフ表示するかどうか
    :param acceptable_risk: 許容最大リスク（リスクをこの値以下の条件下でリターンの最大化を考える）
    :param split: 離散化（最小単位1/split)
    :param p_matrix : (i,j) = 馬券i,jが同時に当たる確率
    :return: 比率、リスク、リターン
    '''
    if p_matrix == None:
        n = len(odds)
        p_matrix = np.zeros([n,n])

    # このret(return)とriskを適切に
    ret = odds * p
    risk = np.array([[odds[i] * odds[i] * p[i] * (1 - p[i]) if i == j
                      else odds[i] * odds[j] * (p_matrix[i][j] - p[i] * p[j]) for i in range(len(ret))] for j in range(len(ret))])

    # 係数を決めている(http://cvxopt.org/userguide/coneprog.html#quadratic-programming 参照)
    P = matrix(risk.tolist())
    q = matrix(np.zeros((risk.shape[0],)).tolist())
    G = matrix((-np.eye(risk.shape[0])).tolist())
    h = matrix((np.zeros((risk.shape[0],))).tolist())

    # Aの1列目はリターンの制約、2列目は賭ける割合の和は1の制約
    A_np = np.ones((2, ret.shape[0]), dtype=np.float64)
    A_np[0, :] = ret
    #A_np = np.array([[ret],[1 for i in range(ret.shape[0])]])
    A = matrix(np.transpose(A_np).tolist())

    d = 0.01 # 幅
    start = int(ret.min() * 100) + 1 # リターン最小
    end = int(ret.max() * 100) + 1 # リターン最大
    sigma_p = [] # リスク(リターンの標準偏差)
    solutions = []
    min_sigma = 1e10
    min_sigma_ratio = None
    preds_return = None

    for i in range(start, end):
        #print i,end
        mu0 = i * d
        b = matrix([mu0, 1.0])
        options = {"show_progress":False}
        sol = solvers.qp(P, q, G, h, A, b,options=options)
        solutions.append(np.array(sol["x"]).T[0])
        sigma_p.append(math.sqrt(sol["primal objective"] * 2))
        if math.sqrt(sol["primal objective"] * 2) < acceptable_risk:
            min_sigma = math.sqrt(sol["primal objective"] * 2)
            min_sigma_ratio = np.array(sol["x"]).T[0]
            preds_return = mu0

    if graph:
        import matplotlib.pyplot as plt
        mu0s = [i * d for i in range(start, end)]
        plt.plot(sigma_p, mu0s)
        plt.xlabel("risk")
        plt.ylabel("return")
        plt.show()
        plt.clf()

    if split:
        min_sigma_ratio = ratio_discretization(min_sigma_ratio,split)

    return min_sigma_ratio,min_sigma,preds_return

def ratio_discretization(ratio,split = 10):
    '''
    比の離散化
    split * 100円の賭け金が必要
    '''
    block = np.array(map(int,ratio * split))
    remain_block = split - block.sum()

    remain = ratio - (block * 1.0 /split)
    remain_rank = np.array(-1 * remain).argsort()
    for i in range(remain_block):
        block[remain_rank[i]] += 1

    return block * 1.0 / split

#ratio = np.array([0.11,0.24,0.32,0.33])
#print ratio_discretization(ratio)

#odds = np.array([9.0,4.0,1.8,1.5])
#p = np.array([0.1,0.2,0.3,0.4])
#print portfolio(odds,p,False)