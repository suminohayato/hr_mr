#!/usr/bin/env python
#  -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import sys
import datetime
from progressbar import ProgressBar

previous_name = ["前１回","前２回","前３回","前４回","前５回"]

race_data_column = {"競馬場":"馬場",
                    "日付":"日付",
                    "着順":"着順",
                    "今回ｸﾗｽ":"クラス",
                    "距離":"距離",
                    "地面":"地面",
                    "人気":"人気",
                    "コース回り":"コース回り",
                    "外回りフラグ":"外回りフラグ",
                    "タイム":"タイム",
                    "タイム差":"ﾀｲﾑ差",
                    "馬場状態":"状態",
                    "馬数":"馬数",
                    "重量":"斤量",
                    "騎手":"騎手",
                    "馬番":"ｹﾞｰﾄ",
                    "2角":"2ｺｰﾅｰ",
                    "3角":"3ｺｰﾅｰ",
                    "4角":"4ｺｰﾅｰ",
                    "体重":"馬体重",
                    "推定3F":"上がり"}


#todo 間隔,ﾀｲﾑ差

def value_exists(val):
    if type(val) == str:
        if val == "":
            return False
        else:
            return True
    elif np.isnan(val):
        return False
    elif val == 0:
        return False
    return True

def time_deco(func):
    """
    デバッグ用デコレータ
    実効時間を表示するデコレータ
    """
    import functools,datetime
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start = datetime.datetime.today()
        result = func(*args, **kwargs)
        end = datetime.datetime.today()
        print '-------------------------------------------------------------'
        print func
        print 'runningtime:', end - start
        print '-------------------------------------------------------------'
        return result
    return wrapper

def search_previous_race_data(df,date,horse_name,previous_count):
    date_5years_ago = str(int(date[:4])-5)+date[4:]
    horse_data = df[df["馬名"] == horse_name][df["日付"] > date_5years_ago][df["日付"] < date]
    return horse_data.iloc[-previous_count:]


def get_previous_race_data(df,series,previous_count = 5):
    previous_race_data = search_previous_race_data(df,series["日付"],series["馬名"],previous_count)
    size = len(previous_race_data)

    previous_datas = []
    for i in range(previous_count):
        if size >= i+1:
            previous_datas.append(previous_race_data.iloc[size-(i+1)].as_matrix())
        else:
            previous_datas.append(np.array([float("nan")] * df.shape[1]))

    return previous_datas

def add_previous_race_data_to_csv_old(df,df_org):
    previous_name = ["前回","前々回","前３回"]
    previous_1s,previous_2s,previous_3s = [],[],[]

    pbar = ProgressBar(maxval = df.shape[0])
    for ix in range(len(df)):
        pbar.update(ix+1)
        previous_datas = get_previous_race_data(df_org,df.iloc[ix])
        for i in [1,2,3]:
            eval("previous_"+str(i)+"s").append(previous_datas[i-1])

    for i in [1,2,3]:
        #print eval("previous_"+str(i)+"s")
        #print np.array(eval("previous_"+str(i)+"s")).shape
        previous_race_data_ = pd.DataFrame(eval("previous_"+str(i)+"s"),columns = df_org.columns)
        for key in race_data_column.keys():
            #print key,previous_race_data_[key]
            df[previous_name[i-1]+race_data_column[key]] = previous_race_data_[key].tolist()

        #特殊処理カラム
        if i == 1:
            df["今回間隔"] = (pd.to_datetime(df["日付"]) - pd.to_datetime(df["前回日付"].fillna("2000-01-01"))).astype("int") / 86400000000000
            df["今回間隔"] = df["今回間隔"].mask(df["今回間隔"] > 1000,0)
        elif i <= 3:
            try:#AWS上でなぜかエラーでるので仕方なく todo
                df[previous_name[i-2]+"間隔"] = (pd.to_datetime(df[previous_name[i-2]+"日付"]).fillna("2000-01-01") -
                                           pd.to_datetime(df[previous_name[i-1]+"日付"].fillna("2000-01-01"))).astype("int") / 86400000000000
                df[previous_name[i-2]+"間隔"] = df[previous_name[i-2]+"間隔"].mask(df[previous_name[i-2]+"間隔"] > 1000,0)
            except:
                df[previous_name[i-2]+"間隔"] = 30

        else:
            pass

        #馬場の処理(阪神→阪)
        if (df[previous_name[i-1]+"馬場"] == df[previous_name[i-1]+"馬場"]).sum() == 0:#全部NaN
            continue
        flag = df[previous_name[i-1]+"馬場"].fillna("0").str.contains("中京")
        df[previous_name[i-1]+"馬場"] = df[previous_name[i-1]+"馬場"].str[:3]
        df[previous_name[i-1]+"馬場"] = df[previous_name[i-1]+"馬場"].mask(flag,"中京")

    return df


def add_previous_race_data_to_csv(df,df_org):
    previous_1s,previous_2s,previous_3s,previous_4s,previous_5s = [],[],[],[],[]

    pbar = ProgressBar(maxval = df.shape[0])
    for ix in range(len(df)):
        pbar.update(ix+1)
        previous_datas = get_previous_race_data(df_org,df.iloc[ix])
        for i in [1,2,3,4,5]:
            eval("previous_"+str(i)+"s").append(previous_datas[i-1])

    for i in [1,2,3,4,5]:
        #print eval("previous_"+str(i)+"s")
        #print np.array(eval("previous_"+str(i)+"s")).shape
        previous_race_data_ = pd.DataFrame(eval("previous_"+str(i)+"s"),columns = df_org.columns)
        for key in pd.read_csv("data/column_org.csv")["column"]:
            #print key,previous_race_data_[key]
            df[previous_name[i-1]+key] = previous_race_data_[key].tolist()

        #特殊処理カラム
        '''AWSで何故かエラー出る 使ってないのでとりあえずコメントアウト
        if i == 1:
            df["今回間隔"] = (pd.to_datetime(df["日付"]) - pd.to_datetime(df["前１回日付"].fillna("2000-01-01"))).astype("int") / 86400000000000
            df["今回間隔"] = df["今回間隔"].mask(df["今回間隔"] > 1000,0)
        elif i <= 5:
            df[previous_name[i-2]+"間隔"] = (pd.to_datetime(df[previous_name[i-2]+"日付"]).fillna("2000-01-01") - pd.to_datetime(df[previous_name[i-1]+"日付"].fillna("2000-01-01"))).astype("int") / 86400000000000
            df[previous_name[i-2]+"間隔"] = df[previous_name[i-2]+"間隔"].mask(df[previous_name[i-2]+"間隔"] > 1000,0)
        else:
            pass
        '''
        #馬場の処理(阪神→阪)
        #if (df[previous_name[i-1]+"馬場"] == df[previous_name[i-1]+"馬場"]).sum() == 0:#全部NaN
        #    continue
        #flag = df[previous_name[i-1]+"馬場"].fillna("0").str.contains("中京")
        #df[previous_name[i-1]+"馬場"] = df[previous_name[i-1]+"馬場"].str[:3]
        #df[previous_name[i-1]+"馬場"] = df[previous_name[i-1]+"馬場"].mask(flag,"中京")

    return df

def add_past_all_result(df,df_org,detail = True):
    pbar = ProgressBar(maxval = df.shape[0])
    j = 0
    rows = []
    for index,row in df.iterrows():
        pbar.update(j+1)
        data = search_previous_race_data(df_org,row["日付"],row["馬名"],99)
        row["合計"] = data.shape[0]

        chakugai = 0
        for i in [1,2,3]:
            n = data["着順"][data["着順"] == str(i)].shape[0]
            row[str(i)+"着計"] = n
            if data.shape[0]:
                row[str(i)+"着率"] = n * 1.0 / row["合計"]
            chakugai += n
        row["着外計"] = chakugai
        row["最高ｸﾗｽ"] = data["今回ｸﾗｽ"].max()

        '''
        if detail:
            row["芝勝率"] =
            row["芝3着内率"] = data[data["着順"] <= 3][data[""]]
            row["直勝率"]
            row["右勝率"]
            row["左勝率"]
        '''
        rows.append(row)
        j += 1
    return pd.DataFrame(rows,columns=df.columns)

if __name__ == "__main__":
    df = pd.read_csv("/Users/HayatoSumino/Desktop/HRdata/HR_data_2006_2016_cleaned_previous_added2.csv")
    #df = pd.read_csv("/Users/HayatoSumino/Desktop/HRdata/test.csv")

    #一応
    df = df.reindex(columns = pd.read_csv("data/column.csv")["column"])

    df_org = df.copy()
    #df_org = pd.read_csv("/Users/HayatoSumino/Desktop/HRdata/HR_data_2006_2016_cleaned_previous_added2.csv")
    #df = add_past_all_result(df,df_org)
    df = add_previous_race_data_to_csv(df,df_org)

    df.to_csv("/Users/HayatoSumino/Desktop/HRdata/HR_data_2006_2016_cleaned_previous_added3.csv",index = False)
