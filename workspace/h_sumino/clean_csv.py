#!/usr/bin/env python
#  -*- coding: utf-8 -*-

import pandas as pd
import sys
import numpy as np

def clean_csv(df):
    #df = drop_shogai(df)
    cdf = pd.DataFrame({},columns = pd.read_csv("column_old0.csv")["column"])

    for key in df.columns:
        cdf[key] = df[key]

    columns = convert_requirement(cdf)
    for key in columns.keys():
        cdf[key] = columns[key]

    cdf["日付"] = pd.to_datetime(cdf["日付"])
    cdf["競馬場"] = cdf["レース"].str[:6]
    cdf["馬場状態"] = convert_condition(cdf["馬場状態"])
    cdf["タイム"] = convert_time(cdf["タイム"])
    cdf["タイム差"] = add_time_diff(cdf)
    cdf["馬数"] = add_n_horse(cdf)
    cdf["今回ｸﾗｽ"] = add_konkai_class(cdf)

    columns = convert_weight(cdf["体重"])
    for key in columns.keys():
        #print key,cdf[key].shape,len(columns[key])
        cdf[key] = columns[key]

    columns = convert_distance(df["距離"])
    for key in columns.keys():
        cdf[key] = columns[key]

    for pre_name in ["","前回","前々回","前３回"]:
        columns = convert_distance(df[pre_name+"距離"],pre_name)
        for key in columns.keys():
            cdf[key] = columns[key]
        cdf[pre_name+"タイム"] = convert_time(cdf[pre_name+"タイム"])

    return cdf

def drop_shogai(df):
    s = df["条件"].str.contains("障害") * 1
    return df[s == 0]

def convert_condition(column):
    def convert_condition_(val):
        vals = val.split("：")
        if len(vals) == 3:
            return val
        else:
            return vals[1]

    return [convert_condition_(val) for val in column]

def convert_distance(column,pre_name = ""):
    columns = {}
    columns[pre_name+"地面"] = column.str.contains("芝") * 1 + column.str.contains("ダ") * 2
    columns[pre_name+"コース回り"] = column.str.contains("右") * 1 + column.str.contains("直") * 2 + column.str.contains("左") * 3
    columns[pre_name+"外回りフラグ"] = column.str.contains("外") * 1
    columns[pre_name+"距離"] = column.str.extract('([0-9]+)').astype(float).dropna(0)
    return columns

def convert_weight(column):
    columns = {}
    columns["体重"] = [str(val).split(" ")[0] for val in column]

    def sabun(val):
        valspl = val.split(" ")
        if len(valspl) > 1:
            return valspl[1]
        else:
            return 0

    columns["体重(差分)"] = [sabun(str(val)) for val in column]
    columns["体重(差分)"] = pd.to_numeric(columns["体重(差分)"], errors='coerce')
    return columns

def convert_time(column):
    column = column.fillna("0:0.0")
    def convert_time_(val):
        vals = str(val).split(":")
        if len(vals) == 2:#例1:15.7
            return float(vals[0])*60 + float(vals[1])
        else:
            try:#例75.7
                return float(val)
            except:#クソデータ (もしあれば)
                return 0

    return [convert_time_(val) for val in column]
    #return [float(str(val).split(":")[0]) * 60 + float(str(val).split(":")[1]) for val in column]

def convert_requirement(df):
    columns = {}
    grade_list = ["新馬","未勝利","500万円以下","1000万円以下","1600万円以下","オープン"]
    grade_list_big = ["ＧＩ","ＧＩＩ","ＧＩＩＩ"]

    columns["グレード"] = pd.DataFrame(np.zeros(df.shape[0]))
    for name in grade_list:
        columns["グレード"] = columns["グレード"].mask(df["条件"].str.contains(name),name)
    for name in grade_list_big:
        columns["グレード"] = columns["グレード"].mask(df["名称"].fillna("0").str.contains(name),name)

    columns["条件_産地"] = pd.DataFrame(np.zeros(df.shape[0]))
    santi_list = ["混合","国際","父","市","九州産馬"]
    for name in santi_list:
        columns["条件_産地"] = columns["条件_産地"].mask(df["条件"].str.contains(name),name)

    columns["条件_指定"] = pd.DataFrame(np.zeros(df.shape[0]))
    sitei_list = ["指定","（指定）","（特指）","若手騎手"]
    for name in sitei_list:
        columns["条件_指定"] = columns["条件_指定"].mask(df["条件"].str.contains(name),name)

    columns["条件_性別"] = pd.DataFrame(np.zeros(df.shape[0]))
    sex_list = ["牡","牝","牡・牝"]
    for name in sex_list:
        columns["条件_性別"] = columns["条件_性別"].mask(df["条件"].str.contains(name),name)

    columns["条件_年齢"] = pd.DataFrame(np.zeros(df.shape[0]))
    age_list = ["2歳","3歳","3歳以上","4歳以上"]
    for name in age_list:
        columns["条件_年齢"] = columns["条件_年齢"].mask(df["条件"].str.contains(name),name)

    return columns

def add_konkai_class(df):
    grade_list = ["未勝利","新馬","500万円以下","1000万円以下","1600万円以下","オープン"]
    grade_list_big = ["ＧＩ","ＧＩＩ","ＧＩＩＩ"]
    cd = [5,10,20,30,40,60]
    cd_big = [30,-10,-10]

    column_ = df["今回ｸﾗｽ"]
    column_ = 0
    for i in range(len(grade_list)):
        column_ += df["条件"].fillna("0").str.contains(grade_list[i]) * cd[i]

    for i in range(len(grade_list_big)):
        column_ += df["名称"].fillna("0").str.contains(grade_list_big[i]) * cd_big[i]
    return column_

def add_n_horse(df):
    index = df.ix[:,["日付","レース"]].drop_duplicates().index
    n_horse_column = []
    for i in range(len(index) - 1):
        n_horse_column += [index[i+1] - index[i]] * (index[i+1] - index[i])
    n_horse_column += [df.shape[0] - index[len(index)-1]] * (df.shape[0] - index[len(index)-1])
    return n_horse_column

def add_time_diff(df):
    index = df.ix[:,["日付","レース"]].drop_duplicates().index.tolist()
    index.append(df.shape[0])

    time_1st_column = []
    for i in range(len(index) - 1):
        time_1st_column += [df.ix[index[i],"タイム"]] * (index[i+1] - index[i])

    return df["タイム"].as_matrix() - np.array(time_1st_column)

def process_diff(cdf):
    cdf = cdf.reindex(columns = pd.read_csv("column.csv")["column"])
    #cdf["タイム差"] = add_time_diff(cdf)
    #cdf["今回ｸﾗｽ"] = add_konkai_class(cdf)
    return cdf


if __name__ == "__main__":
    cdf = clean_csv(pd.read_csv(sys.argv[1]))
    cdf.to_csv(sys.argv[1][:-4]+"_cleaned.csv",index = False)
    #process_diff(pd.read_csv(sys.argv[1])).to_csv(sys.argv[2],index = False)

'''
v4 今回クラス追加
v5 タイム差追加 今回クラス修正(未勝利と新馬逆) 前走日付、カラムだけ追加(前走追加のため)
'''