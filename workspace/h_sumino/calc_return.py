#!/usr/bin/env python
#  -*- coding: utf-8 -*-

import os,sys
from calc_winning_percentage import *
from Predict_Time import Predict_Time
import evaluate_error as ee
import clean_csv as cc
#from draw_figure import *
from solveQP import *

HR_PATH = os.getenv("HR_PATH")
HR_DATA_PATH = os.getenv("HR_DATA_PATH")
HR_TRAIN_RESULT_PATH = os.getenv("HR_TRAIN_RESULT_PATH")
odds_data_path = os.path.join(HR_PATH,"workspace/maruo/scraping/netkeiba/run/output/")
input_data_path = os.path.join(HR_DATA_PATH,"predict_result/RNN1001/")
output_path = os.path.join(HR_TRAIN_RESULT_PATH,"RNN1001/")
'''
place_names = ["2016小倉01","2016小倉02","2016小倉03","2016小倉04","2016小倉05","2016小倉06","2016小倉07","2016小倉08",
               "2016新潟01","2016新潟02","2016新潟03","2016新潟04","2016新潟05","2016新潟06","2016新潟07","2016新潟08","2016新潟09","2016新潟10",
               "2016札幌01","2016札幌02","2016札幌03","2016札幌04","2016札幌05","2016札幌06"]
'''
num_dict = {1:"01",2:"02",3:"03",4:"04",5:"05",6:"06",7:"07",8:"08",9:"09",10:"10",11:"11",12:"12"}
place_names = ["2016小倉"+num_dict[i+1] for i in range(12)] + \
              ["2016新潟"+num_dict[i+1] for i in range(12)] + \
              ["2016札幌"+num_dict[i+1] for i in range(6)] +\
              ["20162札幌"+num_dict[i+1] for i in range(6)]


def print_result(func):
    import functools
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        print result
        return result
    return wrapper

def actual_return(actual_rank, preds_no, odds, ticket="WIN"):
    n = len(actual_rank)

    if ticket == "WIN":
        if actual_rank[preds_no] == 1:
            return odds[preds_no]
    elif ticket == "PLACE":
        if actual_rank[preds_no] <= 3:
            return odds[preds_no]
    elif ticket == "EXACTA":
        preds_horse = [preds_no/n, preds_no%n]
        if actual_rank[preds_horse[0]] == 1 and actual_rank[preds_horse[1]] == 2:
            return odds[preds_no]
    '''
    elif ticket == "QUINELLA":
        if actual_rank[preds_no[0]] <= 2 and actual_rank[preds_no[1]] <= 2:
            return True
    elif ticket == "QUINELLA_PLACE":
        if actual_rank[preds_no[0]] <= 3 and actual_rank[preds_no[1]] <= 3:
            return True
    elif ticket == "BRACKET QUINELLA":
        if actual_rank[preds_no[0]] <= 2 and actual_rank[preds_no[1]] <= 2:
            return True
    elif ticket == "TRIO":
        if actual_rank[preds_no[0]] <= 3 and actual_rank[preds_no[1]] <= 3 and actual_rank[preds_no[2]] <= 3:
            return True
    elif ticket == "TRIFECTA":
        if actual_rank[preds_no[0]] == 1 and actual_rank[preds_no[1]] == 2 and actual_rank[preds_no[2]] == 3:
            return True
    '''
    return 0

#正答率期待値
def correct_expected_val(df,odds,ticket="WIN"):
    if ticket == "WIN":
        val = 1.0
    elif ticket == "PLACE":
        val = 3.0
    return val/len(df)

#全馬賭け（ようは期待値）
def expected_val(df,odds,ticket="WIN"):
    if ticket == "WIN":
        rank = int(df[df["着順"] == 1]["馬番"].iloc[0]) - 1
        return odds[rank] / df.shape[0]
    elif ticket == "PLACE":
        return odds["fixed"].sum() / df.shape[0]

#ランダムに1馬選ぶ
def bet_random(df,odds,ticket = "WIN"):
    rank = int(random() * len(odds))

    if ticket == "WIN":
        return_ = actual_return(df["着順"], rank, odds, ticket)
    elif ticket == "PLACE":
        return_ = actual_return(df["着順"], rank, odds["fixed"], ticket)

    return_ = actual_return(df["着順"], rank, odds, ticket)

    if return_:
        print "当たり! (x%s)" % str(return_)
    else:
        print "はずれ…"
    return return_

#予想勝率が最も高い馬を選ぶ
def bet_certainty(df,odds,ticket="WIN",order = True):
    #order True 値が大きい方がランク高い
    preds = df["予測値"]
    #if df["今回ｸﾗｽ"].mean() == 10:
    #    return

    if order:
        preds_p = preds * -1
        rank = np.array(preds_p).argsort()
    else:
        rank = np.array(preds).argsort()

    #if preds[rank[0]] * len(preds) <= 2.5:
    #    print "じしんない"
    #    return

    if ticket == "WIN":
        return_ = actual_return(df["着順"], rank[0], odds, ticket)
    elif ticket == "PLACE":
        return_ = actual_return(df["着順"], rank[0], odds["fixed"], ticket)

    if return_:
        print "当たり! (x%s)" % str(return_)
    else:
        print "はずれ…"
    return return_#,preds[rank[0]] * len(preds)#(preds[rank[0]] - preds[rank[1]]) * len(preds)


#予測勝率が高い上位3馬を選ぶ
#todo 複勝は最後の処理を変える必要が有る
def bet_certainty_3(df,odds,ticket="WIN",order = True):
    #order True 値が大きい方がランク高い
    preds = df["予測値"]

    if order:
        preds_p = preds * -1
        rank = np.array(preds_p).argsort()
    else:
        rank = np.array(preds).argsort()
    #print preds_time
    #print odds
    #print rank,df["順位"].as_matrix().argsort()

    for i in range(3):
        return_ = actual_return(df["着順"], rank[i], odds, ticket)
        if return_:
            print "当たり! (x%s)" % str(return_)
            return return_ / 3
    else:
        print "はずれ…"
    return 0

def bet_lowest_odds(df,odds,ticket="WIN",order = True):
    #order True 値が大きい方がランク高い
    if ticket == "WIN":
        preds_no = odds.argsort()[0]
        return_ = actual_return(df["着順"], preds_no, odds, ticket)
    elif ticket == "PLACE":
        preds_no = odds["odds"].argsort()[0]
        return_ = actual_return(df["着順"], preds_no, odds["fixed"], ticket)

    if return_:
        print "当たり! (x%s)" % str(return_)
    else:
        print "はずれ…"
    return return_

#リターン期待値(勝率*オッズ)が最も高い馬を選ぶ
def bet_maxreturn(df,odds,ticket="WIN",odds_threshold = 9999):
    #odds_threshold オッズ一定値以上のものを除いて考える
    preds = df["予測値"] / df["予測値"].sum()

    if ticket == "WIN":
        odds[np.where(odds >= odds_threshold)] = 0
        preds_return = odds * preds
    elif ticket == "PLACE":
        odds["odds"][np.where(odds["odds"] >= odds_threshold)] = 0
        preds_return = odds["odds"] * preds

    rank = (-np.array(preds_return)).argsort()
    #print preds
    #print odds
    #print preds_return
    #print rank,df["順位"].as_matrix().argsort()

    if ticket == "WIN":
        return_ = actual_return(df["着順"], rank[0], odds, ticket)
    elif ticket == "PLACE":
        return_ = actual_return(df["着順"], rank[0], odds["fixed"], ticket)

    if return_:
        print "当たり! (x%s)" % str(return_)
    else:
        print "はずれ…"
    return return_

#リターン期待値(勝率*オッズ)が最も高い馬を選ぶ
def bet_maxreturn_3(df,odds,ticket="WIN",odds_threshold = 9999):
    preds = df["予測値"] / df["予測値"].sum()

    if ticket == "WIN":
        odds[np.where(odds >= odds_threshold)] = 0
        preds_return = odds * preds
    elif ticket == "PLACE":
        odds["odds"][np.where(odds["odds"] >= odds_threshold)] = 0
        preds_return = odds["odds"] * preds

    rank = (-np.array(preds_return)).argsort()

    n = preds_return[preds_return > 1].shape[0]
    #n = 3
    if n == 0:
        return

    for i in range(n):
        return_ = actual_return(df["着順"], rank[i], odds, ticket)
        if return_:
            print "当たり! (x%s)" % str(return_)
            return return_ / n
    else:
        print "はずれ…"
    return 0

#WINのみの実装
#ratioの比率で各馬券にbet
def bet_diversify(df,odds,ticket = "WIN",ratio = []):
    assert len(ratio) == len(odds),"ERROR len(ratio) != len(odds)"
    return_ = np.array([actual_return(df["着順"], i, odds, ticket) for i in range(len(odds))])
    return (return_ * ratio).sum()

#WINのみの実装
def bet_min_risk(df,odds,ticket = "WIN",acceptable_risk = 5,returnratio = True):
    if ticket == "ALL":#応急処置 ほんとうはpredsと当たったかどうかのフラグを渡すようにしたい
        #preds = df["予測値"].as_matrix()
        preds = (df["予測値"] / df["予測値"].sum()).as_matrix()
    else:
        preds = (df["予測値"] / df["予測値"].sum()).as_matrix()
    ratio, min_sigma, preds_return = portfolio(odds,preds,acceptable_risk=acceptable_risk)

    if ratio is None:
        return None

    if returnratio:#実際に予測する時はこっち
        print "許容リスク",acceptable_risk
        print "予想%d%%" % (preds_return * df["予測値"].sum() * 100)
        return ratio

    return_ = bet_diversify(df,odds,ticket,ratio)
    print "回収率%d%%(予想%d%%)" % (int((return_ * ratio).sum() * 100),preds_return * 100)
    return np.round(return_,5)

#予想順位と正解順位の類似度を測る
def abs_error(df,odds,ticket="WIN",order = True):
    preds = df["予測値"]

    if order:
        preds_p = preds * -1
        rank = np.array(preds_p).argsort()
    else:
        rank = np.array(preds).argsort()

    rank_noorder = np.zeros(df.shape[0])
    for i in range(len(rank)):
        rank_noorder[rank[i]] = i+1

    actual_rank = map(int,df["着順"].tolist())
    return ee.abs_error(rank_noorder,actual_rank) / df.shape[0]

def bet_hybrid(df,odds,ticket = "WIN"):
    ratio = 0.8
    return ratio * bet_certainty(df,odds,"WIN") + (1-ratio) * bet_certainty_3(df,odds,"WIN")


def twelve_race_predict(place_name,func,ticket = "WIN"):
    returns = []

    for i in range(12):
        csv_name_ = "shutuba_%s_r%s_result.csv" % (place_name,num_dict[i+1])
        if ticket in ["WIN","PLACE"]:
            odds_csv_name = "odds_%s_r%s単複.csv" % (place_name,num_dict[i+1])
        elif ticket == "EXACTA":
            odds_csv_name = "odds_%s_r%s馬単.csv" % (place_name,num_dict[i+1])

        print csv_name_

        odds = load_odds(os.path.join(odds_data_path,place_name,odds_csv_name),ticket = ticket)
        if ticket == "PLACE":
            odds_fixed = pd.read_csv(os.path.join(odds_data_path,place_name,odds_csv_name[:-10]+"複勝結果.csv"))
            odds_fixed = np.array([odds_fixed[odds_fixed["馬番"] == j+1]["複勝結果"].iloc[0] / 100.0 if j+1 in odds_fixed["馬番"].tolist() else 0 for j in range(20)])
            odds = {"odds":odds,"fixed":odds_fixed}
        else:
            pass
            #odds = {"odds":odds}

        csv_path = os.path.join(input_data_path,place_name,csv_name_)
        try:
            df = pd.read_csv(csv_path)
        except:
            print "didn't exist ",csv_path
            continue

        return_ = func(df,odds,ticket = ticket)
        if not return_ is None:
            returns.append(return_)

    return np.array(returns)

def percent_round(num,n = 1):
    #n : 小数点以下何桁表示するか
    return int(num * 100), (num * eval("1e"+str(n+2))) % eval("1e"+str(n))

def main(func = "bet_min_risk",ticket = "WIN"):
    returnss = []
    for place_name in place_names:
        returns = twelve_race_predict(place_name,eval(func),ticket)
        for return_ in returns:
            returnss.append(return_)

    #return_and_preds_data = pd.DataFrame(returnss,columns = ["return_","preds_win_per"])
    returnss = np.array(returnss)
    correct_per = returnss[np.where(returnss !=  0)].shape[0] * 1.0 / len(returnss)

    print returnss
    print "RETURN %d.%d%%" % (percent_round(returnss.mean()))
    print "CORRECT %d.%d%%" % (percent_round(correct_per))
    print "COUNT %d" % len(returnss)

    #correct_per_preds_win_per(return_and_preds_data["preds_win_per"],return_and_preds_data.query("return_ > 0")["preds_win_per"])

    return returnss.mean(),correct_per

def main_single_race(place_name,race_no,func = bet_min_risk,ticket = "WIN"):
    csv_name_ = "shutuba_%s_r%s_result.csv" % (place_name,race_no)
    if not os.path.exists(os.path.join(input_data_path,place_name,csv_name_)):
        csv_name_ = "shutuba_%s_r%s.csv" % (place_name,race_no)

    if not os.path.exists(os.path.join(input_data_path,place_name,csv_name_)):
        print "not predicted. predicting..."
        #import Predict_Time as p
        #p.main_single_race(place_name,race_no)
        import commands
        check = commands.getoutput("python Predict_Time.py %s %s" % (place_name,race_no))
        #print check

    odds = load_odds(odds_data_path,place_name,race_no,ticket = ticket)
    odds = np.array(odds)

    if ticket == "ALL":
        df = pd.read_csv(os.path.join(input_data_path,place_name,csv_name_))
        df_prob = pd.read_csv(os.path.join(input_data_path,place_name,"shutuba_%s_r%s_prob_matrix.csv" % (place_name,race_no)),index_col = 0)
        preds = np.diag(df_prob.as_matrix())
        columns = df_prob.columns
        df_prob = pd.DataFrame(preds,columns = ["予測値"])
        ratio = func(df_prob,odds,sys.argv[2])
    else:
        df = pd.read_csv(os.path.join(input_data_path,place_name,csv_name_))
        df["着順"] = 0
        preds = df["予測値"] / df["予測値"].sum()
        columns = map(lambda x:"単勝_%d" % (x+1),range(len(preds)))
        ratio = func(df,odds,sys.argv[2])


    assert type(ratio) == np.ndarray,"returnratio = Trueにして"

    def keta(arr,n = 3):
        return map(lambda x:"%0{}.{}f".format(n,n) % round(x,n),arr)

    print "発走時刻",df["発走時刻"].iloc[0]
    result =  pd.DataFrame(np.array([keta(preds),keta(odds,1),keta(preds * odds,2),ratio]).T,
                       columns = ["preds","odds","return","ratio"],
                       index = columns)
    result.to_csv(os.path.join(input_data_path,place_name,"shutuba_%s_r%s_prob_all.csv" % (place_name,race_no)))
    print result[result["ratio"] != "0.0"]


if __name__ == "__main__":
    #global pp
    #pp = Predict_Time(output_path=output_path)
    if len(sys.argv) > 4:
        #example python calc_return.py bet_min_risk "WIN" 2016小倉01 2
        func = eval(sys.argv[1])
        ticket = sys.argv[2]
        place_name = sys.argv[3]
        race_no = num_dict[int(sys.argv[4])]
        main_single_race(place_name, race_no, func, ticket)

    elif len(sys.argv) > 2:
        main(sys.argv[1],sys.argv[2])

    else:
        print "関数と馬券の種類を指定してください ex.$python calc_return.py bet_min_risk WIN"