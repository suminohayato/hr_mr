# coding: utf-8

#Unicode Decode Errorは下記参照
#http://d.hatena.ne.jp/shu223/20111201/1328334689


import numpy as np
import pandas
import pandas.tseries.offsets as offsets
import datetime
import itertools
pd=pandas

from pylearnuni.utils import io_handler as IO
from preprocess.hr_converter import hrConverter as Converter
from pylearnuni.preprocess.preprocess import Preprocess as Preprocess
#from predict.linear import Predictor as linear_Predictor
#from predict.NN import Predictor as NN_Predictor
#from predict.rank.NN import Predictor as rank_NN_Predictor
#from predict.rank.RNN3 import Predictor as RNN_Predictor
from predict.rank.LSTM import Predictor as LSTM_Predictor

import sys,os,re

###config
HR_TRAIN_RESULT_PATH = os.getenv("HR_TRAIN_RESULT_PATH")
num_dict = {1:"01",2:"02",3:"03",4:"04",5:"05",6:"06",7:"07",8:"08",9:"09",10:"10",11:"11",12:"12"}
place_names = ["2016小倉"+num_dict[i+1] for i in range(12)] +\
              ["2016新潟"+num_dict[i+1] for i in range(12)] +\
              ["2016札幌"+num_dict[i+1] for i in range(6)] +\
              ["20162札幌"+num_dict[i+1] for i in range(6)]

HR_PATH = os.getenv("HR_PATH")
HR_DATA_PATH = os.getenv("HR_DATA_PATH")
input_data_path  = os.path.join(HR_PATH,"workspace/maruo/scraping/netkeiba/run/output/")
input_cleaned_data_path  = os.path.join(HR_PATH,"workspace/maruo/scraping/netkeiba/run/output_cleaned/")
Predictor = LSTM_Predictor
train_result = "LSTM1018/"
output_path = os.path.join(HR_TRAIN_RESULT_PATH,train_result)
output_data_path = os.path.join(HR_DATA_PATH,"predict_result",train_result)


def time_deco(func):
    """
    デバッグ用デコレータ
    実効時間を表示するデコレータ
    """
    import functools
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start = datetime.datetime.today()
        result = func(*args, **kwargs)
        end = datetime.datetime.today()
        print '-------------------------------------------------------------'
        print func
        print 'runningtime:', end - start
        print '-------------------------------------------------------------'
        return result
    return wrapper


class Predict_Time(object):
    def __init__(self,output_path = "",df = None,csv_name = "",useNADE = False,
                 predictor = Predictor,useRNN = True):
        print "using ...",output_path
        if isinstance(df,pandas.core.frame.DataFrame):
            self.df = df
        else:
            if os.path.isfile(csv_name):
                self.df = pd.read_csv(csv_name)
            else:
                #print "Warning : self.df is None"
                self.df = None

        self.csv_name = csv_name[:-4]
        self.TARGET_LABEL = '着順'
        self.useRNN = useRNN
        self.output_path = output_path
        self.keys = IO.read_yaml(os.path.join(self.output_path,"data.info"))["column_name"]

        self.keys = map(lambda x:x.encode("utf-8"),self.keys)
        self.target_label_remove()
        self.features = IO.read_yaml(os.path.join(self.output_path,"features.yaml"))
        #self.onehot_map = self.make_categorical_mapping(self.keys)
        #self.categorical_position = self.make_categorical_position(self.keys)
        self.predictor = predictor(output_path)

        #何回前までのレースのデータを使うか(前５回〜今回なら6)
        self.n_step = 6
        self.n_features = int(IO.read_yaml(os.path.join(self.output_path,"data.info"))["nvis"]) / self.n_step

        #単勝以外も使う場合はいろいろする
        self.prob_matrix = None

        if useNADE:
            self.nademanager = NADEManager(output_path)
            self.nademanager.sample_init()

    def target_label_remove_old(self):
        self.keys.remove(self.TARGET_LABEL)
        if self.useRNN:
            adding_names = ["前回", "前々回", "前３回"]
            for adding_name in adding_names:
                self.keys.remove(adding_name+self.TARGET_LABEL)

    def target_label_remove(self):
        if self.useRNN:
            adding_names = ["", "前１回", "前２回", "前３回", "前４回", "前５回"]
            for adding_name in adding_names:
                self.keys.remove(adding_name+self.TARGET_LABEL)

    def extend_df_old(self):
        if not self.useRNN:
            return
        target_columns = ["齢", "性別", "調教師", "条件_性別", "条件_産地", "条件_指定"]
        adding_names = ["前回", "前々回", "前３回"]
        for column in target_columns:
            for name in adding_names:
                self.df[name + column] = self.df[column].values

    def extend_df(self):
        adding_names = ["", "前１回", "前２回", "前３回", "前４回", "前５回"]
        unknown_columns_ori = ["着順", "推定3F", "タイム差", "2角", "3角", "4角", "ラップ"]
        unknown_columns = [[name + column for name in adding_names] for column in unknown_columns_ori]
        for unknown in unknown_columns:
            for i, column in enumerate(unknown[:-1]):
                new_column = column + "１つ前"
                self.df[new_column] = self.df[unknown[i + 1]]
            self.df[unknown[-1] + "１つ前"] = np.zeros((self.df.shape[0],))

    def make_arrays_forRNN(self,data):
        return [data[:, i * self.n_features:(i + 1) * self.n_features] for i in range(self.n_step)]

    def assert_value_exists(self):
        def assert_value_exists_(column,val):
            if not "{0}<{1}>".format(column,val) in self.keys:
                print "WARNING: {1} is not in {0}".format(column,val)

        for key in self.features.keys():
            #if not self.df[key][self.df[key] != 0].shape[0] and not key in ["外回りフラグ","着順"]:
            #    print "WARNING all 0",key

            if self.features[key]["feature_type"] == "categorical":
                for val in self.df[key]:
                    if type(val) == str:
                        assert_value_exists_(key,val)

    #NADE不使用
    def add_prediction2(self,normalize = False):
        self.df = self.df.fillna(0)
        self.extend_df()#RNNの為のd整形
        self.assert_value_exists()

        df_convert = Preprocess.convert_dataframe(self.df, self.features, Converter)
        df_1hot = df_convert.reindex(columns = self.keys).fillna(0)

        if self.useRNN:
            input_data = self.make_arrays_forRNN(np.asarray(df_1hot.as_matrix(), dtype='float32'))
        else:
            input_data = np.asarray(df_1hot.as_matrix(), dtype='float32')

        preds = np.asarray(self.predictor.predict(input_data).flatten())

        if normalize:
            self.df['予測値'] = self.denormalize(self.df["距離"].iloc[0],preds)
        else:
            self.df["予測値"] = preds

        columns = list(self.df.keys())
        columns = columns[-1:] + columns[:-1]
        self.df = self.df.reindex(columns = columns)

        if False:
            horses_list,prob_trifecta_list = self.predict_trifecta(df_1hot)
            ix_data,prob_all = self.predict_allticket(horses_list,prob_trifecta_list)
            prob_matrix = self.calc_p_ij(ix_data,prob_all,prob_trifecta_list)
            columns = self.make_column(len(self.df))
            self.prob_matrix = pd.DataFrame(prob_matrix,columns = columns,index = columns).applymap(lambda x:np.round(x,5))
            #pd.DataFrame(ix_data,index = columns).applymap(lambda x:np.round(x,4)).to_csv("index.csv")
            #pd.Series(prob_all,index = columns).apply(lambda x:np.round(x,4)).to_csv("prob.csv")

        return self.df['予測値'].as_matrix()

    #3連単の当選率を予想（これにより枠連以外のすべての当選率が算出可能）
    def predict_trifecta(self, df_1hot):
        '''
        horses_list : 3連単の組み合わせ
        prob_trifecta_list : それに対応する予測確率(つまり上のlistとlengthは一致）
        '''

        preds = []
        for i in range(3):
            df_1hot["馬数"] -= 1
            if self.useRNN:
                input_data = self.make_arrays_forRNN(np.asarray(df_1hot.as_matrix(), dtype='float32'))
            else:
                input_data = np.asarray(df_1hot.as_matrix(), dtype='float32')
            preds.append(np.asarray(self.predictor.predict(input_data).flatten()))

        def predict_trifecta_(preds,i,j,k):
            return preds[0][i] / preds[0].sum() * preds[1][j] / (preds[1].sum() - preds[1][i]) * preds[2][k] / (preds[2].sum() - preds[2][i] - preds[2][j])

        n = len(self.df)
        l = [i for i in range(n)]
        horses_list = []
        prob_trifecta_list = []

        for element in itertools.permutations(l, 3):
            horses_list.append(element)
            prob_trifecta_list.append(predict_trifecta_(preds,*element))

        return horses_list,np.array(prob_trifecta_list)

    @classmethod
    def make_column(self,n):
        columns = []
        for i in range(n):
            columns.append("単勝_%d" % (i+1))
        for i in range(n):
            columns.append("複勝_%d" % (i+1))
        for i in itertools.permutations(range(n), 2):
            columns.append("馬単_%d,%d" % (i[0]+1,i[1]+1))
        for i in itertools.combinations(range(n), 2):
            columns.append("馬連_%d,%d" % (i[0]+1,i[1]+1))
        for i in itertools.combinations(range(n), 2):
            columns.append("ワイド_%d,%d" % (i[0]+1,i[1]+1))
        return columns

    def predict_allticket(self,horses_list,prob_trifecta_list):
        '''
        prob_all : WIN,PLACE,EXACTA,QUINELLA,QUINELLA PLACEについて、勝率予測(この順に並べたリストを作る）
        ix_data : horses_listのどのindexが当選の条件を満たすか prob_allに対応する
        '''
        n = len(self.df)
        horses_list = np.array(horses_list).T

        def calc_probability(hl,df,i,ticket):
            if ticket == "WIN":
                ix = np.where(hl[0] == i)
                return ix[0].tolist(),df[ix].sum()
            elif ticket == "PLACE":
                ix = np.where((hl[0] == i) | (hl[1] == i) | (hl[2] == i))
                return ix[0].tolist(),df[ix].sum()
            elif ticket == "EXACTA":
                ix = np.where((hl[0] == i[0]) & (hl[1] == i[1]))
                return ix[0].tolist(),df[ix].sum()
            elif ticket == "QUINELLA":
                ix,sum = [],0
                for el in itertools.permutations([0,1], 2):
                    ix_ = np.where((hl[el[0]] == i[0]) & (hl[el[1]] == i[1]))
                    ix += ix_[0].tolist()
                    sum += df[ix_].sum()
                return ix,sum
            elif ticket == "QUINELLA PLACE":
                ix,sum = [],0
                for el in itertools.permutations([0,1,2], 2):
                    ix_ = np.where((hl[el[0]] == i[0]) & (hl[el[1]] == i[1]))
                    ix += ix_[0].tolist()
                    sum += df[ix_].sum()
                return ix,sum
            else:
                raise ValueError

        ix_data,prob_all = [],[]
        for i in range(n):
            ix,prob = calc_probability(horses_list,prob_trifecta_list,i,"WIN")
            ix_data.append(ix)
            prob_all.append(prob)
        for i in range(n):
            ix,prob = calc_probability(horses_list,prob_trifecta_list,i,"PLACE")
            ix_data.append(ix)
            prob_all.append(prob)
        for i in itertools.permutations(range(n), 2):
            ix,prob = calc_probability(horses_list,prob_trifecta_list,i,"EXACTA")
            ix_data.append(ix)
            prob_all.append(prob)
        for i in itertools.combinations(range(n), 2):
            ix,prob = calc_probability(horses_list,prob_trifecta_list,i,"QUINELLA")
            ix_data.append(ix)
            prob_all.append(prob)
        for i in itertools.combinations(range(n), 2):
            ix,prob = calc_probability(horses_list,prob_trifecta_list,i,"QUINELLA PLACE")
            ix_data.append(ix)
            prob_all.append(prob)

        return ix_data,prob_all

    def calc_p_ij(self,ix_data,prob_all,prob_trifecta_list):
        n = len(prob_all)
        prob_matrix = np.zeros([n,n])

        for i,j in itertools.product(range(n), range(n)):
            if i == j:
                prob_matrix[i][j] = prob_all[i]
            else:
                intersect = set(ix_data[i]).intersection(set(ix_data[j]))
                prob_matrix[i][j] = prob_trifecta_list[list(intersect)].sum()

        return prob_matrix

    def make_pretty_predictions(self,preds):
        stdv = preds.std(axis=1)
        meanv = preds.mean(axis=1)

        try:
            meanv = np.array([int(i) for i in meanv])
            stdv = np.array([int(i) for i in stdv])
            return meanv, stdv
        except:
            print "WARNING:",meanv
            return 0,0

    def easy_work(self,df):
        self.df = df
        self.add_prediction(batchsz= 0)
        return df[self.TARGET_LABEL].as_matrix()

    #以下タイム予測の為に作られた関数----------------------------------------------
    def load_std(self):
        rmse = np.loadtxt(os.path.join(self.output_path,"rmse.txt"))
        distance = self.df["距離"].iloc[0]
        table = pd.read_csv(os.path.join(self.output_path,"msc_wrt_distance.csv"),index_col = 0)
        return max(rmse * table.ix[int(distance)]["std"],0.001)


    def make_msc_table(self):
        #msc = mean,std,count
        mean_df = pd.read_csv(os.path.join(self.output_path,"means_wrt_distance.csv"),header = None)
        std_df = pd.read_csv(os.path.join(self.output_path,"stds_wrt_distance.csv"),header = None)
        count_df = pd.read_csv(os.path.join(self.output_path,"counts_wrt_distance.csv"),header = None)

        distance_index = []
        for i in self.keys:
            if "距離" in i:
                distance_index.append(re.search("[0-9]+", i).group())
        assert(mean_df.shape[0] == len(distance_index))
        table = pd.DataFrame(index = distance_index,columns = ["mean","std","count"])
        for c in table.columns:
            table[c] = eval(c+"_df").iloc[:,0].tolist()
        table.to_csv(os.path.join(self.output_path,"msc_wrt_distance.csv"))

    def denormalize(self,distance,preds):
        table = pd.read_csv(os.path.join(self.output_path,"msc_wrt_distance.csv"),index_col = 0)
        mean,std = table.ix[int(distance)]["mean"],table.ix[int(distance)]["std"]
        return (preds * std) + mean

    def normalize(self,distance,preds):
        table = pd.read_csv(os.path.join(self.output_path,"msc_wrt_distance.csv"),index_col = 0)
        mean,std = table.ix[int(distance)]["mean"],table.ix[int(distance)]["std"]
        if std == 0:
            std = 0.001
        return (preds - mean) / std


#for debug

def print_type(df):
    for i in df.index:
        print i,df[i]#," -> ",type(df.ix[0,i])


def compare_value(df,df2):
    for i in df.columns:
        if i in df2.columns:
            print i,df.ix[0,i]," -> ",df2.ix[0,i]

def time_(name):
    print name,datetime.datetime.today().second,datetime.datetime.today().microsecond


def main(place_names):
    for place_name in place_names:
        for i in range(12):
            csv_name_ = "shutuba_%s_r%s_result.csv" % (place_name,num_dict[i+1])
            try:
                df = pd.read_csv(os.path.join(input_cleaned_data_path,place_name,csv_name_))
            except:
                print "didn't exist %s\n" % csv_name_
                continue

            print "\n----%s" % csv_name_
            if not os.path.exists(os.path.join(output_data_path,place_name)):
                print "mkdir",os.path.join(output_data_path,place_name)
                os.mkdir(os.path.join(output_data_path,place_name))

            pp.df = df
            preds = pp.add_prediction2()
            print "\n予測勝率:",preds / preds.sum()
            print "preds.sum",preds.sum()
            pp.df.to_csv(os.path.join(output_data_path,place_name,csv_name_),index = False)

def main_single_race(place_name,race_no):
    csv_name_ = "shutuba_%s_r%s_result.csv" % (place_name,race_no)
    if not os.path.exists(os.path.join(input_data_path,place_name,csv_name_)):
        csv_name_ = "shutuba_%s_r%s.csv" % (place_name,race_no) #競技結果なければこっちを選択

    try:
        df = pd.read_csv(os.path.join(input_cleaned_data_path,place_name,csv_name_))
    except:
        print "not cleaned. cleaning..."
        import commands
        check = commands.getoutput("python input_dataset_reform.py %s %s" % (place_name,race_no))
        print check
        #import input_dataset_reform as idr
        #idr.main_single_race(place_name,race_no)

        try:
            df = pd.read_csv(os.path.join(input_cleaned_data_path,place_name,csv_name_))
        except:
            print "please scraping!!"
            exit()

    pp.df = df
    preds = pp.add_prediction2()
    print "\n予測勝率:",preds / preds.sum()
    print "preds.sum",preds.sum()

    if not os.path.exists(os.path.join(output_data_path,place_name)):
        print "mkdir",os.path.join(output_data_path,place_name)
        os.mkdir(os.path.join(output_data_path,place_name))

    pp.df.to_csv(os.path.join(output_data_path,sys.argv[1],csv_name_),index = False)
    pp.prob_matrix.to_csv(os.path.join(output_data_path,sys.argv[1],"shutuba_%s_r%s_prob_matrix.csv" % (place_name,race_no)))


if __name__ == '__main__':
    global pp
    pp = Predict_Time(output_path=output_path,useRNN=True)

    if len(sys.argv) > 2:
        place_name = sys.argv[1]
        race_no = num_dict[int(sys.argv[2])]
        main_single_race(place_name,race_no)
    else:
        main(place_names)

'''
    def make_categorical_mapping(self,keys):
        mapping_dict = {}
        for onehot_key in keys:
            if "<" in onehot_key:
                key = onehot_key.split("<")[0]
                if not key in mapping_dict.keys():
                    mapping_dict[key] = []
                mapping_dict[key].append(onehot_key)

        return mapping_dict

    def make_categorical_position(self,keys):
        mapping_dict = {}
        for i in range(len(keys)):
            if "<" in keys[i]:
                key = keys[i].split("<")[0]
                if not key in mapping_dict.keys():
                    mapping_dict[key] = []
                mapping_dict[key].append(i)

        position_dict = {}
        for key in mapping_dict.keys():
            position_dict[key] = {"start":mapping_dict[key][0],"end":mapping_dict[key][len(mapping_dict[key])-1]+1}

        return position_dict

    def dataset_to1hot(self,dataset,keys,converted = False):


        Parameters
        ----------
        dataset_ pd.Series 1hot化前のdataset
        keys list

        Returns pd.Series 1hot化されたdataset
        -------


        dataset2 = pd.Series(index = keys)

        if not converted:
            dataset = pd.DataFrame([dataset])
            dataset = Preprocess.convert_dataframe(dataset, self.features, Converter).iloc[0]

        for key in dataset.index:
            if key == self.TARGET_LABEL:
                continue

            if key in dataset2.index:
                if "<" in key and dataset[key] == 1:
                    for onehot_key in self.onehot_map[key.split("<")[0]]:
                        dataset2[onehot_key] = 0
                dataset2[key] = dataset[key]

        #print "\nto1hot complete. the shape is ",dataset2.shape

        return dataset2

    def setup_psuedo_data(self,dataset_,batchsz = 15):

        NADEによる欠損値補完

        Parameters
        ----------
        dataset_:pd.Series 欠損データ(1hot化前)
        batchsz:int サンプリング数(サンプリング0の場合は欠損値補完しない)

        Returns:np.array 1hot表現にして欠損値補完してnp.arrayに直したもの
        -------


        dataset = self.dataset_to1hot(dataset_,self.keys)

        if batchsz == 0:
            dataset = dataset.fillna(0)
            return [dataset.as_matrix()]

        dataset_np = dataset.as_matrix()
        dataset_np = np.array([dataset_np],dtype = np.float32)

        res = self.nademanager.sample(dataset_np,batchsz)

        return res[0].T

    def main(self,series,id = None,csv_output = False,batchsz = 15):

        欠損値補完＆価格予測

        csv_output : 通常はFalseでよい（実際のサンプル結果を出力したければTrue）
        batchsz : サンプリング数(サンプリング0の場合は欠損値補完しない)


        #サンプリング
        buf = self.setup_psuedo_data(series,batchsz)

        #予測値出す
        preds = np.asarray(self.kerasmodel.predict(np.asarray(buf, dtype='float32')).flatten())
        meanv, stdv = self.make_pretty_predictions(np.array([preds]))

        #結果csv出力
        if csv_output:
            self.make_csv_with_prediction(series,buf,self.keys,preds,name = id)

        return [int(meanv),int(stdv)]

    def add_prediction(self,batchsz = 15):
        rslt = np.array([self.main(self.df.ix[i],batchsz = batchsz) for i in self.df.index])
        meanv,stdv = rslt.T[0],rslt.T[1]

        #入力のcsv_dataに予測値付けてreturn
        print "meanv:",meanv,"stdv:",stdv
        self.df['予測タイム'] = meanv
        self.df['下限'] = meanv - stdv
        self.df['上限'] = meanv + stdv
        columns = list(self.df.keys())
        columns = columns[-1:] + columns[:-1]
        self.df = self.df.reindex(columns = columns)
    '''
