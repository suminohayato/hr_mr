#!/usr/bin/env python
#  -*- coding: utf-8 -*-

import os,sys, math
from calc_winning_percentage import *
from Predict_Time import *
import evaluate_error as ee
import input_dataset_reform as idr

mysse = 0.0
mymoment = 0.0
mytotal = 0.0
mycount = 0

input_data_path = "/Users/sugiyamakanta/hr_mr/workspace/maruo/scraping/netkeiba/run/shutuba_output/"
odds_data_path = "/Users/sugiyamakanta/hr_mr/workspace/maruo/scraping/netkeiba/run/odds_output/"
place_names = ["2016小倉01","2016小倉02","2016小倉03","2016小倉04","2016新潟01","2016新潟02","2016新潟03","2016新潟04"]
#place_names = ["2016小倉02","2016小倉03","2016小倉04","2016新潟01","2016新潟02","2016新潟03","2016新潟04"]

#全馬賭け（ようは期待値）
def WIN_expected_val(csv_name,odds):
    try:
        df = pd.read_csv(csv_name)
    except:
        print "didn't exist ",csv_name
        return

    rank = int(df[df["順位"] == 1]["馬番"].iloc[0]) - 1
    return odds[rank] / df.shape[0]

#ランダムに1馬選ぶ
def bet_WIN_random(csv_name,odds):
    try:
        df = pd.read_csv(csv_name)
    except:
        print "didn't exist ",csv_name
        return

    rank = int(random() * df.shape[0])

    #a = np.array([i for i in range(df.shape[0])])
    #rank = choice(a,df.shape[0],replace=False)
    #return ee.abs_error(rank,map(int,df["順位"].tolist())) / df.shape[0]

    if df.ix[rank,"順位"] == 1:
        print "当たり! (x%s)" % str(odds[rank])
        return odds[rank]
    else:
        print "はずれ…"
        return 0

#予想タイムが最も速い馬を選ぶ
def bet_WIN_certainty(csv_name,odds):
    global mysse, mymoment, mytotal, mycount
    try:
        df = pd.read_csv(csv_name)
    except:
        print "didn't exist ",csv_name
        return

    df_cleaned = idr.input_dataset_reform(pd.read_csv(csv_name))
    #df_cleaned.to_csv("test.csv")
    pp.df = df_cleaned
    preds_time = pp.add_prediction2()
    print preds_time

    def get_time_from_str(s):
        if type(s) == str:
            split = s.split(":")
            min = float(split[0])
            sec = float(split[1])
            return min * 60 + sec
        elif type(s) == float:
            return s
        else:
            print "error"
            exit()
    ans_time = map(get_time_from_str, df["タイム"].values)
    print ans_time
    dist = df_cleaned["距離"].values[0]
    print pp.normalize(dist, ans_time)
    ans = pp.normalize(dist, ans_time)
    for i, x in enumerate(ans):
        if math.isnan(x) or math.isinf(x):
            ans[i] = 0

    mysse += np.sum((np.array(preds_time) - ans) * (np.array(preds_time) - ans))
    mymoment += np.sum(ans * ans)
    mytotal += np.sum(ans)
    mycount += ans.shape[0]

    print mysse, mymoment, mytotal, mycount
    print mytotal / mycount
    print np.sqrt(mysse / mycount)
    print mymoment / mycount - (mytotal / mycount) * (mytotal / mycount)

    rank = np.array(preds_time).argsort()
    print rank
    #return ee.abs_error(rank,map(int,df["順位"].tolist())) / df.shape[0]
    print df["順位"]
    if df.ix[rank[0],"順位"] == 1:
        print "当たり! (x%s)" % str(odds[rank[0]])
        return odds[rank[0]]
    else:
        print "はずれ…"
        return 0

#予想順位の正解データとの類似度を測る
def abs_error(csv_name,odds):
    try:
        df = pd.read_csv(csv_name)
    except:
        print "didn't exist ",csv_name
        return

    df_cleaned = idr.input_dataset_reform(pd.read_csv(csv_name))
    pp.df = df_cleaned
    preds_time = pp.add_prediction2()
    rank = np.array(preds_time).argsort()
    return ee.abs_error(rank,map(int,df["順位"].tolist())) / df.shape[0]

#予想タイムが速い上位3馬を選ぶ
def bet_WIN_certainty_3(csv_name,odds):
    try:
        df = pd.read_csv(csv_name)
    except:
        print "didn't exist ",csv_name
        return

    df_cleaned = idr.input_dataset_reform(pd.read_csv(csv_name))
    pp.df = df_cleaned
    preds_time = pp.add_prediction2()
    rank = np.array(preds_time).argsort()
    #print preds_time
    #print df["タイム"]
    #print rank + 1
    #print map(int,df["順位"].tolist())

    for i in range(3):
        if df.ix[rank[i],"順位"] == 1:
            print "当たり! (x%s)" % str(odds[rank[i]])
            return odds[rank[i]]/3
    else:
        print "はずれ…"
        return 0


#リターン期待値(勝率*オッズ)が最も高い馬を選ぶ
def bet_WIN_maxreturn(csv_name,odds):
    try:
        df = pd.read_csv(csv_name)
    except:
        print "didn't exist ",csv_name
        return

    df_cleaned = idr.input_dataset_reform(pd.read_csv(csv_name))
    pp.df = df_cleaned
    n_horse = pp.df.shape[0]

    preds_time = pp.add_prediction2()
    preds_std = [pp.load_std()] * n_horse

    rankings = do_random_race(preds_time,preds_std,2000)
    win_rate = make_winning_rate_table(rankings,n_horse,"WIN")#np.array([calc_winning_rate(rankings,i,"WIN") for i in range(n_horse)])
    preds_return = odds * win_rate
    rank = (-np.array(preds_return)).argsort()
    #print win_rate,preds_return,rank

    if df.ix[rank[0],"順位"] == 1:
        print "当たり! (x%s)" % str(odds[rank[0]])
        return odds[rank[0]]
    else:
        print "はずれ…"
        return 0

def twelve_race_predict(place_name,func):
    returns = []

    for i in range(12):
        csv_name_,odds_csv_name = None,None
        if i+1 < 10:
            csv_name_ = "shutuba_%s_r0%d_result.csv" % (place_name,i+1)
            odds_csv_name = "odds_%s_r0%d単複.csv" % (place_name,i+1)
        else:
            csv_name_ = "shutuba_%s_r%d_result.csv" % (place_name,i+1)
            odds_csv_name = "odds_%s_r%d単複.csv" % (place_name,i+1)

        #print csv_name_

        odds = load_odds(os.path.join(odds_data_path,odds_csv_name),ticket = "WIN")
        return_ = func(os.path.join(input_data_path,csv_name_),odds)
        if not return_ == None:
            returns.append(return_)
    return np.array(returns)

def main():
    returnss = []
    for place_name in place_names:
        returns = twelve_race_predict(place_name,bet_WIN_certainty)
        for return_ in returns:
            returnss.append(return_)
    returnss = np.array(returnss)
    print returnss
    print returnss.shape[0]
    print "RETURN %d%%" % int((returnss.mean()) * 100)
    print "CORRECT %d%%" % (returnss[np.where(returnss !=  0)].shape[0] * 100 / returnss.shape[0])
    return int((np.array(returnss).flatten().mean()) * 100)

if __name__ == "__main__":
    global pp
    pp = Predict_Time(output_path=output_path)
    main()