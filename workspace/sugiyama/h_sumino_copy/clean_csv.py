#!/usr/bin/env python
#  -*- coding: utf-8 -*-

import pandas as pd
import sys
import numpy as np


def clean_csv(df):
    #df = drop_shogai(df)
    cdf = pd.DataFrame({},columns = pd.read_csv("column.csv")["column"])

    for key in df.columns:
        cdf[key] = df[key]

    cdf["競馬場"] = cdf["レース"].str[:6]
    cdf["馬場状態"] = convert_condition(cdf["馬場状態"])
    cdf["タイム"] = convert_time(cdf["タイム"])

    columns = convert_weight(cdf["体重"])
    for key in columns.keys():
        #print key,cdf[key].shape,len(columns[key])
        cdf[key] = columns[key]

    columns = convert_distance(df["距離"])
    for key in columns.keys():
        cdf[key] = columns[key]

    for pre_name in ["","前回","前々回","前３回"]:
        columns = convert_distance(df[pre_name+"距離"],pre_name)
        for key in columns.keys():
            cdf[key] = columns[key]
        cdf[pre_name+"タイム"] = convert_time(cdf[pre_name+"タイム"])

    return cdf

def drop_shogai(df):
    s = df["条件"].str.contains("障害") * 1
    return df[s == 0]

def convert_condition(column):
    def convert_condition_(val):
        vals = val.split("：")
        if len(vals) == 3:
            return val
        else:
            return vals[1]

    return [convert_condition_(val) for val in column]

def convert_distance(column,pre_name = ""):
    columns = {}
    columns[pre_name+"地面"] = column.str.contains("芝") * 1 + column.str.contains("ダ") * 2
    columns[pre_name+"コース回り"] = column.str.contains("右") * 1 + column.str.contains("直") * 2 + column.str.contains("左") * 3
    columns[pre_name+"外回りフラグ"] = column.str.contains("外") * 1
    columns[pre_name+"距離"] = column.str.extract('([0-9]+)').astype(float).dropna(0)
    return columns

def convert_weight(column):
    columns = {}
    columns["体重"] = [str(val).split(" ")[0] for val in column]

    def sabun(val):
        valspl = val.split(" ")
        if len(valspl) > 1:
            return valspl[1]
        else:
            return 0

    columns["体重(差分)"] = [sabun(str(val)) for val in column]
    columns["体重(差分)"] = pd.to_numeric(columns["体重(差分)"], errors='coerce')
    return columns

def convert_time(column):
    column = column.fillna("0:0.0")
    def convert_time_(val):
        vals = str(val).split(":")
        if len(vals) == 2:#例1:15.7
            return float(vals[0])*60 + float(vals[1])
        else:
            try:#例75.7
                return float(val)
            except:#クソデータ (もしあれば)
                return 0

    return [convert_time_(val) for val in column]
    #return [float(str(val).split(":")[0]) * 60 + float(str(val).split(":")[1]) for val in column]

if __name__ == "__main__":
    cdf = clean_csv(pd.read_csv(sys.argv[1]))
    cdf.to_csv(sys.argv[1][:-4]+"_cleaned.csv")