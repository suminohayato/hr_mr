#!/usr/bin/env python
#  -*- coding: utf-8 -*-

import numpy as np
from numpy.random import *
import pandas as pd
import sys
import itertools

jap_eng_name = {"WIN":"単勝",
                "PLACE":"複勝",
                "EXACTA":"馬単",
                "QUINELLA":"馬連",
                "QUINELLA PLACE":"ワイド",
                "BRACKET QUINELLA":"枠連",
                "TRIO":"3連複",
                "TRIFECTA":"3連単"}

n = 8
test_time = np.array([100.1,100.2,100.6,100.7,101,101.2,101.5,101.6])
test_std = np.array([1 for i in range(n)])
test_win_odds = np.array([2.2,2.4,7.3,7.6,8.9,11.6,18.8,20.9])
test_bracket = [1,1,2,2,3,3,4,4]

def do_random_race(time_data,std_data,n_race):
    num = len(time_data)
    rankings = []
    for i in range(n_race):
        p_time = np.array([normal(time_data[i],std_data[i]) for i in range(num)])
        rankings.append(p_time.argsort()[:3])

    return rankings

def convert_bracket(rankings):
    for i in rankings:
        for j in i:
            i[j] = test_bracket[i[j]]
    return rankings

def calc_winning_rate(rankings,i,ticket = "WIN"):
    '''
    mode
     WIN : 単勝
     PLACE : 複勝
     EXACTA : 馬単
     QUINELLA : 馬連
     BRACKET QUINELLA : 枠連
     QUINELLA_PLACE : ワイド
     TRIO : 三連複
     TRIFECTA : 三連単
    '''
    df = pd.DataFrame(rankings,columns=["first","second","third"])
    size = df.shape[0]

    if ticket == "WIN":
        return df[df["first"] == i].shape[0] * 1.0 / size
    elif ticket == "PLACE":
        str_ = 'first != %d & second != %d & third != %d' % (i,i,i)
        return (size - df.query(str_).shape[0]) * 1.0 / size
    elif ticket == "EXACTA":
        str_ = 'first == %d & second == %d' % (i[0],i[1])
        return df.query(str_).shape[0] * 1.0 / size
    elif ticket == "QUINELLA":
        str1 = 'first == %d & second == %d' % (i[0],i[1])
        str2 = 'first == %d & second == %d' % (i[1],i[0])
        return (df.query(str1).shape[0] + df.query(str2).shape[0]) * 1.0 / size
    elif ticket == "QUINELLA_PLACE":
        str1 = 'first == %d & second == %d' % (i[0],i[1])
        str2 = 'first == %d & second == %d' % (i[1],i[0])
        str3 = 'second == %d & third == %d' % (i[0],i[1])
        str4 = 'second == %d & third == %d' % (i[1],i[0])
        str5 = 'third == %d & first == %d' % (i[0],i[1])
        str6 = 'third == %d & first == %d' % (i[1],i[0])
        return (df.query(str1).shape[0] + df.query(str2).shape[0] + df.query(str3).shape[0] + df.query(str4).shape[0] + df.query(str5).shape[0] + df.query(str6).shape[0]) * 1.0 / size
    elif ticket == "BRACKET QUINELLA":
        df = pd.DataFrame(convert_bracket(rankings),columns=["first","second","third"])
        if i[0] != i[1]:
            str1 = 'first == %d & second == %d' % (i[0],i[1])
            str2 = 'first == %d & second == %d' % (i[1],i[0])
            return (df.query(str1).shape[0] + df.query(str2).shape[0]) * 1.0 / size
        else:
            str_ = 'first == %d & second == %d' % (i[0],i[1])
            return df.query(str_).shape[0] * 1.0 / size
    elif ticket == "TRIO":
        str1 = 'first == %d & second == %d & third == %d' % (i[0],i[1],i[2])
        str2 = 'first == %d & second == %d & third == %d' % (i[0],i[2],i[1])
        str3 = 'first == %d & second == %d & third == %d' % (i[1],i[0],i[2])
        str4 = 'first == %d & second == %d & third == %d' % (i[1],i[2],i[0])
        str5 = 'first == %d & second == %d & third == %d' % (i[2],i[0],i[1])
        str6 = 'first == %d & second == %d & third == %d' % (i[2],i[1],i[0])
        return (df.query(str1).shape[0] + df.query(str2).shape[0] + df.query(str3).shape[0] + df.query(str4).shape[0] + df.query(str5).shape[0] + df.query(str6).shape[0]) * 1.0 / size
    elif ticket == "TRIFECTA":
        str_ = 'first == %d & second == %d & third == %d' % (i[0],i[1],i[2])
        return df.query(str_).shape[0] * 1.0 / size

def make_winning_rate_table(rankings,n_horse,ticket = "WIN"):
    #枠数もn_horseで…
    if ticket in ["WIN","PLACE"]:
        return np.array([calc_winning_rate(rankings,i,ticket) for i in range(n_horse)])
    elif ticket in ["EXACTA","QUINELLA","QUINELLA_PLACE","BRACKET QUINELLA"]:
        table = np.array([[float("nan")] * n_horse] * n_horse)
        for i,j in itertools.product(range(n_horse), range(n_horse)):
            if ticket == "EXACTA" or i < j:
                table[i][j] = calc_winning_rate(rankings,[i,j],ticket)
        return table
    elif ticket in ["TRIFECTA","TRIO"]:
        table = np.array([[[float("nan")] * n_horse] * n_horse] * n_horse)
        for i,j,k in itertools.product(range(n_horse), range(n_horse)):
            if ticket == "TRIFECTA" or (i < j and j < k):
                table[i][j][k] = calc_winning_rate(rankings,[i,j,k],ticket)
        return table
    else:
        return None


def load_odds(odds_csv_name,ticket = "WIN"):
    try:
        odds = pd.read_csv(odds_csv_name)
    except:
        print "didn't exist ",odds_csv_name
        return

    if ticket == "WIN":
        return odds["単勝"].as_matrix()
    elif ticket == "PLACE":
        print "ごめん未実装"
        exit()
    elif ticket in ["EXACTA","QUINELLA","QUINELLA PLACE","BRACKET QUINELLA"]:
        column_name = "枠番" if ticket == "BRACKET QUINELLA" else "馬番"
        n_horse = odds[column_name+"1"].drop_duplicates().shape[0]
        table = np.array([[float("nan")] * n_horse] * n_horse)

        for i in odds.index:
            table[odds.ix[i,column_name+"1"]-1][odds.ix[i,column_name+"2"]-1] = odds.ix[i,jap_eng_name[ticket]]
        return table
    elif ticket in ["TRIFECTA","TRIO"]:
        n_horse = odds["馬番1"].drop_duplicates().shape[0]
        table = np.array([[[float("nan")] * n_horse] * n_horse] * n_horse)

        for i in odds.index:
            table[odds.ix[i,"馬番1"]-1][odds.ix[i,"馬番2"]-1][odds.ix[i,"馬番3"]-1] = odds.ix[i,jap_eng_name[ticket]]
        return table
    else:
        return None


if __name__ == "__main__":
    df = pd.read_csv("shutuba_%s_cleaned_preds.csv" % sys.argv[1])
    n_horse = df.shape[0]

    preds_time = df["予測タイム"]
    preds_std = [2.21] * n_horse
    win_odds = load_odds("odds/"+sys.argv[1],"WIN")

    rankings =  do_random_race(preds_time,preds_std,10000)
    win_rate = np.array([calc_winning_rate(rankings,i,"WIN") for i in range(n_horse)])
    print "予想順位\n",(-np.array(win_rate)).argsort()+1


    print "馬番,予想勝率,オッズ,予想リターン"
    preds_return = win_odds * win_rate
    for i in (-np.array(preds_return)).argsort():
        print i+1,win_rate[i],win_odds[i],preds_return[i]

    #for i in range(df.shape[0]):
    #    print i+1,win_rate[i],win_odds[i],win_odds[i] * win_rate[i]