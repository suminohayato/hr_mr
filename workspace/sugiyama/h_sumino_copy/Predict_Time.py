# coding: utf-8

import pickle
import numpy as np
import pandas
import pandas.tseries.offsets as offsets
import datetime
pd=pandas

from pylearnuni.utils import io_handler as IO
from preprocess.hr_converter import hrConverter as Converter
from preprocess.hr_filter import hrFilter as Filter
from pylearnuni.preprocess.preprocess import Preprocess as Preprocess
from predict.linear import Predictor as linear_Predictor
from predict.NN import Predictor as NN_Predictor

import sys,os,re


def time_deco(func):
    """
    デバッグ用デコレータ
    実効時間を表示するデコレータ
    """
    import functools
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start = datetime.datetime.today()
        result = func(*args, **kwargs)
        end = datetime.datetime.today()
        print '-------------------------------------------------------------'
        print func
        print 'runningtime:', end - start
        print '-------------------------------------------------------------'
        return result
    return wrapper


class Predict_Time(object):
    def __init__(self,output_path = "",df = None,csv_name = "",useNADE = False):
        print "using ...",output_path
        if isinstance(df,pandas.core.frame.DataFrame):
            self.df = df
        else:
            if os.path.isfile(csv_name):
                self.df = pd.read_csv(csv_name)
            else:
                print "Warning : self.df is None"
                self.df = None

        self.csv_name = csv_name[:-4]
        self.TARGET_LABEL = 'タイム'
        self.output_path = output_path
        self.keys = IO.read_yaml(os.path.join(self.output_path,"data.info"))["column_name"]
        self.keys.remove(self.TARGET_LABEL)
        self.features = IO.read_yaml(os.path.join(self.output_path,"features.yaml"))
        self.onehot_map = self.make_categorical_mapping(self.keys)
        self.categorical_position = self.make_categorical_position(self.keys)

        #todo 選択可能にしたい
        #self.predictor = linear_Predictor(output_path)
        self.predictor = NN_Predictor(output_path)
        if useNADE:
            self.nademanager = NADEManager(output_path)
            self.nademanager.sample_init()

    def load_dataset(self):
        print "loading dataset.."
        self.dataset_org = pd.read_csv(self.dataset_org_path)

    def load_std(self):
        rmse = np.loadtxt(os.path.join(self.output_path,"rmse.txt"))
        distance = self.df["距離"].iloc[0]
        table = pd.read_csv(os.path.join(self.output_path,"msc_wrt_distance.csv"),index_col = 0)
        return max(rmse * table.ix[int(distance)]["std"],0.001)

    def make_categorical_mapping(self,keys):
        mapping_dict = {}
        for onehot_key in keys:
            if "<" in onehot_key:
                key = onehot_key.split("<")[0]
                if not key in mapping_dict.keys():
                    mapping_dict[key] = []
                mapping_dict[key].append(onehot_key)

        return mapping_dict

    def make_categorical_position(self,keys):
        mapping_dict = {}
        for i in range(len(keys)):
            if "<" in keys[i]:
                key = keys[i].split("<")[0]
                if not key in mapping_dict.keys():
                    mapping_dict[key] = []
                mapping_dict[key].append(i)

        position_dict = {}
        for key in mapping_dict.keys():
            position_dict[key] = {"start":mapping_dict[key][0],"end":mapping_dict[key][len(mapping_dict[key])-1]+1}

        return position_dict

    def dataset_to1hot(self,dataset,keys,converted = False):
        '''

        Parameters
        ----------
        dataset_ pd.Series 1hot化前のdataset
        keys list

        Returns pd.Series 1hot化されたdataset
        -------

        '''
        dataset2 = pd.Series(index = keys)

        if not converted:
            dataset = pd.DataFrame([dataset])
            dataset = Preprocess.convert_dataframe(dataset, self.features, Converter).iloc[0]

        for key in dataset.index:
            if key == self.TARGET_LABEL:
                continue

            if key in dataset2.index:
                if "<" in key and dataset[key] == 1:
                    for onehot_key in self.onehot_map[key.split("<")[0]]:
                        dataset2[onehot_key] = 0
                dataset2[key] = dataset[key]

        #print "\nto1hot complete. the shape is ",dataset2.shape

        return dataset2

    def dataset_to1hot_(self,dataset,keys,converted = False):
        '''

        Parameters
        ----------
        dataset_ pd.Series 1hot化前のdataset
        keys list

        Returns pd.Series 1hot化されたdataset
        -------

        '''
        array = np.array([float("nan") for i in range(len(keys))])

        if not converted:
            dataset = pd.DataFrame([dataset])
            dataset = Preprocess.convert_dataframe(dataset, self.features, Converter).iloc[0]

        #todo inspection
        for key in self.categorical_position.keys():
            if type(dataset[key]) == str and dataset[key] != "":
                array[self.categorical_position[key]["start"]:self.categorical_position[key]["end"]] = 0
            elif not np.isnan(dataset[key]):
                array[self.categorical_position[key]["start"]:self.categorical_position[key]["end"]] = 0

        dataset2 = pd.Series(array,index = keys)

        dataset = dataset.dropna()
        for key in dataset.index:
            if key == self.TARGET_LABEL:
                continue

            if key in dataset2.index:
                dataset2[key] = dataset[key]

        #print "\nto1hot complete. the shape is ",dataset2.shape

        return dataset2

    def setup_psuedo_data(self,dataset_,batchsz = 15):
        '''
        NADEによる欠損値補完

        Parameters
        ----------
        dataset_:pd.Series 欠損データ(1hot化前)
        batchsz:int サンプリング数(サンプリング0の場合は欠損値補完しない)

        Returns:np.array 1hot表現にして欠損値補完してnp.arrayに直したもの
        -------
        '''

        dataset = self.dataset_to1hot(dataset_,self.keys)

        if batchsz == 0:
            dataset = dataset.fillna(0)
            return [dataset.as_matrix()]

        dataset_np = dataset.as_matrix()
        dataset_np = np.array([dataset_np],dtype = np.float32)

        res = self.nademanager.sample(dataset_np,batchsz)

        return res[0].T

    def main(self,series,id = None,csv_output = False,batchsz = 15):
        '''
        欠損値補完＆価格予測

        csv_output : 通常はFalseでよい（実際のサンプル結果を出力したければTrue）
        batchsz : サンプリング数(サンプリング0の場合は欠損値補完しない)
        '''

        #サンプリング
        buf = self.setup_psuedo_data(series,batchsz)

        #予測値出す
        preds = np.asarray(self.kerasmodel.predict(np.asarray(buf, dtype='float32')).flatten())
        meanv, stdv = self.make_pretty_predictions(np.array([preds]))

        #結果csv出力
        if csv_output:
            self.make_csv_with_prediction(series,buf,self.keys,preds,name = id)

        return [int(meanv),int(stdv)]

    def add_prediction(self,batchsz = 15):
        rslt = np.array([self.main(self.df.ix[i],batchsz = batchsz) for i in self.df.index])
        meanv,stdv = rslt.T[0],rslt.T[1]

        #入力のcsv_dataに予測値付けてreturn
        print "meanv:",meanv,"stdv:",stdv
        self.df['予測タイム'] = meanv
        self.df['下限'] = meanv - stdv
        self.df['上限'] = meanv + stdv
        columns = list(self.df.keys())
        columns = columns[-1:] + columns[:-1]
        self.df = self.df.reindex(columns = columns)

    #NADE不使用
    def add_prediction2(self):
        df_convert = Preprocess.convert_dataframe(self.df, self.features, Converter)

        df_1hot = pd.DataFrame({},columns=self.keys)
        for key in self.keys:
            if key in df_convert:
                df_1hot[key] = df_convert[key]
            else:
                df_1hot[key] = np.zeros(df_convert.shape[0])

        preds = np.asarray(self.predictor.predict(np.asarray(df_1hot.as_matrix(), dtype='float32')).flatten())
        self.df['予測タイム'] = self.denormalize(self.df["距離"].iloc[0],preds)

        columns = list(self.df.keys())
        columns = columns[-1:] + columns[:-1]
        self.df = self.df.reindex(columns = columns)
        return self.df['予測タイム']


    def make_pretty_predictions(self,preds):
        stdv = preds.std(axis=1)
        meanv = preds.mean(axis=1)

        try:
            meanv = np.array([int(i) for i in meanv])
            stdv = np.array([int(i) for i in stdv])
            return meanv, stdv
        except:
            print "WARNING:",meanv
            return 0,0

    def all(self,name):
        self.add_prediction(batchsz= 30)
        self.load_dataset()
        self.add_stat_data()
        self.df.to_csv(self.csv_name+"_preds_"+name+".csv")#,index = False)

    def easy_work(self,df):
        self.df = df
        self.add_prediction(batchsz= 0)
        return df["タイム"].as_matrix()

    def make_msc_table(self):
        #msc = mean,std,count
        mean_df = pd.read_csv(os.path.join(self.output_path,"means_wrt_distance.csv"),header = None)
        std_df = pd.read_csv(os.path.join(self.output_path,"stds_wrt_distance.csv"),header = None)
        count_df = pd.read_csv(os.path.join(self.output_path,"counts_wrt_distance.csv"),header = None)

        distance_index = []
        for i in self.keys:
            if "距離" in i:
                distance_index.append(re.search("[0-9]+", i).group())
        assert(mean_df.shape[0] == len(distance_index))
        table = pd.DataFrame(index = distance_index,columns = ["mean","std","count"])
        for c in table.columns:
            table[c] = eval(c+"_df").iloc[:,0].tolist()
        table.to_csv(os.path.join(self.output_path,"msc_wrt_distance.csv"))

    def denormalize(self,distance,preds):
        table = pd.read_csv(os.path.join(self.output_path,"msc_wrt_distance.csv"),index_col = 0)
        mean,std = table.ix[int(distance)]["mean"],table.ix[int(distance)]["std"]
        return preds#(preds * std) + mean

    def normalize(self,distance,preds):
        table = pd.read_csv(os.path.join(self.output_path,"msc_wrt_distance.csv"),index_col = 0)
        mean,std = table.ix[int(distance)]["mean"],table.ix[int(distance)]["std"]
        return (preds - mean) / std

#for debug

def print_type(df):
    for i in df.index:
        print i,df[i]#," -> ",type(df.ix[0,i])


def compare_value(df,df2):
    for i in df.columns:
        if i in df2.columns:
            print i,df.ix[0,i]," -> ",df2.ix[0,i]

def time_(name):
    print name,datetime.datetime.today().second,datetime.datetime.today().microsecond


###config
#output_path = "/Users/HayatoSumino/Desktop/HRdata/train_result/linear/"
output_path = "/Users/sugiyamakanta/hr_mr_data/NN0811/"

if __name__ == '__main__':
    print "applying missing completion to {}...".format(sys.argv[1])
    pp = Predict_Time(csv_name = sys.argv[1],output_path=output_path)
    pp.make_msc_table()
    print pp.add_prediction2()
    pp.df.to_csv(sys.argv[1][:-4]+"_preds.csv",index = False)