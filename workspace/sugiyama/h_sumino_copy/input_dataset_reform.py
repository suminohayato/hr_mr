#!/usr/binenv python
# -*- coding: utf-8 -*-

import pandas as pd
import sys

def input_dataset_reform(df):
    df = df.rename(columns = {"斤量":"重量"})
    df["地面"] = df["レース情報"].str.contains("芝") * 1 + df["レース情報"].str.contains("ダート") * 2
    df["外回りフラグ"] = df["レース情報"].str.contains("外") * 1
    df["コース回り"] = df["レース周り"].str.contains("右") * 1 + df["レース周り"].str.contains("直") * 2
    df["距離"] = df["レース情報"].str.extract('([0-9]+)').astype(float).dropna(0)
    df = df.reindex(columns = pd.read_csv("column.csv")["column"])
    return df

if __name__ == "__main__":
    input_dataset_reform(pd.read_csv(sys.argv[1])).to_csv(sys.argv[1][:-4]+"_cleaned.csv")