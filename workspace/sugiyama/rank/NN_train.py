# coding:utf-8
import tensorflow as tf
import numpy as np
import math
import sys, csv, h5py, yaml

path = "/Users/sugiyamakanta/hr_mr_data/rankNN0822_name/"
MAX_EPOCH = 1000
BATCH_SIZE = 1000
patience = 10

# tensorflowの準備
info = yaml.load(open(path + "data.info", "r"))
features = int(info["nvis"])
print features
BATCH_SIZE = 1000
UNIT_NOS = [100,100]

hidden_no = len(UNIT_NOS)
x = tf.placeholder(tf.float32, [None, features])
W_list = []
b_list = []

old_unit_no = features
z = x
for i, unit_no in enumerate(UNIT_NOS):
    W = tf.Variable(tf.truncated_normal([old_unit_no, unit_no], stddev=0.1))
    b = tf.Variable(tf.constant(0.1, shape=[unit_no]))
    W_list.append(W)
    b_list.append(b)
    z = tf.nn.relu(tf.matmul(z, W) + b)
    old_unit_no = unit_no
W_last = tf.Variable(tf.truncated_normal([UNIT_NOS[-1], 1], stddev=0.1))
b_last = tf.Variable(tf.constant(0.0, shape=[1]))
y = tf.nn.sigmoid(tf.matmul(z, W_last) + b_last)

y_ = tf.placeholder(tf.float32, [None, 1])
mse = tf.reduce_mean(-(y_ * tf.log(y) + (1.0 - y_) * tf.log(1.0 - y)))

train_step = tf.train.AdamOptimizer(1e-4).minimize(mse)
init = tf.initialize_all_variables()

sess = tf.Session()
sess.run(init)

h5data = h5py.File(path + "data.h5")

batch_no = (h5data["X"].shape[0] - 1) / BATCH_SIZE + 1

means = np.zeros((features,), dtype=np.float32)
stds = np.zeros((features,), dtype=np.float32)

max_array = -np.ones((features,), dtype=np.float32) * 10000.0
min_array = np.ones((features,), dtype=np.float32) * 10000.0

for i in range(batch_no):
    max_array = np.maximum(max_array, np.max(h5data["X"][i * BATCH_SIZE:(i + 1) * BATCH_SIZE], axis=0))
    min_array = np.minimum(min_array, np.min(h5data["X"][i * BATCH_SIZE:(i + 1) * BATCH_SIZE], axis=0))

# [0,1]に正規化することにした
means = min_array
stds = max_array - min_array

print means
print stds

"""
for i in range(batch_no):
    means += np.sum(h5data["X"][i * BATCH_SIZE:(i + 1) * BATCH_SIZE], axis=0)
means /= h5data["X"].shape[0]

for i in range(batch_no):
    stds += np.sum((h5data["X"][i * BATCH_SIZE:(i + 1) * BATCH_SIZE] - means) * (h5data["X"][i * BATCH_SIZE:(i + 1) * BATCH_SIZE] - means), axis=0)
stds /= h5data["X"].shape[0]
stds = np.sqrt(stds)
"""

for i, std in enumerate(stds):
    if std == 0:
        stds[i] = float("inf")

np.savetxt(path + "means.csv", means)
np.savetxt(path + "stds.csv", stds)

def normalize(X):
    return (X - means) / stds

count = 0
min = 100000.0

"""
print sess.run(W_list[0])
print sess.run(b_list[0])
print sess.run(W_last)
print sess.run(b_last)
print sess.run(tf.matmul(x, W_list[0]) + b, feed_dict={x: normalize(h5data["test"]["X"][:]), y_: h5data["test"]["y"][:].reshape(h5data["test"]["y"][:].shape[0], 1)})
"""

for i in range(MAX_EPOCH):

    # SGDを実装している
    for j in range(batch_no):
        sys.stderr.write('\r\033[K' + str(j + 1) + "/" + str(batch_no))
        sys.stderr.flush()
        np_data = h5data["X"][j * BATCH_SIZE:(j + 1) * BATCH_SIZE]
        batch_xs = normalize(np_data)
        batch_ys = h5data["y"][j * BATCH_SIZE:(j + 1) * BATCH_SIZE].reshape(h5data["y"][j * BATCH_SIZE:(j + 1) * BATCH_SIZE].shape[0], 1)
        sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})
    sys.stderr.write('\r\033[K')
    sys.stderr.flush()

    cost = 0
    valid_batch_no = (h5data["valid"]["X"].shape[0] - 1) / BATCH_SIZE + 1
    for j in range(valid_batch_no):
        cost += sess.run(mse, feed_dict={
            x: normalize(h5data["valid"]["X"][j * BATCH_SIZE:(j + 1) * BATCH_SIZE]),
            y_: h5data["valid"]["y"][j * BATCH_SIZE:(j + 1) * BATCH_SIZE].reshape(h5data["valid"]["y"][j * BATCH_SIZE:(j + 1) * BATCH_SIZE].shape[0], 1)})
    cost /= valid_batch_no
    print str(i + 1) + "epoch:valid cost=" + str(cost)

    test_cost = sess.run(mse, feed_dict={x: normalize(h5data["test"]["X"][:]), y_: h5data["test"]["y"][:].reshape(h5data["test"]["y"][:].shape[0], 1)})
    print "test cost=" + str(test_cost)
    """
    print sess.run(b_list[0])
    print sess.run(W_last)
    print sess.run(b_last)
    print sess.run(z, feed_dict={x: normalize(h5data["test"]["X"][:]), y_: h5data["test"]["y"][:].reshape(h5data["test"]["y"][:].shape[0], 1)})
    """

    if cost < min:
        count = 0
        for i, W, b in zip(range(hidden_no), W_list, b_list):
            np.savetxt(path + "W" + str(i + 1) + ".csv", sess.run(W), delimiter=",")
            np.savetxt(path + "b" + str(i + 1) + ".csv", sess.run(b), delimiter=",")
        np.savetxt(path + "W.csv", sess.run(W_last), delimiter=",")
        np.savetxt(path + "b.csv", sess.run(b_last), delimiter=",")
        min = cost
    else:
        count += 1

    if count == patience:
        break
