# coding:utf-8
from predict.rank.RNN import Predictor
import h5py
import numpy as np
import yaml

path = "/Users/sugiyamakanta/hr_mr_data/RNN0905/"
info = yaml.load(open(path + "data.info", "r"))
features = int(info["nvis"]) / 4
predictor = Predictor(path)
data = h5py.File(path + "data.h5")
print predictor.predict([data["test"]["X"][:100, i * features:(i + 1) * features] for i in range(4)])
print data["test"]["y"][:100, -1]

pred = predictor.predict([data["test"]["X"][:, i * features:(i + 1) * features] for i in range(4)])
print pred.shape
ans = data["test"]["y"][:, -1].flatten()
mean = np.mean(ans)
print np.sum(ans)
threshold =  np.sort(pred)[-np.sum(ans)]
print "threshold=" + str(threshold)
pred_count = np.sum((pred >= threshold))
print "pred count=" + str(pred_count)

print "top rate=" + str(mean)
correct = np.sum((pred >= threshold) * ans)

print correct
print float(correct) / pred_count

print "-uniform loglikelihood=" + str(-(mean * np.log(mean) + (1.0 - mean) * np.log(1.0 - mean)))
print "-predictor loglikelihood=" + str(-np.mean((ans * np.log(pred) + (1.0 - ans) * np.log(1.0 - pred))))