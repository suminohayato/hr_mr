# coding:utf-8
import tensorflow as tf
import numpy as np
import math
import sys, csv, h5py, yaml, os
import matplotlib.pyplot as plt

test_path = "/Users/sugiyamakanta/hr_mr_data/LSTM1018/"

def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1, name="test")
    return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)


class RNNtest:

    def __init__(self, path):
        self.batch_size = 1000
        self.sequence_len = 6
        self.max_epoch = 2000
        self.patience = 10
        info = yaml.load(open(path + "data.info", "r"))
        self.features = int(info["nvis"]) / self.sequence_len
        self.dim_hid = 20
        self.dim_hid2 = [15, 15]  # 次元圧縮の層のユニット数
        self.lambda_reg = 0.001

        compression_names = [u"調教師", u"騎手"]
        self.compression_no = len(compression_names)
        self.compression_features = []
        for name in compression_names:
            start = -1
            end = -1
            for i, column in enumerate(info["column_name"]):
                s = column.split("<")[0]
                if s == name and start < 0:
                    start = i
                if s != name and start >= 0 and end < 0:
                    end = i
                    break
            self.compression_features.append(end - start)
        self.features1 = self.features - sum(self.compression_features)
        print self.features1, self.compression_features

        self.compression_w_in = [weight_variable([self.compression_features[i], self.dim_hid2[i]])
                                 for i in range(self.compression_no)]
        self.compression_b_in = [bias_variable([self.dim_hid2[i]]) for i in range(self.compression_no)]
        self.w_out = weight_variable([self.dim_hid, 1])
        self.b_out = bias_variable([1])
        self.path = path

        # LSTM系
        self.lstm_cell = tf.nn.rnn_cell.BasicLSTMCell(self.dim_hid, state_is_tuple=True)

    def inference(self, x):
        """xはplaceholderのリスト"""

        state = self.lstm_cell.zero_state(self.batch_size, tf.float32)  # tf.zeros([self.batch_size, self.dim_hid])
        outputs = []
        with tf.variable_scope('my_lstm') as scope:
            self.scope = scope
            for i in range(self.sequence_len):
                if i > 0:
                    scope.reuse_variables()
                x1 = tf.slice(x[i], [0, 0], [-1, self.features1])
                x2 = [tf.slice(x[i], [0, self.features1 + sum(self.compression_features[:j])], [-1, self.compression_features[j]])
                      for j in range(self.compression_no)]
                temp = [tf.nn.relu(tf.matmul(x2[j], self.compression_w_in[j]) + self.compression_b_in[j]) for j in range(self.compression_no)]
                input = tf.concat(1, [x1] + temp)
                output, state = self.lstm_cell(input, state)
                outputs.append(output)

        return [tf.nn.sigmoid(tf.matmul(outputs[i], self.w_out) + self.b_out) for i in range(self.sequence_len)]

    def loss(self, y, y_):
        """yは予測結果、y_は正解"""
        loss = [tf.reduce_mean(-(y_[i] * tf.log(y[i]) + (1.0 - y_[i]) * tf.log(1.0 - y[i]))) for i in range(self.sequence_len)]
        loss = tf.add_n(loss) / float(self.sequence_len)
        loss_reg = loss
        for i in range(self.compression_no):
            loss_reg += self.lambda_reg * tf.nn.l2_loss(self.compression_w_in[i])
        with tf.variable_scope('my_lstm') as scope:
            scope.reuse_variables()
            loss_reg += self.lambda_reg * tf.nn.l2_loss(tf.get_variable("BasicLSTMCell/Linear/Matrix"))
        loss_reg += self.lambda_reg * tf.nn.l2_loss(self.w_out)
        return loss_reg, loss

    def train_algorithm(self, loss):
        return tf.train.RMSPropOptimizer(1e-3).minimize(loss)

    def train(self):
        x = [tf.placeholder(tf.float32, [None, self.features]) for i in range(self.sequence_len)]
        y_ = [tf.placeholder(tf.float32, [None, 1]) for i in range(self.sequence_len)]

        inference = self.inference(x)
        loss_reg, loss = self.loss(inference, y_)
        train_step = self.train_algorithm(loss_reg)
        init = tf.initialize_all_variables()

        sess = tf.Session()
        sess.run(init)

        #print tf.get_default_graph().get_collection(self.scope)

        h5data = h5py.File(self.path + "data.h5")

        batch_no = (h5data["X"].shape[0] - 1) / self.batch_size + 1

        means = np.zeros((self.features * self.sequence_len,), dtype=np.float32)
        stds = np.zeros((self.features * self.sequence_len,), dtype=np.float32)

        max_array = -np.ones((self.features * self.sequence_len,), dtype=np.float32) * 10000.0
        min_array = np.ones((self.features * self.sequence_len,), dtype=np.float32) * 10000.0

        for i in range(batch_no):
            max_array = np.maximum(max_array, np.max(h5data["X"][i * self.batch_size:(i + 1) * self.batch_size], axis=0))
            min_array = np.minimum(min_array, np.min(h5data["X"][i * self.batch_size:(i + 1) * self.batch_size], axis=0))

        # [0,1]に正規化することにした
        means = min_array
        stds = max_array - min_array

        for i, std in enumerate(stds):
            if std == 0:
                stds[i] = float("inf")

        np.savetxt(self.path + "means.csv", means)
        np.savetxt(self.path + "stds.csv", stds)

        def normalize(X, i):
            return (X - means[i * self.features:(i + 1) * self.features]) / stds[i * self.features:(i + 1) * self.features]

        count = 0
        min = 100000.0

        ema = 0.0
        train_losses = []
        valid_losses = []
        for i in range(self.max_epoch):
            for j in range(batch_no):
                start = j * self.batch_size
                end = (j + 1) * self.batch_size
                feed_dict = {}
                for k in range(self.sequence_len):
                    feed_dict[x[k]] = normalize(h5data["X"][start:end, k * self.features:(k + 1) * self.features].reshape(-1, self.features), k)
                    feed_dict[y_[k]] = h5data["y"][start:end, k].reshape(-1, 1)

                sess.run(train_step, feed_dict=feed_dict)
                if ema == 0.0:
                    ema = sess.run(loss, feed_dict=feed_dict)
                else:
                    ema = 0.95 * ema + 0.05 * sess.run(loss, feed_dict=feed_dict)
                sys.stderr.write('\r\033[K' + str(j + 1) + "/" + str(batch_no) + " " + str(ema))
                sys.stderr.flush()
            train_losses.append(ema)

            feed_dict_valid = {}
            valid_loss = 0
            valid_batch_no = h5data["valid"]["X"].shape[0] / self.batch_size
            for j in range(valid_batch_no):
                for k in range(self.sequence_len):
                    feed_dict_valid[x[k]] = normalize(h5data["valid"]["X"][j * self.batch_size:(j + 1) * self.batch_size, k * self.features:(k + 1) * self.features].reshape(-1, self.features), k)
                    feed_dict_valid[y_[k]] = h5data["valid"]["y"][j * self.batch_size:(j + 1) * self.batch_size, k].reshape(-1, 1)
                valid_loss += sess.run(loss, feed_dict=feed_dict_valid)
            valid_loss /= valid_batch_no
            valid_losses.append(valid_loss)

            feed_dict_test = {}
            test_loss = 0
            test_batch_no = h5data["test"]["X"].shape[0] / self.batch_size
            for j in range(test_batch_no):
                for k in range(self.sequence_len):
                    feed_dict_test[x[k]] = normalize(h5data["test"]["X"][j * self.batch_size:(j + 1) * self.batch_size, k * self.features:(k + 1) * self.features].reshape(-1, self.features), k)
                    feed_dict_test[y_[k]] = h5data["test"]["y"][j * self.batch_size:(j + 1) * self.batch_size, k].reshape(-1, 1)
                test_loss += sess.run(loss, feed_dict=feed_dict_test)
            test_loss /= test_batch_no
            print " {0}epoch: valid loss = {1}, test loss = {2}".format(str(i), str(valid_loss), str(test_loss)),

            if valid_loss < min:
                print " improved"
                count = 0
                for i in range(self.compression_no):
                    np.savetxt("{0}w_in{1}.csv".format(self.path, i), sess.run(self.compression_w_in[i]), delimiter=",")
                    np.savetxt("{0}b_in{1}.csv".format(self.path, i), sess.run(self.compression_b_in[i]), delimiter=",")
                np.savetxt(self.path + "w_out.csv", sess.run(self.w_out), delimiter=",")
                np.savetxt(self.path + "b_out.csv", sess.run(self.b_out), delimiter=",")

                with tf.variable_scope('my_lstm') as scope:
                    scope.reuse_variables()
                    matrix = sess.run(tf.get_variable("BasicLSTMCell/Linear/Matrix"))
                    bias = sess.run(tf.get_variable("BasicLSTMCell/Linear/Bias"))
                    np.savetxt(self.path + "i_w.csv", matrix[:, :self.dim_hid], delimiter=",")
                    np.savetxt(self.path + "j_w.csv", matrix[:, self.dim_hid:self.dim_hid * 2], delimiter=",")
                    np.savetxt(self.path + "f_w.csv", matrix[:, self.dim_hid * 2:self.dim_hid * 3], delimiter=",")
                    np.savetxt(self.path + "o_w.csv", matrix[:, self.dim_hid * 3:], delimiter=",")
                    np.savetxt(self.path + "i_b.csv", bias[:self.dim_hid], delimiter=",")
                    np.savetxt(self.path + "j_b.csv", bias[self.dim_hid:self.dim_hid * 2], delimiter=",")
                    np.savetxt(self.path + "f_b.csv", bias[self.dim_hid * 2:self.dim_hid * 3], delimiter=",")
                    np.savetxt(self.path + "o_b.csv", bias[self.dim_hid * 3:], delimiter=",")

                min = valid_loss
            else:
                print ""
                count += 1

            if count == self.patience:
                break

            plt.clf()
            plt.plot(np.array(train_losses), label="train")
            plt.plot(np.array(valid_losses), label="valid")
            plt.legend()
            plt.savefig(os.path.join(test_path, "loss.png"))

        #print np.array(sess.run(self.inference(x), feed_dict=feed_dict)).reshape(self.sequence_len, -1)


if __name__ == "__main__":
    trainer = RNNtest(test_path)
    trainer.train()