# coding:utf-8
import tensorflow as tf
import numpy as np
import math
import sys, csv, h5py

def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

class RNNtest:

    def __init__(self):
        self.nb_of_samples = 10
        self.sequence_len = 4
        self.max_epoch = 2000

        self.w_in = weight_variable([1, 1])
        self.w = weight_variable([1, 1])
        self.w_out = weight_variable([1, 1])

    def inference(self, x):
        """xはplaceholderのリスト"""
        s = []
        s.append(tf.nn.relu(tf.matmul(x[0], self.w_in)))

        for i in range(1, self.sequence_len):
            s.append(tf.nn.relu(tf.matmul(x[i], self.w_in) + tf.matmul(s[i - 1], self.w)))

        return [tf.matmul(s[i], self.w_out) for i in range(self.sequence_len)]

    def loss(self, y, y_):
        """yは予測結果、y_は正解"""
        mse = [tf.reduce_mean((y[i] - y_[i]) * (y[i] - y_[i])) for i in range(self.sequence_len)]
        return tf.add_n(mse) / float(self.sequence_len)

    def train_algorithm(self, loss):
        return tf.train.RMSPropOptimizer(1e-3).minimize(loss)

    def train(self):
        x = [tf.placeholder(tf.float32, [None, 1]) for i in range(self.sequence_len)]
        y_ = [tf.placeholder(tf.float32, [None, 1]) for i in range(self.sequence_len)]

        loss = self.loss(self.inference(x), y_)
        train_step = self.train_algorithm(loss)
        init = tf.initialize_all_variables()

        # Create the sequences
        X = np.zeros((self.nb_of_samples, self.sequence_len))
        for row_idx in range(self.nb_of_samples):
            X[row_idx,:] = np.around(np.random.rand(self.sequence_len))
        # Create the targets for each sequence
        t = [np.sum(X[:, :i + 1], axis=1) for i in range(self.sequence_len)]

        feed_dict = {}
        for i in range(self.sequence_len):
            feed_dict[x[i]] = X[:, i].reshape(-1, 1)
            feed_dict[y_[i]] = t[i].reshape(-1, 1)
        print feed_dict

        sess = tf.Session()
        sess.run(init)

        for i in range(self.max_epoch):
            sess.run(train_step, feed_dict=feed_dict)
            print sess.run(loss, feed_dict=feed_dict)

        print np.array(X)
        print np.array(t)
        print np.array(sess.run(self.inference(x), feed_dict=feed_dict)).reshape(self.sequence_len, -1)


if __name__ == "__main__":
    trainer = RNNtest()
    trainer.train()