# coding:utf-8
import tensorflow as tf
import numpy as np
import math
import sys, csv, h5py, yaml

test_path = "/Users/sugiyamakanta/hr_mr_data/RNN2_0911/"

def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

class RNNtest:

    def __init__(self, path):
        self.batch_size = 1000
        self.sequence_len = 4
        self.max_epoch = 2000
        self.patience = 10
        info = yaml.load(open(path + "data.info", "r"))
        self.features = int(info["nvis"]) / self.sequence_len
        self.dim_hid = 50
        self.dim_hid2 = [10, 10]  # 次元圧縮の層のユニット数

        compression_names = [u"調教師", u"騎手"]
        self.compression_no = len(compression_names)
        self.compression_features = []
        for name in compression_names:
            start = -1
            end = -1
            for i, column in enumerate(info["column_name"]):
                s = column.split("<")[0]
                if s == name and start < 0:
                    start = i
                if s != name and start >= 0 and end < 0:
                    end = i
                    break
            self.compression_features.append(end - start)
        self.features1 = self.features - sum(self.compression_features)
        print self.features1, self.compression_features

        self.w_in = weight_variable([self.features1 + sum(self.dim_hid2), self.dim_hid])
        self.compression_w_in = [weight_variable([self.compression_features[i], self.dim_hid2[i]])
                                 for i in range(self.compression_no)]
        self.b_in = bias_variable([self.dim_hid])
        self.compression_b_in = [bias_variable([self.dim_hid2[i]]) for i in range(self.compression_no)]
        self.w = weight_variable([self.dim_hid, self.dim_hid])
        self.w_out = weight_variable([self.dim_hid, 1])
        self.b_out = bias_variable([1])
        self.path = path

    def inference(self, x):
        """xはplaceholderのリスト"""
        s = []
        x1 = tf.slice(x[0], [0, 0], [-1, self.features1])
        x2 = [tf.slice(x[0], [0, self.features1 + sum(self.compression_features[:i])], [-1, self.compression_features[i]])
              for i in range(self.compression_no)]
        temp = [tf.nn.relu(tf.matmul(x2[i], self.compression_w_in[i]) + self.compression_b_in[i]) for i in range(self.compression_no)]
        s.append(tf.nn.relu(tf.matmul(tf.concat(1, [x1] + temp), self.w_in) + self.b_in))

        for i in range(1, self.sequence_len):
            x1 = tf.slice(x[i], [0, 0], [-1, self.features1])
            x2 = [tf.slice(x[i], [0, self.features1 + sum(self.compression_features[:j])], [-1, self.compression_features[j]])
                  for j in range(self.compression_no)]
            temp = [tf.nn.relu(tf.matmul(x2[j], self.compression_w_in[j]) + self.compression_b_in[j]) for j in range(self.compression_no)]
            s.append(tf.nn.relu(tf.matmul(tf.concat(1, [x1] + temp), self.w_in) + self.b_in + tf.matmul(s[i - 1], self.w)))

        return [tf.nn.sigmoid(tf.matmul(s[i], self.w_out) + self.b_out) for i in range(self.sequence_len)]

    def loss(self, y, y_):
        """yは予測結果、y_は正解"""
        loss = [tf.reduce_mean(-(y_[i] * tf.log(y[i]) + (1.0 - y_[i]) * tf.log(1.0 - y[i]))) for i in range(self.sequence_len)]
        return tf.add_n(loss) / float(self.sequence_len)

    def train_algorithm(self, loss):
        return tf.train.RMSPropOptimizer(1e-3).minimize(loss)

    def train(self):
        x = [tf.placeholder(tf.float32, [None, self.features]) for i in range(self.sequence_len)]
        y_ = [tf.placeholder(tf.float32, [None, 1]) for i in range(self.sequence_len)]

        loss = self.loss(self.inference(x), y_)
        train_step = self.train_algorithm(loss)
        init = tf.initialize_all_variables()

        sess = tf.Session()
        sess.run(init)

        h5data = h5py.File(self.path + "data.h5")
        print h5data["X"].shape

        batch_no = (h5data["X"].shape[0] - 1) / self.batch_size + 1

        means = np.zeros((self.features * self.sequence_len,), dtype=np.float32)
        stds = np.zeros((self.features * self.sequence_len,), dtype=np.float32)

        max_array = -np.ones((self.features * self.sequence_len,), dtype=np.float32) * 10000.0
        min_array = np.ones((self.features * self.sequence_len,), dtype=np.float32) * 10000.0

        for i in range(batch_no):
            max_array = np.maximum(max_array, np.max(h5data["X"][i * self.batch_size:(i + 1) * self.batch_size], axis=0))
            min_array = np.minimum(min_array, np.min(h5data["X"][i * self.batch_size:(i + 1) * self.batch_size], axis=0))

        # [0,1]に正規化することにした
        means = min_array
        stds = max_array - min_array

        for i, std in enumerate(stds):
            if std == 0:
                stds[i] = float("inf")

        np.savetxt(self.path + "means.csv", means)
        np.savetxt(self.path + "stds.csv", stds)

        def normalize(X, i):
            return (X - means[i * self.features:(i + 1) * self.features]) / stds[i * self.features:(i + 1) * self.features]

        count = 0
        min = 100000.0

        feed_dict_valid = {}
        for k in range(self.sequence_len):
            feed_dict_valid[x[k]] = normalize(h5data["valid"]["X"][:, k * self.features:(k + 1) * self.features].reshape(-1, self.features), k)
            feed_dict_valid[y_[k]] = h5data["valid"]["y"][:, k].reshape(-1, 1)

        feed_dict_test = {}
        for k in range(self.sequence_len):
            feed_dict_test[x[k]] = normalize(h5data["test"]["X"][:, k * self.features:(k + 1) * self.features].reshape(-1, self.features), k)
            feed_dict_test[y_[k]] = h5data["test"]["y"][:, k].reshape(-1, 1)

        for i in range(self.max_epoch):
            for j in range(batch_no):
                start = j * self.batch_size
                end = (j + 1) * self.batch_size
                feed_dict = {}
                for k in range(self.sequence_len):
                    feed_dict[x[k]] = normalize(h5data["X"][start:end, k * self.features:(k + 1) * self.features].reshape(-1, self.features), k)
                    feed_dict[y_[k]] = h5data["y"][start:end, k].reshape(-1, 1)
                sys.stderr.write('\r\033[K' + str(j + 1) + "/" + str(batch_no))
                sys.stderr.flush()
                sess.run(train_step, feed_dict=feed_dict)

            valid_loss = sess.run(loss, feed_dict=feed_dict_valid)
            test_loss = sess.run(loss, feed_dict=feed_dict_test)
            print " {0}epoch: valid loss = {1}, test loss = {2}".format(str(i), str(valid_loss), str(test_loss)),

            if valid_loss < min:
                print " improved"
                count = 0
                np.savetxt(self.path + "w_in.csv", sess.run(self.w_in), delimiter=",")
                np.savetxt(self.path + "b_in.csv", sess.run(self.b_in), delimiter=",")
                for i in range(self.compression_no):
                    np.savetxt("{0}w_in{1}.csv".format(self.path, i), sess.run(self.compression_w_in[i]), delimiter=",")
                    np.savetxt("{0}b_in{1}.csv".format(self.path, i), sess.run(self.compression_b_in[i]), delimiter=",")
                np.savetxt(self.path + "w.csv", sess.run(self.w), delimiter=",")
                np.savetxt(self.path + "w_out.csv", sess.run(self.w_out), delimiter=",")
                np.savetxt(self.path + "b_out.csv", sess.run(self.b_out), delimiter=",")
                min = valid_loss
            else:
                print ""
                count += 1

            if count == self.patience:
                break

        #print np.array(sess.run(self.inference(x), feed_dict=feed_dict)).reshape(self.sequence_len, -1)


if __name__ == "__main__":
    trainer = RNNtest(test_path)
    trainer.train()