# coding:utf-8
import tensorflow as tf
import numpy as np
import math
import sys, csv, h5py

def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

class RNNtest:

    def __init__(self, path):
        self.batch_size = 1000
        self.sequence_len = 4
        self.max_epoch = 2000
        self.patience = 10

        self.w_in = weight_variable([1, 1])
        self.b_in = bias_variable([1])
        self.w = weight_variable([1, 1])
        self.w_out = weight_variable([1, 1])
        self.b_out = bias_variable([1])
        self.path = path

    def inference(self, x):
        """xはplaceholderのリスト"""
        s = []
        s.append(tf.nn.relu(tf.matmul(x[0], self.w_in) + self.b_in))

        for i in range(1, self.sequence_len):
            s.append(tf.nn.relu(tf.matmul(x[i], self.w_in) + self.b_in + tf.matmul(s[i - 1], self.w)))

        return [tf.nn.sigmoid(tf.matmul(s[i], self.w_out) + self.b_out) for i in range(self.sequence_len)]

    def loss(self, y, y_):
        """yは予測結果、y_は正解"""
        loss = [tf.reduce_mean(-(y_[i] * tf.log(y[i]) + (1.0 - y_[i]) * tf.log(1.0 - y[i]))) for i in range(self.sequence_len)]
        return tf.add_n(loss) / float(self.sequence_len)

    def train_algorithm(self, loss):
        return tf.train.RMSPropOptimizer(1e-2).minimize(loss)

    def train(self):
        x = [tf.placeholder(tf.float32, [None, 1]) for i in range(self.sequence_len)]
        y_ = [tf.placeholder(tf.float32, [None, 1]) for i in range(self.sequence_len)]

        loss = self.loss(self.inference(x), y_)
        train_step = self.train_algorithm(loss)
        init = tf.initialize_all_variables()

        sess = tf.Session()
        sess.run(init)

        h5data = h5py.File(self.path + "data.h5")

        batch_no = (h5data["X"].shape[0] - 1) / self.batch_size + 1

        means = np.zeros((4,), dtype=np.float32)
        stds = np.zeros((4,), dtype=np.float32)

        for i in range(batch_no):
            means += np.sum(h5data["X"][i * self.batch_size:(i + 1) * self.batch_size], axis=0)
        means /= h5data["X"].shape[0]

        for i in range(batch_no):
            stds += np.sum((h5data["X"][i * self.batch_size:(i + 1) * self.batch_size] - means) * (h5data["X"][i * self.batch_size:(i + 1) * self.batch_size] - means), axis=0)
        stds /= h5data["X"].shape[0]
        stds = np.sqrt(stds)

        np.savetxt(self.path + "means.csv", means)
        np.savetxt(self.path + "stds.csv", stds)

        def normalize(X):
            return (X - means) / stds

        count = 0
        min = 100000.0

        feed_dict_valid = {}
        for k in range(self.sequence_len):
            feed_dict_valid[x[k]] = h5data["valid"]["X"][:, k].reshape(-1, 1)
            feed_dict_valid[y_[k]] = h5data["valid"]["y"][:, k].reshape(-1, 1)

        feed_dict_test = {}
        for k in range(self.sequence_len):
            feed_dict_test[x[k]] = h5data["test"]["X"][:, k].reshape(-1, 1)
            feed_dict_test[y_[k]] = h5data["test"]["y"][:, k].reshape(-1, 1)

        for i in range(self.max_epoch):
            for j in range(batch_no):
                start = j * self.batch_size
                end = (j + 1) * self.batch_size
                feed_dict = {}
                for k in range(self.sequence_len):
                    feed_dict[x[k]] = h5data["X"][start:end, k].reshape(-1, 1)
                    feed_dict[y_[k]] = h5data["y"][start:end, k].reshape(-1, 1)
                sys.stderr.write('\r\033[K' + str(j + 1) + "/" + str(batch_no))
                sys.stderr.flush()
                sess.run(train_step, feed_dict=feed_dict)

            valid_loss = sess.run(loss, feed_dict=feed_dict_valid)
            test_loss = sess.run(loss, feed_dict=feed_dict_test)
            print " {0}epoch: valid loss = {1}, test loss = {2}" .format(str(i), str(valid_loss), str(test_loss))

            if valid_loss < min:
                count = 0
                np.savetxt(self.path + "w_in.csv", sess.run(self.w_in), delimiter=",")
                np.savetxt(self.path + "b_in.csv", sess.run(self.b_in), delimiter=",")
                np.savetxt(self.path + "w.csv", sess.run(self.w), delimiter=",")
                np.savetxt(self.path + "w_out.csv", sess.run(self.w_out), delimiter=",")
                np.savetxt(self.path + "b_out.csv", sess.run(self.b_out), delimiter=",")
                min = valid_loss
            else:
                count += 1

            if count == self.patience:
                break

        #print np.array(sess.run(self.inference(x), feed_dict=feed_dict)).reshape(self.sequence_len, -1)


if __name__ == "__main__":
    trainer = RNNtest("/Users/sugiyamakanta/hr_mr_data/RNNtest/")
    trainer.train()