# coding:utf-8
import tensorflow as tf
import numpy as np
import math
import sys, csv, h5py

path = "/Users/sugiyamakanta/hr_mr_data/linear0810_big/"
MAX_EPOCH = 20
BATCH_SIZE = 1000
patience = 5

# tensorflowの準備
features = 93
x = tf.placeholder(tf.float32, [None, features])
W = tf.Variable(tf.random_normal([features, 1], mean=0.0, stddev=0.05))
b = tf.Variable(tf.random_normal([1], mean=0.0, stddev=0.05))
y = tf.matmul(x, W) + b

y_ = tf.placeholder(tf.float32, [None, 1])
mse = tf.reduce_mean((y - y_) * (y - y_))

train_step = tf.train.GradientDescentOptimizer(0.01).minimize(mse)
init = tf.initialize_all_variables()

sess = tf.Session()
sess.run(init)

h5data = h5py.File(path + "data.h5")

batch_no = (h5data["X"].shape[0] - 1) / BATCH_SIZE + 1

means = np.zeros((features,), dtype=np.float32)
stds = np.zeros((features,), dtype=np.float32)

for i in range(batch_no):
    means += np.sum(h5data["X"][i * BATCH_SIZE:(i + 1) * BATCH_SIZE], axis=0)
means /= h5data["X"].shape[0]

for i in range(batch_no):
    stds += np.sum((h5data["X"][i * BATCH_SIZE:(i + 1) * BATCH_SIZE] - means) * (h5data["X"][i * BATCH_SIZE:(i + 1) * BATCH_SIZE] - means), axis=0)
stds /= h5data["X"].shape[0]
stds = np.sqrt(stds)

for i, std in enumerate(stds):
    if std == 0:
        stds[i] = float("inf")

print means
print stds

np.savetxt(path + "means.csv", means)
np.savetxt(path + "stds.csv", stds)

def normalize(X):
    return (X - means) / stds

count = 0
min = 100000.0
for i in range(MAX_EPOCH):

    # SGDを実装している
    for j in range(batch_no):
        sys.stderr.write('\r\033[K' + str(j + 1) + "/" + str(batch_no))
        sys.stderr.flush()
        np_data = h5data["X"][j * BATCH_SIZE:(j + 1) * BATCH_SIZE]
        batch_xs = normalize(np_data)
        batch_ys = h5data["y"][j * BATCH_SIZE:(j + 1) * BATCH_SIZE].reshape(h5data["y"][j * BATCH_SIZE:(j + 1) * BATCH_SIZE].shape[0], 1)
        sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})
    sys.stderr.write('\r\033[K')
    sys.stderr.flush()

    cost = 0
    valid_batch_no = (h5data["valid"]["X"].shape[0] - 1) / BATCH_SIZE + 1
    for j in range(valid_batch_no):
        cost += sess.run(mse, feed_dict={
            x: normalize(h5data["valid"]["X"][j * BATCH_SIZE:(j + 1) * BATCH_SIZE]),
            y_: h5data["valid"]["y"][j * BATCH_SIZE:(j + 1) * BATCH_SIZE].reshape(h5data["valid"]["y"][j * BATCH_SIZE:(j + 1) * BATCH_SIZE].shape[0], 1)})
    cost /= valid_batch_no
    print str(i + 1) + "epoch:valid cost(mse)=" + str(cost) + ", rmse=" + str(math.sqrt(cost))

    if cost < min:
        count = 0
        min = cost
    else:
        count += 1

    if count == patience:
        break

test_rmse = math.sqrt(sess.run(mse, feed_dict={x: normalize(h5data["test"]["X"][:]), y_: h5data["test"]["y"][:].reshape(h5data["test"]["y"][:].shape[0], 1)}))
print "test rmse=" + str(test_rmse)
np.savetxt(path + "W.csv", sess.run(W), delimiter=",")
np.savetxt(path + "b.csv", sess.run(b), delimiter=",")
open(path + "rmse.txt", "w").write(str(test_rmse))
