#!/usr/binenv python
# -*- coding: utf-8 -*-

import pandas
import numpy
from pylearnuni.preprocess.filter import Filter
import datetime as dt
import math
import pandas as pd
import numpy as np

class hrFilter(Filter):
    exclusion_codes = []
    inclusion_codes = []

    @classmethod
    def filter(cls, df):
        contain_str = ["体重", "前１回体重", "前２回体重", "前３回体重", "着順", "前１回着順", "前２回着順", "前３回着順", "タイム差１つ前", "前１回タイム差１つ前", "前２回タイム差１つ前"]
        exclude = ["推定3F１つ前", "前１回推定3F１つ前", "前２回推定3F１つ前", "調教師", "タイム", "前１回タイム", "前２回タイム", "前３回タイム", "地面", "前１回地面", "前２回地面", "前３回地面"]
        int_names = ["4角１つ前", "前１回4角１つ前", "前２回4角１つ前", "3角１つ前", "前１回3角１つ前", "前２回3角１つ前", "2角１つ前", "前１回2角１つ前", "前２回2角１つ前", "人気", "前１回人気", "前２回人気", "前３回人気", "馬番", "前１回馬番", "前２回馬番", "前３回馬番"]

        df = df[pd.to_datetime(df["日付"]) < pd.to_datetime('2016-07-30 00:00')]

        for s in contain_str:
            df[s] = df[s].astype(str)
            df = df[df[s].str.replace(".", "").str.isdigit()]
            df[s] = df[s].astype(float)

        for s in exclude:
            df = df[(df[s] != 0)]
            df = df.dropna(subset=[s])

        for s in int_names:
            df[s] = df[s].astype(str)
            df[s] = df[s].str.replace(".0", "")
            df[s] = df[s].str.replace("nan", "0")
            #print pd.DataFrame(np.zeros(df.shape, dtype=np.int32), columns=map(str, range(df.shape[1])))["0"]
            df[s] = df[s].where(df[s] != "", pd.DataFrame(np.zeros(df.shape, dtype=np.int32), columns=map(str, range(df.shape[1])))["0"])

        return df
