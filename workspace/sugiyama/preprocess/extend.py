# coding:utf-8
import pandas as pd
import numpy as np

adding_names = ["", "前１回", "前２回", "前３回"]
unknown_columns_ori = ["着順", "推定3F", "タイム差", "2角", "3角", "4角", "ラップ"]
unknown_columns = [[name + column for name in adding_names] for column in unknown_columns_ori]

path = "/Users/sugiyamakanta/hr_mr_data/RNN0908/"

data = pd.read_csv(path + "HR_data_2006_2016_cleaned_previous_added3.csv")

for unknown in unknown_columns:
    for i, column in enumerate(unknown[:-1]):
        new_column = column + "１つ前"
        data[new_column] = data[unknown[i + 1]]
    data[unknown[-1] + "１つ前"] = np.zeros((data.shape[0],))

data.to_csv(path + "extended.csv")