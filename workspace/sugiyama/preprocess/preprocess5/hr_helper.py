#!/usr/binenv python
# -*- coding: utf-8 -*-

import os
import numpy
import pandas as pd

from pylearnuni.helper.helper import Helper as Helper
from pylearnuni.preprocess.preprocess import Preprocess as Preprocess
from hr_filter import hrFilter as Filter
from hr_converter import hrConverter as Converter


class hrHelper(Helper):
    """ Machine Learning Helper class """

    def __init__(self):
        Helper.__init__(self)
        self.target_features = ["前５回着順", "前４回着順", "前３回着順", "前２回着順", "前１回着順", "着順"]
        self.threshold = 1e8

        self.version = "G1.0"

    def get_column_name(self):
        return self._get_column_name(Converter)

    def get_column_name_dict(self):
        return self._get_column_name_dict(Converter)

    def process_data(self):
        if not os.path.isfile(os.path.join(self.output_path, "data.info")):
            self._process_data(self.convert_dataframe)

    def generate_csv(self):
        self._generate_csv(self.convert_dataframe)

    def convert_dataframe(self, df, convert=True):
        # 範囲外の値を除外
        df = Filter.filter(df)
        # 行をシャッフル
        df = Preprocess.shuffle_index(df, self.SEED)
        # target_featreが閾値以下の物を除外
        #df = Filter.target_value_upperboud(df, self.target_features, self.threshold)
        # Nanを0で埋める
        if self.fillna:
            df = Preprocess.fill_nan_with_0(df)
        #binary型のerrorを落とす
        if self.validate_bin:
            for column in [_ for _ in self.features.keys() if self.features[_]["feature_type"] == "binary" or self.features[_]["feature_type"] == "color"]:
                df = df[(df[column] == 0)|(df[column] == 1)]
        if convert:
            # 機械学習に読める形に変換
            df = Preprocess.convert_dataframe(df, self.features, Converter)
            # Nanを0で埋める
            if self.fillna:
                df = Preprocess.fill_nan_with_0(df)
            if self.nade:
                df = self.nadefiller(df)
            # 何をしている？
            Preprocess.validate_dataframe(df)

        return df


    """
    def add_stat_data(self,csv_path,dataset_csv_path,output_csv_path = None):
        if not output_csv_path:
            output_csv_path = os.path.join(self.output_path,"result_with_stat.csv")

        result_df = pd.read_csv(csv_path)
        dataset = Filter.filter(pd.read_csv(dataset_csv_path))
        ist = Add_stat_data(result_df,dataset,target_feature = "最新価格",maker = "メーカーコード",car = "車種コード", year = "年式", distance = "走行")
        ist.main().to_csv(output_csv_path,index=False)
    """