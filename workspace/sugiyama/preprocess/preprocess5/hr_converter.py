#!/usr/binenv python
# -*- coding: utf-8 -*-

import pandas
import numpy
import re

from pylearnuni.preprocess.converter import Converter
from pylearnuni.utils import column_name_generater as Name


class hrConverter(Converter):
    feature_types = ("unary", "categorical", "binary")
    log_multi_scaled = False

    @classmethod
    def convert_binary(cls, column_name, df):
        return cls.convert_unary(column_name, df)

    @classmethod
    def generate_binary_column_name(cls, column_name, df):
        return cls.generate_unary_column_name(column_name, df)

