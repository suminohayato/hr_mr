#!/usr/binenv python
# -*- coding: utf-8 -*-

from hr_helper import hrHelper as Helper
from pylearnuni.preprocess.preprocess import Preprocess as Preprocess
import h5py, yaml, os
import math
import numpy as np

path = "/Users/sugiyamakanta/hr_mr_data/RNN1001/"
chunk_size = 10000
past = ["前５回", "前４回", "前３回", "前２回", "前１回", ""]

def extend_features():
    with open(path + "features.yaml", "w") as f:
        for s in past:
            for i, row in enumerate(open(path + "features_original.yaml", "r")):
                if i % 3 == 0:
                    f.write(s + row)
                else:
                    f.write(row)
        for s in past:
            f.write(s + "着順:" + os.linesep)
            f.write("  feature_type: unary" + os.linesep)
            f.write("  dtype: int8" + os.linesep)

if __name__ == "__main__":
    Preprocess.shuffle = True

    extend_features()

    helper = Helper()
    # データに対応するfeatures.yamlのパスを設定
    helper.load_features(path + "features.yaml")
    # 読み込むCSVデータのパスを設定
    helper.load_data(path + "extended.csv", chunk_size=chunk_size, chunk=True)

    # 出力データを保存するフォルダへのパス（ここにすべて出力される）
    helper.set_output_path(path)

    # データをpylearn2が読み込める形に変形
    helper.process_data()
    helper.generate_dataset()

    data = h5py.File(path + "data.h5")
    chunk_no = (data["X"].shape[0] - 1) / chunk_size + 1
    for i in range(chunk_no):
        data["y"][i * chunk_size:(i + 1) * chunk_size] = (data["y"][i * chunk_size:(i + 1) * chunk_size] == 1)
    data["valid"]["y"][:] = (data["valid"]["y"][:] == 1)
    data["test"]["y"][:] = (data["test"]["y"][:] == 1)


