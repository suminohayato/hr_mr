#!/usr/binenv python
# -*- coding: utf-8 -*-

import pandas
import numpy
from pylearnuni.preprocess.filter import Filter
import datetime as dt
import math
import pandas as pd
import numpy as np

class hrFilter(Filter):
    exclusion_codes = []
    inclusion_codes = []

    @classmethod
    def filter(cls, df):
        adding_names = ["", "前１回", "前２回", "前３回", "前４回", "前５回"]
        contain_str_ori = ["体重", "着順", "タイム差"]
        contain_str = []
        for column in contain_str_ori:
            contain_str.extend([name + column for name in adding_names])
        exclude = ["推定3F１つ前", "前１回推定3F１つ前", "前２回推定3F１つ前", "前３回推定3F１つ前", "前４回推定3F１つ前", "調教師", "タイム", "前１回タイム", "前２回タイム", "前３回タイム", "前４回タイム", "前５回タイム", "地面", "前１回地面", "前２回地面", "前３回地面", "前４回地面", "前５回地面"]
        int_names_ori = ["着順１つ前", "4角１つ前", "3角１つ前", "2角１つ前", "人気", "馬番"]
        int_names = []
        for column in int_names_ori:
            int_names.extend([name + column for name in adding_names])

        df = df[pd.to_datetime(df["日付"]) < pd.to_datetime('2016-07-30 00:00')]

        for s in contain_str:
            df[s] = df[s].astype(str)
            df = df[df[s].str.replace(".", "").str.isdigit()]
            df[s] = df[s].astype(float)

        for s in exclude:
            df = df[(df[s] != 0)]
            df = df.dropna(subset=[s])

        for s in int_names:
            df[s] = df[s].astype(str)
            df[s] = df[s].str.replace(".0", "")
            df[s] = df[s].str.replace("nan", "0")
            #print pd.DataFrame(np.zeros(df.shape, dtype=np.int32), columns=map(str, range(df.shape[1])))["0"]
            df[s] = df[s].where(df[s] != "", pd.DataFrame(np.zeros(df.shape, dtype=np.int32), columns=map(str, range(df.shape[1])))["0"])

        return df
