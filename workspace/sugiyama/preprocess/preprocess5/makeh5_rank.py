#!/usr/binenv python
# -*- coding: utf-8 -*-

from hr_helper import hrHelper as Helper
from pylearnuni.preprocess.preprocess import Preprocess as Preprocess
import h5py, yaml
import math
import numpy as np

path = "/Users/sugiyamakanta/hr_mr_data/ranklinear0831/"
chunk_size = 10000

if __name__ == "__main__":
    Preprocess.shuffle = True

    helper = Helper()

    # 読み込むCSVデータのパスを設定
    helper.load_data(path + "HR_data_2006_2016_cleaned_previous_added.csv", chunk_size=chunk_size, chunk=True)
    # データに対応するfeatures.yamlのパスを設定
    helper.load_features(path + "features.yaml")
    # 出力データを保存するフォルダへのパス（ここにすべて出力される）
    helper.set_output_path(path)

    # データをpylearn2が読み込める形に変形
    helper.process_data()
    helper.generate_dataset()

    data = h5py.File(path + "data.h5")
    chunk_no = (data["X"].shape[0] - 1) / chunk_size + 1
    for i in range(chunk_no):
        data["y"][i * chunk_size:(i + 1) * chunk_size] = (data["y"][i * chunk_size:(i + 1) * chunk_size] == 1)
    data["valid"]["y"][:] = (data["valid"]["y"][:] == 1)
    data["test"]["y"][:] = (data["test"]["y"][:] == 1)


