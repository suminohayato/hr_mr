# coding:utf-8

import pandas as pd
import numpy as np
import numpy
from cvxopt import matrix, solvers
import math
import matplotlib.pyplot as plt

# 経済工学の名残
ori = pd.read_csv("closing_price.csv")
data = ori.values[:, 1:]
data = data[::-1].astype(dtype=np.float64)
Yield = np.array([365.0 * np.log(data[i + 1] / data[i]) for i in range(data.shape[0] - 1)])

# このret(return)とriskを適切に
ret = np.mean(Yield, axis=0)
risk = np.cov(np.transpose(Yield))

# 係数を決めている(http://cvxopt.org/userguide/coneprog.html#quadratic-programming 参照)
P = matrix(risk.tolist())
q = matrix(np.zeros((risk.shape[0],)).tolist())
G = matrix((-np.eye(risk.shape[0])).tolist())
h = matrix((np.zeros((risk.shape[0],))).tolist())

# Aの1列目はリターンの制約、2列目は賭ける割合の和は1の制約
A_np = np.ones((2, ret.shape[0]), dtype=np.float64)
A_np[0, :] = ret
A = matrix(np.transpose(A_np).tolist())

d = 0.01 # 幅
start = -75 # リターン最小
end = 4 # リターン最大
sigma_p = [] # リスク(リターンの標準偏差)
solutions = []
for i in range(start, end):
    print i
    mu0 = i * d
    b = matrix([mu0, 1.0])
    sol = solvers.qp(P, q, G, h, A, b)
    solutions.append(sol["x"])
    sigma_p.append(math.sqrt(sol["primal objective"] * 2))

mu0s = [i * d for i in range(start, end)]
plt.plot(sigma_p, mu0s)
plt.xlabel("risk")
plt.ylabel("return")
plt.show()
plt.clf()
